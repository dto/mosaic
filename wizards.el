;;; wizards.el --- SchemeMosaic wizards              -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <dto@nomad>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defface mosaic-wizard-title-face '((t (:foreground "blue" :bold t :weight bold :height 1.3)))
  "Face for Wizard titles." :group 'mosaic)

(defclass mosaic-wizard (eieio-persistent eieio-speedbar-file-button)
  ((file :initform "mosaic-wizard.eieio")
   (completed-p :initform nil)
   (wizard-title :initform "Generic wizard" :initarg :wizard-title)
   (wizard-description :initform "Generic description" :initarg :wizard-description)))

(defun mosaic-prettify-eieio-custom-maybe ()
  (assert (eq major-mode 'eieio-custom-mode))
  (when (slot-boundp eieio-co 'wizard-title)
    (save-excursion
      (let ((inhibit-read-only t))
        (rename-buffer (concat "*" (slot-value eieio-co 'wizard-title) "*") t)
        (goto-char (point-min))
        (insert "\n")
        (insert (propertize (slot-value eieio-co 'wizard-title)
                            'face 'mosaic-wizard-title-face))
        (insert "\n\n")
        (insert (slot-value eieio-co 'wizard-description))
        (insert "\n\n")
        (let ((beg (point)))
          (forward-line 6)
          (delete-region beg (point)))))))

(defvar mosaic-current-wizard nil)

(defvar mosaic-current-wizard-buffer nil)

(defun mosaic-destroy-current-wizard ()
  (when mosaic-current-wizard-buffer
    (kill-buffer mosaic-current-wizard-buffer)
    (setf mosaic-current-wizard-buffer nil)))

(cl-defmethod eieio-customize-object :before ((wizard mosaic-wizard))
  (mosaic-destroy-current-wizard))

(defvar mosaic-marked-files ())

(defun mosaic-wizard-apply-settings ()
  (widget-apply eieio-wo :value-get))

(cl-defmethod eieio-customize-object :after ((wizard mosaic-wizard) &optional group)
  (mosaic-prettify-eieio-custom-maybe)
  ;; (mosaic-insinuate-header-line)
  (setf mosaic-current-wizard-buffer (current-buffer)))

;;; Hello wizard

(defcustom mosaic-inhibit-hello-wizard nil
  "When non-nil, stop Mosaic from showing the hello wizard on
startup."
  :tag "Inhibit Hello wizard on startup?"
  :group 'mosaic
  :type 'boolean)

(defclass mosaic-hello-wizard (mosaic-wizard)
  ((file :initform "mosaic-hello-wizard.eieio")
   (wizard-title :initform "Welcome to Mosaic")
   (wizard-description :initform
"SchemeMosaic is a digital music mashup tool. The main technique
employed is concatenative synthesis. It is written in S7 Scheme
as an extension to the Snd Editor, and is released under the GPL
Version 3. Right now SchemeMosaic is under construction, and only
tested with Snd 20.9 and greater.

SchemeMosaic is designed to assist with making various kinds of
electronic music, such as so-called chopped and screwed or
vaporwave music. When you run SchemeMosaic, the input file is
sliced into (usually) beat-sized regions; these beat-slices are
then subject to chopping (rearrangement, repetition, skipping)
and screwing (pitch-shifting, time-stretching, and other
effects.) With various helper functions, SchemeMosaic will
automatically chop and screw according to your
specifications. This can include arbitrary Scheme code with the
full power of Snd at your disposal. Combined with a comprehensive
Emacs-based frontend, the result is a kind of audiovisual
exploratory concatenative synthesis. Output can be saved to WAV
or FLAC for use with other applications such as Audacity, Mixxx,
LMMS, Ardour, and so on.

To begin working with sound files in a new session, mark them in
a Dired buffer and then do M-x mosaic-show-new-session-wizard, or
choose the Session wizard option on the Mosaic menu.")
   (customize :initform nil :initarg :customize
              :label "Customize global settings"
              :documentation "Set directories, audio devices, and other options."
              :custom (push-button :format "%[ Customize %]\n%d" :action (lambda (a b) (mosaic-customize-global-settings))))
   (new-session-wizard :initform nil
                       :label "New session wizard"
                       :documentation "This switches to a wizard useful for creating new sessions."
                       :custom (push-button :format "%[ Create a new session %]\n%d"
                                            :action (lambda (a b) (mosaic-show-new-session-wizard))))
   (find-session :initform nil
                 :label "Open existing session"
                 :documentation "This will prompt for a session directory to open."
                 :custom (push-button :format "%[ Open an existing session %]\n%d"
                                      :action (lambda (a b)
                                                (call-interactively 'mosaic-find-session))))
   (inhibit-next-time-p :initform nil
                        :label "Don't show this wizard on startup"
                        :documentation "\nUse the Apply button below to save this setting."
                        :custom (checkbox))))

(cl-defmethod eieio-done-customizing :after ((wizard mosaic-hello-wizard))
  (when (slot-value wizard 'inhibit-next-time-p)
    (customize-save-variable 'mosaic-inhibit-hello-wizard t)))

(defun mosaic-show-hello-wizard ()
  (interactive)
  (setf mosaic-current-wizard (make-instance 'mosaic-hello-wizard))
  (eieio-customize-object mosaic-current-wizard))

;;; New session wizard
                               
(defclass mosaic-new-session-wizard (mosaic-wizard)
  ((file :initform "mosaic-new-session-wizard.eieio")
   (wizard-title :initform "New session wizard")
   (wizard-description :initform
"A `session' organizes all the audio and data files for a given
song or project into one directory. This directory names the
session and is given the `.mosaic' extension. 

A session can have many audio files, and one or more special
files called `sheets'. These have the file extension `.cell' and
include several of Mosaic's main screens. To create a new
session, add or change the relevant data below and press the
`Apply' button.")
   (customize :initform nil :initarg :customize
              :label "Customize global settings"
              :custom (push-button :format "%[ Customize %]\n%d" :action (lambda (a b) (mosaic-customize-global-settings)))
              :documentation "Check your settings before creating a new session.")
   (new-session-directory :initform (file-name-as-directory
                                     (expand-file-name (concat "session-"
                                                               (format-time-string "%Y-%m-%d--%H-%M-%S" (current-time)) ".mosaic")
                                                       mosaic-session-parent-directory))
                          :label "New session directory"
                          :custom directory
                          :documentation
"Choose a new directory to hold your session. This directory will
be automatically created.")
   (browse-dir :initform nil :initarg :show-dir
               :label "Browse a directory for files to copy."
               :documentation
"Open a directory in Dired. You can mark files, and then M-x
mosaic-show-new-session-wizard to return to this screen with the
marked files selected for copying."
             :custom (push-button :format "%[ Browse a directory %]\n%d" :action (lambda (a b) (call-interactively 'dired))))
   (create-session-scheme :initform t
                          :custom checkbox
                          :label "Create session Scheme file"
                          :documentation
"You can place session-specific Scheme definitions in a file
called `session.scm' in your session directory. You can use this
to define variables, CHOP patterns, :PROCESSOR functions,
:SCORE-FUNCTIONs, and other useful entities for your cell sheets.
This file is loaded into Scheme before rendering any job. Check
this box to create an example Scheme file from a template.")
   (import-wizard-p :initform nil
                    :initarg :import-wizard-p
                    :label "Open Import wizard"
                    :custom (checkbox)
                    :documentation
                    " When checked, open the Import wizard after creating the session.")
   (chop-wizard-p :initform nil
                    :initarg :chop-wizard-p
                    :label "Open Chop wizard"
                    :custom (checkbox)
                    :documentation
                    " When checked, open the Chop wizard after creating the session.")
   (files-to-copy :initform (eval 'mosaic-marked-files)
                  :initarg :files-to-copy
                  :label "Files to copy in"
                  :custom (repeat file)
                  :documentation "These files will be copied into the new session after creation.")))

(cl-defmethod eieio-done-customizing ((wizard mosaic-new-session-wizard))
  (mosaic-find-session (slot-value wizard 'new-session-directory))
  (when (slot-value wizard 'create-session-scheme)
    (mosaic-ensure-session-scheme-exists))
  (dolist (file (slot-value wizard 'files-to-copy))
    (copy-file file (file-name-as-directory mosaic-selected-session-directory) :ok-if-already-exists))
  (if (slot-value wizard 'import-wizard-p)
      (mosaic-show-import-wizard :keep-marked-files)
    (when (slot-value wizard 'chop-wizard-p)
      (mosaic-show-chop-wizard))))

(defun mosaic-show-new-session-wizard ()
  (interactive)
  (let (files)
    (when (eq major-mode 'dired-mode)
      (setf files (dired-get-marked-files)))
    (setf mosaic-marked-files files)
    (setf mosaic-current-wizard (make-instance 'mosaic-new-session-wizard :files-to-copy mosaic-marked-files))
    (eieio-customize-object mosaic-current-wizard)))

;;; Import wizard

(defclass mosaic-import-wizard (mosaic-wizard)
  ((wizard-title :initform "Import sounds into database")
   (wizard-description :initform
"This wizard helps you choose files to import into the database,
set relevant options, and start the import process. The chosen
files are sliced on the beats according to session settings.")
   (find-beat-p :initform (eval 'mosaic-import-find-offset)
                :initarg :find-beat-p
                :label "Find beat offset"
                :custom (checkbox)
                :documentation
" When checked, Mosaic will attempt to detect 
the first drum beat in each sound file, and begin beat-slicing
from that offset instead of at the beginning of the file. If this
fails, try un-checking this option, or trim your file in an audio
editor.")
   (find-tempo-p :initform t
                 :initarg :find-tempo-p
                 :label "Find tempo"
                 :custom (checkbox)
                 :documentation
" When checked, Mosaic will attempt to guess 
the tempo of each sound file (measured in beats per minute), and
slice on that basis. In that case, the currently set tempo is
ignored.")
   (read-whole :initform nil
               :initarg :read-whole
               :label "Read whole files without slicing"
               :custom (checkbox)
               :documentation
               "Don't slice when checked.")
   (tempo :initform (eval 'mosaic-beats-per-minute)
          :initarg :tempo
          :label "Tempo (beats per minute)"
          :custom (number :format "%v\n%d")
          :documentation
"Use this tempo. If you checked `Find tempo' above, this setting
is ignored.")
   (slice-size :initform (eval 'mosaic-slice-size)
               :initarg :tempo
               :label "Slice size"
               :custom (choice (number) (choice (const whole-note)
                                                (const half-note)
                                                (const quarter-note)
                                                (const eighth-note)
                                                (const sixteenth-note)))
               :documentation
"Use this slice size. A numeric option is in seconds; choosing a
note value computes this based on the tempo. If you enabled
automatic tempo detection above, you should use one of the note
symbols.")
   (show-dir :initform nil :initarg :show-dir
             :label "Show session directory"
             :documentation
"Open a directory in Dired. You can mark files, and then M-x
mosaic-show-import-wizard to return to this screen with the
marked files selected for import."
             :custom (push-button :format "%[ Open session directory %]\n%d" :action (lambda (a b) (mosaic-visit-session-directory))))
   (files-to-import :initform (eval 'mosaic-marked-files)
                    :initarg :files-to-import
                    :label "Sound files to import"
                    :custom (repeat file)
                    :documentation "These files will be imported into the database.")))

(cl-defmethod eieio-done-customizing :after ((wizard mosaic-import-wizard))
  (message "Mosaic import wizard running...")
  (delete-other-windows)
  (mosaic-show-scheme)
  (with-slots (files-to-import slice-size tempo read-whole find-tempo-p find-beat-p) wizard
    (when tempo (setf *mosaic-beats-per-minute* tempo))
    (when slice-size (setf *mosaic-slice-size* slice-size))
    (mosaic-configure-slices*)
    (let ((files (mapcar 'file-name-nondirectory files-to-import)))
      (let ((mosaic-import-find-offset find-beat-p)
            (mosaic-import-find-tempo find-tempo-p))
        (mosaic-sending-scheme
         (mosaic-tell-session-directory)
         (mosaic-import-sounds files tempo slice-size read-whole)))))
  (message "Mosaic import wizard running... Done."))

(defun mosaic-show-import-wizard (&optional keep-marked-files)
  (interactive)
  (let (files)
    (when (and (not keep-marked-files)
               (eq major-mode 'dired-mode))
      (setf files (dired-get-marked-files)))
    (when keep-marked-files
      (setf files mosaic-marked-files))
    (setf mosaic-marked-files (mapcar 'file-name-nondirectory files))
    (setf mosaic-current-wizard (make-instance 'mosaic-import-wizard :files-to-import mosaic-marked-files))
    (eieio-customize-object mosaic-current-wizard)))

;;; Chop wizard

(defclass mosaic-chop-wizard (mosaic-wizard)
  ((file :initform "mosaic-chop-wizard.eieio")
   (wizard-title :initform "Chop and screw wizard")
   (wizard-description :initform
"This wizard helps you chop and screw a backing track, then match
segments from a given database. This creates a reusable cell
sheet, to which you can add additional databases and match other
files, etc.")
   (show-dir :initform nil :initarg :show-dir
             :label "Show session directory"
             :documentation "Open the session directory in Dired."
             :custom (push-button :format "%[ Open session directory %]\n%d" :action (lambda (a b) (mosaic-visit-session-directory))))
   (input-file :initform (eval (file-name-nondirectory (or (car-safe mosaic-marked-files) "")))
               :label "Input file to chop and screw"
               :custom (file :must-match t)
               :documentation "Which input file to load as the rhythm track.")
   (find-tempo-p :initform t
                 :initarg :find-tempo-p
                 :label "Find tempo"
                 :custom (checkbox)
                 :documentation
                 " When checked, Mosaic will attempt to guess the
tempo of the input file (measured in beats per minute), and slice
on that basis. In that case, the currently set tempo is
ignored.")
   (tempo :initform (eval 'mosaic-beats-per-minute)
          :initarg :tempo
          :label "Tempo (beats per minute)"
          :custom (number :format "%v\n%d")
          :documentation
          "Use this tempo. If you checked `Find tempo' above, this setting
is ignored.")
   (slice-size :initform (eval 'mosaic-slice-size)
               :initarg :tempo
               :label "Slice size"
               :custom (choice (number) (choice (const whole-note)
                                                (const half-note)
                                                (const quarter-note)
                                                (const eighth-note)
                                                (const sixteenth-note)))
               :documentation
"Use this slice size. A numeric option is in seconds; choosing a
note value computes the real slice size based on the tempo. If
you enabled automatic tempo detection above, you should use one
of the note symbols.")
   (processor :initform '(vaporwave laidback-8)
              :custom sexp
              :initarg :processor
              :documentation
"Scheme expression specifying how the input file is to be processed.
Several operators are available. (CHOP) reshuffles beats according to
a pattern where every eight incoming beats are given letters A through
H. You can say:

  (chop vapor-8)  

to get a simple pattern. VAPOR-8 is defined as (a a c d e f f g), so
it would double the first and sixth regions (A and F in our example)
and leave out regions B and H.

You could specify:  

  (chop '(a b c d e f g h))

and the result would be unchanged, since the letters are in the same
order as the input.

If you want to slow down the music like a vinyl record, try:

  (screw vinyl)

Where VINYL is defined as 0.741, which slows it down about as much as
a 45 RPM record sounds played at 33.3 RPM. If you want to pitch-shift
instead of resampling, use (shift). If you want to time-stretch
without pitch-shifting, use (stretch).

To chain multiple processors, use CHAIN:

  (chain (chop '(a b b c a d f g h j)) (stretch 1.5))

This first chops the track according to the pattern, and then
stretches the length of each region by 1.5x.

More operators will be added. You can use multiple MOSAIC-SYNTH
objects in a chopper, each with its own processor function.

To reuse patterns and parameters, use DEFINE :

  (define my-pattern '(a a b c d d e f g g h h))
  (define my-speed 1.18)

Then in your processor cell:

  (chain (chop my-pattern) (screw my-speed))

If you really want to get wild  :

  (define shifty '(let ((z 1.1)) `(a a c d 
                                   (shift-region e ,z) 
                                   (shift-region e ,z) 
                                   (shift-region g ,z) 
                                   (shift-region h ,z))))

Then in your processor cell:

  (chop shifty)")
   (database :initform ""
             :custom (directory :must-match t)
             :label "Database to match input file against"
             :documentation "Load this database before matching.")
   (properties->query :initform 'match-all
                      :custom sexp
                      :label "Properties query function"
                      :documentation "Function to control which regions are scored.")
   (score-function :initform '(weighted-distance 1.0 2.0 0.1 2.0 0.0)
                   :custom sexp
                   :label "Score function"
                   :documentation
                   "Function to compute similarity score between regions.")))

(cl-defmethod eieio-done-customizing :after ((wizard mosaic-chop-wizard))
  (let (rows)
    (with-slots (input-file find-tempo-p tempo slice-size
                            processor database properties->query score-function)
        wizard
      (push (list "chop" ":play" "true" ":input-file" (prin1-to-string input-file) ":processor" (prin1-to-string processor)) rows)
      (push (list "search" ":play" "false" ":target-regions" "(from-row 0)" ":score-function" (prin1-to-string score-function)
                  ":properties->query" (prin1-to-string properties->query)
                  ":database" (prin1-to-string database)) rows)
      (push (list "stretch" ":play" "true" ":target-regions" "(from-row 0)" ":source-regions" "(from-row 1)") rows)
      (push (list (format "[mosaic-beats-per-minute %s]" (prin1-to-string tempo))) rows)
      (push (list (format "[mosaic-slice-size %s]" (prin1-to-string slice-size))) rows))
    (let ((buffer (get-buffer-create "*chopper from wizard*")))
      (switch-to-buffer buffer)
      (cell-mode-with-class 8 16 'mosaic-chopper)
      (cell-copy-cells-from-sexps (nreverse rows))
      (cell-sheet-move-bob)
      (cell-sheet-paste)
      (dolist (cell (cell-collect-cells cell-current-sheet))
        (mosaic-fontify-cell cell))
      (clear-image-cache)
      (cell-sheet-update)
      (cell-sheet-after-open-hook cell-current-sheet))))

(defun mosaic-show-chop-wizard ()
  (interactive)
  (setf mosaic-current-wizard (make-instance 'mosaic-chop-wizard))
  (eieio-customize-object mosaic-current-wizard))

;;; Score wizard

(defclass mosaic-score-wizard (mosaic-wizard)
  ((wizard-title :initform "Score function wizard")
   (wizard-description :initform
"This wizard helps you come up with a score function to control
how database matches are compared for similarity with their
synthesis targets. When you press the Apply button on this
wizard, text for the score function will be copied to the kill
ring for pasting into your Chopper cell sheet.

You must choose five numbers that are the weights (coefficients)
of the terms to be summed in scoring. Each weight controls how
much the similarity of the corresponding property contributes to
the final score. 

As the underlying data are not normalized, 1.0 is not necessarily
a good default. Experimentation will be required to determine the
correct settings for a given combination of source and target
material.")
   (spectrum :initform 1.0
             :custom number
             :label "Spectrum"
             :documentation "The weight to assign spectrum distance.")
   (centroid :initform 1.0
             :custom number
             :label "Centroid"
             :documentation "The weight to assign spectral centroid distance.")
   (tempo :initform 0.0
          :custom number
          :label "Tempo"
          :documentation "The weight to assign difference in tempo.")
   (energy :initform 0.0
           :custom number
           :label "Energy"
           :documentation "The weight to assign difference in  total energy.")
   (pitches :initform 0.0
            :custom number
            :label "Pitches"
            :documentation "The weight to assign similarity of detected pitches.")))
   
(cl-defmethod eieio-done-customizing :after ((wizard mosaic-score-wizard))
  (with-slots (spectrum centroid tempo energy pitches) wizard
    (let ((form (list 'weighted-distance spectrum centroid tempo energy pitches)))
      (with-temp-buffer
        (insert (prin1-to-string form))
        (kill-ring-save (point-min) (point-max))))))

(defun mosaic-show-score-wizard ()
  (interactive)
  (setf mosaic-current-wizard (make-instance 'mosaic-score-wizard))
  (eieio-customize-object mosaic-current-wizard))

;;; XM Slicer wizard

;; (defclass mosaic-xm-slicer-wizard (mosaic-wizard)
;;   ((wizard-title :initform "XM (eXtended Module) Slicer Wizard")
;;    (wizard-des

(provide 'wizards)
;;; wizards.el ends here
