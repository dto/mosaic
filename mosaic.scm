;;; mosaic.scm --- fun with concatenative synthesis -*- snd-scheme -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:

;; SchemeMosaic implements a concatenative synthesis system (and some
;; other things) within the Snd editor.  See the "docs/" subdirectory for
;; full documentation.

;; Audio functionality is built on Bill Schottstaedt's Snd editor.
;; https://ccrma.stanford.edu/software/snd/snd/snd.html Snd is
;; distributed under an MIT-like license.

;; Several functions adapted from Snd library code and/or
;; documentation are marked as such in their header comments

;; The neural network implementation in "quickprop.scm" is in the
;; public domain, and is written by Scott E. Fahlman for
;; Carnegie-Mellon University.

;;; Code:

;;; Required libraries

;; We load only things included with Snd.

(load "extensions.scm") ;; various common things
(load "ws.scm")         ;; with-sound
(load "env.scm")        ;; envelopes
(load "examp.scm")      ;; examples 
(load "dsp.scm")        ;; digital signal processing
(load "clm-ins.scm")    ;; Common Lisp Music instruments
(load "marks.scm")      ;; interactive marking

(load "fullmix.scm")    ;; full-featured mixing
(load "mix.scm")        ;; other mix related functions
(load "v.scm")          ;; FM violin
(load "fmv.scm")        ;; FM violin extensions
(load "rgb.scm")        ;; for colors
(load "stuff.scm")      ;; Object orientation and other CL emulation
(load "play.scm")       ;; For PLAY-REGION-FOREVER and START-DAC etc
(load "jcrev.scm")      ;; JC reverb
(load "nrev.scm")       ;; new reverb
(load "debug.scm")      ;; for TRACE

;; Use these on the Emacs side to get around read-syntax difference

(define true #t)
(define false #f)
(define t #t)
(define nil #f)
(define soundp sound?)

;;; Utilities

(define-macro (pop! x)
  `(let ((head (car ,x)))
     (set! ,x  (cdr ,x))
     head))

(define-macro (push! x place)
  `(set! ,place (cons ,x ,place)))

;;; Definer macros

;; The macro DEFINE^ gives CL-style inline docstrings in function
;; definitions. It also hooks into a documentation generator.

;; S7 rather oddly doesn't allow variables to have docstrings, only
;; procedures. I use the macro DOCUMENT^ to add extra documentation,
;; and EMIT-ORG-ENTRIES to spit out Org mode syntax for making a nice
;; HTML page.

(define *documentation* (make-hash-table 32 equal?))

(define-macro* (define^ spec :rest body)
  (let ((docstring (when (string? (car body))
                     (car body))))
    ;; store func name, arglist, docstring in table
    (set! (*documentation* spec) (or docstring ""))
    `(define ,(car spec)
       (let ((+documentation+ ,(or docstring "")))
         (lambda* ,(cdr spec)
           ,@(if (string? docstring) (cdr body) body))))))

(define-macro* (document^ spec string)
  `(set! (*documentation* ',spec) ,string))

(define^ (org-docstring spec)
  (format #f "** ~S \n ~s\n\n" spec (*documentation* spec)))

(define^ (emit-org-entries)
  (let ((keys (map car *documentation*)))
    (set! keys (sort! keys (lambda (a b)
                             (string<=? (format #f "~A" a)
                                        (format #f "~A" b)))))
    (let ((p (open-output-file (project-file "entries.org"))))
      (map (lambda (key)
             (display (org-docstring key) p))
           keys)
      (close-output-port p))))

(define^ (write-timestamp-file (n #f))
  (let ((p (open-output-file (session-file (if (eq? #f n)
                                               ".timestamp.nil"
                                               (format #f ".timestamp-~d.nil" n))))))
    (display "timestamp" p)
    (close-output-port p)))

(define^ (write-error-file text)
  (let ((p (open-output-file (mosaic-error-file))))
    (display text p)
    (close-output-port p)))

;; This is modified from Snd S7 docs.
(define (catch-all* thunk)
  (catch #t (lambda ()
              (thunk))
    (lambda args
      (let ((text (format #f "~A: ~A~%~A[~A]:~%~A~%" 
                          (car args)                        
                          (apply format #f (cadr args))     
                          (port-filename) (port-line-number)
                          (stacktrace))))                   
        (display text (current-error-port))
        (write-error-file text)))))

(define (catch-silently thunk)
  (catch #t (lambda () (thunk))
         (lambda args #f)))

;;; Managing temp files

(define *mosaic-temp-files* ())

(define^ (mosaic-temp-file) ()
  (let ((file (snd-tempnam)))
    (push! file *mosaic-temp-files*)
    file))

(define^ (mosaic-close-all-temp-files)
  (map (lambda (file)
         (let ((sound (find-sound file)))
           (if (not (eq? #f sound))
               (close-sound sound)))
         (without-errors (delete-file file)))
       *mosaic-temp-files*)
  (set! *mosaic-temp-files* ()))

(define^ (mosaic-clean-up-offset-files)
  (let ((all-files (directory->list "/tmp")))
    (do ((fs all-files (cdr fs)))
        ((null? fs))
      (let ((file (car fs)))
        (when (or (number? (string-position ".offsets.scm" file))
                  (number? (string-position ".snd" file)))
          (delete-file file))))))

(define^ (mosaic-clean-up-snd-files)
  (let ((all-files (directory->list "/tmp")))
    (do ((fs all-files (cdr fs)))
        ((null? fs))
      (let ((file (car fs)))
        (when (number? (string-position ".snd" file))
          (delete-file file))))))
  
;;; A set of sound-producing "Synth" objects whose numbering Scheme
;;; and Emacs agree on. See synth.scm for more information.

(define *mosaic-resources* (make-list 1024))

(define^ (mosaic-resource index)
  (list-ref *mosaic-resources* index))

(set! (setter mosaic-resource)
  (lambda (index value)
    (set! (list-ref *mosaic-resources* index)
      value)))

(define^ (clear-resources)
  (set! *mosaic-resources* (make-list 1024)))

(define^ (find-resource-file index)
  (let ((resource (mosaic-resource index)))
    (cond ((string? resource)
           resource)
          ((not (null? resource))
           (find-output-sound-file resource))
          (#t #f))))

(define^ (from-row n)
  (find-regions (mosaic-resource n)))

;;; Initialization

(define^ (initialize-mosaic)
  (display "SchemeMosaic is Copyright (c) 2006-2007, 2019-2022\n")
  (display "by David O'Toole <deeteeoh1138@gmail.com>\n")
  (display "SchemeMosaic is Free Software under the MIT license.\n")
  (display "See the included file LICENSE for more information.\n")
  (display "SchemeMosaic is based on Bill Schottstaedt's Snd editor.\n")
  (display "See also https://ccrma.stanford.edu/software/snd/\n"))
  ;; (mosaic-load-net (mosaic-percussion-3))

(define^ (emacs-mosaic?)
  (provided? 'emacs-mosaic))

;; Suppressing errors. This is modified from Snd docs. 

(define-macro (without-errors func) 
  `(catch #t 
     (lambda ()
       ,func)
     (lambda args 
       (car args))))

;;; Finding files

(define *mosaic-output-file* #f)

(define^ (mosaic-output-file)
  "Where to place final SchemeMosaic output for this run."
  (or *mosaic-output-file* (mosaic-temp-file)))

(set! (setter mosaic-output-file)
  (lambda (value)
    (set! *mosaic-output-file* value)))

(define *mosaic-project-directory* "~/mosaic/")

(define^ (mosaic-project-directory)
  "Base directory for finding project files. See also PROJECT-FILE.
You can set this with (SET! (MOSAIC-PROJECT-DIRECTORY) mypath)"
  *mosaic-project-directory*)

(set! (setter mosaic-project-directory)
  (lambda (value)
    (set! *mosaic-project-directory* value)))

(define *mosaic-temp-directory* #f)

(define *mosaic-session-directory* #f)

(define^ (mosaic-session-directory)
  "Base directory for finding session files. See also SESSION-FILE.
You can set this with (SET! (MOSAIC-SESSION-DIRECTORY) mypath)"
  (or *mosaic-session-directory* *mosaic-project-directory*))

(set! (setter mosaic-session-directory)
  (lambda (value)
    (set! *mosaic-session-directory* value)))

(define^ (trailing-slash? string)
  "Return #t if there is a slash at the end of STRING."
  (eq? #\/ (string (- (length string) 1))))

(define^ (append-trailing-slash string)
  "Return a copy of STRING with a slash appended."
  (string-append string "/"))

(define^ (project-file file)
  "Return the full path name of FILE within your project directory.
To access subdirectories you can put slashes in the FILE argument."
  (string-append
   (if (trailing-slash? *mosaic-project-directory*)
       *mosaic-project-directory* 
       (append-trailing-slash *mosaic-project-directory*))
   file))

(define^ (session-file file (dir (mosaic-session-directory)))
  "Return the full path name of FILE within your session directory.
To access subdirectories you can put slashes in the FILE argument."
  (string-append
   (if (trailing-slash? dir)
       dir
       (append-trailing-slash dir))
   file))

(define *mosaic-return-file* #f)

(define^ (mosaic-return-file)
  (if (string? *mosaic-return-file*)
      *mosaic-return-file*
      (session-file ".return.scm")))

(define^ (mosaic-tell-emacs value)
  (sexp->file value (mosaic-return-file)))

(define^ (mosaic-tell-squeak value)
  (sexp->file value (mosaic-return-file)))

(define *mosaic-error-file* #f)

(define^ (mosaic-error-file)
  (if (string? *mosaic-error-file*)
      *mosaic-error-file*
      (session-file ".error.scm")))

;;; Object-orientation accessory functions.

;; These are modified from s7test.scm

(define define-class^ define-class)

(document^ '(define-class^ class-name inherited-classes
              (slots ())
              (methods ()))
           "Define a new class.")

(define define-generic^ define-generic)

(define-generic^ initialize-instance)

(define^ (make-instance^ class . args)
  "Create a new object instance of class CLASS with the initialization
keyword argument/value pairs in ARGS."
  (let* ((cls (if (symbol? class) (symbol->value class) class))
         (make (symbol->value (symbol "make-" (symbol->string (cls 'class-name))))))
    (let ((object (apply make args)))
      (initialize-instance object)
      object)))

(document^ '(define-method^ (name (instance class) :rest body))
           "Define a new method.")

;;; DEFINE-METHOD-WITH-NEXT-METHOD, from s7test.scm

(define-macro (define-method^ name-and-args . body)
  `(let* ((outer-env (outlet (curlet)))
          (method-name (car ',name-and-args))
          (method-args (cdr ',name-and-args))
          (object (caar method-args))
          (class (symbol->value (cadar method-args)))
          (old-method (class method-name))
          (arg-names (map (lambda (arg)
                            (if (pair? arg) (car arg) arg))
                          method-args))
          (next-class (and (pair? (class 'inherited))
                           (car (class 'inherited)))) ; or perhaps the last member of this list?
          (nwrap-body (if next-class
                          `((let ((call-next-method
                                   (lambda new-args
                                     (apply (,next-class ',method-name)
                                            (or new-args ,arg-names)))))
                              ,@',body))
                          ',body))
          (method (apply lambda* method-args nwrap-body)))

     ;; define the method as a normal-looking function
     (varlet outer-env
             (cons method-name
                   (apply lambda* method-args
                          `(((,object ',method-name) ,@arg-names)))))

     ;; add the method to the class
     (varlet (outlet (outlet class))
             (cons method-name method))

     ;; if there are inheritors, add it to them as well, but not if they have a shadowing version
     (for-each
      (lambda (inheritor)
        (if (not (eq? (inheritor method-name) #<undefined>)) ; defined? goes to the global env
            (if (eq? (inheritor method-name) old-method)
                (set! (inheritor method-name) method))
            (varlet (outlet (outlet inheritor))
                    (cons method-name method))))
      (class 'inheritors))
     method-name))

;;; Updated version of TRACE from Snd docs. 

;; (define-macro (trace f)
;;   (let ((old-f (gensym)))
;;     `(define ,f
;;        (let ((,old-f ,f))
;;          (apply lambda 'args
;;                 `((format () "(~S ~{~S~^ ~}) -> " ',',f args)
;;                   (let ((val (apply ,,old-f args)))
;;                     (format () "~S~%" val)
;;                     val)))))))

;;; Assertion facility from Snd docs.

(define *debugging* #t)

(define-expansion (assert assertion)
  (if *debugging*          
      `(unless ,assertion
         (format *stdout* "~A: ~A failed~%" (*function*) ',assertion))
      (values)))

;;; Showing progress.

(define *show-progress-style* 'verbose)

(define *show-progress-n* 0)

(define^ (show-progress* s data)
  (format () "~A ~A\n" *show-progress-n* s)
  ;; flush output so that Squeak always gets data immediately
  (flush-output-port *stdout*)
  (flush-output-port *stderr*)
  (set! *show-progress-n* (+ 1 *show-progress-n*)))

(define-expansion (show-progress s)
  `(show-progress* ,s (list (*function*) (*function* (curlet) :arglist))))

;;; Housekeeping and resource management.

(define^ (forget-all-regions)
  "Forget all Snd regions."
  (for-each forget-region (regions)))

(define^ (close-all-sounds)
  "Close all Snd sounds."
  (for-each close-sound (sounds)))

(define^ (close-other-sounds sound)
  "Close all sounds except SOUND."
  (map (lambda (s)
         (when (not (= (sound->integer s)
                       (sound->integer sound)))
           (close-sound sound)))
       (sounds)))

(define^ (close-other-sounds* sound*)
  "Close all sounds except those in the list SOUND*."
  (map save-sound (sounds))
  (let ((ss* (map sound->integer (map find-sound sound*))))
    (map (lambda (s)
           (when (not (member (sound->integer s) ss*))
             (close-sound s)))
         (sounds))))

(define^ (only-resource-sounds)
  "Close all sounds, and then reopen those in the resource list."
  (map save-sound (sounds))
  (let ((files ()))
    (do ((i 0 (+ 1 i)))
        ((null? (mosaic-resource i)))
      (let ((r (mosaic-resource i)))
        (push! (find-output-sound-file r) files)))
    (map close-sound (sounds))
    (map open-sound files)))

(define^ (cleanup)
  "Free up resources."
  (show-progress "cleanup")
  (forget-all-regions)
  (close-all-temp-files)
  (close-all-sounds)
  (forget-all-region-properties))

(define-macro (with-cleanup . body)
  (let ((fn (gensym)))
    `(let ((,fn (lambda ()
                  ,@body)))
       (cleanup)
       (let ((value (,fn)))
         (cleanup)
         value))))

(define^ (startup)
  "Begin with a clean slate, no sounds open, and all resources cleared
up."
  (show-progress "startup")
  (close-all-sounds)
  (cleanup)
  (initialize-mosaic))

(define-macro (with-full-cleanup . body)
  (let ((fn (gensym)))
    `(let ((,fn (lambda ()
                  ,@body)))
       (startup)
       (let ((value (,fn)))
         (cleanup)
         value))))

;;; Other utilities.

(define^ (make-graphable seq)
  (let ((output ()))
    (do ((i 0 (+ i 1)))
        ((null? seq))
      (set! output (cons i output))
      (set! output (cons (car seq) output))
      (set! seq (cdr seq)))
    (reverse output)))

(define^ (random-choose lst)
  (list-ref lst (random (length lst))))

(define^ (del-assoc key lst)
  (let ((entries ()))
    (when (not (null? lst))
      (map (lambda (l)
             (when (list? l)
               (when (not (eq? key (car l)))
                 (set! entries (cons l entries)))))
           lst))
    entries))

(define^ (flatten lst)
  (cond ((null? lst) '())
        ((pair? lst)
         (append (flatten (car lst)) (flatten (cdr lst))))
        (else (list lst))))

(define^ (remove-duplicates* seq (eq-func? equal?))
  (cond ((null? seq) ())
        ((null? (cdr seq)) seq)
        ;; duplicate.
        ((eq-func? (car seq) (cadr seq))
         (remove-duplicates* (cdr seq) eq-func?))
        ;; not a duplicate
        (#t (cons (car seq)
                  (remove-duplicates* (cdr seq) eq-func?)))))

(define^ (remove-duplicates** seq (eq-func? equal?))
  (remove-duplicates* (sort! (copy seq)
                             (lambda (a b)
                               (string<=? (format #f "~A" a)
                                          (format #f "~A" b))))
                      eq-func?))

;;; Converting between pitches and MIDI note numbers.

(define *a4-pitch* 440.0)

(define^ (mosaic-a4-pitch)
  "Master tuning parameter. Pitch of A above middle C.
You can set this with (SET! (MOSAIC-A4-PITCH) VALUE)."
  *a4-pitch*)

(define^ (set-mosaic-a4-pitch value)
  (set! *a4-pitch* value))

(set! (setter mosaic-a4-pitch) set-mosaic-a4-pitch)

(define *a4-note* 69)

(define (log-2 x) 
  (/ (log x) (log 2)))

(define^ (pitch->note pitch)
  "Return the MIDI note number closest to PITCH."
  (let ((value (+ *a4-note*
                  (* 12 (log-2 (/ pitch *a4-pitch*))))))
    (if (real? value)
        (round value)
        -1)))

(define^ (note->pitch note)
  "Return the pitch of MIDI note NOTE."
  (* *a4-pitch*
     (expt 2 (/ (- note *a4-note*) 12))))

;;; Global variables.

(define *mosaic-beats-per-measure* 4)
(define *mosaic-input* "oboe.snd")
(define *mosaic-beats-per-minute* 120)
(define *mosaic-slice-size* 0.25)

;; Slice size determines the whole grid a mosaic operates on.

(define^ (mosaic-divisor sym)
  (cond
   ((eq? sym 'whole-note) 0.25)
   ((eq? sym 'half-note) 0.5)
   ((eq? sym 'quarter-note) 1.0)
   ((eq? sym 'eighth-note) 2.0)
   ((eq? sym 'sixteenth-note) 4.0)))

(define^ (mosaic-slice-size (tempo *mosaic-beats-per-minute*)
                            (divisor 'quarter-note))
  (/ 1.0 (/ tempo 60.00) (mosaic-divisor divisor)))

;; Slice size functions to pass to MAKE-MOSAIC's :SLICE-SIZE parameter.

(define^ (quarter-note)
  (mosaic-slice-size *mosaic-beats-per-minute* 'quarter-note))

(define^ (half-note) (* 2 (quarter-note)))
(define^ (whole-note) (* 4 (quarter-note)))
(define^ (eighth-note) (* 0.5 (quarter-note)))
(define^ (sixteenth-note) (* 0.25 (quarter-note)))
(define whole-beat quarter-note)
(define half-beat eighth-note)

;;; Scanning and processing sounds.

(define^ (scan-sound^ func block-len snd chn (save-position? #t) (offset #f))
  "Scan through a sound and call (FUNC beg end snd chn) for each block
of BLOCK-LEN seconds in length, collecting the results in a list."
  (show-progress (format #f "scanning with block-len: ~A" block-len))
  (let* ((len (framples snd chn))
         (offset (if (number? offset) offset 0))
         (num-blocks (floor (/ (- len offset)
                               (srate snd) block-len))))
    (assert (not (= 0 num-blocks)))
    (when (= 0 num-blocks)
      (set! num-blocks 1))
    (let ((actual-block-len (ceiling (* block-len (srate snd))))
          (results ())
          (slice-number 0))
      (do ((beg offset (+ beg actual-block-len))
           (pos offset (+ pos 1)))
          ((>= beg len) (reverse results))
        (let ((output (func beg (min len (+ beg actual-block-len))
                            snd #t)))
          ;; keep track of its position within the original sound
          ;; so that adjacency can be scored
          ;; TODO: move this part to an intermediate function,
          ;;       since we might want to process non-regions
          (when save-position?
            (set! (@position output) pos)
            (set! (@slice-number output) slice-number))
          (set! slice-number (+ 1 slice-number))
          (set! results (cons output results)))))))

;;; Splitting sounds into uniformly sized regions.

;; Originally SchemeMosaic used real Snd regions but this uses too
;; many temporary files and is slower. The following compatibility
;; functions rewire SchemeMosaic to use lists of float-vectors
;; internally instead.

(define^ (make-region* beg end snd chn)
  (cond ((= 1 (channels snd))
         (list (channel->float-vector beg (- end beg) snd 0)
               (channel->float-vector beg (- end beg) snd 0)))
        ((= 2 (channels snd))
         (list (channel->float-vector beg (- end beg) snd 0)
               (channel->float-vector beg (- end beg) snd 1)))))

(define^ (insert-region* r pos snd chn)
  (when (sound? snd) (select-sound snd))
  (do ((cs r (cdr cs))
       (n 0 (+ 1 n)))
      ((null? cs))
    (float-vector->channel (car cs) pos (length (car cs)) snd n)))

(define^ (float-vector->region fv)
  (let ((sound (new-sound :channels 1)))
    (float-vector->channel fv 0 (length fv) sound 0)
    (let ((value (make-region* 0 (length fv) sound 0)))
      (close-sound sound)
      value)))

(define^ (save-region* region file (sample-type mus-lshort) (header-type mus-riff))
  (let ((s (new-sound :channels (length region) :sample-type sample-type :header-type header-type)))
    (select-sound s)
    (assert (sound? s))
    (insert-region* region 0 s #t)
    (save-sound-as file s)
    (close-sound s)
    (open-sound file)
    ;;(assert (sound? (find-sound file)))
    s))

(define^ (framples* r)
  (framples (car r)))

(define^ (split-sound-to-regions block-len snd (chn 0))
  "Split the sound SND into regions of size SLICE-SIZE seconds in
length."
  (scan-sound^ make-region* block-len snd chn))

;;; Converter functions. These aren't currently used.

(define^ (mosaic-region->snd-region r)
  "Return a SND region version of the Mosaic region R."
  (assert (pair? r))
  (assert (float-vector? (car r)))
  (let ((s (new-sound :channels (length r)))
        (reg ()))
    (select-sound s)
    (assert (sound? s))
    (insert-region* r 0 s)
    (set! reg (make-region 0 (length (car r)) s))
    reg))

(define^ (snd-region->mosaic-region r)
  "Return a Mosaic region version of the SND region R."
  (assert (region? r))
  (let ((s (new-sound :channels (chans r)))
        (reg ()))
    (select-sound s)
    (insert-region r 0 s)
    (set! reg (make-region* 0 (framples r) s))
    reg))

;;; Click reduction at the boundaries of merged regions. Needs
;;; work. This should be adjustable.

(define^ (reduce-clicks pos len snd chn)
  "Attempt to reduce clicks at edges of concatenated sections."
  (env-channel '(0.0 0.0 .005 1.0 .995 1.0 1.0 0.0) pos len snd 0)
  (env-channel '(0.0 0.0 .005 1.0 .995 1.0 1.0 0.0) pos len snd 1))

(define^ (reduce-clicks-region region)
  (let* ((new-region ())
         (temp-file (mosaic-temp-file))
         (new-sound (new-sound :channels 2 :srate 44100 :file temp-file)))
    (insert-region* region 0 new-sound #f)
    (reduce-clicks 0 (framples new-sound) new-sound #t)
    (set! new-region (make-region* 0 (framples* region) new-sound #t))
    (close-sound new-sound)
    new-region))

;;; Layering sounds and synths with specified amplitudes.

(define^ (mixdown :rest spec)
  "Mix multiple sounds with amplitudes applied.
Example: (MIXDOWN MY-SOUND-1 0.8 
                  MY-SOUND-2 0.5
                  ...)"
  (let ((output-file (mosaic-temp-file)))
    (with-sound (:channels 2 :output output-file)
                (do ((ss spec))
                    ((null? ss))
                  (fullmix (file-name (pop! ss))
                           :beg 0 ;; mix at beginning of output sound
                           :matrix (pop! ss) ;; pass amplitude
                           )))
    (save-sound (find-sound output-file))
    (find-sound output-file)))

(define^ (mixdown-synths :rest spec)
  "Like MIXDOWN, but the sound arguments are synths."
  (apply mixdown (map (lambda (s)
                        (if (not (number? s))
                            (merge-sound-as-vectors (new-sound :channels 2)
                                                    (find-regions s))
                            s))
                      spec)))

;;; Scaling regions by a given amplitude.

(define^ (scale-region region (amp 1.0))
  "Amplify REGION by AMP."
  (show-progress "scale")
  (as-one-edit
   (lambda ()
     (let ((sound (new-sound :channels 2)))
       (select-sound sound)
       (insert-region* region 0)
       (env-channel (list 0 amp 1 amp) 0 (framples* region) sound 0)
       (env-channel (list 0 amp 1 amp) 0 (framples* region) sound 1)
       (save-sound sound)
       (sound->region! sound)))))

;;; Merging regions into a single sound.

(define^ (merge-sound-insert^ fn snd (chn 0) regions)
  (show-progress "merge")
  (let ((pos 0))
    (for-each (lambda (r)
                (insert-region* r pos snd chn)
                (when (not (null? fn))
                  (fn pos (framples* r) snd chn))
                (set! pos (+ pos (framples* r))))
              regions)
    snd))

(define^ (merge-sound-insert snd (chn 0) regions)
  (merge-sound-insert^ reduce-clicks snd chn regions))

;; without click reduction
(define^ (merge-sound-insert-raw snd (chn 0) regions)
  (merge-sound-insert^ (lambda (w x y z) #f) snd chn regions))

(define^ (merge-sound snd regions)
  "Merge a list of REGIONS into one sound, SND. Applies click
reduction."
  (set! regions (map reduce-clicks-region regions))
  (cond ((= 1 (channels snd))
         ;; mono to mono, or stereo to mono. discard right channel
         (merge-sound-insert snd 0 regions))
        ((= 2 (channels snd))
         ;; stereo to stereo, or mono to stereo.
         (merge-sound-insert snd 0 regions)
         (merge-sound-insert snd 1 regions))))

;; Previously we used the above functions to insert many regions one
;; after another directly into a Snd sound. But this causes issues
;; with memory allocation because of the long edit list thus formed.
;; So, these new functions below use a big float-vector to merge
;; regions first, then inserts the whole thing into a sound.

(define^ (merge-sound-insert-v vecs chn regions)
  (show-progress "merge-V")
  (let ((pos 0)
        (vec (vecs chn)))
    (for-each (lambda (r)
                (let ((k pos)
                      (block-len (framples* r)))
                  (do ((j 0 (+ j 1)))
                      ((= j block-len))
                    (set! (vec (+ k j))
                          ((r chn) j))))
                (set! pos (+ pos (framples* r))))
              regions)
    vecs))

(define^ (merge-sounds-v vecs regions)
  (set! regions (map reduce-clicks-region regions))
  (cond ((= 1 (length (car regions)))
         ;; mono to mono, or stereo to mono. discard right channel
         (merge-sound-insert-v vecs 0 regions))
        ((= 2 (length (car regions)))
         ;; stereo to stereo, or mono to stereo.
         (merge-sound-insert-v vecs 0 regions)
         (merge-sound-insert-v vecs 1 regions))))

(define^ (merge-sound-as-vectors snd regions)
  (let* ((total-len (apply + (map framples* regions)))
         (vecs (list (make-float-vector total-len)
                      (make-float-vector total-len))))
    (merge-sounds-v vecs regions)
    (insert-region* vecs 0 snd #f)
    snd))

(define^ (merge-sound-raw snd regions)
  "Merge a list of REGIONS into one sound, SND. Does not apply click
reduction."
  (merge-sound-insert-raw snd 0 regions))

;;; Transforming lists of regions according to patterns

(define^ (shuffle regions pattern (amount 1.0))
  "Rearrange the REGIONS according to the PATTERN. Every group of four
regions is bound to variables A B C D and then PATTERN is evaluated to
produce the results. Specify (D C B A) instead to reverse every four
regions, and so on. Because PATTERN is evaluated, you can include
arbitrary code modifying the regions A, B, C, and D in any way you
like, including SHIFT-REGION, STRETCH-REGION, and so on."
  (if (null? regions)
      ()
      (if (<= (random 1.0) amount)
          (if (> (length regions) 3)
              (let ((a (car regions))
                    (b (car (cdr regions)))
                    (c (car (cdr (cdr regions))))
                    (d (car (cdr (cdr (cdr regions))))))
                ;; shuffle according to pattern (which can include its own calls!)
                ;; It's a dirty trick, but works great.
                (append (eval `(list ,@pattern))
                        (shuffle (cdr (cdr (cdr (cdr regions)))) pattern amount)))
              ;; don't shuffle
              regions)
          ;; don't shuffle
          (cons (car regions)
                (shuffle (cdr regions) pattern amount)))))

(define^ (shuffle-8 regions pattern (amount 1.0))
  "Like SHUFFLE, but taking eight regions at a time and giving eight
variables: A B C D E F G H."
  (if (null? regions)
      ()
      (if (<= (random 1.0) amount)
          (if (> (length regions) 7)
              ;; clean this up; use FIRST, SECOND, THIRD etc.
              (let ((a (car regions))
                    (b (car (cdr regions)))
                    (c (car (cdr (cdr regions))))
                    (d (car (cdr (cdr (cdr regions)))))
                    (e (car (cdr (cdr (cdr (cdr regions))))))
                    (f (car (cdr (cdr (cdr (cdr (cdr regions)))))))
                    (g (car (cdr (cdr (cdr (cdr (cdr (cdr regions))))))))
                    (h (car (cdr (cdr (cdr (cdr (cdr (cdr (cdr regions))))))))))
                (append (eval `(list ,@pattern))
                        (shuffle-8 (cdr (cdr (cdr (cdr (cdr (cdr (cdr (cdr regions))))))))
                                   pattern amount)))
              ;; don't shuffle
              regions)
          ;; don't shuffle
          (cons (car regions)
                (shuffle-8 (cdr regions) pattern amount)))))

(define^ (skitter-regions regions)
  (shuffle regions '(a a c d)))

(define^ (skatter-regions regions)
  (shuffle regions '(a a b b c c d d e f f f e g g g)))

(define^ (identity-regions regions) regions)

;;; Rhythmic patterns for use with SHUFFLE and SHUFFLE-8

(define footwork-8 '(a a c d d e e f))
(define vapor-8 '(a a c d e f f g))
(define laidback-8 '(a a c d e f f h))
(define frasier-8 '(a a b c a a b c b b c d e f g h))

(define durango-8 '(a b a b
                      (silent-region b) (silent-region c) b c 
                      c (silent-region d) (silent-region d) e
                      (silent-region f) (silent-region g) h h))

(define fandango-8 '((shift-region a 1.0) (shift-region a
1.025) (shift-region c 1.025) (shift-region d 1.035) (shift-region e
1.05) (shift-region f 1.10) (shift-region g 1.1) (shift-region g
1.15)))

(define vinyl 0.741)

;;; Allow freely composing chop/screw procedures

(define^ (unlambdify-regions x)
  (if (procedure? x) (x) x))

(define^ (process^ fn amount)
  (lambda (regions*)
    (map (lambda (r)
           (fn r amount))
         (unlambdify-regions regions*))))

(define^ (chop (pattern vapor-8))
  (lambda (regions)
    (show-progress "chop")
    (shuffle-8 (unlambdify-regions regions) pattern)))

(define^ (screw (amount vinyl)) (process^ screw-region amount))
(define^ (shift (amount vinyl)) (process^ shift-region* amount))
(define^ (stretch (amount (/ 1.0 vinyl))) (process^ stretch-region* amount))

(define^ (chain :rest ops*)
  (lambda (regions)
    (let ((regs regions))
      (do ((ops ops* (cdr ops)))
          ((null? ops) regs)
        (set! regs
              ((car ops) regs))))))

(define^ (vaporwave (pattern vapor-8))
  (chain (chop pattern) (screw vinyl)))

;;; Adding echo

(define^ (echo-sound sound (time 0.25) (scaler 0.4))
  "Add delay (echo) to a sound. Try using (quarter-note)
or (eighth-note) as values for TIME."
  (show-progress "echo")
  (let ((new-sound (new-sound :channels 2 :srate 44100 :file (mosaic-temp-file)))
        (len (framples sound)))
    (insert-region* (sound->region! sound) 0 new-sound #f)
    (map-channel (echo scaler time) 0 len new-sound 1)
    (map-channel (echo scaler time) 0 len new-sound 0)
    new-sound))

;;; Pitch shifting regions

;; This is based on modifying EXPSRC from Snd's example file
;; (examp.scm) The main changes are that grain length and distance are
;; scaled by the RATE argument and "epsilon", a fudge factor.

(define expsrc*
  (lambda* (rate snd chn)
    (let ((sr (make-src :srate rate
                        :input (let ((gr
                                      (make-granulate :expansion rate
                                                      :input (make-sampler 0 snd chn)
                                                      :scaler 0.6
                                                      :ramp 0.15
                                                      ;; tweak parameters here.
                                                      ;; scale defaults by rate, epsilon
                                                      :length (* 0.1 1.0)
                                                      :jitter 0.0
                                                      :hop (* 0.075 1.0))))
                                 (lambda (dir) 
                                   (granulate gr))))))
      (lambda (inval)
        (src sr 0.0)))))

(define^ (shift-region region factor)
  "Pitch shift REGION by FACTOR."
  (show-progress "shift")
  (as-one-edit
   (lambda ()
     (let* ((new-region ())
            (temp-file (mosaic-temp-file))
            (new-sound (new-sound :channels 2 :srate 44100 :file temp-file)))
       (insert-region* region 0 new-sound #f)
       (map-channel (expsrc* factor) 0 (framples* region) new-sound 1)
       (map-channel (expsrc* factor) 0 (framples* region) new-sound 0)
       (set! new-region (make-region* 0 (framples* region) new-sound #t))
       (close-sound new-sound)
       (delete-file temp-file)
       new-region))))

(define src*
  (lambda* (rate snd chn)
    (let ((sr (make-src :srate rate
                        :input (make-sampler 0 snd chn))))
      (lambda (inval)
        (src sr)))))

;;; Screwing regions (sample rate conversion)
                    
(define^ (screw-region region factor)
  (show-progress "screw")
  (as-one-edit
   (lambda ()
     (let* ((new-region ())
            (temp-file (mosaic-temp-file))
            (s (new-sound :channels 2 :srate 44100 :file temp-file)))
       (insert-region* region 0 s #f)
       (map-channel (src* factor) 0 (framples* region) s 1)
       (map-channel (src* factor) 0 (framples* region) s 0)
       (set! new-region (make-region* 0 (framples* region) s #t))
       (close-sound s)
       (delete-file temp-file)
       new-region))))

;;; Unvoicing regions

(define^ (unvoice-region region)
  (show-progress "unvoice")
  (as-one-edit
   (lambda ()
     (let* ((new-region ())
            (temp-file (mosaic-temp-file))
            (s (new-sound :channels 2 :srate 44100 :file temp-file)))
       (insert-region* region 0 s #f)
       (voiced->unvoiced 1.0 512 3.0 1.0 s 0)
       (voiced->unvoiced 1.0 512 3.0 1.0 s 1)
       (set! new-region (make-region* 0 (framples* region) s #t))
       (close-sound s)
       (delete-file temp-file)
       new-region))))

;;; Cross synthesis

(define^ (cross-regions region-1 region-2)
  (show-progress "cross")
  (as-one-edit
   (lambda ()
     (let* ((new-region ())
            (temp-file-1 (mosaic-temp-file))
            (temp-file-2 (mosaic-temp-file))
            (s1 (new-sound :channels 2 :srate 44100 :file temp-file-1))
            (s2 (new-sound :channels 2 :srate 44100 :file temp-file-2)))
       (insert-region* region-1 0 s1 #f)
       (insert-region* region-2 0 s2 #f)
       (select-sound s1)
       (map-channel (cross-synthesis s2 0.1 128 2.0)
                    0
                    (framples* region-1)
                    s1
                    0)
       (map-channel (cross-synthesis s2 0.1 128 2.0)
                    0
                    (framples* region-1)
                    s1
                    1)
       (set! new-region (make-region* 0 (framples* region-1) s1 #t))
       (close-sound s1)
       (close-sound s2)
       (delete-file temp-file-1)
       (delete-file temp-file-2)
       new-region))))

;;; Time stretching regions

;; This is based on EXPANDN and needs tuning or replacement. I think I
;; may need to use (/ 1 FACTOR) instead of FACTOR, and different
;; epsilons.

(define^ (stretch-region region factor)
  "Time stretch REGION by FACTOR."
  (show-progress "stretch")
  (assert (pair? region))
  (assert (float-vector? (car region)))
  (as-one-edit
   (lambda ()
     (let* ((new-region ())
            (temp-1 (mosaic-temp-file))
            (temp-2 (mosaic-temp-file))
            (len (floor (* factor (framples* region)))))
       (save-region* region temp-1)
       (open-sound temp-1)
       (assert (sound? (find-sound temp-1)))
       (with-sound (:output temp-2)
                   (expandn 0 (* factor (samples->seconds (framples* region)))
                            temp-1
                            0.6
                            :expand factor
                            :grain-amp 0.7
                            :ramp 0.01
                            ;; multiply defaults by factor and "epsilon"
                            :seglen (* factor 0.15 2)
                            ;; notice that hop has its own "epsilon"
                            :hop (* factor 0.05 0.5) 
                            :amp-env '(0 1 100 1)))
       (select-sound (open-sound temp-2))
       (close-sound (find-sound temp-1))
       (let ((r* (sound->region! (selected-sound))))
         (close-sound (selected-sound))
         (delete-file temp-1)
         (delete-file temp-2)
         r*)))))

;;; Other region operations

(define^ (sound->region! name-or-sound)
  "Convert SOUND (object or filename) to a region. Closes SOUND."
  (let ((sound (if (string? name-or-sound)
                   (find-sound name-or-sound)
                   name-or-sound)))
    (save-sound sound)
    (select-sound sound)
    (let ((region (make-region* 0 (framples sound) sound #t)))
      (close-sound sound)
      region)))

(define^ (join-regions a b)
  "Return a new region that is the concatenation of regions A and B."
  (show-progress "join")
  (sound->region! (with-sound (:output (mosaic-temp-file))
                              (insert-region* a 0)
                              (insert-region* b (framples* a)))))

(define^ (simple-drum) 
  (sound->region! (with-sound (:output (mosaic-temp-file)) (fm-drum 0 0.25 55 .5 4 #f))))

(define^ (silent-region region)
  "Return a silent region the same length as REGION."
  (show-progress "silent")
  (as-one-edit
   (lambda ()
     (let* ((len (framples* region))
            (samps (make-float-vector len))
            (sound (new-sound)))
       (insert-samples 0 len samps sound 0) 
       ;; (when (> (length region) 1)
       ;;   (insert-samples 0 len samps sound 1))
       (sound->region! sound)))))

(define^ (quiet-region region)
  (scale-region region 0.3))

(define^ (mix-sounds a b)
  "Return a new sound that is the mix of sounds A and B."
  (show-progress "mix-s")
  (save-sound a)
  (save-sound b)
  (as-one-edit
   (lambda ()
     (let ((sound (new-sound :file (mosaic-temp-file))))
       (select-sound sound)
       (insert-sound (file-name a))
       (mix-sound (file-name b) 0)
       (save-sound sound)
       sound))))

;; Slowing a region down or speeding it up via sample rate
;; conversion. Right now this doesn't work and hangs Snd.
;; This function is obsolete. 

(define^ (speed-region region factor)
  (show-progress "src")
  (let ((new-region #f)
        (file (mosaic-temp-file)))
    (as-one-edit
     (lambda ()
       (with-sound (:channels 2 :output file)
                   (insert-region* region 0)
                   (save-sound (find-sound file))
                   (src-sound factor 1.0 (find-sound file))
                   (set! new-region (make-region* 0 (framples (find-sound file))
                                                 (find-sound file))))
       new-region))))

;;; Alternative shift/stretch functions using Snd's EXP-SND

;; This version of EXP-SND, titled EXP-SND*, is modified from Snd's
;; included clm-ins.scm in order to set :JITTER lower in MAKE-GRANULATE.

(definstrument (exp-snd* file beg dur amp (exp-amt 1.0) (ramp .4) (seglen .15) (sr 1.0) (hop .05) ampenv)
  ;; granulate with envelopes on the expansion amount, segment envelope shape,
  ;; segment length, hop length, and input file resampling rate
  (let ((max-seg-len (if seglen (if (pair? seglen) (max-envelope seglen) seglen) .15))
	(initial-seg-len (if seglen (if (pair? seglen) (cadr seglen) seglen) .15))
	(rampdata (if (pair? ramp) ramp (list 0 ramp 1 ramp)))
	(max-out-hop (if hop (if (pair? hop) (max-envelope hop) hop) .05))
	(initial-out-hop (if hop (if (pair? hop) (cadr hop) hop) .05))
	(min-exp-amt (if exp-amt (if (pair? exp-amt) (min-envelope exp-amt) exp-amt) 1.0))
	(initial-exp-amt (if exp-amt (if (pair? exp-amt) (cadr exp-amt) exp-amt) 1.0)))
    (if (or (<= (min-envelope rampdata) 0.0)
	    (>= (max-envelope rampdata) 0.5))
	(format #t "ramp argument to exp-snd must always be between 0.0 and 0.5: ~A" ramp)
	(let* ((st (seconds->samples beg))
	       (nd (seconds->samples (+ beg dur)))
	       (f0 (make-readin file 0))
	       (expenv (make-env (if (pair? exp-amt) 
				     exp-amt
				     (list 0 exp-amt 1 exp-amt))
				 :duration dur))
	       (lenenv (make-env (if (pair? seglen) 
				     seglen
				     (list 0 seglen 1 seglen))
				 :scaler *clm-srate* :duration dur))
	       (scaler-amp (if (> max-seg-len .15) (/ (* 0.6 .15) max-seg-len) 0.6))
	       (srenv  (make-env (if (pair? sr) 
				     sr 
				     (list 0 sr 1 sr))
				 :duration dur))
	       (rampenv (make-env rampdata :duration dur))
	       (initial-ramp-time (if ramp (if (pair? ramp) (cadr ramp) ramp) .4))
	       (max-in-hop (/ max-out-hop min-exp-amt))
	       (max-len (seconds->samples (+ (max max-out-hop max-in-hop) max-seg-len)))
	       (hopenv (make-env (if (pair? hop) 
				     hop
				     (list 0 hop 1 hop))
				 :duration dur))
	       (ampe (make-env (or ampenv '(0 0 .5 1 1 0)) :scaler amp :duration dur))
	       (exA (make-granulate :expansion initial-exp-amt
				    :input f0
                                    :jitter 0.4
				    :max-size max-len
				    :ramp initial-ramp-time 
				    :hop initial-out-hop
				    :length initial-seg-len 
				    :scaler scaler-amp))
	       (vol (env ampe)))
	  (do ((valA0 (* vol (granulate exA)))
	       (valA1 (* vol (granulate exA)))
	       (ex-samp 0.0)
	       (next-samp 0.0)
	       (i st (+ i 1)))
	      ((= i nd))
	    (let ((sl (env lenenv))) ;current segment length
	      ;; now we set the granulate generator internal state to reflect all these envelopes
	      (set! vol (env ampe))
	      (set! (mus-length exA) (round sl))
	      (set! (mus-ramp exA) (floor (* sl (env rampenv))))) ;current ramp length (0 to .5)
	    (set! (mus-frequency exA) (env hopenv))               ;current hop size
	    (set! (mus-increment exA) (env expenv))               ;current expansion amount
	    (set! next-samp (+ next-samp (env srenv)))            ;current resampling increment
	    (if (> next-samp (+ 1 ex-samp))
		(let ((samps (floor (- next-samp ex-samp))))
		  (if (> samps 2)
		      (do ((k 0 (+ k 1)))
			  ((= k (- samps 2)))
			(granulate exA)))
		  (set! valA0 (if (>= samps 2)
				  (* vol (granulate exA))
				  (set! valA0 valA1)))
		  (set! valA1 (* vol (granulate exA)))
		  (set! ex-samp (+ ex-samp samps))))
	    
	    (outa i (if (= next-samp ex-samp)
			valA0
			(+ valA0 (* (- next-samp ex-samp) (- valA1 valA0))))))))))

;; EXP-SND only works on one channel, so we split the region into two
;; files first, and then merge the results.

(define^ (extract-channel filename snd chn)
  (save-sound-as filename snd :channel chn))

(define^ (warp region (shift 1.0) (stretch 1.0) (seglen .15) (hop .05) (ramp .01))
  (show-progress "warp")
  (assert (pair? region))
  (assert (float-vector? (car region)))
  (let* ((file-0 (mosaic-temp-file))
         (file-1 (mosaic-temp-file))
         (input-sound (new-sound :channels 2))
         (output-file (mosaic-temp-file))
         (output-sound (new-sound :channels 2 :srate 44100 :file output-file))
         (new-region ()))
    (insert-region* region 0 input-sound)
    (extract-channel file-0 input-sound 0)
    (extract-channel file-1 input-sound 1)
    (let ((sound-1 (find-sound (with-sound (:channels 1)
                                           (exp-snd* file-0
                                                    :beg 0
                                                    :dur (* stretch (samples->seconds (framples* region)))
                                                    :amp 0.60
                                                    :exp-amt stretch
                                                    :ramp ramp
                                                    :seglen seglen
                                                    :sr shift
                                                    :hop hop
                                                    :ampenv '(0 1 1 1)))))
          (sound-2 (find-sound (with-sound (:channels 1)
                                           (exp-snd* file-1
                                                    :beg 0
                                                    :dur (* stretch (samples->seconds (framples* region)))
                                                    :amp 0.60
                                                    :exp-amt stretch
                                                    :ramp ramp
                                                    :seglen seglen
                                                    :sr shift
                                                    :hop hop
                                                    :ampenv '(0 1 1 1))))))
      (save-sound sound-1)
      (save-sound sound-2)
      (mix-channel (list (find-sound file-0) 0 0) :snd output-sound :chn 0)
      (mix-channel (list (find-sound file-1) 0 0) :snd output-sound :chn 1)
      (save-sound output-sound)
      (select-sound output-sound)
      (set! new-region (sound->region! output-sound))
      (close-sound sound-1)
      (close-sound sound-2)
      (close-sound input-sound)
      ;; (delete-file file-0)
      ;; (delete-file file-1)
      new-region)))

;; These are currently experimental, and can be substituted for their
;; asterisk-less counterparts at your own risk!
;;
;; <2021-01-01 Fri> These are now the standard. 

(define^ (shift-region* region factor)
  (show-progress "shift*")
  (let ((epsilon 1.08)
        (new-region ()))
    (as-one-edit (lambda ()
                   (set! new-region (warp region
                                          :shift factor
                                          :seglen (* epsilon factor 0.22)
                                          :hop (* epsilon factor 0.03)))))))
  
(define^ (stretch-region* region factor)
  (show-progress "stretch*")
  (let ((epsilon 3.0)
        (new-region ()))
    (as-one-edit (lambda ()
                   (set! new-region (warp region
                                          :stretch factor
                                          :seglen (* epsilon 0.015)
                                          :hop (* epsilon 0.005)))))
    new-region))

;;; Other region functions.

(define^ (down-octave-region region (factor 2))
  (show-progress "down-oct")
  (let ((output (sound->region!
                 (let ((sound (new-sound :file (mosaic-temp-file))))
                   (select-sound sound)
                   (insert-region* region 0)
                   (down-oct factor sound 0)
                   (down-oct factor sound 1)))))
    (close-sound sound)
    output))

(define^ (mix-regions* a b)
  "Return a new region that is the mix of regions A and B."
  (show-progress "mix")
  (assert a)
  (assert (not (null? a)))
  (assert b)
  (assert (not (null? b)))
  (sound->region!
   (let* ((file-a (mosaic-temp-file))
          (file-b (mosaic-temp-file))
          (sound-a (new-sound :channels 2 :file file-a))
          (sound-b (new-sound :channels 2 :file file-b)))
     (assert sound-a)
     (assert sound-b)
     (select-sound sound-b)
     (insert-region* b 0 sound-b)
     (save-sound sound-b)
     (select-sound sound-a)
     (insert-region* a 0 sound-a)
     (mix-sound file-b 0)
     (save-sound sound-a)
     sound-a)))

(define^ (normalize-region region (amp 0.8))
  (show-progress "normalize-region")
  (let ((sound2 #f))
    (let ((region (sound->region!
                   (let* ((sound (new-sound :channels 2 :file (mosaic-temp-file))))
                     (select-sound sound)
                     (insert-region* region 0 sound)
                     ;;(scale-to amp sound 0)
                     (set! sound2 sound)
                     sound))))
      (close-sound sound2)
      region)))

;;; Pitch detection and spectral analysis

;; This is modified from code in generators.scm and clm-ins.scm
;; Original defaults for FFTsize=2048 highest-bin=512

(define *s0* 8192)

(define *mosaic-max-peaks* 64)

(define* (find-spectrum region
                        (fftsize (* 1 8192)) 
                        (highest-bin (* 1 4096))    ; how high in fft data should we search for peaks
                        (max-peaks 64))       ; how many spectral peaks to track at the maximum
  (let ((max-peaks-1 max-peaks)
        (output ())
        (current-freq** 60.0)
        (file (mosaic-temp-file))
        (fftsize-1 fftsize)
        (highest-bin-1 highest-bin)
        (start 0)
        (time-scaler 1.0)
        (attack #f)
        (attack-size 1))
    (assert (pair? region))
    (save-region* region file)
    (let* ((hop (floor (/ fftsize-1 4)))
           (outhop (floor (* time-scaler hop))))
      (let ((ifreq (/ 1.0 outhop))
            (max-oscils (* 2 max-peaks-1)))
        (let ((end (+ start (framples* region)))
              (fil (make-readin file))
              (fdr (make-float-vector fftsize-1))
              (fdi (make-float-vector fftsize-1))
              (window (make-fft-window blackman2-window fftsize-1))
              (current-peak-freqs (make-float-vector max-oscils))
              (last-peak-freqs (make-float-vector max-oscils))
              (current-peak-amps (make-float-vector max-oscils))
              (last-peak-amps (make-float-vector max-oscils))
              (peak-amps (make-float-vector max-peaks-1))
              (peak-amps-copy #f)
              (peak-freqs (make-float-vector max-peaks-1))
              (amps (make-float-vector max-oscils))     ;run-time generated amplitude and frequency envelopes
              (rates (make-float-vector max-oscils))
              (freqs (make-float-vector max-oscils))
              (sweeps (make-float-vector max-oscils))
              ;; (lowest-magnitude .001)
              (ihifreq (hz->radians ifreq))
              (fftscale (/ 1.0 fftsize-1 .42323)) ;integrate Blackman-Harris window = .42323*window width and shift by fftsize-1
              (fft-mag (/ *clm-srate* fftsize-1))
              (furthest-away-accepted .1)
              (filptr 0)
              (filend 0)
              (cur-oscils max-oscils)
              (splice-attack (number? attack))
              (ramped-attack (make-float-vector attack-size)))
          (let ((obank #f)) ;; (make-oscil-bank freqs (make-float-vector max-oscils) amps)))
            
            (set! filend (mus-length fil))
            (float-vector-scale! window fftscale)
            
            (when (< start end)
              (do ((i start (+ i outhop)))
                  ((>= i end))
                (when (<= filptr filend)
                  ;; get next block of data and apply window to it
                  (set! (mus-location fil) filptr)
                  (do ((k 0 (+ k 1)))
                      ((= k fftsize-1))
                    (float-vector-set! fdr k (readin fil)))
                  (float-vector-multiply! fdr window)
                  (set! filptr (+ filptr hop))
                  (fill! fdi 0.0)
                  ;; get the fft 
                  (mus-fft fdr fdi fftsize-1 1)
                  ;; change to polar coordinates (ignoring phases)
                  (rectangular->magnitudes fdr fdi)
                  (float-vector-scale! fdr 2.0)
                  
                  (float-vector-subseq current-peak-freqs 0 max-oscils last-peak-freqs)
                  (float-vector-subseq current-peak-amps 0 max-oscils last-peak-amps)
                  (fill! current-peak-amps 0.0)
                  (fill! peak-amps 0.0)
                  
                  (let ((peaks 0))
                    (let ((ra (fdr 0))
                          (la 0.0)
                          (ca 0.0))
                      ;; search for current peaks following Xavier Serra's recommendations in
                      ;; "A System for Sound Analysis/Transformation/Synthesis 
                      ;;      Based on a Deterministic Plus Stochastic Decomposition"
                      (do ((k 0 (+ k 1)))
                          ((= k highest-bin-1))
                        (set! la ca)
                        (set! ca ra)
                        (set! ra (fdr k))
                        (unless (or (<= ca 0.001)  ; lowest-magnitude
                                    (<= ca ra) 
                                    (<= ca la) 
                                    (zero? ra) 
                                    (zero? la))
                          ;; found a local maximum above the current threshold (its bin number is k-1)
                          (let ((logla (log la 10.0))
                                (logca (log ca 10.0))
                                (logra (log ra 10.0)))
                            (let ((offset (/ (* .5 (- logla logra)) (+ logla (* -2 logca) logra)))) ; isn't logca always 0?
                              (let ((amp (expt 10.0 (- logca (* .25 (- logla logra) offset))))
                                    (freq (* fft-mag (+ k offset -1))))
                                ;; (if (not (real? amp)) (format *stderr* "~A ~A ~A -> ~A ~A~%" la ca ra offset amp))
                                (if (= peaks max-peaks-1)
                                    ;; gotta either flush this peak, or find current lowest and flush him
                                    (let ((minp 0)
                                          (minpeak (peak-amps 0)))
                                      (do ((j 1 (+ j 1)))
                                          ((= j max-peaks-1))
                                        (if (< (peak-amps j) minpeak)
                                            (begin
                                              (set! minp j)
                                              (set! minpeak (peak-amps j)))))
                                      (if (> amp minpeak)
                                          (begin
                                            (set! (peak-freqs minp) freq)
                                            (set! (peak-amps minp) amp))))
                                    (begin
                                      (set! (peak-freqs peaks) freq)
                                      (set! (peak-amps peaks) amp)
                                      (set! peaks (+ peaks 1))))))))))
                    ;; now we have the current peaks
                    (when (eq? #f peak-amps-copy)
                      (set! peak-amps-copy (make-float-vector (length peak-amps)))
                      (do ((n 0 (+ n 1)))
                          ((= n (length peak-amps)))
                        (set! (peak-amps-copy n) (peak-amps n)))
                      (set! peak-amps-copy (copy peak-amps-copy)))
                    ;; find largest amp
                    (do ((k 0 (+ k 1)))
                        ((= k peaks))
                      (let ((pl (float-vector-peak-and-location peak-amps)))
                        (let ((maxpk (car pl))
                              (maxp (cadr pl)))
                          ;;(set! (peak-amps-copy k) maxpk)
                          (when (> maxpk 0.0)
                            ;; now maxp points to next largest unmatched peak
                            (let ((closestp -1)
                                  (closestamp 10.0)
                                  (current-freq (peak-freqs maxp)))
                              (set! current-freq** current-freq))))))))))
                    ;; return data
                    (set! output (list 'freq current-freq**
                                       'peak-freqs (copy peak-freqs)
                                       'peak-amps (copy peak-amps-copy)))))))
    ;; (mosaic-close-all-temp-files)
    output))

(define (spectrum-amps s) (s 5))
(define (spectrum-freqs s) (s 3))
(define (spectrum-freq s) (s 1))

;;; Simpler pitch detection. Wonky but serviceable. 

(define^ (region-pitch-simple region)
  (let ((sound (new-sound)))
    (select-sound sound)
    (insert-region* region 0 sound)
    (let ((value (spot-freq *s0* sound 0)))
      (close-sound sound)
      value)))

;;; Matching regions to each other in various ways.

(define^ (pitch-distance region-1 region-2)
  (abs (- (region-pitch* region-1)
          (region-pitch* region-2))))

(define^ (try-match target score-function source)
  (assert (not (null? source)))
  (assert source)
  (do ((s source (cdr s)))
      ((null? s))
    (assert (string? (car s))))
  (let* ((best (car source))
         (best-score (score-function target best)))
    (do ((s source (cdr s)))
        ((null? s))
      (let ((score (score-function target (car s))))
        (when (<= score best-score)
          (set! best (car s))
          (set! best-score score))))
    best))

(define^ (try-match-properties properties score-function source)
  (assert (not (null? properties)))
  (let ((silence (list (make-float-vector 10 0.0)
                       (make-float-vector 10 0.0))))
    (set! (*region->properties* silence) properties)
    (assert (not (null? (*region->properties* silence))))
    (assert (not (eq? #f (*region->properties* silence))))
    (try-match silence score-function source)))

(define^ (match-regions score-function target-regions source-regions)
  "Attempt to match each region in TARGET-REGIONS with its best
match in SOURCE-REGIONS. The regions are compared via
SCORE-FUNCTION, which by default is PITCH-DISTANCE. MATCH-REGIONS
returns a list of regions chosen from SOURCE-REGIONS chosen to best
match (in each case) their respective TARGET-REGIONS. See also
MATCH-PROPERTIES and FUZZY-MATCH-PROPERTIES."
  (show-progress "match")
  (map (lambda (t)
         (try-match t score-function source-regions))
       target-regions))

;;; Shift matching

(define^ (shift-match-regions target-regions source-regions (tolerance 5.0) (max-shift 440.0))
  "Works like MATCH-REGIONS, but pitch shifts the best match if the
best match is more than TOLERANCE Hz off the target pitch."
  (show-progress "shift-match")
  (assert (pair? target-regions))
  (assert (pair? source-regions))
  (assert (pair? (car target-regions)))
  (assert (pair? (car target-regions)))
  (assert (float-vector? (car (car target-regions))))
  (assert (float-vector? (car (car source-regions))))
  (map (lambda (target)
         (let* ((best-match (try-match target pitch-distance source-regions))
                (pitch-ratio (/ (region-pitch* target)
                                (region-pitch* best-match))))
           (if (and (> (pitch-distance target best-match) tolerance)
                    (< (pitch-distance target best-match) max-shift))
               ;; do I have this backwards? should be (/ 1 ratio)?
               (shift-region* pitch-ratio best-match )
               best-match)))
       target-regions))

;;; Stretch matching

(define^ (stretch-match-regions target-regions
                                source-regions
                                (stretch-fn stretch-region*))
  "Works like MATCH-REGIONS, but stretches the SOURCE-REGIONS to
match the lengths of their corresponding TARGET-REGIONS. Because
SchemeMosaic's stretch functions still need work, the result may be
less than optimal. Try passing STRETCH-REGION* (or your own function)
instead of STRETCH-REGION."
  (show-progress "stretch-match")
  (let ((results ()))
    (do ((ts target-regions (cdr ts))
         (ss source-regions (cdr ss)))
        ((null? ts) (begin
                      (let ((a (apply + (map framples* results)))
                            (b (apply + (map framples* target-regions))))
                        (assert (= a b))
                        (when (not (= a b))
                          (show-progress (format #f "stretch-match overall failed: target-length = ~A / result length = ~A" b a))))
                      (reverse results)))
      (let* ((target-length (framples* (car ts)))
             (length-ratio (/ target-length (framples* (car ss))))
             (result (stretch-fn (car ss) length-ratio)))
          (assert (= target-length (framples* result)))
          (if (not (= target-length (framples* result)))
              (show-progress (format #f "stretch-match failed: target-length = ~A / result length = ~A" target-length (framples* result)))
              (show-progress (format #f "stretch-match succeeded: ~A framples" target-length)))
          (push! result results)))))

;;; Layering

(define^ (mix-pairwise regions1 regions2)
  "Return a list of regions where the first of each set are mixed
together, the second of each set, etc. The output regions are the same
size as the longer of the two regions being mixed at each step."
  (show-progress "mix/p")
  (let ((pos 0)
        (output-regions ()))
    (let ((r2 regions2))
      (do ((r1 regions1 (cdr regions1)))
          ((null? r1) output-regions)
        (if (not (null? r2))
            (begin (push! (mix-regions (car r1) (car r2))
                          output-regions)
                   (set! r2 (cdr r2)))
            (push! (car r1) output-regions))))))

(define^ (merge-pairwise snd chn regions1 regions2)
  "Merge REGIONS-1 and REGIONS-2 pairwise into the sound SND, but
align the beginnings of respective region pairs so that the outcome is
the same as having used MIX-PAIRWISE first and then MERGE-SOUND."
  (show-progress "merge/p")
  (let ((pos 0))
    (let ((r2 regions2))
      (do ((r1 regions1 (cdr regions1)))
          ((null? r1))
        (insert-region* (car r1) pos snd chn)
        (when (not (null? r2))
          (mix-region (car r2) pos snd chn)
          (set! r2 (cdr r2)))
        ;; output hop is size of r1, not r2!
        (set! pos (+ pos (framples* (car r1))))))
    (save-sound snd)
    snd))

;;; Tempo detection using Soundtouch as an external program

;; This is not always accurate. Soundtouch must be installed on your
;; system, along with its "soundstretch" command line program. This
;; currently works with soundstretch 1.9.2.

(define *bpm-output-marker* "Detected BPM rate ")
(define (find-marker str) (string-position *bpm-output-marker* str))
;; Because the info is output to *stderr*, we need the 2>&1 at the end.
(define (bpm-command-string file) (format #f "soundstretch ~s -bpm 2>&1" file))
(define (command-to-string com) (system com #t))

(define^ (find-tempo file)
  "Attempt to use Soundtouch to find tempo of FILE."
  (show-progress "find-tempo")
  (let* ((output (command-to-string (bpm-command-string file)))
         (pos (find-marker output)))
    (if (number? pos)
        (let* ((value+ (substring output (+ pos (length *bpm-output-marker*))))
               (end (char-position #\newline value+)))
          (if (integer? end)
              (string->number (substring value+ 0 end))
              (error 'external-program "Error parsing soundstretch output.")))
        (error 'external-program "Error running soundstretch."))))

;;; UUID generation via external utility

;; This section is a simple wrapper around the command-line UUID
;; generator "uuidgen." This is available on Debian in the package
;; "e2fsprogs".

(define uuidgen-program "uuidgen")

(define^ (make-uuid)
  (let* ((output (command-to-string uuidgen-program)))
    ;; remove trailing newline
    (substring output 0 (char-position #\newline output))))

;; Calls (FUNC reg beg len new chn) where REG is input region and NEW
;; is new sound, returning the new sound. This is not currently used.

(define^ (process-sound^ func block-len snd chn)
  (let* ((len (framples* snd chn))
         (num-blocks (floor (/ len (srate snd) block-len)))
         (new (new-sound)))
    (if (> num-blocks 1)
        (let ((actual-block-len (ceiling (/ len num-blocks))))
          (do ((n 0 (+ n 1)))
              ((= n num-blocks) (save-sound new))
            (let ((beg (* n actual-block-len)))
              (let ((reg (make-region* beg (+ beg actual-block-len -1) snd chn)))
                (func reg beg actual-block-len new chn)
                (forget-region reg))))
          new))))

(define^ (mosaic-stomp file time)
  (mus-file-mix *output* file (seconds->samples time)))

(define^ (mosaic-pad sound len)
  (let ((len-framples (seconds->samples len)))
    (select-sound sound)
    (mix-float-vector (make-float-vector len-framples 0.0) 0 sound)
    (set! (framples sound) len-framples)
    (save-sound sound)))

(define^ (mosaic-play-sound-file file)
  (let ((sound (find-sound file)))
    (when (eq? #f sound)
      (set! sound (open-sound file)))
    (play sound)))

;; the following is modified from play.scm

(define play-once*
  (lambda (reason)
    (if (= reason 0)
        (play (selected-sound) :start 0 :stop play-once*))))

(define^ (mosaic-play-looping file)
  (let ((sound (find-sound file)))
    (when (eq? #f sound)
      (set! sound (open-sound file)))
    (select-sound sound)
    (play (selected-sound) :start 0 :stop play-once*)))

(define^ (scan-until^ func block-len offset snd chn)
  "Scan through a sound and call (FUNC beg end snd chn) for each block
of BLOCK-LEN seconds in length, until the FUNC returns #t. Returns 
the position in samples of the point at which FUNC returned #t, 
or #f if FUNC never does so."
  (show-progress "scan-until")
  (let* ((len (framples snd chn))
         (num-blocks (floor (/ len (srate snd) block-len)))
         (actual-block-len (ceiling (/ len num-blocks)))
         (quit #f)
         (pos* offset))
    (do ((beg offset (+ beg actual-block-len))
         (pos offset (+ pos 1)))
        ((or (eq? quit #t)
             (>= beg len)))
      (let ((result (func beg (+ beg actual-block-len) snd #t)))
        (when (not (eq? result #f))
          (set! pos* result)
          (set! quit #t))))
    (if (eq? quit #t)
        pos*
        #f)))

(define *mosaic-beat-energy-threshold* 0.1)

(define^ (region-beat-energy? beg end snd nil)
  (show-progress "region-beat-energy?")
  (let ((region (make-region* beg end snd)))
    (let ((sound (new-sound :channels 2)))
      (insert-region* region beg sound)
      (let ((total-energy (channel-total-energy sound 0)))
        (display (list 'total-energy total-energy beg end))
        (close-sound sound)
        (if (> total-energy *mosaic-beat-energy-threshold*)
            (let ((d ()))
              beg)
            #f)))))

(define^ (region-beat-power? beg end snd nil)
  (show-progress "region-beat-power?")
  (let ((region (make-region* beg end snd)))
    (let ((sound (new-sound :channels 2)))
      (insert-region* region beg sound)
      (let ((average-power (channel-average-power sound 0)))
        (display (list 'average-power average-power))
        (close-sound sound)
        (if (> average-power 0.002)
            (let ((d ()))
              beg)
            #f)))))

(define *mosaic-minimum-beat-amp* 0.014)
(define *mosaic-maximum-beat-freq* 200)

(define *mosaic-find-beat-slice-size* 0.010)

(define^ (find-beat sound (offset 0))
  "Attempt to find the sample offset of the first drum beat in SOUND.
This can fail ungracefully; if you wish to disable beat detection,
pass #t for the FIND-OFFSET argument of MOSAIC-IMPORT-SOUND on the
Scheme side, or set `mosaic-import-find-offset' to `nil' on the Emacs
side."
  (scan-until^ region-beat-energy? *mosaic-find-beat-slice-size* offset sound #f))

(define (replace-bad-chars chars)
  (if (pair? chars)
      (if (or (equal? (car chars) #\space)
              (equal? (car chars) #\/))
          (replace-bad-chars (cdr chars))
          (cons (car chars) (replace-bad-chars (cdr chars))))
      ()))

(define (clean-filename filename)
  (let ((chars (string->list filename)))
    (apply string (replace-bad-chars chars))))

;; this is modified from Snd sources in examp.scm
(define^ (mosaic-explode-soundfont file)
  (display (format #f "Exploding Soundfont: ~s..." file))
  (let ((sound (open-sound file))
        (output-files ()))
    (select-sound sound)
    (let sf2it ((lst (soundfont-info)))
      (if (pair? lst)
	  (let* ((vals (car lst))
		 (start (cadr vals)))
	    (let ((end (if (null? (cdr lst))
			   (framples)
			   (cadadr lst)))
		  (loop-start (- (caddr vals) start))
		  (loop-end (- (cadddr vals) start))
		  (filename (session-file (clean-filename (string-append (car vals) ".aif")))))
              (push! session-file output-files)
              (display (format #f "Saving AIF sample: ~S" (list filename loop-start loop-end)))
	      (if (selection?)
		  (set! (selection-member? #t) #f))
	      (set! (selection-member?) #t)
	      (set! (selection-position) start)
	      (set! (selection-framples) (- end start))
	      (save-selection filename (selection-srate) mus-bshort mus-aiff)
	      (let ((temp (open-sound filename)))
		(set! (sound-loop-info temp) (list loop-start loop-end))
		(close-sound temp))
	      (sf2it (cdr lst))))))
    (close-sound sound)
    (display (format #f "Exploding Soundfont: ~s... Done." file))
    output-files))

(define^ (mosaic-find-offsets file (skip-amount 4096))
  (let ((sound (open-sound file))
        (offsets ())
        (end? #f))
    (select-sound sound)
    (let ((len (framples sound)))
      (do ((pos 0))
          ((or end? (>= pos len))
           (reverse offsets))
        (set! pos (find-beat sound pos))
        (if pos
            (begin (push! pos offsets)
                   (set! pos (+ pos skip-amount)))
            (set! end? #t))))
    (select-sound sound)
    (reverse offsets)))

(define^ (mosaic-tell-offsets file (skip-amount 4096) net-file)
  (mosaic-select-net (mosaic-read-net net-file))
  (mosaic-tell-emacs (mosaic-find-offsets file skip-amount)))

;;; Slicing and stretching beats

(define^ (mosaic-slice-on-offsets sound offsets)
  (show-progress (format #f "slice-on-offsets: ~d offsets / first offset ~d"
                         (length offsets)
                         (car offsets)))
  (assert (sound? sound))
  (select-sound sound)
  (assert (pair? offsets))
  (assert (integer? (car offsets)))
  (let ((regions ())
        (total-len (framples sound)))
    (do ((pos (car offsets)))
        ((null? offsets) (reverse regions))
      (let ((end (if (null? (cdr offsets))
                     total-len
                     (cadr offsets))))
        (push! (make-region* pos end sound) regions)
        (assert (= 2 (length (car regions))))
        (set! pos end)
        (pop! offsets)))
    (show-progress "/slice-on-offsets")
    (reverse regions)))

(define^ (mosaic-slice-to-files sound offsets file-prefix)
  (let ((regions (mosaic-slice-on-offsets sound offsets)))
    (show-progress "slice-to-files")
    (do ((n 0 (+ 1 n)))
        ((or (null? regions)
             (= n (length regions))))
      (save-region* (pop! regions)
                    (session-file (append file-prefix
                                          (format #f ".slice-~d.wav" n)))))))

(define^ (pad-region region new-length)
  (show-progress "pad-region")
  (assert (= 2 (length region)))
  (assert (> new-length (framples* region)))
  (let* ((padding-length (- new-length (framples* region)))
         (padding (list (make-float-vector padding-length)
                        (make-float-vector padding-length))))
    (let ((new-region (list (append (car region)
                                    (car padding))
                            (append (cadr region)
                                    (cadr padding)))))
      (let ((sound (new-sound :channels 2)))
        (insert-region* new-region 0 sound)
        (reduce-clicks 0 (framples* region) sound)
        (reduce-clicks (framples* region) new-length sound)
        (make-region* 0 new-length sound)))))

(define^ (subregion region beg end)
  (let ((sound (new-sound :channels 2)))
    (insert-region* region 0 sound)
    (make-region* beg end sound)))

(define^ (append-regions a b)
  (let ((new-region (list (append (car a)
                                  (car b))
                          (append (cadr a)
                                  (cadr b)))))
    (let ((sound (new-sound :channels 2)))
      (insert-region* new-region 0 sound)
      (reduce-clicks 0 (framples* a) sound)
      (reduce-clicks (framples* a) (framples* b) sound)
      (make-region* 0 (+ (framples* a) (framples* b)) sound))))

(define^ (pad-region-stretch region new-length)
  (show-progress "pad-region-stretch")
  (assert (= 2 (length region)))
  (assert (> new-length (framples* region)))
  (let* ((old-length (framples* region))
         (attack-length (round (* old-length 0.5)))
         (decay-length (round (* old-length 0.5)))
         (attack-region (subregion region 0 attack-length))
         (decay-region (subregion region attack-length (+ attack-length decay-length)))
         (stretched-decay-length (- new-length attack-length))
         (stretched-decay-region (stretch-region* decay-region (/ stretched-decay-length decay-length))))
    (append-regions attack-region stretched-decay-region)))

(define^ (truncate-region region new-length)
  (show-progress "truncate-region")
  (assert (< new-length (framples* region)))
  (list (subsequence (car region) 0 new-length)
        (subsequence (cadr region) 0 new-length)))

(define^ (mosaic-trim-regions regions scaled-offsets total-length stretch-tails)
  (show-progress "trim-regions")
  (let ((output ()))
    (do ((n 0 (+ n 1))
         (pos 0))
        ((or (null? regions)
             (null? scaled-offsets))
         (reverse output))
      (let* ((rlen (framples* (car regions)))
             (slen (if (null? (cdr scaled-offsets))
                       (- total-length pos)
                       (- (cadr scaled-offsets) pos)))
             (lengthen? (< rlen slen))
             (shorten? (> rlen slen))
             (same? (= rlen slen)))
        (push! (cond (same? (car regions))
                     (lengthen? (if stretch-tails
                                    (pad-region-stretch (car regions) slen)
                                    (pad-region (car regions) slen)))
                     (shorten? (truncate-region (car regions) slen)))
               output)
        (pop! regions)
        (pop! scaled-offsets)
        (set! pos (+ pos slen))))))

(define^ (mosaic-scale-beats input-file scale threshold skip-amount stretch-tails)
  (show-progress "Scaling beats...")
  (assert (file-exists? input-file))
  (set! *mosaic-beat-energy-threshold* threshold)
  (let* ((offsets (mosaic-find-offsets input-file skip-amount))
         (scaled-offsets (map (lambda (offset)
                                (round (* offset scale)))
                              offsets))
         (sound (selected-sound))
         (total-length (framples sound))
         (scaled-length (round (* scale total-length)))
         (beats (mosaic-slice-on-offsets sound offsets))
         (scaled-beats (mosaic-trim-regions beats scaled-offsets scaled-length stretch-tails)))
    scaled-beats))

(define^ (mosaic-load-extensions)
  (load (project-file "properties.scm"))
  (load (project-file "sequitur.scm"))
  (load (project-file "synth.scm"))
  (load (project-file "data.scm"))
  (load (project-file "orchestra.scm"))
  (load (project-file "quickprop.scm"))
  (load (project-file "neural.scm"))
  (load (project-file "percussion-3.scm"))
  (load (project-file "groove-16.scm")))

(define^ (mosaic-chunk-offsets-file file)
  (string-append file ".offsets.scm"))

(define^ (mosaic-chunk-offsets-file-exists? file)
  (file-exists? (mosaic-chunk-offsets-file file)))

(define^ (mosaic-write-region-offsets regions file)
  (let ((output-file (mosaic-chunk-offsets-file file))
        (offsets (find-region-offsets regions)))
    (show-progress (format #f "write-region-offsets: ~d regions / ~d offsets / ~d framples for file ~A"
                           (length regions)
                           (length offsets)
                           (apply + (map framples* regions))
                           file))
    (sexp->file offsets output-file)))

(define^ (find-region-offsets regions)
  (show-progress (format #f "find-region-offsets: ~d regions" (length regions)))
  (let ((offset 0)
        (offsets ()))
    (let ((result
           (do ((rs regions (cdr rs)))
               ((null? rs) (reverse offsets))
             (set! offsets (cons offset offsets))
             (set! offset (+ offset (framples* (car rs)))))))
      (show-progress (format #f "/find-region-offsets: ~d offsets / ~d framples"
                             (length result)
                             (apply + (map framples* regions))))
      result)))

(define^ (from-file file slice-size)
  (let ((sound (open-sound file)))
    (if (not (mosaic-chunk-offsets-file-exists? file))
        ;; slice regions normally
        (let ((regions (scan-sound^ make-region* slice-size sound)))
          (close-sound sound)
          regions)
        ;; slice on saved offsets and ignore slice-size arg
        (let ((output-regions
               (mosaic-slice-on-offsets sound
                                        (cdr (file->sexp (mosaic-chunk-offsets-file file))))))
          (show-progress (format #f "Loaded ~d regions with ~d framples from ~A"
                                 (length output-regions)
                                 (apply + (map framples* output-regions))
                                 file))
          (close-sound sound)
          output-regions))))

(define^ (from-files files slice-size)
  (let ((regions* ()))
    (do ((f files (cdr f)))
        ((null? f) (apply append (reverse regions*)))
      (push! (from-file (car f) slice-size)
             regions*))))

(define^ (mosaic-make-chunks file num-chunks slice-size prefix)
  (let* ((sound (open-sound file))
         ;; (regions (scan-sound^ make-region* slice-size sound))
         (regions (from-file file slice-size))
         (num-regions (length regions))
         (regions-per-chunk (truncate (/ num-regions num-chunks)))
         (chunks ())
         (chunk-file #f))
    (do ((i 0 (+ i 1)))
        ((= i num-chunks))
      (push! (subsequence regions
                          (* i regions-per-chunk)
                          (min (+ num-regions 1)
                               (* (1+ i) regions-per-chunk)))
             chunks))
    (set! chunks (reverse chunks))
    (do ((i 0 (+ i 1)))
        ((= i num-chunks))
      (let ((chunk-sound (new-sound :channels 2 :srate (srate sound)
                                    :header-type mus-riff :sample-type mus-lshort)))
        (set! chunk-file (append prefix (format #f "~d" (+ i 1)) ".wav"))
        (merge-sound-as-vectors chunk-sound (chunks i))
        (save-sound-as chunk-file chunk-sound)
        (mosaic-write-region-offsets (chunks i) chunk-file)
        (close-sound chunk-sound)))
    (close-sound sound)))

(define^ (mosaic-merge-chunks files slice-size output-file)
  (show-progress "merge-chunks A")
  (let ((regions (from-files files slice-size)))
    (let ((sound (new-sound :channels 2 :srate 44100
                            :header-type mus-riff :sample-type mus-lshort
                            :file output-file)))
      (merge-sound-as-vectors sound regions)
      (show-progress "merge-chunks B")
      (save-sound sound)
      (close-sound sound)
      ;; save offsets
      (mosaic-write-region-offsets regions output-file))))

(define^ (mosaic-test-merge file num-chunks slice-size)
  (let ((chunk-files (let ((fs ()))
                       (do ((i 0 (+ i 1)))
                           ((= i num-chunks) (reverse fs))
                         (push! (format #f "/tmp/chunk-~d.wav" (+ i 1))
                                fs)))))
    (let* ((whole-sound (open-sound file))
           (whole-len (framples whole-sound)))
      (mosaic-make-chunks file num-chunks slice-size "/tmp/chunk-")
      (mosaic-merge-chunks chunk-files slice-size "/tmp/merged.wav")
      (let* ((merged-sound (open-sound "/tmp/merged.wav"))
             (merged-len (framples merged-sound)))
        (format #t "Whole: ~d framples / Merged: ~d framples" whole-len merged-len)))))
                              
(define (assoc* key alist)
  (assoc key alist))

(define^ (mosaic-set-loop-points file start end)
  (let ((sound (open-sound file)))
    (set! (sound-loop-info sound) (list start end
                                        0
                                        0))
    (close-sound sound)))

;; The following function is adapted from Snd sources.
(define^ (mosaic-find-zero+ vec start)
  (let ((lastn 0.0))
    (do ((index start (+ index 1)))
        ((or (= index (length vec))
             (and (< lastn 0.0)
                  (>= (vec index) 0.0)))
         (if (= index (length vec))
             start
             index))
      (set! lastn (vec index)))))

(define^ (mosaic-set-loop-points-and-base-note file start end base-note)
  (let ((sound (open-sound file)))
    (set! (sound-loop-info sound) (list start end
                                        0
                                        0
                                        base-note))
    (close-sound sound)))

(define^ (mosaic-set-loop-points-and-base-note-with-adjustment file start end base-note)
  (let ((sound (open-sound file)))
    (let ((vec (car (make-region* 0 (framples sound) sound #t))))
      (let* ((start* (mosaic-find-zero+ vec start))
             (end* (mosaic-find-zero+ vec end)))
        (show-progress (format #f "set-loop-points-+++ ~S ~S" start* end*))
        (set! (sound-loop-info sound) (list start* end*
                                            0
                                            0
                                            base-note))
        (close-sound sound)))))
    
;; The commented version below is used for debugging.

;; (define (assoc* key alist)
;;   (display (list '%%% key))
;;   (assert alist)
;;   (assert (not (null? alist)))
;;   (assert key)
;;   (assert (not (null? key)))
;;   (let ((result (assoc key alist)))
;;     (if (eq? #f result)
;;         (begin
;;           (show-progress (list "WARNING: No entry for key: " key))
;;           result)
;;         result)))

;; (map trace '(properties->features mosaic-net-test-region percussion-3-guess percussion-3 percussion-3-guess-multiple mosaic-find-guesses percussion-3 mosaic-tell-labels percussion-3-from-properties percussion-3-distance mosaic-match-beats mosaic-beatbox-resynth region-kick% region-snare% region-cymbal% mosaic-load-database mosaic-slice-on-offsets remember-region-properties test-example set-region-property! region-property note-lists-only percussion-3-guesses-only find-percussion-3-structure ))

;;(map trace '(mosaic-load-database mosaic-slice-on-offsets note-lists-only percussion-3-guesses-only find-percussion-3-structure find-structure structure->grammar rewrite-expansion!))

(provide 'mosaic.scm)
;; Local Variables:
;; indent-tabs-mode: nil
;; End:
;;; mosaic.scm ends here
