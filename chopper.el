;;; mosaic-chopper.el --- basic cell sheet type for Mosaic  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Chopper sheet for concatenative synthesis matching

(defclass mosaic-chopper (mosaic-sheet eieio-speedbar-file-button) ())

(defun mosaic-expand-synth-class-name (name)
  (or (cl-case name
        ('file 'file-synth)
        ('chop 'mosaic-synth)
        ('search 'search-match-synth)
        ('trim 'trim-match-synth)
        ('shift-search 'shift-search-match-synth)
        ('shift 'shift-match-synth)
        ('unvoice 'unvoice-synth)
        ('stretch 'stretch-match-synth)
        ('cross 'cross-synth)
        ('everything 'everything-synth)
        ('morph 'morph-match-synth)
        ('morph-search 'morph-search-match-synth)
        ('beat 'beat-synth))
      name))

(defun mosaic-configure-slices* ()
  (when (and (not (null mosaic-slice-size))
             (symbolp mosaic-slice-size))
    (let ((divisor
           (cl-case mosaic-slice-size
             ('whole-note 0.25)
             ('half-note 0.5)
             ('quarter-note 1.0)
             ('eighth-note 2.0)
             ('sixteenth-note 4.0))))
      (if (null divisor)
          (progn
            ;;(setf mosaic-slice-size (/ (/ 1.0 (/ (float mosaic-beats-per-minute) 60.0))))
            (mosaic-error "Bad value for MOSAIC-SLICE-SIZE"))
        (let ((val (/ (/ 1.0 (/ (float mosaic-beats-per-minute) 60.0))
                      divisor)))
          (message "Mosaic: setting slice size to %S" val)
          (setf mosaic-slice-size val))))))

(defmethod cell-sheet-apply-settings :after ((m mosaic-chopper))
  (mosaic-configure-slices*))
 
(defun mosaic-fontify-cell (cell)
  "Apply fontification to CELL depending on its content."
  (when (cell-label cell)
    (setf (cell-face cell)
          (or (cl-case (aref (cell-label cell) 0)
                (?: 'font-lock-variable-name-face)
                (?\[ 'cell-comment-face)
                (?\" 'font-lock-string-face)))))
  (cell-set-value cell (cell-value cell)))

(cl-defmethod cell-sheet-after-update-hook ((sheet mosaic-chopper))
  (let ((modified-p (buffer-modified-p (current-buffer))))
    (cell-with-current-cell-sheet
     (let ((cell (cell-grid-get grid cursor-row cursor-column)))
       (when cell (mosaic-fontify-cell cell)))
     (mosaic-insinuate-header-line)
     (setf (buffer-modified-p (current-buffer)) modified-p))))

(cl-defmethod cell-sheet-after-open-hook ((sheet mosaic-chopper))
  (cell-with-current-cell-sheet
   (dotimes (r (length grid))
     (dotimes (c (length (aref grid 0)))
       (let ((cell (cell-grid-get grid r c)))
         (when cell
           (mosaic-fontify-cell cell)))))
   (cl-call-next-method)))

(cl-defmethod mosaic-scheme-collect-cell-values ((sheet mosaic-chopper) &optional all-p)
  (mapcar (lambda (row)
            (mapcar #'cell-find-value row))
          (cell-collect-cells-by-row sheet all-p)))

(defun mosaic-sheet-apply-settings ()
  "Apply settings in the current sheet.
These are cells of the form [VAR VALUE].  Neither the VAR nor the
VALUE are evaluated."
  (interactive)
  (cell-with-current-cell-sheet
   (cell-sheet-apply-settings cell-current-sheet)))

(defun mosaic-scheme-filter-value (value)
  "Filter VALUE for a `mosaic-chopper' object."
  (cond
   ((stringp value)
    ;; allow passing absolute paths
    (if (file-name-absolute-p value)
        (expand-file-name value)
      `(session-file ,value)))
   ;; ((integerp value) `(find-regions (mosaic-resource ,value)))
   ;; ((numberp value) value)
   ;; delete settings cells
   ((vectorp value) nil)
   (t value)))

(defvar mosaic-chopper-play-column ())

(defun mosaic-scheme-create-process-forms (forms)
  "Process the table rows in FORMS to create Scheme."
  (let ((index 0)
        (results ())
        (play-column ()))
    (dolist (form forms)
      (push `(begin
              (set! (mosaic-output-file)
                    (session-file ,(mosaic-resource-file-name index)))
              (set! (mosaic-resource ,index)
                    (make-instance^ ',(mosaic-expand-synth-class-name (cl-first form))
                                    ,@(cl-rest form)))
              (find-output-sound (mosaic-resource ,index))
              (write-timestamp-file ,index)
              (let ((s (find-sound ,(mosaic-resource-file-name index))))
                (when (soundp s)
                  (close-sound s)))
              ;; trigger garbage collection after each block
              ;;(mosaic-close-all-temp-files)
              ;;(forget-all-region-properties)
              (gc)
              )
            results)
      (cl-incf index))
    ;; keep track of play column for looper
    (dolist (form forms)
      (let ((play-p (or (plist-get (cdr form) :play) 'false)))
        (push (eq 'true play-p) play-column)))
    (setf mosaic-chopper-play-column (nreverse play-column))
    ;; return scheme
    (nreverse results)))

(cl-defmethod mosaic-scheme-from-sheet ((sheet mosaic-chopper))
  (mosaic-scheme-create-process-forms
   (let ((rows (mapcar (lambda (row)
                         (delq nil (mapcar 'mosaic-scheme-filter-value row)))
                       (mosaic-scheme-collect-cell-values sheet))))
     (let (results)
       (dolist (row rows)
         ;; don't include rows that had settings
         (unless (cl-every #'null row)
           (push row results)))
       (nreverse results)))))

(defun mosaic-resource-files (n)
  "Return N resource filenames, numbered 0 through N."
  (let (files)
    (dotimes (i n)
      (push (mosaic-resource-file-name i) files))
    (mapcar 'mosaic-session-file (nreverse files))))

(defun mosaic-notify-files (files)
  "Set up timers to notify the user when each of FILES is written."
  (lexical-let ((n (length files))
                (m 0)
                (buffer (current-buffer))
                (last-file (nth (1- (length files)) files))
                (old-header-line-format header-line-format))
    (dolist (file files)
      (lexical-let ((j m)
                    (f file))
        (mosaic-wait-for-file file
          (with-current-buffer buffer
            (setf header-line-format
                  (apply #'format `("SchemeMosaic: wrote output file (%d of %d) %s" ,(+ j 1) ,n ,f)))
            (when (string= file last-file)
              (run-at-time 10.0 nil (lambda ()
                                      (with-current-buffer buffer
                                        (setf header-line-format
                                              old-header-line-format))))))))
      (cl-incf m))))

(cl-defmethod mosaic-process-sheet ((sheet mosaic-chopper))
  (let ((scheme (mosaic-scheme-from-sheet sheet)))
    (mosaic-tell-scheme-file `(begin ,@scheme))
    (mosaic-resource-files (length scheme))))

(cl-defmethod mosaic-scheme-resource-count ((sheet mosaic-chopper))
  (length (mosaic-scheme-from-sheet sheet)))

(defun mosaic-ensure-sheet-exists (name)
  "Make sure a cell-sheet called NAME exists in the current session.
If it does not exist, it is copied from a template."
  (unless (file-exists-p (mosaic-session-file name))
    (copy-file (mosaic-project-file name)
               (mosaic-session-file name))))

(defun mosaic-find-sheet (name)
  "Return the cell-sheet object named NAME."
  (mosaic-ensure-sheet-exists name)
  (with-current-buffer (save-window-excursion
                         (find-file (mosaic-session-file name))
                         (current-buffer))
    cell-current-sheet))

(defun mosaic-sheet-name (sheet)
  "Return the name of the sheet SHEET, without any file extension."
  (let ((file (buffer-file-name (cell-sheet-buffer sheet))))
    (if file
        (file-name-sans-extension (file-name-nondirectory file))
      "sequence")))

(defun mosaic-engine-remove-track ()
  (interactive)
  (mosaic-remove-track mosaic-engine-object
                       (completing-read "Remove track: "
                                        (mapcar (lambda (track)
                                                  (slot-value track 'name))
                                                (slot-value mosaic-engine-object 'tracks))))
  (speedbar-refresh))

(defun mosaic-render-chopper ()
  "Render the current chopper to WAV via Snd/Scheme."
  (interactive)
  (mosaic-kill-thunks)
  (mosaic-clear-timestamps)
  (let ((sheet cell-current-sheet))
    (with-current-buffer (slot-value sheet 'buffer)
      (cell-sheet-apply-settings sheet)
      (setf mosaic-engine-object (make-instance 'mosaic-engine))
      (mosaic-tell-session-directory mosaic-selected-session-directory)
      (mosaic-tell-tempo)
      ;; (mosaic-clear-resources)
      (let* ((files (mosaic-process-sheet sheet))
             (last-file (nth (1- (length files)) files)))
        ;;(mosaic-notify-files files)
        ;;(mosaic-show-scheme)
        (setf mosaic-looper-output-files (mapcar #'mosaic-session-file files))
        (lexical-let ((start-time (time-to-seconds (current-time))))
          (mosaic-wait-for-file (mosaic-timestamp-file)
            (let* ((end-time (time-to-seconds (current-time)))
                   (duration (- end-time start-time))
                   (text (format "Mosaic: Finished rendering chopper in %s."
                                 (format-seconds "%h hours, %m minutes, %s seconds" duration))))
              (save-window-excursion
                (ignore-errors (mosaic-notify-sound))
                (ignore-errors (mosaic-notify-popup text))
                (mosaic-clear-timestamps)
                (mosaic-create-resource-tracks mosaic-engine-object (length files) mosaic-chopper-play-column)
                (mosaic-turn-on-speedbar)
                ;;
                (mosaic-close-all-sounds)
                ;; (mapc #'mosaic-open-sound files)
                ;;(mosaic-show-scheme-dwim)
                (mosaic-set-status-icon :ok)
                (mosaic-update-header-line-in-all-sheets)
                ;;(run-at-time 1.0 (lambda () (unless mosaic-suppress-snd-gui-p (mosaic-show-scheme-gui))))
                ;;
                (let ((sheet2 (mosaic-find-sheet mosaic-looper-file)))
                  (with-current-buffer (cell-sheet-buffer sheet2)
                    (mosaic-populate-looper*)))))))))))

(provide 'chopper)
;;; chopper.el ends here
