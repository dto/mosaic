;;; beat.el --- rhythm tools                         -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Beat slicer wizard

(defclass mosaic-beat-slicer-wizard (mosaic-wizard)
  ((wizard-title :initform "Beat slicer wizard")
   (wizard-description :initform
"This wizard helps you slice a drum loop on the beats. Choose a
sensitivity value and click the `Rescan' button to update the
graph. When you're ready, hit the `Apply' button to create a
chopper sheet.")
   (net-file :initform "~/mosaic/percussion-3.nn"
             :custom file
             :initarg :net-file
             :label "Neural network file"
             :documentation
"Use this neural network to detect beats. For real percussion, 
use percussion-3.nn; for beatboxing, try bb-3.nn")
   (play :initform nil :initarg :play
           :label "Play sound file"
           :custom (push-button :format "%[ Play %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-play)))
           :documentation "Click this button to play.")
   (rescan :initform nil :initarg :rescan
           :label "Rescan for beats"
           :custom (push-button :format "%[ Rescan %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-rescan)))
           :documentation "Click this button to rescan.")
   (threshold :initform 10
              :custom number
              :initarg :threshold
              :label "Energy threshold"
              :documentation
              "Higher numbers will cause fewer beats to be detected.")
   (skip-amount :initform 4096
                :custom integer
                :initarg :skip-amount
                :label "Skip N samples after detecting a beat."
              :documentation
              "Number of audio samples to skip after detecting a beat.
If this is too low, you may get redundant beats.")
   (input-file :initform ""
               :initarg :input-file
               :custom file
               :label "Input file"
               :documentation "The file to slice.")
   (scale-amount :initform 1
                :custom number
                :initarg :scale-amount
                :label "Amount to scale beats by.")
   (quantize :initform 16
                :custom (choice number symbol)
                :initarg :quantize
                :label "Amount to quantize beats by."
                :documentation
"If this is the symbol `false', then don't quantize.")
   (slice-to-files :initform nil :initarg :slice-to-files
                   :label "Slice to files"
                   :custom (push-button :format "%[ Slice %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-slice-to-files)))
                   :documentation "Click this button to slice to files.")
   (scale-to-file :initform nil :initarg :scale-to-file
                  :label "Scale tempo"
                  :custom (push-button :format "%[ Scale %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-scale-to-file)))
                  :documentation "Click this button to scale to a file.")
   (resynth-to-file :initform nil :initarg :resynth-to-file
                  :label "Resynthesize with database matches"
                  :custom (push-button :format "%[ Resynth %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-resynth-to-file)))
                  :documentation "Click this button to resynth to a file.")
   (resynth-database :initform "~/sessions/mysession.mosaic" :initarg :resynth-database
                     :custom file
                     :label "Using database"
                     :documentation "Resynth matches will be pulled from this directory.")
   (play-output :initform nil :initarg :play-output
                :label "Play output file"
                :custom (push-button :format "%[ Play output %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-play-processed)))
              :documentation "Play the beat rendered by the `Scale to file' button.")
   (stretch-tails :initform t
                  :custom checkbox
                  :label "Stretch tails when padding?"
                  :documentation " Check to stretch decay tails when padding.")
   (beat-start :initform 0
               :custom integer
               :label "Offset number of one beat's beginning"
               :documentation "Which offset (red line) is at the start of the beat.")
   (beat-end :initform 1
             :custom integer
             :label "Offset number of one beat's end"
             :documentation "Which offset (red line) is at the end of the beat.")
   (kick-samples :initform nil :initarm :kick-samples
                 :label "Kick samples"
                 :custom (repeat file)
                 :documentation "List of kick samples.")
   (snare-samples :initform nil :initarm :snare-samples
                  :label "Snare samples"
                  :custom (repeat file)
                  :documentation "List of snare samples.")
   (cymbal-samples :initform nil :initarm :cymbal-samples
                   :label "Cymbal samples"
                   :custom (repeat file)
                 :documentation "List of cymbal samples.")
   (guess-bpm :initform nil :initarg :guess-bpm
              :label "Guess BPM"
              :custom (push-button :format "%[ Guess %]\n%d" :action (lambda (a b) (mosaic-beat-slicer-guess-bpm)))
              :documentation "Guess BPM based on BEAT-START and BEAT-END.")))

(cl-defmethod eieio-customize-object :after ((wizard mosaic-beat-slicer-wizard))
  (let ((file (mosaic-offset-image-file))
        (inhibit-read-only t))
    (when (file-exists-p file)
      (save-excursion
        (goto-char (point-min))
        (insert-image (create-image file))
        (insert "\n")))))

(defun mosaic-beat-slicer-play ()
  (mosaic-play-sound-asynchronously (slot-value mosaic-current-wizard 'input-file)))

(defun mosaic-beat-slicer-play-processed ()
  (mosaic-play-sound-asynchronously (mosaic-output-file)))

(defun mosaic-beat-slicer-guess-bpm ()
  (mosaic-wizard-apply-settings)
  (with-slots (threshold input-file net-file skip-amount scale-amount beat-start beat-end) mosaic-current-wizard
    (mosaic-delete-return-file)
    (mosaic-tell-scheme-file
     `(begin (set! *mosaic-beat-energy-threshold* ,threshold)
             (mosaic-tell-offsets (session-file ,input-file)
                                  ,skip-amount
                                  ,(expand-file-name net-file))))
    (mosaic-sit-for-scheme offsets
      (let* ((start-sample (nth beat-start offsets))
             (end-sample (nth beat-end offsets))
             (length-in-samples (- end-sample start-sample))
             (seconds-per-beat (/ length-in-samples 44100.0))
             (beats-per-second (/ 1.0 seconds-per-beat))
             (beats-per-minute (* 60.0 beats-per-second)))
        (setf mosaic-found-tempo beats-per-minute)
        (message "Mosaic: Guessed tempo is %f BPM, scale-amount produces %f BPM"
                 beats-per-minute
                 (* beats-per-minute (/ 1.0 scale-amount)))
        beats-per-minute))))

(defun mosaic-offset-image-file ()
  (mosaic-session-file ".offset.png"))

(cl-defun mosaic-graph-offsets (file &optional (threshold 10) (skip-amount 4096) net-file)
  (mosaic-delete-return-file)
  (mosaic-tell-scheme `(begin (set! *mosaic-beat-energy-threshold* ,threshold)
                              (mosaic-tell-offsets (session-file ,file) ,skip-amount ,(expand-file-name net-file))))
  (mosaic-sit-for-scheme offsets
    (assert (consp offsets))
    (assert (cl-every #'integerp offsets))
    (mosaic-find-sound-length file)
    (mosaic-sit-for-scheme len
       (mosaic-tell-scheme-file `(mosaic-tell-labels (session-file ,file) ',offsets))
       (mosaic-sit-for-scheme labels
         (mosaic-render-waveform :input-file file
                                 :output-file (mosaic-offset-image-file)
                                 :width mosaic-waveform-zoomed-width
                                 :height mosaic-waveform-zoomed-height
                                 :offsets offsets
                                 :labels labels
                                 :data-length len)
         (clear-image-cache)))))

(defun mosaic-beat-slicer-rescan ()
  (mosaic-wizard-apply-settings)
  (message "Mosaic: Rescanning for beats to slice...")
  (with-slots (threshold input-file skip-amount net-file) mosaic-current-wizard
    (mosaic-graph-offsets input-file threshold skip-amount net-file))
    ;; (let ((new-wizard (make-instance 'mosaic-beat-slicer-wizard
    ;;                                  :threshold threshold
    ;;                                  :input-file input-file
    ;;                                  :skip-amount skip-amount)))
    ;;   (setf mosaic-current-wizard new-wizard)
    ;;   (eieio-customize-object new-wizard)))
  (message "Mosaic: Rescanning for beats to slice... Done."))

(defun mosaic-beat-slicer-slice-to-files ()
  (interactive)
  (message "Mosaic: Slicing beats...")
  (mosaic-wizard-apply-settings)
  (with-slots (threshold input-file net-file skip-amount scale-amount) mosaic-current-wizard
    (mosaic-delete-return-file)
    (mosaic-tell-scheme
     `(begin (set! *mosaic-beat-energy-threshold* ,threshold)
             (mosaic-tell-offsets (session-file ,input-file) ,skip-amount ,net-file)))
    (mosaic-sit-for-scheme offsets
      (mosaic-tell-scheme-file
       `(mosaic-slice-to-files (open-sound (session-file ,input-file))
                               ',offsets
                               ,input-file))))
  (message "Mosaic: Slicing beats... Done."))

(defun mosaic-beat-slicer-scale-to-file ()
  (message "Mosaic: Scaling beat to file...")
  (mosaic-wizard-apply-settings)
  (with-slots (threshold input-file skip-amount scale-amount stretch-tails) mosaic-current-wizard
    (mosaic-tell-scheme-file
     `(let ((beats (mosaic-scale-beats (session-file ,input-file) ,scale-amount ,threshold ,skip-amount ,stretch-tails)))
        (select-sound (merge-sound-as-vectors (new-sound :channels 2) beats))
        (save-sound-as ,(mosaic-output-file) (selected-sound)
                       :header-type mus-riff
                       :sample-type mus-lshort))))
  (message "Mosaic: Scaling beat to file... Done."))

(defun mosaic-beat-slicer-resynth-to-file ()
  (message "Mosaic: Resynthesizing beat to file...")
  (mosaic-wizard-apply-settings)
  (with-slots (threshold kick-samples snare-samples cymbal-samples quantize
                         input-file skip-amount scale-amount stretch-tails net-file
                         resynth-database) mosaic-current-wizard
    (mosaic-delete-return-file)
    (mosaic-tell-scheme
     `(begin (set! *mosaic-beat-energy-threshold* ,threshold)
             (mosaic-tell-offsets (session-file ,input-file) ,skip-amount ,net-file)))
    (mosaic-sit-for-scheme offsets
                           (if kick-samples
                               (mosaic-tell-scheme-file `(mosaic-beatbox-resynth-with-samples
                                                          (session-file ,input-file)
                                                          ',offsets
                                                          ,stretch-tails
                                                          ,(expand-file-name net-file)
                                                          ',kick-samples
                                                          ',snare-samples
                                                          ',cymbal-samples
                                                          ,quantize))
                             (mosaic-tell-scheme-file `(mosaic-beatbox-resynth
                                                        (session-file ,input-file)
                                                        ',offsets
                                                        ,(expand-file-name resynth-database)
                                                        ,stretch-tails
                                                        ,(expand-file-name net-file)
                                                        ,quantize
                                                        ))))))

(defun mosaic-show-beat-slicer-wizard (&optional input-file)
  (interactive)
  (mosaic-select-percussion-3)
  (let ((input-file (or input-file (when (eq major-mode 'dired-mode)
                                     (car-safe (dired-get-marked-files))))))
    (when input-file (setf mosaic-marked-files (list input-file)))
    (setf mosaic-current-wizard
          (make-instance 'mosaic-beat-slicer-wizard
                         :input-file (if input-file (file-name-nondirectory input-file) "")))
    (eieio-customize-object mosaic-current-wizard)
    (mosaic-beat-slicer-rescan)))

(cl-defmethod eieio-done-customizing :after ((wizard mosaic-beat-slicer-wizard))
  (with-slots (threshold input-file skip-amount scale-amount stretch-tails) wizard
    (let* ((old-tempo (mosaic-beat-slicer-guess-bpm))
           (new-tempo (* old-tempo (/ 1.0 scale-amount))))
      (let (rows)
        (push (list "beat"
                    ":play" "true"
                    ":input-file" (prin1-to-string input-file)
                    ":threshold" (prin1-to-string threshold)
                    ":skip-amount" (prin1-to-string skip-amount)
                    ":scale-amount" (prin1-to-string scale-amount)
                    ":repeat" (prin1-to-string 4)
                    ":stretch-tails" (prin1-to-string stretch-tails))
              rows)
        (push (list (format "[mosaic-beats-per-minute %s]" (prin1-to-string new-tempo))) rows)
        (push (list (format "[mosaic-slice-size %s]" (prin1-to-string 'quarter-note))) rows)
        (let ((buffer (get-buffer-create "*chopper from wizard*")))
          (switch-to-buffer buffer)
          (cell-mode-with-class 8 16 'mosaic-chopper)
          (cell-copy-cells-from-sexps (nreverse rows))
          (cell-sheet-move-bob)
          (cell-sheet-paste)
          (dolist (cell (cell-collect-cells cell-current-sheet))
            (mosaic-fontify-cell cell))
          (clear-image-cache)
          (cell-sheet-update)
          (cell-sheet-after-open-hook cell-current-sheet)
          (cell-mode))))))

;;; Neural network wizard

(defclass mosaic-nn-wizard (mosaic-wizard)
  ((wizard-title :initform "Neural network beat jam wizard")
   (wizard-description :initform "Description goes here.")
   (net-file :initform "~/mosaic/jam-16.nn"
             :custom (file :must-match t)
             :initarg :net-file
             :label "Neural network file"
             :documentation
             "Use this neural network to synthesize beats.")
   (net-size :initform 16
             :custom number
             :initarg :net-size
             :label "Size of output; 16 or 32 beats.")
   (resynth-database :initform "~/sessions/percussion-3.mosaic" :initarg :resynth-database
                     :custom file
                     :label "Using database"
                     :documentation "Resynth matches will be pulled from this directory.
If you specify sample files instead, this setting is ignored.")
   (tempo :initform 120.0
          :custom number
          :label "Tempo"
          :documentation "Synthesize at this tempo.")
   (squelch :initform 0.92
          :custom number
          :label "Squelch factor (0.0-1.0)"
          :documentation "Only play beats higher than this threshold.
Higher number means less-busy beats.")
   (iterations :initform 4
          :custom number
          :label "Compose this many loops.")
   (resynth-to-file :initform nil :initarg :resynth-to-file
                  :label "Resynthesize with database matches (or files, if given)"
                  :custom (push-button :format "%[ Resynth %]\n%d" :action (lambda (a b) (mosaic-nn-resynth-to-file)))
                  :documentation "Click this button to resynth to a file.")
   (play-output :initform nil :initarg :play-output
                :label "Play output file"
                :custom (push-button :format "%[ Play output %]\n%d" :action (lambda (a b) (mosaic-nn-play-processed)))
                :documentation "Play the beat rendered by the `Resynth' button.")
   (kick-samples :initform nil :initarm :kick-samples
                 :label "Kick samples"
                 :custom (repeat file)
                 :documentation "List of kick samples.")
   (snare-samples :initform nil :initarm :snare-samples
                 :label "Snare samples"
                 :custom (repeat file)
                 :documentation "List of snare samples.")
   (cymbal-samples :initform nil :initarm :cymbal-samples
                 :label "Cymbal samples"
                 :custom (repeat file)
                 :documentation "List of cymbal samples.")
   (export-hydrogen :initform nil :initarg :export-hydrogen
                    :label "Export patterns to Hydrogen"
                    :custom (push-button :format "%[ Export to Hydrogen %]\n%d" :action (lambda (a b) (mosaic-nn-export-hydrogen)))
                    :documentation "EXPERIMENTAL: Export the rendered beat to Hydrogen.")
   (export-lmms :initform nil :initarg :export-lmms
                    :label "Export patterns to Lmms"
                    :custom (push-button :format "%[ Export to LMMS %]\n%d" :action (lambda (a b) (mosaic-nn-export-lmms)))
                :documentation "EXPERIMENTAL: Export the rendered beat to LMMS.
Note that this will encode absolute file paths into the resulting h2song file.")))

;; see hydrogen.el for implementation of the export buttons.

(defun mosaic-nn-play-processed ()
  (mosaic-wizard-apply-settings)
  (mosaic-play-sound-asynchronously (mosaic-output-file)))

(defun mosaic-nn-resynth-to-file ()
  (mosaic-wizard-apply-settings)
  (mosaic-clean-up-temporary-files)
  (with-slots (net-file net-size squelch tempo iterations resynth-database kick-samples snare-samples cymbal-samples) mosaic-current-wizard
    (mosaic-tell-scheme-file `(begin
                               (mosaic-select-net (mosaic-read-net ,net-file))
                               (mosaic-load-database ,(expand-file-name resynth-database))
                               (set! (mosaic-output-file) (session-file "mosaic-output.wav"))
                               (jam-16/resynth-multiple (jam-16/features->phrase (test-example (jam-16/noise-1 ,net-size)))
                                                        ,tempo
                                                        ,iterations
                                                        ',kick-samples ',snare-samples ',cymbal-samples
                                                        ,squelch)
                               (close-all-sounds)
                               (mosaic-close-all-temp-files)))))

(defun mosaic-show-nn-wizard ()
  (interactive)
  (let ((net-file (when (eq major-mode 'dired-mode)
                    (car-safe (dired-get-marked-files)))))
    (when net-file (setf mosaic-marked-files (list net-file)))
    (setf mosaic-current-wizard
          (make-instance 'mosaic-nn-wizard
                         :net-file (if net-file (file-name-nondirectory net-file) "")))
    (eieio-customize-object mosaic-current-wizard)))

(provide 'beat)
;;; beat.el ends here
