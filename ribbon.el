;;; mosaic-ribbon.el --- clickable Unicode icon bar  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Unicode icons.

(defvar mosaic-icon-characters
  '(:checkbox-on ?☑
    :checkbox-off ?☐
    :ok ?✔
    :render ?✍
    :radio-on ?◉
    :radio-off ?◌
    :increase-font-size ?🗚
    :decrease-font-size ?🗛
    :stereo ?⚇
    :mono ?⚆
    :window-new-below ?╏
    :window-new-right ?╍
    :shuffle ?🔀
    :play-or-pause ?⏯
    :stop ?◾
    :looping-play ?🔁
    :looping-solo ?🔂
    :rewind ?⏪ 
    :rewind-to-beginning ?⏮
    :fast-forward ?⏩
    :fast-forward-to-end ?⏭
    :winner-undo ?⎌
    :progress-1 ?⧖
    :progress-2 ?⧗
    :bell ?🔔
    :speaker-0 ?🔇
    :speaker-1 ?🔈
    :speaker-2 ?🔉
    :speaker-3 ?🔊
    :tracker ?🎹
    :looper ?🗘
    :knobs ?🎛
    :faders ?🎚
    :cancel ?🗙
    :eject ?⏏
    :arrow-double-up ?⏫
    :arrow-double-down ?⏬
    :new-session ?⛶
    :save-session ?🖫
    :key ?🔑
    :tools ?🔧
    :settings ?⚙
    :lock-closed ?🔒
    :lock-open ?🔓
    :help ?📚
    :copyright ?©
    :information ?🛈
    :hyper ?❖
    :error ?💣
    :option ?⌘
    :headphones ?🎧
    :microphone ?🎤
    :chopper ?💿
    :share ?🖧
    :boombox ?📾
    :file ?🗋
    :files ?🗍
    :attach ?📎
    :folder-closed ?📁
    :folder-open ?📂
    :scale ?🎼
    :stomper ?🥁
    :syntax ?🗟
    :violin ?🎻
    :editor ?🗠
    :graph-bar ?📊
    :keyboard-midi ?🎘
    :keyboard-computer ?🖮
    :bear ?🐻
    :notes ?📝
    :home ?🏠
    :browser ?🌐
    :window-minimize ?🗕
    :window-maximize ?🗖
    :window-split ?🗗
    :window-clear ?⎚
    :window-new ?🖵
    :triangle-filled-up ?▲
    :triangle-filled-down ?▼
    :triangle-filled-left ?◀
    :triangle-filled-right ?▶
    :triangle-empty-left ?◁
    :triangle-empty-right ?▷
    :triangle-empty-up ?△
    :triangle-empty-down ?▽
    ))

(defun-memo mosaic-icon-character (icon-name) (:key 'identity)
  (cl-getf mosaic-icon-characters icon-name))

(defun-memo mosaic-icon-string (icon-name) (:key 'identity)
  (make-string 1 (mosaic-icon-character icon-name)))

;;; Fancy icons.

(defvar mosaic-fancy-icon-names '(:mosaic))

(defun mosaic-fancy-icon-file (icon-name)
  (mosaic-project-file (concat
                        ;; skip initial colon in keyword's name
                        (substring (symbol-name icon-name) 1)
                        ".png")))

(defun mosaic-fancy-icon-image (icon-name)
  (create-image (mosaic-fancy-icon-file icon-name)))

;;; Icon wrapper classes.

(defface mosaic-ribbon-face '((t (:foreground "gray20" :background "gray90")))
  "Face for mosaic ribbon." :group 'mosaic)

(defclass mosaic-icon ()
  ((text :initform "" :initarg :text :accessor mosaic-icon-text)
   (name :initform :hyper :initarg :name :accessor mosaic-icon-name)
   (data :initform nil :initarg :data :accessor mosaic-icon-data)
   (label :initform "" :initarg :label :accessor mosaic-icon-label)
   (face :initform 'mosaic-ribbon-face :initarg :face :accessor mosaic-icon-face)))

(cl-defmethod mosaic-icon-description ((icon mosaic-icon))
  (substring (symbol-name (mosaic-icon-name icon)) 1))

(cl-defmethod mosaic-insert-icon ((icon mosaic-icon))
  (insert (mosaic-icon-text icon)))

(cl-defmethod mosaic-activate-cell ((c cell)) nil)

(cl-defmethod mosaic-activate-icon ((icon mosaic-icon)) nil)
  ;; (with-slots (data) icon
  ;;   (when data
  ;;     (mosaic-activate-cell data))))

(defclass mosaic-plain-icon (mosaic-icon) ())
(defclass mosaic-fancy-icon (mosaic-icon) ())

(cl-defmethod initialize-instance :after ((icon mosaic-plain-icon) &rest slots)
  (with-slots (text name label face) icon
    (let ((map (make-sparse-keymap)))
      (lexical-let ((icon* icon))
        (let ((closure (lambda (event) (interactive "e")
                         (select-window (posn-window (event-start event)))
                         (mosaic-activate-icon icon*))))
          (define-key map [mouse-1] closure)
          (define-key map [header-line mouse-1] closure))
        ;;(define-key map [(return)] closure)))
        (setf text (propertize (mosaic-icon-string name)
                               'mouse-face 'highlight
                               'face face
                               ;; 'read-only t
                               'help-echo (mosaic-icon-description icon)
                               'keymap map))))))

(cl-defmethod initialize-instance :after ((icon mosaic-fancy-icon) &rest slots)
  (with-slots (text name label face) icon
    (let ((map (make-sparse-keymap)))
      (lexical-let ((icon* icon))
        (let ((closure (lambda () (interactive)
                         (mosaic-activate-icon icon*))))
          (define-key map [mouse-1] closure)
          (define-key map [header-line mouse-1] closure)))
        ;; (define-key map [(return)] closure)))
      (setf text (propertize (substring (symbol-name name) 0)
                             'mouse-face 'highlight
                             'face face
                             ;; 'read-only t
                             'help-echo (mosaic-icon-description icon)
                             'keymap map
                             'display (cons 'image (mosaic-fancy-icon-image name)))))))

(defun mosaic-fancy-icon-name-p (name)
  (member name mosaic-fancy-icon-names))

(defun mosaic-make-icon (icon-name &optional data)
  (make-instance (mosaic-icon-class icon-name) :data data))

(defun mosaic-icon-class (icon-name)
  (intern (concat "mosaic-icon/"
                  (substring (symbol-name icon-name) 1))))

(cl-defmacro mosaic-define-icon (icon-name slot-defs)
  `(defclass ,(mosaic-icon-class icon-name)
     (,(if (mosaic-fancy-icon-name-p icon-name)
          'mosaic-fancy-icon
         'mosaic-plain-icon))
     ((name :initform ,icon-name)
      ,@slot-defs)))

(defun mosaic-plain-icon-names ()
  (cl-remove-if-not #'symbolp mosaic-icon-characters))

(defclass mosaic-cell (cell)
  ((icon :initform nil :initarg :icon :accessor mosaic-cell-icon)
   (icon-class :initform nil :initarg :icon-class :accessor mosaic-cell-icon-class)))

(cl-defmethod initialize-instance :after ((this-cell mosaic-cell) &rest slots)
  (with-slots (icon icon-class label) this-cell
    (setf icon (make-instance icon-class :data this-cell))
    (setf label (mosaic-icon-text icon))))

(cl-defmacro mosaic-define-icon-classes ()
  `(progn
     ,@(cl-loop for name in (append (mosaic-plain-icon-names)
                                    mosaic-fancy-icon-names)
                append `((mosaic-define-icon ,name ())
                         (defclass ,(intern (concat "mosaic-cell/" (substring (symbol-name name) 1)))
                           (mosaic-cell)
                           ((icon-class :initform ,(mosaic-icon-class name))))))))

(mosaic-define-icon-classes)

(defclass mosaic-button (cell-expression)
  ((icon-name :initform :trouble :initarg :icon-name :accessor mosaic-button-icon-name)
   (icon :initform :trouble :initarg :icon :accessor mosaic-button-icon)))

(cl-defmethod initialize-instance :after ((button mosaic-button) &rest slots)
  (with-slots (icon-name icon label) button
    (setf icon (mosaic-make-icon icon-name))
    (setf label (mosaic-icon-text icon))))

;;; Clickable header line.

(defun make-instance-or-nil (class-or-nil)
  (when class-or-nil
    (make-instance (mosaic-icon-class class-or-nil))))

(defun mosaic-header-line-items ()
  (mapcar #'make-instance-or-nil
          '(nil
            :stop
            :play-or-pause
            :looping-play
            :looping-solo
            nil
            :rewind-to-beginning
            :rewind
            :fast-forward
            :fast-forward-to-end
            nil
            :home
            :editor
            :chopper
            :stomper
            :tracker
            :looper
            :browser
            nil
            :render
            :save-session
            :folder-open
            :share
            :graph-bar
            :faders
            nil
            :triangle-filled-left
            :triangle-filled-right
            :triangle-filled-up
            :triangle-filled-down
            nil
            :files
            :help
            nil
            :window-new-below
            :window-new-right
            :window-maximize
            :cancel)))

(defmacro mosaic-define-icon-command (icon-name command-name)
  `(cl-defmethod mosaic-activate-icon ((icon ,(mosaic-icon-class icon-name)))
     (,command-name)))

(defmacro mosaic-define-icon-description (icon-name description)
  `(cl-defmethod mosaic-icon-description ((icon ,(mosaic-icon-class icon-name)))
     ,description))

(defun text-scale-increase* () (interactive) (text-scale-increase 1))
(defun text-scale-decrease* () (interactive) (text-scale-decrease 1))

(mosaic-define-icon-command :browser mosaic-visit-browser)
(mosaic-define-icon-command :cancel delete-window)
(mosaic-define-icon-command :chopper mosaic-visit-chopper)
(mosaic-define-icon-command :decrease-font-size text-scale-decrease*)
(mosaic-define-icon-command :editor mosaic-show-scheme-dwim)
(mosaic-define-icon-command :error mosaic-show-scheme)
(mosaic-define-icon-command :faders mosaic-mix-sound)
(mosaic-define-icon-command :files ibuffer)
(mosaic-define-icon-command :folder-open mosaic-visit-session-directory)
(mosaic-define-icon-command :graph-bar mosaic-mixdown)
(mosaic-define-icon-command :help mosaic-help-on-command)
(mosaic-define-icon-command :increase-font-size text-scale-increase*)
(mosaic-define-icon-command :keyboard-computer mosaic-define-octave-keys)
(mosaic-define-icon-command :keyboard-midi mosaic-enable-midi)
(mosaic-define-icon-command :looper mosaic-visit-looper)
(mosaic-define-icon-command :looping-play mosaic-start-playing)
(mosaic-define-icon-command :ok mosaic-show-scheme)
(mosaic-define-icon-command :play-or-pause mosaic-start-playing)
(mosaic-define-icon-command :progress-1 mosaic-show-scheme)
(mosaic-define-icon-command :progress-2 mosaic-show-scheme)
(mosaic-define-icon-command :render mosaic-render)
(mosaic-define-icon-command :save-session mosaic-save-session)
(mosaic-define-icon-command :settings mosaic-customize-global-settings)
(mosaic-define-icon-command :stomper mosaic-visit-stomper)
(mosaic-define-icon-command :stop mosaic-stop-playing)
(mosaic-define-icon-command :tracker mosaic-visit-tracker)
(mosaic-define-icon-command :triangle-filled-down cell-sheet-insert-row)
(mosaic-define-icon-command :triangle-filled-left cell-sheet-delete-column)
(mosaic-define-icon-command :triangle-filled-right cell-sheet-insert-column)
(mosaic-define-icon-command :triangle-filled-up cell-sheet-delete-row)
(mosaic-define-icon-command :window-clear mosaic-close-all-sounds)
(mosaic-define-icon-command :window-maximize delete-other-windows)
(mosaic-define-icon-command :window-minimize delete-window)
(mosaic-define-icon-command :window-new mosaic-new-sound)
(mosaic-define-icon-command :window-new-below split-window-below)
(mosaic-define-icon-command :window-new-right split-window-right)
(mosaic-define-icon-command :window-split split-window-right)
(mosaic-define-icon-command :winner-undo winner-undo)

(mosaic-define-icon-description :browser "Browser: find sounds locally or online")
(mosaic-define-icon-description :cancel "Delete this window")
(mosaic-define-icon-description :chopper "Chopper: chop, screw, and match beats")
(mosaic-define-icon-description :decrease-font-size "Decrease font size.")
(mosaic-define-icon-description :editor "Show Snd editor")
(mosaic-define-icon-description :error "<ERROR> Show Snd Scheme")
(mosaic-define-icon-description :faders "Mix output file into editor")
(mosaic-define-icon-description :files "Show buffer list")
(mosaic-define-icon-description :folder-open "Browse session directory")
(mosaic-define-icon-description :graph-bar "Mixdown in editor")
(mosaic-define-icon-description :help "Show help on command")
(mosaic-define-icon-description :home "Home")
(mosaic-define-icon-description :increase-font-size "Increase font size.")
(mosaic-define-icon-description :keyboard-computer "Enable F-key note input")
(mosaic-define-icon-description :keyboard-midi "Enable MIDI note input")
(mosaic-define-icon-description :looper "Looper: loop beats, melodies, and other sounds")
(mosaic-define-icon-description :looping-play "Play looping")
(mosaic-define-icon-description :ok "<OK> Show Snd Scheme")
(mosaic-define-icon-description :play-or-pause "Play or pause")
(mosaic-define-icon-description :progress-1 "<PROGRESS-1> Show Snd Scheme")
(mosaic-define-icon-description :progress-2 "<PROGRESS-2> Show Snd Scheme")
(mosaic-define-icon-description :render "Render current sheet")
(mosaic-define-icon-description :save-session "Save current session")
(mosaic-define-icon-description :settings "Customize global settings")
(mosaic-define-icon-description :stomper "Stomper: beat sequencer")
(mosaic-define-icon-description :stop "Stop playing")
(mosaic-define-icon-description :tracker "Tracker: synth sequencer")
(mosaic-define-icon-description :triangle-filled-down "Insert row")
(mosaic-define-icon-description :triangle-filled-left "Delete column")
(mosaic-define-icon-description :triangle-filled-right "Insert column")
(mosaic-define-icon-description :triangle-filled-up "Delete row")
(mosaic-define-icon-description :window-clear "Close all sounds")
(mosaic-define-icon-description :window-maximize "Delete other windows")
(mosaic-define-icon-description :window-minimize "Delete this window")
(mosaic-define-icon-description :window-new "Create new sound")
(mosaic-define-icon-description :window-new-below "New window below")
(mosaic-define-icon-description :window-new-right "New window on right")
(mosaic-define-icon-description :window-split "Split window")
(mosaic-define-icon-description :winner-undo "Back to previous window configuration")

(defun mosaic-visit-session-directory ()
  "Visit the current session directory in Dired."
  (interactive)
  (dired mosaic-selected-session-directory))

(cl-defmethod mosaic-activate-icon ((icon mosaic-icon/home)) nil)
(cl-defmethod mosaic-activate-icon ((icon mosaic-icon/share)) nil)

(defvar mosaic-header-line-cache nil)

(defvar mosaic-status-icon-name :ok)

(defun mosaic-set-status-icon (icon-name)
  (cl-assert (member icon-name '(:ok :error :progress-1 :progress-2)))
  (setf mosaic-status-icon-name icon-name))

(defun mosaic-header-line (&optional sheet)
  (let ((sheet (or sheet cell-current-sheet)))
    (when sheet
      (with-temp-buffer
        (insert " ")
        (insert (mosaic-icon-text (make-instance (mosaic-icon-class mosaic-status-icon-name))))
        (insert " ")
        (dolist (item (mosaic-header-line-items))
          (if (null item)
              (insert "  ")
            (progn (insert (mosaic-icon-text item))
                   (insert " "))))
        (buffer-substring (point-min) (point-max))))))

(defun mosaic-find-header-line ()
  (if (null mosaic-header-line-cache)
      (setf mosaic-header-line-cache (mosaic-header-line))
    mosaic-header-line-cache))

(defcustom mosaic-ribbon-location 'header-line-format
  "The symbol to set when insinuating the ribbon.
Valid choices are `header-line-format', `mode-line-format', and
`tab-line-format'."
  :tag "Name of ribbon location"
  :group 'mosaic
  :type 'symbol)

(defun mosaic-click-tab-line (click)
  (interactive "e")
  (let ((oframe (selected-frame))
        (frame (window-frame (posn-window (event-start click)))))
    (select-window (posn-window (event-start click)))
    (let ((object (posn-object (event-start click))))
      (when object
        (let (keymap)
          (cl-destructuring-bind (string . pos) object
            (with-temp-buffer
              (insert string)
              (setf keymap (get-text-property pos 'keymap string))))
          (assert (keymapp keymap))
          (let ((closure (lookup-key keymap [mouse-1])))
            (funcall closure click)))))))

(defun mosaic-insinuate-header-line ()
  (when (eq major-mode 'cell-mode)
    (set mosaic-ribbon-location '(:eval (mosaic-find-header-line)))
    (when (eq 'tab-line-format mosaic-ribbon-location)
      (local-set-key [tab-line mouse-1] 'mosaic-click-tab-line)
      (setf header-line-format (mosaic-nano-header-line (current-buffer))))))

(add-hook 'cell-mode-hook 'mosaic-insinuate-header-line)
(add-hook 'after-save-hook 'mosaic-insinuate-header-line)

(defun mosaic-update-header-line (&optional status)
  (interactive)
  (when status
    (setf mosaic-status-icon-name status))
  (setf mosaic-header-line-cache (mosaic-header-line)))

(defun mosaic-nano-header-line (buffer)
    (let ((buffer-name (format-mode-line "%b" nil nil buffer))
          (mode-name   (format-mode-line "%m" nil nil buffer))
          (class-name  (prin1-to-string (eieio-object-class cell-current-sheet)))
          (branch      (vc-branch))
          (position    (format-mode-line "%l:%c")))
      (nano-modeline-compose (nano-modeline-status)
                             buffer-name
                             (concat "(" mode-name
                                     (if branch (concat ", "
                                            (propertize branch 'face 'italic)))
                                     ")"
                                     " [" class-name "] "
                                     )
                             position)))

(provide 'mosaic-ribbon)
;;; mosaic-ribbon.el ends here
