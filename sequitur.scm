;;; sequitur.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Structural analysis.

;; What follows is an implementation of the Nevill-Manning "Sequitur"
;; process. See http://www.sequitur.info/ for more information on
;; this amazing algorithm.

;; Sequitur analyzes its input (a sequence of discrete symbols) and
;; recovers a deterministic context-free grammar whose rules
;; re-generate the original sequence. This grammar's production rules
;; capture all the nested repetitions and branching found during
;; scanning. Automated recovery of branching structure requires
;; optional "oblivious reparsing" (see Sequitur PhD thesis). This
;; option is not yet implemented.

;; Sequitur is not guaranteed to produce an optimal grammar. My
;; implementation is not linear-time but should suffice for sequences
;; of musically usable length.

;; Later in this program we will apply Sequitur to analysis and
;; resynthesis of sequences of regions, using these regions' property
;; association lists as the discrete symbols of the sequence to be
;; analyzed, and then matching regions together based on similarity of
;; properties.

(define *symbols* (make-hash-table 32 equal?))
(define *sequence* ())
(define *alphabet* '(A B C D E F G H I J K L M N O P Q R S T U V W X Y
Z A* B* C* D* E* F* G* H* I* J* K* L* M* N* O* P* Q* R* S* T* U* V* W*
X* Y* Z* A** B** C** D** E** F** G** H** I** J** K** L** M** N** O**
P** Q** R** S** T** U** V** W** X** Y** Z** A*** B*** C*** D*** E***
F*** G*** H*** I*** J*** K*** L*** M*** N*** O*** P*** Q*** R*** S***
T*** U*** V*** W*** X*** Y*** Z***))

(define *names* *alphabet*)

(define^ (terminal? symbol)
  (and (not (member symbol *alphabet*))
       (not (gensym? symbol))))

(define^ (nonterminal? symbol)
  (not (terminal? symbol)))

(define^ (append-symbol s (seq *sequence*))
  (append seq (list s)))

(define^ (last-symbol (seq *sequence*))
  (when (not (null? seq))
    (list-ref seq (- (length seq) 1))))

(define^ (second-last-symbol (seq *sequence*))
  (when (and (not (null? seq))
             (>= (length seq) 2))
    (list-ref seq (- (length seq) 2))))

(define^ (delete-last-symbol!)
  (set! *sequence* 
    (subsequence *sequence* 0 (max 1 (- (length *sequence*) 1)))))

(define^ (pair-symbol a b)
  (list a b))

(define^ (find-pair seq a b)
  (cond ((null? seq) #f)
        ((null? (cdr seq)) #f)
        ((and (equal? a (car seq))
              (equal? b (cadr seq)))
         seq)
        (#t (find-pair (cdr seq) a b))))

(define^ (replace-pair! seq a b c)
  (let ((cell (find-pair seq a b)))
    (when cell
      (set! (car cell) c)
      (set! (cdr cell) (cdr (cdr cell))))
    seq))

(define^ (pair-variable a b)
  (*symbols* (pair-symbol a b)))

(define^ (seen-pair? a b)
  (or (pair-expansion a b)
      (find-pair *sequence* a b)))

(define^ (make-variable-name)
  (if (null? *names*)
      (gensym)
      (let ((n (car *names*)))
        (set! *names* (cdr *names*))
        n)))

(define^ (record-pair! a b c)
  ;; record new variable name->expansion
  (set! (*symbols* c) (list a b))
  ;; store also expansion->name for easy lookup of seen pairs
  (set! (*symbols* (pair-symbol a b)) c)
  ;; replace previous occurrence in seq
  (set! *sequence* (replace-pair! *sequence* a b c))
  ;; replace other occurrences to enforce digram uniqueness.
  ;; this rescanning results in quadratic time.
  (map (lambda (entry)
         (let ((var (car entry))
               (expansion (cdr entry)))
           (when (and (list? expansion)
                      ;; don't clobber same var definition
                      (not (equal? var c)))
             (when (find-pair expansion a b)
               (set! (*symbols* var)
                 (replace-pair! expansion a b c))))))
       *symbols*)
  ;; return new variable name
  c)

(define^ (simple-expansion symbol)
  (let ((entry (*symbols* symbol)))
    (when (pair? entry)
      (cdr entry))))

(define^ (rewrite-expansion! expansion symbol definition)
  (let ((results (map (lambda (s)
                        (if (equal? s symbol)
                            definition
                            (list s)))
                      expansion)))
    (apply append results)))

(define^ (delete-rule! deleted-symbol)
  (let ((definition (*symbols* deleted-symbol)))
    (set! (*symbols* deleted-symbol) #f)
    (map (lambda (entry)
           (let ((sym (car entry))
                 (exp (cdr entry)))
             (when (list? exp)
               (set! (*symbols* sym)
                 (rewrite-expansion! exp deleted-symbol definition)))))
         *symbols*)
    ;; also replace var in sequence
    (set! *sequence* (rewrite-expansion! *sequence* deleted-symbol definition))))

(define^ (count-uses search-symbol)
  (let ((n 0))
    ;; check rules
    (map (lambda (entry)
           (let ((sym (car entry))
                 (expansion (cdr entry)))
             (when (list? expansion)
               (map (lambda (s)
                      (when (equal? s search-symbol)
                        (set! n (+ 1 n))))
                    expansion))))
         *symbols*)
    ;; also check current sequence
    (map (lambda (s)
           (when (equal? s search-symbol)
             (set! n (+ 1 n))))
         *sequence*)
    n))

(define^ (variables)
  (let ((keys ()))
    (map (lambda (entry)
           (when (symbol? (car entry))
             (set! keys (cons (car entry) keys))))
         *symbols*)
    keys))

(define^ (useful-rule? search-symbol)
  (and (nonterminal? search-symbol)
       (< 1 (count-uses search-symbol))))

(define^ (enforce-utility!)
  (map (lambda (k)
         (when (not (useful-rule? k))
           (delete-rule! k)))
       (variables)))

(define^ (prefixes symbol seq)
  (let ((ps ())
        (last #f))
    (do ((s seq (cdr s)))
        ((null? s)
         (remove-duplicates** (cons symbol ps)))
      (when (and last (equal? (car s) symbol))
        (set! ps (cons last ps)))
      (set! last (car s)))))

(define^ (suffixes symbol seq)
  (let ((ss ()))
    (do ((s seq (cdr s)))
        ((null? s)
         (remove-duplicates** (cons symbol ss)))
      (when (and (equal? (car s) symbol)
                 ;; not at end of string
                 (not (null? (cdr s))))
        ;; save next symbol 
        (set! ss (cons (cadr s) ss))))))

(define^ (all-suffixes symbol seq)
  (if (terminal? symbol)
      ;; every terminal symbol has itself as suffix
      (list symbol)
      (flatten
       (append (list (suffixes symbol *sequence*))
               ;; also collect suffixes from definitions of variables
               (map (lambda (entry)
                      (if (terminal? entry)
                          ()
                          (suffixes symbol (cdr entry))))
                    *symbols*)))))

(define^ (extend-left-maybe! sym)
  ;; eat common suffix of all SYM predecessors, if any.
  ;; this is not yet implemented.
  (display "{L}"))

(define^ (extend-right-maybe! sym)
  ;; eat common prefix of all SYM successors, if any.
  ;; this is not yet implemented.
  (display "{R}"))

(define^ (reparse! (seq *sequence*))
  (let ((entry (*symbols* (last-symbol seq))))
    (when (pair? entry)
      (reparse! (cdr entry)))))

(define^ (special-match* var input)
  (cons var (cdr input)))

(define +axiom-symbol+ 'AXIOM^)

(define^ (show-structure (grammar *symbols*))
  (format #t "\n::Sequitur output::\n")
  (map (lambda (entry)
         (when (symbol? (car entry))
           (format #t "~S -> ~S\n" (car entry) (cdr entry))))
       grammar)
  grammar)

(define^ (find-structure seq (reparse? #f) (show? #f))
  "Return a hash table of production rules characterizing the
structure found by Sequitur upon analyzing SEQUENCE. The argument
SHOW? means whether to display each step; this is used for debugging.
The parameter REPARSE? indicates whether to use oblivious reparsing,
but this is not yet implemented."
  (show-progress "struct")
  (set! *symbols* (make-hash-table))
  (set! *sequence* ())
  (set! *names* *alphabet*)
  (do ((input seq))
      ((null? input))
    (let ((A (last-symbol))
          (B (car input)))
      (cond
       ;; begin
       ((null? *sequence*)
        (set! *sequence* (subsequence input 0 2))
        (set! input (subsequence input 2)))
       ;; previously substituted
       ((pair-variable A B)
        (let ((var (pair-variable A B)))
          (delete-last-symbol!)
          ;; set up special match by pushing new var onto input
          (set! input (special-match* var input))))
       ;; not previously substituted 
       ((find-pair (subsequence *sequence* 0 (- (length *sequence*) 1)) A B)
        (delete-last-symbol!)
        (set! *sequence* (append-symbol (record-pair! A B
                                                      (make-variable-name))
                                        *sequence*))
        (set! input (cdr input)))
       ;; typical case, not a new pair
       (#t (set! *sequence*
             (append-symbol (car input)
                            *sequence*))
           (set! input (cdr input)))))
    (enforce-utility!)
    (when reparse?
      (extend-left-maybe! (last-symbol *sequence*))
      (extend-right-maybe! (second-last-symbol *sequence*))
      (reparse! *sequence*))
    (when show?
      (show-structure)))
  (set! (*symbols* +axiom-symbol+) *sequence*)
  ;; invert non-symbol entries
  (map (lambda (entry)
         (when (not (symbol? (car entry)))
           (set! (*symbols* (car entry)) #f)))
       *symbols*)
  (copy *symbols*))

(define^ (structure-rules structure)
  (map (lambda (entry) entry) structure))

;;; Resynthesis.

;; Converting Sequitur structure output into parametric L-system-like
;; generative grammars. Each rule is of the form: (S (((p1 v1) (p2 v2)
;; ...)  ((p1 v1) (p2 v2) ...)  ...)  Where the S are symbols and the
;; P,V are property/value pairs.  Each ((P V) (P V)...) property alist
;; is called a descriptor. By default each rule has just one
;; expansion, but more than one can be added and then dynamically
;; chosen by GENERATE-PHRASE.

(define^ (convert-rule simple-rule
                       (params-fn (lambda (s) (list s))))
  ;; make an alist entry.
  ;; the right hand side is rewritten as (P S) pairs
  (list (car simple-rule)
        (map (lambda (descriptor)
               (if (symbol? descriptor)
                   descriptor
                   (copy (params-fn descriptor))))
             (cdr simple-rule))))

(define^ (grammar-rule grammar symbol)
  (assoc* symbol grammar))

(define^ (rule-variable g) (car g))
(define^ (rule-expansions g)
  (if (pair? g)
      (cdr g)
      #f))

;; (define descriptor-parameters car)
;; (define descriptor-symbol cadr)

(define^ (descriptor? x)
  "Return #t if the argument X is a region descriptor."
  (and (pair? x)
       (pair? (car x))
       (symbol? (car (car x)))
       (member (car (car x)) *region-property-names*)))

(define^ (show-grammar grammar)
  "Display the rules of GRAMMAR."
  (format #t "\nSchemeMosaic: Sequitur grammar output\n")
  (map (lambda (entry)
         (when (and (symbol? (car entry))
                    (not (eq? +axiom-symbol+ (car entry))))
           (format #t "~S -> ~S\n" (car entry) (cdr entry))))
       grammar)
  (format #t "~W" (cdr (assoc +axiom-symbol+ grammar)))
  (format #t "---------------------------------------\n")
  grammar)

(define^ (show-structure (grammar *symbols*))
  (format #t "\n::Sequitur output::\n")
  (map (lambda (entry)
         (when (symbol? (car entry))
           (format #t "~S -> ~S\n" (car entry) (cdr entry))))
       grammar)
  grammar)

(define^ (find-axiom grammar)
  "Return the axiom sequence of GRAMMAR."
  (copy (car (rule-expansions
              (grammar-rule grammar +axiom-symbol+)))))

(define (structure->grammar structure)
  (map convert-rule (structure-rules structure)))

(define^ (find-grammar regions)
  "Return the grammar found by FIND-STRUCTURE when analyzing REGIONS."
  (structure->grammar (find-sound-structure regions)))

(define^ (region->descriptor r)
  "Default function for mapping a region to its descriptor. Here we
pre-cache spectra, pitch, and note properties by default when regions
are added to a synth."
  (remember-region-properties '(spectrum pitch pitches note notes centroid total-energy) r)
  (let ((properties (*region->properties* r)))
    (assert (list? properties))
    (descriptor-properties-only (list properties))))

(define^ (verify-all-terminals phrase)
  (map (lambda (p)
         (assert (descriptor? p)))
       phrase))

(define^ (generate-phrase grammar phrase)
  "Expand PHRASE using the rules in GRAMMAR."
  ;; Adapted from Peter Norvig's version. See also the notice at top
  ;; of this file.
  (cond ((descriptor? phrase)
         (list phrase))
        ((list? phrase)
         (apply append (map (lambda (p)
                              (generate-phrase grammar p))
                            phrase)))
        ((rule-expansions (grammar-rule grammar phrase))
         (generate-phrase grammar
                          ;; change this to read PROBABILITY values if present
                          (random-choose
                           (rule-expansions (grammar-rule grammar phrase)))))
        (#t (list phrase))))

;;; Sequitur tests

(define^ (sequitur-test-1 (reparse? #f) (show? #f))
  (find-structure '(a b c d b c a b c d)
                  reparse? show?))

(define^ (sequitur-test-2 (reparse? #f) (show? #f))
  (find-structure '(a b c d b c a b c d d c b a c b d c b a)
                  reparse? show?))

(define (sequitur-test-3)
  (map (lambda (entry)
         (display entry)
         (display #\newline))
       (let ((u '(u a b c d b c a b c d))
             (v '(v a b c d b c a b c d d c b a c b d c b a))
             (w '(w a a b c d e e g f g h))
             (x '(x e f g e f e f g h))
             (y '(y 1 2 3 4 5))
             (z '(z a a b c a a b c a d d b c e f g h)))
         (find-structure (flatten (list 0 u 1 v 2 w 3 x 4 y 5 z
                                        6 u 7 v 8 v 9 y 10 x 11 z))))))

(define^ (mosaic-postprocess-grammar grammar)
  "Replace region properties with their UUID's.
Otherwise the region data gets written to the output file along with
the contents of the hash table."
  (map (lambda (entry)
         (let ((sym (car entry))
               (expansion (cdr entry)))
           (cons sym
                 (map (lambda (item)
                        (if (list? item)
                            (cdr (assoc* 'uuid item))
                            item))
                      expansion))))
       grammar))

(define *mosaic-grammar* ())
