;;; percussion-3.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define (noisy-features features)
  (list->vector
   (map (lambda (f)
          (min 1.0 (+ f (random 0.0025))))
        features)))

(define *bb-cymbal-files*
  '("bb-cymbals.wav.slice-0.wav"
    "bb-cymbals.wav.slice-1.wav"
    "bb-cymbals.wav.slice-2.wav"
    "bb-cymbals.wav.slice-3.wav"
    "bb-cymbals.wav.slice-4.wav"
    "bb-cymbals.wav.slice-5.wav"
    "bb-cymbals.wav.slice-6.wav"
    "bb-cymbals.wav.slice-7.wav"))

(define *bb-kick-files*
  '("bb-kicks.wav.slice-0.wav"
    "bb-kicks.wav.slice-1.wav"
    "bb-kicks.wav.slice-2.wav"
    "bb-kicks.wav.slice-3.wav"
    "bb-kicks.wav.slice-4.wav"
    "bb-kicks.wav.slice-5.wav"
    "bb-kicks.wav.slice-6.wav"
    "bb-kicks.wav.slice-7.wav"
    "bb-kicks.wav.slice-8.wav"
    "bb-kicks.wav.slice-9.wav"
    "bb-kicks.wav.slice-10.wav"
    "bb-kicks.wav.slice-11.wav"
    "bb-kicks.wav.slice-12.wav"
    "bb-kicks.wav.slice-13.wav"
    "bb-kicks.wav.slice-14.wav"
    "bb-kicks.wav.slice-15.wav"
    "bb-kicks.wav.slice-16.wav"
    "bb-kicks.wav.slice-17.wav"
    "bb-kicks.wav.slice-18.wav"))

(define *bb-snare-files*
  '("bb-snares.wav.slice-0.wav"
    "bb-snares.wav.slice-1.wav"
    "bb-snares.wav.slice-2.wav"
    "bb-snares.wav.slice-3.wav"
    "bb-snares.wav.slice-4.wav"
    "bb-snares.wav.slice-5.wav"
    "bb-snares.wav.slice-6.wav"
    "bb-snares.wav.slice-7.wav"
    "bb-snares.wav.slice-8.wav"
    "bb-snares.wav.slice-9.wav"
    "bb-snares.wav.slice-10.wav"))

(define *bb-kicks* ())
(define *bb-snares* ())
(define *bb-cymbals* ())

(define (load-bb-kicks)
  (let ((kicks (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *bb-kick-files*))))
    (set! *bb-kicks* kicks)))

(define (load-bb-snares)
  (let ((snares (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *bb-snare-files*))))
    (set! *bb-snares* snares)))

(define (load-bb-cymbals)
  (let ((cymbals (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *bb-cymbal-files*))))
    (set! *bb-cymbals* cymbals)))

(define (load-bb-sounds)
  (set! *bb-kicks* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *bb-kick-files*)))
  (set! *bb-snares* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *bb-snare-files*)))
  (set! *bb-cymbals* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *bb-cymbal-files*))))

(define (analyze-bb-sounds)
  (map (lambda (region)
         (remember-region-properties '(spectrum centroid) region))
       (append *bb-kicks* *bb-snares* *bb-cymbals* 
               )))

(define (learn-bb-kicks)
  (learn-drums *bb-kicks* 1.0 0.0 0.0 0.0))

(define (learn-bb-snares)
  (learn-drums *bb-snares* 0.0 1.0 0.0 0.0))

(define (learn-bb-cymbals)
  (learn-drums *bb-cymbals* 0.0 0.0 1.0 0.0))

(define (learn-bb-kicks-with-noise)
  (learn-drums-with-noise *bb-kicks* 1.0 0.0 0.0))

(define (learn-bb-snares-with-noise)
  (learn-drums-with-noise *bb-snares* 0.0 1.0 0.0))

(define (learn-bb-cymbals-with-noise)
  (learn-drums-with-noise *bb-cymbals* 0.0 0.0 1.0))

(define *cymbal-files*
  '("n_cymbal-10.wav"
    "n_cymbal-1.wav"
    "n_cymbal-2.wav"
    "n_cymbal-3.wav"
    "n_cymbal-4.wav"
    "n_cymbal-5.wav"
    "n_cymbal-6.wav"
    "n_cymbal-7.wav"
    "n_cymbal-8.wav"
    "n_cymbal-9.wav"
    "n_cymbal-20.wav"
    "n_cymbal-11.wav"
    "n_cymbal-12.wav"
    "n_cymbal-13.wav"
    "n_cymbal-14.wav"
    "n_cymbal-15.wav"
    "n_cymbal-16.wav"
    "n_cymbal-17.wav"
    "n_cymbal-18.wav"
    "n_cymbal-19.wav"
    "n_Ridecymb.wav"
    "n_ride cymbal 505.wav"
    "n_MTCrashCymbl.wav"
    "n_hicymbal.wav"
    ))

(define *tom-files*
  '("n_tom-10.wav"
    "n_tom-1.wav"
    "n_tom-2.wav"
    "n_tom-3.wav"
    "n_tom-4.wav"
    "n_tom-5.wav"
    "n_tom-6.wav"
    "n_tom-7.wav"
    "n_tom-8.wav"
    "n_tom-9.wav"
    "n_test-tom-6.wav"
    "n_test-tom-7.wav"
    "n_test-tom-8.wav"
    "n_test-tom-9.wav"
    "n_test-tom-10.wav"))

(define *test-cymbal-files*
  '("n_test-cymbal-1.wav"
    "n_test-cymbal-2.wav"))

(define *test-tom-files*
  '("n_test-tom-1.wav"
    "n_test-tom-2.wav"
    "n_test-tom-3.wav"
    "n_test-tom-4.wav"
    "n_test-tom-5.wav"))
    
(define *kick-files*
  '("n_kick-10.wav"
    "n_kick-11.wav"
    "n_kick-12.wav"
    "n_kick-13.wav"
    "n_kick-14.wav"
    "n_kick-15.wav"
    "n_kick-16.wav"
    "n_kick-17.wav"
    "n_kick-18.wav"
    "n_kick-19.wav"
    "n_kick-2.wav"
    "n_tr808kick31.wav"
    "n_tr808kick32.wav"
    "n_tr808kick33.wav"
    "n_tr808kick34.wav"
    "n_tr808kick35.wav"
    "n_tr808kick36.wav"
    "n_tr909kick - 01.wav"
    "n_tr909kick - 02.wav"
    "n_tr909kick - 03.wav"
    "n_tr909kick - 04.wav"
    "n_tr909kick - 05.wav"
    "n_kick-20.wav"
    "n_kick-21.wav"
    "n_kick-22.wav"
    "n_kick-23.wav"
    "n_kick-3.wav"
    "n_kick-4.wav"
    "n_kick-5.wav"
    "n_kick-6.wav"
    "n_kick-7.wav"
    "n_kick-8.wav"
    "n_kick-9.wav"
    "n_tr909kick - 06.wav"
    "n_tr909kick - 07.wav"
    "n_tr909kick - 08.wav"
    "n_tr909kick - 12.wav"
    "n_tr909kick - 13.wav"
    "n_tr909kick - 14.wav"
    "n_tr909kick - 15.wav"
    "n_tr909kick - 16.wav"
    "n_tr909kick - 17.wav"
    "n_tr909kick - 18.wav"
    "n_tr909kick - 19.wav"
    "n_tr909kick - 20.wav"
    "n_tr909kick - 21.wav"
    "n_tr909kick - 22.wav"
    "n_tr909kick - 23.wav"
    "n_tr909kick - 24.wav"
    "n_tr808kick01.wav"
    "n_tr808kick02.wav"
    "n_tr808kick03.wav"
    "n_tr808kick04.wav"
    "n_tr808kick05.wav"
    "n_tr808kick06.wav"
    "n_tr808kick07.wav"
    "n_tr808kick08.wav"
    "n_tr808kick09.wav"
    "n_tr808kick10.wav"
    "n_tr808kick11.wav"
    "n_tr808kick12.wav"
    "n_tr808kick13.wav"
    "n_tr808kick14.wav"
    "n_tr808kick15.wav"
    "n_tr808kick16.wav"
    "n_tr808kick17.wav"
    "n_tr808kick18.wav"
    "n_tr808kick19.wav"
    "n_tr808kick20.wav"
    "n_tr808kick21.wav"
    "n_tr808kick22.wav"
    "n_tr808kick23.wav"
    "n_tr808kick24.wav"
    "n_tr808kick25.wav"
    "n_tr808kick26.wav"
    "n_tr808kick27.wav"
    "n_tr808kick28.wav"
    "n_tr808kick29.wav"
    "n_tr808kick30.wav"))

(define *snare-files*
  '("n_snare-1.wav"
    "n_snare-10.wav"
    "n_snare-11.wav"
    "n_snare-12.wav"
    "n_test-snare-4.wav"
    "n_test-snare-5.wav"
    "n_test-snare-6.wav"
    "n_test-snare-7.wav"
    "n_tr909sn - 01.wav"
    "n_tr909sn - 02.wav"
    "n_tr909sn - 03.wav"
    "n_snare-13.wav"
    "n_snare-14.wav"
    "n_snare-15.wav"
    "n_snare-16.wav"
    "n_snare-17.wav"
    "n_snare-18.wav"
    "n_snare-19.wav"
    "n_snare-2.wav"
    "n_snare-20.wav"
    "n_snare-3.wav"
    "n_snare-4.wav"
    "n_snare-5.wav"
    "n_snare-6.wav"
    "n_snare-7.wav"
    "n_snare-8.wav"
    "n_snare-9.wav"
    "n_tr808sn - 01.wav"
    "n_tr808sn - 02.wav"
    "n_tr808sn - 03.wav"
    "n_tr808sn - 04.wav"
    "n_tr808sn - 05.wav"
    "n_tr808sn - 06.wav"
    "n_tr808sn - 07.wav"
    "n_tr808sn - 08.wav"
    "n_tr808sn - 09.wav"
    "n_tr808sn - 10.wav"
    "n_tr808sn - 11.wav"
    "n_tr808sn - 12.wav"
    "n_tr808sn - 13.wav"
    "n_tr808sn - 14.wav"
    "n_tr808sn - 15.wav"
    "n_tr808sn - 16.wav"
    "n_tr808sn - 17.wav"
    "n_tr808sn - 18.wav"
    "n_tr808sn - 19.wav"
    "n_tr808sn - 20.wav"
    "n_tr808sn - 21.wav"
    "n_tr808sn - 22.wav"
    "n_tr808sn - 23.wav"
    "n_tr808sn - 24.wav"
    "n_tr808sn - 25.wav"
    "n_tr909sn - 06.wav"
    "n_tr909sn - 07.wav"
    "n_tr909sn - 08.wav"
    "n_tr909sn - 09.wav"
    "n_tr909sn - 10.wav"
    ))

(define *test-kick-files*
  '("n_test-kick-1.wav"
    "n_test-kick-2.wav"
    "n_test-kick-3.wav"
    "n_test-kick-4.wav"
    "n_tr909kick - 09.wav"
    "n_tr909kick - 10.wav"
    "n_tr909kick - 11.wav"
))

(define *test-snare-files*
  '("n_test-snare-1.wav"
    "n_test-snare-2.wav"
    "n_test-snare-3.wav"
    "n_tr909sn - 04.wav"
    "n_tr909sn - 05.wav"
    ))

(define *test-kicks* ())
(define *test-snares* ())

(define *kicks* ())

(define *snares* ())

(define *test-cymbals* ())
(define *test-toms* ())

(define *cymbals* ())

(define *toms* ())

(define (load-kicks)
  (let ((kicks (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *kick-files*))))
    (set! *kicks* kicks)))

(define (load-snares)
  (let ((snares (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *snare-files*))))
    (set! *snares* snares)))

(define (load-cymbals)
  (let ((cymbals (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *cymbal-files*))))
    (set! *cymbals* cymbals)))

(define (load-toms)
  (let ((toms (map (lambda (file)
                      (sound->region! (open-sound file)))
                    (map session-file *tom-files*))))
    (set! *toms* toms)))

(define (load-test-sounds)
  (set! *test-kicks* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *test-kick-files*)))
  (set! *test-snares* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *test-snare-files*)))
  (set! *test-toms* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *test-tom-files*)))
  (set! *test-cymbals* (map (lambda (file)
                            (sound->region! (open-sound file)))
                          (map session-file *test-cymbal-files*))))

(define (analyze-beats)
  (map (lambda (region)
         (remember-region-properties '(spectrum centroid) region))
       (append *kicks* *snares* *test-kicks* *test-snares*
               *cymbals* *toms* *test-cymbals* *test-toms*
               )))

(define (learn-drums regions kick snare cymbal tom)
  (map (lambda (region)
         (percussion-3/learn-region region kick snare cymbal tom))
       regions))

(define (learn-kicks)
  (learn-drums *kicks* 1.0 0.0 0.0 0.0))

(define (learn-snares)
  (learn-drums *snares* 0.0 1.0 0.0 0.0))

(define (learn-cymbals)
  (learn-drums *cymbals* 0.0 0.0 1.0 0.0))

(define (learn-toms)
  (learn-drums *toms* 0.0 0.0 0.0 1.0))

(define (test-kicks)
  (display "\nKICKS: ")
  (map (lambda (r)
         (display (mosaic-net-test-region r percussion-3/properties->features)))
       *test-kicks*)
  (display "\n"))

(define (test-snares)
  (display "\nSNARES: ")
  (map (lambda (r)
         (display (mosaic-net-test-region r percussion-3/properties->features)))
       *test-snares*)
  (display "\n"))

(define (test-cymbals)
  (display "\nCYMBALS: ")
  (map (lambda (r)
         (display (mosaic-net-test-region r percussion-3/properties->features)))
       *test-cymbals*)
  (display "\n"))

(define (test-toms)
  (display "\nTOMS: ")
  (map (lambda (r)
         (display (mosaic-net-test-region r percussion-3/properties->features)))
       *test-toms*)
  (display "\n"))

(define^ (percussion-3/violin-region start-time duration frequency amplitude (amp-env '(0 1 1 1)))
  (sound->region! (with-sound (:channels 2)
                              (fm-violin start-time
                                         duration
                                         frequency
                                         amplitude
                                         :amp-env amp-env))))

(define^ (percussion-3/random-notes block-len)
  (let* ((notes (list (+ 20 (random 80)) (+ 20 (random 80)) (+ 20 (random 80))
                      (+ 20 (random 80)) (+ 20 (random 80)) (+ 20 (random 80))))
         (output-region #f)
         (duration (/ block-len 44100.0))
         (note-1 (car notes))
         (note+ (if (pair? (cdr notes))
                    (cdr notes)
                    #f)))
    (set! output-region
          (percussion-3/violin-region 0
                                      duration
                                      (note->pitch note-1)
                                      0.12
                                      :amp-env '(0 1 1 1)))
    (do ((ns note+ (cdr ns))
         (j 1 (+ 1 j)))
        ((null? ns) output-region)
      (set! output-region
            (mix-regions* output-region
                          (percussion-3/violin-region 0
                                                      duration
                                                      (note->pitch (car ns))
                                                      0.12
                                                      :amp-env '(0 1 1 1)))))))

(define^ (percussion-3/random-music block-len)
  (let ((uuids (map car *uuid->properties*)))
    (lazy-region (random-choose uuids))))
  
(define^ (percussion-3/learn-region region kick snare cymbal tom)
  (do ((n 0 (1+ n)))
      ((> n 4))
    (learn! (noisy-features (percussion-3/properties->features (*region->properties* region)))
            (vector kick snare cymbal))))

(define^ (percussion-3/learn-region-cleanly region kick snare cymbal tom)
  (learn! (percussion-3/properties->features (*region->properties* region))
          (vector kick snare cymbal)))
  ;; (learn! (percussion-3/properties->features (*region->properties* region))
  ;;         (vector kick snare cymbal)))

(define^ (percussion-3/learn-region-with-noise region kick snare cymbal)
  (let ((noisy-region-1 (mix-regions* region (percussion-3/random-notes (length (car region)))))
        ;; (noisy-region-2 (mix-regions* region (percussion-3/random-notes (length (car region)))))
        ;; (noisy-region-3 (mix-regions* region (percussion-3/random-music (length (car region)))))
        (noisy-region-4 (mix-regions* region (percussion-3/random-music (length (car region))))))
    (remember-region-properties '(spectrum centroid) noisy-region-1)
    ;; (remember-region-properties '(spectrum centroid) noisy-region-2)
    ;; (remember-region-properties '(spectrum centroid) noisy-region-3)
    (remember-region-properties '(spectrum centroid) noisy-region-4)
    (learn! (noisy-features (percussion-3/properties->features (*region->properties* noisy-region-1)))
            (vector kick snare cymbal))
    ;;  (learn! (noisy-features (percussion-3/properties->features (*region->properties* noisy-region-2)))
    ;;          (vector kick snare cymbal))
    ;; (learn! (noisy-features (percussion-3/properties->features (*region->properties* noisy-region-3)))
    ;;          (vector kick snare cymbal))
    (learn! (noisy-features (percussion-3/properties->features (*region->properties* noisy-region-4)))
            (vector kick snare cymbal))))

(define (learn-drums-with-noise regions kick snare cymbal)
  (map (lambda (region)
         (percussion-3/learn-region-with-noise region kick snare cymbal))
       regions))

(define (learn-kicks-with-noise)
  (learn-drums-with-noise *kicks* 1.0 0.0 0.0))

(define (learn-snares-with-noise)
  (learn-drums-with-noise *snares* 0.0 1.0 0.0))

(define (learn-cymbals-with-noise)
  (learn-drums-with-noise *cymbals* 0.0 0.0 1.0))

(define (learn-toms-with-noise)
  (learn-drums-with-noise *toms* 0.0 0.0 0.0))

(define^ (percussion-3/train!)
  (let* ((X (length (car *net-inputs*)))
         (Y 128))
    (build-classifier X Y 3)
    (set! *training-inputs* (list->vector *net-inputs*))
    (set! *training-outputs* (list->vector *net-outputs*))
    (set! *restart* #f)
    (train-test 1 1000 1)))

(define^ (percussion-3/build-net)
  ;;(mosaic-load-database "/home/dto/sessions/drop.mosaic/")
  (load-kicks)
  (load-snares)
  (load-cymbals)
  (load-toms)
  (load-test-sounds)
  (analyze-beats)
  (learn-kicks)
  (learn-snares)
  (learn-cymbals)
;;  (le
  ;; (learn-kicks-with-noise)
  ;; (learn-snares-with-noise)
  ;; (learn-cymbals-with-noise)
  ;;(learn-toms-with-noise)
  (percussion-3/train!)
  (display "-------------\n")
  (test-kicks)
  (test-snares)
  (test-cymbals))
;; (test-toms)

(define^ (percussion-3/save-net)
  (mosaic-save-net "/home/dto/mosaic/percussion-3.nn"))

(define^ (bb-3/build-net)
  (load-bb-kicks)
  (load-bb-snares)
  (load-bb-cymbals)
  (load-bb-sounds)
  (analyze-bb-sounds)
  (learn-bb-kicks)
  (learn-bb-snares)
  (learn-bb-cymbals)
  (percussion-3/train!))

(define^ (bb-3/save-net)
  (mosaic-save-net "/home/dto/mosaic/bb-3.nn"))
