;;; properties.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Region properties.

;; Annotating sound slices ("regions") with metadata.

(define *region->properties* (make-hash-table 128 eq?))

(define^ (forget-region-properties region)
  "Clear the cached properties of REGION."
  (set! (*region->properties* region) #f))

(define^ (forget-all-region-properties)
  "Clear all cached region properties."
  (set! *region->properties* (make-hash-table 128 eq?)))

(define^ (mosaic-all-regions)
  "Return a list of all regions in the property table."
  (show-progress "m-a-r")
  (map car *region->properties*))

(define^ (region-property property region)
  "Return the value of property PROPERTY for REGION."
  (let ((properties (*region->properties* region)))
    (if (not (pair? properties))
        #f
        (let ((entry (assoc* property properties)))
          (if (pair? entry)
              (cdr entry)
              #f)))))

(define^ (normalize-region-properties! region)
  "Rewrite a region's property list so that the alist entries are
sorted alphabetically by property name. This is done to set up
property alists for comparison with EQUAL? and other functions."
  (set! (*region->properties* region)
    (sort! (copy (*region->properties* region))
           (lambda (a b)
             (string<? (symbol->string (car a))
                       (symbol->string (car b)))))))

(define^ (set-region-property! property region value)
  "Set the value of PROPERTY in REGION to VALUE."
  (let ((entries (*region->properties* region)))
    ;; begin with empty list if region has no properties
    (when (eq? #f entries)
      (set! entries ()))
    ;; prevent duplicate entries 
    (when (assoc* property entries)
      (set! (*region->properties* region)
        (del-assoc property entries)))
    ;; store new value
    (set! (*region->properties* region)
      (cons (cons property value)
            entries))
    ;; keep lists sorted so they can be used as keys 
    (normalize-region-properties! region)
    ;; return
    value))

(set! (setter region-property) set-region-property!)

(define-macro (define-region-property name fname)
  (let ((region (gensym))
        (value (gensym)))
    `(define (,fname ,region)
       (region-property ',name ,region))))

(define-macro (define-region-property-setter name fname)
  (let ((region (gensym))
        (value (gensym)))
    `(set! (setter ,fname) (lambda (,region ,value)
                             (set-region-property! ',name ,region ,value)))))


(define *region-property-names* '(position pitch pitch-class rms slice-number centroid
                                           percussivity interval amplitude note notes spectrum probability down?
                                           kick snare cymbal percussion-3-guess-multiple total-energy
                                           up? on? off? strong?  weak? syncopated?))

(define-region-property percussion-3-guess-multiple @percussion-3-guess-multiple)
(define-region-property kick @kick)
(define-region-property snare @snare)
(define-region-property cymbal @cymbal)
(define-region-property centroid @centroid)
(define-region-property total-energy @total-energy)
(define-region-property rms @rms)
(define-region-property pitch @pitch)
(define-region-property pitches @pitches)
(define-region-property pitch-class @pitch-class)
(define-region-property interval @interval)
(define-region-property amplitude @amplitude)
(define-region-property note @note)
(define-region-property notes @notes)
(define-region-property spectrum @spectrum)
(define-region-property probability @probability)
(define-region-property down? @down?)
(define-region-property up? @up?)
(define-region-property on? @on?)
(define-region-property off? @off?)
(define-region-property strong? @strong?)
(define-region-property weak? @weak?)
(define-region-property syncopated? @syncopated?)
(define-region-property position @position)
(define-region-property slice-number @slice-number)
(define-region-property percussivity @percussivity)

(define-region-property-setter percussion-3-guess-multiple @percussion-3-guess-multiple)
(define-region-property-setter kick @kick)
(define-region-property-setter snare @snare)
(define-region-property-setter cymbal @cymbal)
(define-region-property-setter centroid @centroid)
(define-region-property-setter total-energy @total-energy)
(define-region-property-setter rms @rms)
(define-region-property-setter pitch @pitch)
(define-region-property-setter pitches @pitches)
(define-region-property-setter pitch-class @pitch-class)
(define-region-property-setter interval @interval)
(define-region-property-setter amplitude @amplitude)
(define-region-property-setter note @note)
(define-region-property-setter notes @notes)
(define-region-property-setter spectrum @spectrum)
(define-region-property-setter probability @probability)
(define-region-property-setter down? @down?)
(define-region-property-setter up? @up?)
(define-region-property-setter on? @on?)
(define-region-property-setter off? @off?)
(define-region-property-setter strong? @strong?)
(define-region-property-setter weak? @weak?)
(define-region-property-setter syncopated? @syncopated?)
(define-region-property-setter position @position)
(define-region-property-setter slice-number @slice-number)
(define-region-property-setter percussivity @percussivity)

(define^ (annotate property region (value #t))
  (set! (region-property property region) value)
  ;; return the region, not the property value
  ;; so that this can be used inline in patterns
  region)

(define^ (annotate-up-and-down-beats-8 a b c d e f g h)
  (list (annotate 'downbeat? a)
        b c d
        (annotate 'downbeat? e)
        f g
        (annotate 'upbeat? h)))

(define^ (delete-properties deleted-properties source-properties)
  (let ((p source-properties))
    (map (lambda (property)
           (set! p (del-assoc property p)))
         deleted-properties)
    p))

(define^ (descriptor-properties-only properties)
  "The function DESCRIPTOR-PROPERTIES-ONLY removes floating point
properties from the alist PROPERTIES. A descriptor must contain only
discrete properties to be matched, i.e. no floating point numbers
allowed. 

This function also removes the special POSITION property. Position is
a discrete property, but should be matched through other means."
  (assert (list? properties))
  (delete-properties '(spectrum rms pitch pitches position kick snare notes
                                cymbal slice-number uuid source-file tempo) properties))

(define^ (note-lists-only properties)
  (map (lambda (p)
         (list (assoc* 'notes p)))
       properties))

(define^ (percussion-3-guesses-only properties)
  (map (lambda (p)
         (list (assoc* 'percussion-3-guess-multiple p)))
       properties))

(define^ (percussion-3-scalars-only p)
  (list (cons 'kick (cdr (assoc* 'kick p)))
        (cons 'snare (cdr (assoc* 'snare p)))
        (cons 'cymbal (cdr (assoc* 'cymbal p)))))

(define^ (percussion-3-scalars-only-noisy p)
  (list (cons 'kick (min 0.92 (+ (random 0.02) (cdr (assoc* 'kick p)))))
        (cons 'snare (min 0.92 (+ (random 0.02) (cdr (assoc* 'snare p)))))
        (cons 'cymbal (min 0.92 (+ (random 0.02) (cdr (assoc* 'cymbal p)))))))

(define^ (find-sound-structure regions
                               (show? #f)
                               (properties-fn descriptor-properties-only))
  "Return the structure Sequitur finds for REGIONS. SHOW? determines
whether to show each step for debugging. PROPERTIES-FN should remove
any properties you don't want to match on. By default this is
DISCRETE-PROPERTIES-ONLY."
  (find-structure
   (properties-fn
    (map *region->properties* regions))
   :show? show?))

;;; Matching properties

(define^ (match-properties properties regions (match-fn equal?))
  "Return a list of regions from REGIONS (if any) matching PROPERTIES.
This is also used by GENERATE-REGIONS.  By default, the REGIONS come
from the current synth's SOURCE-REGIONS; if these are empty, then from
TARGET-REGIONS. If this isn't what you want, pass the REGIONS you want
to search instead. MATCH-FN determines the equality test used for
matching descriptors. To match against the entire database instead of
just the contents of this synth, pass (MOSAIC-ALL-REGIONS) as the
REGIONS argument."
  ;; unwrap any extra wrapping
  (when (pair? properties)
    (when (descriptor? (car properties))
      (set! properties (car properties))))
  ;;
  (do ((results ())
       (regions-1 (or regions (*synth* 'target-regions))
                  (cdr regions-1)))
      ((null? regions-1) (reverse results))
    (let ((a (car (descriptor-properties-only
                   (list (*region->properties* (car regions-1))))))
          (b properties))
      (when (match-fn a b)
        (set! results (cons (car regions-1) results))))))

;;; Fuzzy matching

(define^ (fuzzy-match-descriptors descriptor-1 descriptor-2 (match-fn equal?))
  ;; unwrap any extra wrapping
  (when (pair? descriptor-1)
    (when (descriptor? (car properties))
      (set! descriptor-1 (car descriptor-1))))
  (when (pair? descriptor-2)
    (when (descriptor? (car properties))
      (set! descriptor-2 (car descriptor-2))))
  ;;
  (let ((match? #f)
        (no-value (gensym)))
    (do ((properties-1 descriptor-1 (cdr properties-1)))
        ((null? properties-1) match?)
      (let* ((property-name (car (car properties-1)))
             (property-value-1 (cadr (car properties-1)))
             (property-entry-2 (assoc* property-name descriptor-2))
             (property-value-2 (if (pair? property-entry-2)
                                   (cadr property-entry-2)
                                   no-value)))
        (when (and (not (eq? no-value property-value-2))
                   (match-fn property-value-1 property-value-2))
          (set! match? #t))))))

(define^ (fuzzy-match-properties properties regions (match-fn equal?))
  "Similar to MATCH-PROPERTIES, but matches two regions if any
property value is MATCH-FN between them, whereas regular
MATCH-PROPERTIES requires that all property values match."
  (match-properties properties regions
                    (lambda (a b)
                      (fuzzy-match-descriptors a b match-fn))))

;;; Cached properties.

;; The following functions all cache the region property being sought.

(define^ (percussion-3-guess-multiple* region)
  (or (@percussion-3-guess-multiple region)
      (set! (@percussion-3-guess-multiple region)
            (percussion-3-guess-multiple region))))

(define^ (region-kick% region)
  (or (@kick region)
      (set! (@kick region)
            ((percussion-3 region) 0))))

(define^ (region-snare% region)
  (or (@snare region)
      (set! (@snare region)
            ((percussion-3 region) 1))))

(define^ (region-cymbal% region)
  (or (@cymbal region)
      (set! (@cymbal region)
            ((percussion-3 region) 2))))

(define^ (region-centroid* region)
  (or (@centroid region)
      (set! (@centroid region)
            (let ((file (mosaic-temp-file)))
              (save-region* region file)
              (let ((centroid (scentroid file 0.0 (samples->seconds (framples* region)))))
                (delete-file file)
                centroid)))))

(define region-centroid region-centroid*)

(define^ (region-spectrum* region)
  (or (@spectrum region)
      (set! (@spectrum region)
        (begin
          (show-progress "spec")
          (find-spectrum region)))))

(define region-spectrum region-spectrum*)

(define^ (region-total-energy* region)
  (or (@total-energy region)
      (set! (@total-energy region)
            (let ((sound (new-sound :channels 2)))
              (insert-region* region 0 sound)
              (let ((energy (channel-total-energy sound 0)))
                (close-sound sound)
                energy)))))

(define region-total-energy region-total-energy*)

(define^ (region-rms* region)
  (or (@rms region)
      (set! (@rms region)
            (let ((sound (new-sound :channels 2)))
              (insert-region* region 0 sound)
              (let ((rms (channel-rms sound 0)))
                (close-sound sound)
                rms)))))

(define region-rms region-rms*)

(define^ (region-loudest-pitch region)
  (spectrum-freq (region-spectrum* region)))

(define^ (region-pitch* region)
  (or (@pitch region)
      (set! (@pitch region)
        (region-loudest-pitch region))))

(define region-pitch region-pitch*)

(define^ (region-pitches* region)
  (or (@pitches region)
      (set! (@pitches region)
        (let* ((spec (region-spectrum* region))
               (freqs (spectrum-freqs spec))
               (amps (spectrum-amps spec)))
          (loudest-freqs
           (sort-frequencies amps freqs)
           *maximum-notes*)))))

(define region-pitches region-pitches*)

(define^ (region-note* region)
  (or (@note region)
      (set! (@note region)
        (let ((n (region-pitch* region)))
          (if (real? n)
              (pitch->note n)
              440.0)))))

(define region-note region-note*)

(define^ (sort-frequencies amps freqs)
  (let ((pairs ())
        (len (length amps)))
    (do ((i 0 (+ i 1)))
        ((= i len))
      (set! pairs (cons (list (freqs i) (amps i))
                        pairs)))
    (sort! pairs (lambda (a b)
                   (>= (cadr a)
                       (cadr b))))))

(define^ (loudest-freqs pairs (n 2))
  (map car (subsequence pairs 0 n)))

(define^ (loudest-notes pairs (n 2))
  (map pitch->note (loudest-freqs pairs n)))

(define *maximum-notes* 10)

(define^ (region-notes* region)
  (or (@notes region)
      (set! (@notes region)
        (let* ((spec (region-spectrum* region))
               (freqs (spectrum-freqs spec))
               (amps (spectrum-amps spec)))
          (loudest-notes
           (sort-frequencies amps freqs)
           *maximum-notes*)))))

(define region-notes region-notes*)

(define^ (find-highest-amplitudes region) 
  (let* ((notes (region-notes* region))
         (spec (region-spectrum* region))
         (freqs (spectrum-freqs spec))
         (amps (spectrum-amps spec))
         (pairs (sort-frequencies amps freqs))
         (pair #f)
         (amps 0.0))
    (do ((p pairs (cdr p)))
        ((null? p) (reverse amps))
      (set! amps (cons (cadr (car p))
                       amps)))))

(define^ (remember-region-properties names region)
  "Cache the properties listed in NAMES for REGION. This currently
only works for SPECTRUM, PITCH, NOTE, PITCHES, and NOTES. Of these,
the first three are cached by default anyway upon loading into a
synth; if you want to analyze NOTES etc, do:
  (REMEMBER-REGION-PROPERTIES '(NOTES) MY-REGION) first."
  (map (lambda (name)
         (case name
           ((rms) (region-rms region))
           ((centroid) (region-centroid region))
           ((note) (region-note region))
           ((notes) (region-notes region))
           ((spectrum) (region-spectrum region))
           ((pitch) (region-pitch region))
           ((pitches) (region-pitches region))
           ((total-energy) (region-total-energy region))
           ((kick) (region-kick% region))
           ((snare) (region-snare% region))
           ((cymbal) (region-cymbal% region))
           ((percussion-3-guess-multiple)
            (percussion-3-guess-multiple* region))))
       names))

