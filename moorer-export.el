
(defun mosaic-spectr-as-smalltalk (spectr)
  (format "{ '%S' -> {%s}}"
          (intern (string-replace "-" "X" (symbol-name (car spectr))))
          (mapconcat 'prin1-to-string (cadr spectr) " . ")))

(with-temp-buffer
  (insert (concat "{" (mapconcat #'mosaic-spectr-as-smalltalk mosaic-spectr-data " . \n") "}"))
  (write-file "~/mosaic/moorer-spectra.st"))
