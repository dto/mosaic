;;; mosaic-orch.el --- liquid orchestra              -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Extracting data from `moorer-spectra.el'

(defvar mosaic-instrument-keys '((bass-clarinet . bc)
                                 (bassoon . bsn)
                                 (cello . c)
                                 (clarinet . cl)
                                 (french-horn . fh)
                                 (flute . fl)
                                 (oboe . ob)
                                 (piano . p)
                                 (sax . sax)
                                 (soprano-sax . ssax)
                                 (unknown-1 . tbf)
                                 (unknown-2 . tbp)
                                 (trumpet . trp)
                                 (trumpet-f . trpf)
                                 (violin . vl)
                                 (unknown-3 . almf)))

(defvar mosaic-instrument-names (mapcar #'cl-first mosaic-instrument-keys))

(defun mosaic-spectr-key->instrument-key (key)
  (let* ((s (symbol-name key))
         (p (cl-position ?- s))
         (ins (substring s 0 p)))
    (intern ins)))

(defun mosaic-spectr-key->note-key (key)
  (let* ((s (symbol-name key))
         (p (cl-position ?- s))
         (n (substring s (1+ p))))
    (intern n)))

(defun mosaic-instrument-name->instrument-key (name)
  (cdr (assoc name mosaic-instrument-keys)))

(defun mosaic-spectr-key->note-number (key)
  (let* ((s (symbol-name (mosaic-spectr-key->note-key key)))
         (name (make-string 1 (aref s 0)))
         (sharp (= ?s (aref s 1)))
         (octave (car (read-from-string
                       (make-string 1
                                    (if sharp
                                        (aref s 2)
                                      (aref s 1))))))
         (pcs '("c" "cs" "d" "ds" "e" "f" "fs" "g" "gs" "a" "as" "b"))
         (pitch-class (cl-position (concat name (if sharp "s" ""))
                                pcs :test 'equal)))
    (+ 12 pitch-class (* 12 octave))))

(defun mosaic-spectr-key->hash-key (key)
  (list (mosaic-spectr-key->instrument-key key)
        (mosaic-spectr-key->note-number key)))

(defvar mosaic-spectr nil)

(defun mosaic-preprocess-spectr ()
  (interactive)
  (setf mosaic-spectr (make-hash-table :test 'equal))
  (dolist (entry mosaic-spectr-data)
    (cl-destructuring-bind (key partials) entry
      (setf (gethash (mosaic-spectr-key->hash-key key)
                     mosaic-spectr)
            ;; why REST here?
            (cl-rest partials)))))

(defun log-2 (x) 
    (/ (log x) (log 2)))

(defun mosaic-hz->radians (hz)
  "Convert Hz (cycles/second) to radians/second.
This depends on the current `mosaic-sample-rate'."
  (/ (* hz pi 2.0)
     (float mosaic-sample-rate)))

(defun mosaic-pitch->note-number (pitch)
  "Return the MIDI note number closest to PITCH."
  (let ((value (+ 69
                  (* 12 (log-2 (/ pitch mosaic-a4-pitch))))))
    (round value)))

(defun mosaic-find-spectrum (instrument pitch)
  (when (null mosaic-spectr)
    (mosaic-preprocess-spectr))
  (let ((ik (mosaic-instrument-name->instrument-key instrument))
        (nn (mosaic-pitch->note-number pitch)))
    (cl-first (gethash (list ik nn)
                    mosaic-spectr))))

;;; Liquid orchestra

;; This section pairs an Emacs Lisp score interpreter class, `orch',
;; with a Snd-CLM instrument of the same name on the Scheme
;; side. Experimental granular synthesis support is here but currently
;; not documented.

(defclass orch (player)
  ((clm-instrument :initform 'orch)
   (instrument :initform 'violin :initarg :instrument :accessor orch-instrument)))

;; stub class for A/B testing
(defclass orch* (orch)
  ;; instantiates test version of instrument class on scheme side
  ((clm-instrument :initform 'orch*)))

(cl-defmethod orch-spectrum ((o orch) pitch)
  (mosaic-find-spectrum (orch-instrument o)
                        pitch))

(defclass cello (orch) ((instrument :initform 'cello)))
(defclass violin (cello) ((instrument :initform 'violin)))
(defclass bass-clarinet (orch) ((instrument :initform 'bass-clarinet)))
(defclass bassoon (orch) ((instrument :initform 'bassoon)))
(defclass clarinet (orch) ((instrument :initform 'clarinet)))
(defclass french-horn (orch) ((instrument :initform 'french-horn)))
(defclass flute (orch) ((instrument :initform 'flute)))
(defclass oboe (orch) ((instrument :initform 'oboe)))
(defclass piano (orch) ((instrument :initform 'piano)))
(defclass sax (orch) ((instrument :initform 'sax)))
(defclass soprano-sax (orch) ((instrument :initform 'soprano-sax)))
(defclass unknown-1 (orch) ((instrument :initform 'unknown-1)))
(defclass unknown-2 (orch) ((instrument :initform 'unknown-2)))
(defclass trumpet (orch) ((instrument :initform 'trumpet)))
(defclass trumpet-f (orch) ((instrument :initform 'trumpet-f)))
(defclass unknown-3 (orch) ((instrument :initform 'unknown-3)))

(cl-defmethod player-vibrato-parameters ((c cello) duration freq volume)
  (list :periodic-vibrato-rate (+ 3.5 (/ 1.6 freq))
        :periodic-vibrato-amplitude 0.0030
        :random-vibrato-rate 16.0
        :random-vibrato-amplitude 0.0036
        :vibrato-env (or (player-vibrato-env c)
                         '(0 0 1 0))
        ))

(cl-defmethod player-vibrato-parameters ((c clarinet) duration freq volume)
  (list :periodic-vibrato-rate 0.0
        :periodic-vibrato-amplitude 0.0
        :random-vibrato-rate 16.0
        :random-vibrato-amplitude 0.0012))

(cl-defmethod player-position-parameters ((p player) duration freq volume)
  (list :distance 5.0
        :degree 90))

(cl-defmethod player-position-parameters ((c cello) duration freq volume)
  (list :distance 2.0
        :degree 120))

(cl-defmethod player-position-parameters ((c clarinet) duration freq volume)
  (list :distance 3.0
        :degree 54))

(cl-defmethod player-position-parameters ((v violin) duration freq volume)
  (list :distance 3.0
        :degree 60))

(cl-defmethod player-note-parameters ((o orch) duration freq volume)
  (let ((partials (mosaic-find-spectrum (orch-instrument o) freq)))
    (append (list :partials partials
                  :partial-amp-envs (player-partial-amp-envs o partials duration freq volume)
                  :attack-noise-amp-env (player-noise-amp-env o partials duration freq volume)
                  :attack-noise-freq (+ 300.0 (/ freq 6.0))
                  :amp-env (player-amp-env o duration))
            (player-position-parameters o duration freq volume)
            (player-vibrato-parameters o duration freq volume))))

(cl-defmethod player-change-expression :before ((o orch) exp-symbol)
  (setf (player-was-legato-p o)
        (player-legato-p o)))

(cl-defmethod player-default-attack-point ((p player) duration)
  0.16)

(cl-defmethod player-default-release-point ((p player) duration)
  0.21)

(cl-defmethod player-default-decay-point ((p player) duration)
  0.75)

(cl-defmethod player-default-attack-point ((c cello) duration)
  (if (player-accent-p c)
      (/ 0.13 duration)
    (/ 0.18 duration)))

(cl-defmethod player-default-release-point ((c cello) duration)
  (if (player-accent-p c)
      0.75
    0.75))

(cl-defmethod player-default-amp-env ((p player) duration)
  '(0 0  25 1  75 1  100 0))

(cl-defmethod player-default-amp-env ((c clarinet) duration)
  '(0 0 8 1 93 1 100 0))

(cl-defmethod player-default-amp-env :around ((p player) duration)
  (mosaic-check-envelope (cl-call-next-method)))

(cl-defmethod player-amp-env ((o orch) duration)
  (let* ((legato-time (player-legato-time o))
         (staccato-time (player-staccato-time o))
         (left-shoulder (* 0.9 (/ legato-time duration)))
         (attack-point (player-default-attack-point o duration))
         (release-point (player-default-release-point o duration))
         (right-shoulder (- 1.0 (* 0.35 ;; transition into new note is sharper
                                   (/ legato-time duration)))))
    (cond
     ((player-staccato-p o)
      `(0 0
          (+ ,(/ 0.02 duration) (/ (random 0.015) ,duration))
          (+ 0.9 (/ (random 0.07) ,duration))
          (+ ,(/ (+ 0.02 staccato-time) duration) 0.0151 (/ (random 0.02) ,duration))
          0.0))
     ((and (player-was-legato-p o) 
           (player-legato-p o))
      `(0 0 ,left-shoulder 1  ,right-shoulder 1  1 0))
     ((and (player-was-legato-p o)
           (not (player-legato-p o)))
      `(0 0  ,left-shoulder 1  0.80 1  1 0))
     ((and (not (player-was-legato-p o))
           (player-legato-p o))
      `(0 0 ,(player-default-attack-point o duration) 1 ,right-shoulder 1 1 0))
     (t `(0 0 ,(player-default-attack-point o duration) 1
            ,(player-default-release-point o duration) 1 1 0)))))

(cl-defmethod player-amp-env :around ((o orch) duration)
  (if (< duration 0.214285)
      ;; don't barf on tiny notes when absolute seconds values are used in envelopes elsewhere
      (player-default-amp-env o duration)
    (mosaic-check-envelope (cl-call-next-method))))

(cl-defmethod player-default-amp-env ((o orch) duration)
  '(0 0  25 1  75 1  100 0))

(cl-defmethod player-partial-amp-envs ((o orch) partials duration freq volume)
  (let ((num-partials (/ (length partials) 2)))
    (let (results)
      (dotimes (n num-partials)
        (push (copy-tree '(0 1 1 1)) results))
      (nreverse results))))

(cl-defmethod player-partial-amp-envs ((c cello) partials duration freq volume)
  (let* ((num-partials (/ (length partials) 2))
         (envs (make-list num-partials (copy-tree '(0 1 1 1))))
         (attack-mid (/ 0.1 duration))
         (attack-end (/ 0.2 duration)))
    (let ((brightness (if (player-was-legato-p c) 0.7 1.7)))
      (let ((i 7))
        (while (< i num-partials)
          (setf (nth i envs)
                ;; start with brightness increase that rapidly falls off
                `(0 (+ ,brightness (random 0.4)) ,attack-mid 0.5 (+ ,attack-end (random 0.5)) 0
                    ;; then dim according to volume if needed
                    ;; ,(+ attack-end 0.6) ,(* -1.0 (/ 1.0 i) (- 1.0 (/ volume 6.0)))
                    ))
          (cl-incf i))))
    envs))

(cl-defmethod player-partial-amp-envs :around ((o orch) partials duration freq volume)
  (mapcar #'mosaic-check-envelope (cl-call-next-method)))

(cl-defmethod player-noise-amp-env ((o orch) partials duration freq volume)
  '(0 0 1 0))

(cl-defmethod player-noise-amp-env :around ((o orch) partials duration freq volume)
  (mosaic-check-envelope (cl-call-next-method)))

(cl-defmethod player-noise-amp-env ((c cello) partials duration freq volume)
  (let* ((attack-mid (/ 0.3 duration))
         (attack-end (/ 0.5 duration)))
    (let ((max (if (player-staccato-p c) 0.5
                 (if (player-was-legato-p c)
                     0.7 1.8))))
        `(0 ,max ,attack-mid ,(/ max 2.0) ,attack-end 0.0))))

(cl-defmethod player-noise-amp-env :around ((o orch) partials duration freq volume)
  (mosaic-check-envelope (cl-call-next-method)))

(defvar mosaic-mix-number 0)

(defvar mosaic-mix-sequence-name "untitled-sequence")

(cl-defun mosaic-mix-file (&optional (n mosaic-mix-number))
  (mosaic-session-file (format ".mix-%s-%d.wav" mosaic-mix-sequence-name n)))

(cl-defun mosaic-mix-data-file (&optional (n mosaic-mix-number))
  (mosaic-session-file (format ".mix-%s-%d.scm" mosaic-mix-sequence-name n)))

(cl-defun mosaic-mix-new-data-file (&optional (n mosaic-mix-number))
  (mosaic-session-file (format ".mix-new-%s-%d.scm" mosaic-mix-sequence-name n)))

(cl-defmethod player-emit-mix-block ((p player) start-time)
  (with-slots (note-list mix-blocks) p
    (when note-list
      (let ((notes (reverse note-list)))
        ;; store data for later scheme generation
        (push `(,mosaic-mix-number ,notes ,start-time)
              mix-blocks)
        (setf note-list nil)
        (cl-incf mosaic-mix-number)))))

(cl-defun mosaic-match-mix-data (note-list &optional (n mosaic-mix-number))
  (when (file-exists-p (mosaic-mix-data-file n))
    (with-temp-buffer
      (fundamental-mode)
      (insert (prin1-to-string note-list))
      (let ((make-backup-files nil))
        (mosaic-write-file (mosaic-mix-new-data-file n))))
    (zerop (call-process "diff" nil nil nil
                         (mosaic-mix-data-file n)
                         (mosaic-mix-new-data-file n)))))

(cl-defmethod player-emit-event ((v player) time duration pitch volume parameters)
  (push `(,(player-clm-instrument v) ,time ,duration ,pitch ,volume ,@parameters)
        (player-note-list v)))

(cl-defmethod player-emit-event :around ((o orch) time duration pitch volume parameters)
  ;; don't emit note events that have no spectrum
  (if (mosaic-find-spectrum (orch-instrument o) pitch)
      (cl-call-next-method)
    (message "Mosaic warning: Failed to emit note event %S with no spectrum."
             (list (orch-instrument o) pitch))))

(cl-defmethod player-emit-cloud ((v player)
                                 ;; I wish EIEIO supported keyword arguments.
                                 ;; Note: I found out it does, via CL-DEFMETHOD.
                              pitch start-time duration volume num-segments
                              jitter overlap amp-env gliss-env glissando-amount)
  (let* ((time (or start-time (player-time v)))
         (segment-length (/ duration num-segments)))
    (dotimes (i num-segments)
      (player-emit-event v time (+ segment-length overlap) pitch volume
                         (if (= 0 i)
                             ;; send new note info with first block
                             (append `(:amp-env ',amp-env :full-duration ,duration)
                                     (player-note-parameters v duration pitch 1.0))
                           ()))
      (cl-incf time segment-length))))

(cl-defmethod player-play-note ((v player) start-time note note-symbol volume-symbol exp-symbol)
  (player-emit-event v
                     start-time ;; (player-time v)
                     (player-note-duration v note-symbol)
                     (player-note-pitch v note)
                     (player-volume v)
                     (player-note-parameters v
                                             (player-note-duration v note-symbol)
                                             (player-note-pitch v note)
                                             (player-volume v))))

(cl-defmethod player-change-expression ((p player) exp-symbol)
  (setf (player-expression p) exp-symbol))

(cl-defmethod player-play-note :before ((v player) start-time note note-symbol volume-symbol exp-symbol)
  (player-change-expression v exp-symbol)
  (if volume-symbol
      (setf (player-volume v)
            (player-note-volume v volume-symbol))
    (player-volume v)))

;; (cl-defmethod player-play-note ((o orch-cloud) note note-symbol volume-symbol exp-symbol)
;;   (let* ((duration (player-note-duration o note-symbol))
;;          (pitch (player-note-pitch o note))
;;          (fundamental-period (/ pitch mosaic-sample-rate))
;;          (segment-length (* 16.0 fundamental-period))
;;          (num-segments (floor (/ duration segment-length)))
;;          (overlap (* 0.25 segment-length)))
;;     (player-emit-cloud o
;;                        pitch
;;                        nil
;;                        duration
;;                        (player-volume o)
;;                        num-segments
;;                        0.0
;;                        0.0 ;; overlap (temporarily disabled)
;;                        '(0 0 0.25 1 0.75 1 1 0)
;;                        '(0 0 1 0)
;;                        0)))
  
(cl-defmethod player-play-sequence ((v player) sequence start-time)
  (dolist (event sequence)
    (if (= 1 (length event))
        ;; handle rests
        (player-advance-time v (player-rest-duration v (cl-first event)))
      (cl-destructuring-bind (note note-symbol volume-symbol exp-symbol) event
        (player-play-note v start-time note note-symbol volume-symbol exp-symbol)
        (let* ((len (player-note-duration v note-symbol))
               (len2 (if (player-legato-p v)
                         (- len (player-legato-time v))
                       len)))
          (player-advance-time v len2)
          (cl-incf start-time len2))))))

(cl-defmethod player-play-measure ((p player) measure)
  (cl-destructuring-bind (syms sequence) (cl-coerce measure 'list)
    ;; check for tempo and other conductor symbols
    (let ((tempo (cl-find-if #'numberp syms)))
      (when (numberp tempo)
        (setf (player-tempo p) tempo)))
    ;; now generate scheme.
    ;; first save the start time, which will be our mixing offset.
    (let ((start-time (player-time p)))
      ;; play at 0.0 so that this measure starts at the beginning of its audio file
      (player-play-sequence p sequence 0.0)
      ;; mix at correct position later
      (player-emit-mix-block p start-time))))

(cl-defmethod mosaic-tracker->players ((sheet mosaic-tracker))
  (setf mosaic-mix-sequence-name (mosaic-sheet-name sheet))
  (with-current-buffer (cell-sheet-buffer sheet)
    (let ((classes (mosaic-tracker-find-voice-classes sheet)))
      (mosaic-voices->players (mosaic-tracker-collect-voices sheet) 0.0 classes))))

(cl-defmethod mosaic-tracker->section ((sheet mosaic-tracker))
  (setf mosaic-mix-sequence-name (mosaic-sheet-name sheet))
  (with-current-buffer (cell-sheet-buffer sheet)
    (let ((classes (mosaic-tracker-find-voice-classes sheet))
          (voices (mosaic-tracker-collect-voices sheet)))
      (vector classes voices))))

(defun mosaic-section-classes (sec) (aref sec 0))
(defun mosaic-section-voices (sec) (aref sec 1))

(defun mosaic-section->players (section time)
  (mosaic-voices->players (mosaic-section-voices section)
                          time
                          (mosaic-section-classes section)))

(cl-defun mosaic-voices->players (voices &optional (time 0.0) (class-or-classes 'violin))
  (setf mosaic-mix-number 0)
  (let ((classes (if (and (not (null class-or-classes))
                          (symbolp class-or-classes))
                     (make-list (length voices) class-or-classes)
                   class-or-classes)))
    (let ((num-voices (length voices))
          (voices^ (copy-tree voices :vec))
          ;; set up N players of the correct classes
          (players (mapcar #'make-instance classes)))
      ;; sync up everyone at TIME
      (player-sync-players players time)
      ;; play each measure from all N players until we run out of measures
      (while (not (cl-every #'null voices^))
        (dotimes (n num-voices)
          (let ((measure (pop (nth n voices^)))
                (v (nth n players)))
            (player-play-measure v measure)))
        ;; sync all players to current time at end of each measure
        (player-sync-players players))
      ;; return completed players
      players)))

(defun mosaic-append-voices (a b)
  (cl-map 'list #'append a b))

(defvar mosaic-after-render-forms ())

(defun mosaic-offset-note-list (note-list offset)
  (mapcar #'(lambda (note)
              (append (list (cl-first note))
                      (list (+ offset (cl-second note)))
                      (cl-rest (cl-rest note))))
          note-list))

(defun mosaic-postprocess-note-list (note-list)
  (mapcar #'(lambda (note)
              (append (list (cl-first note))
                      (list (max 0.0 (+ (cl-second note)
                                        ;; TODO move the random call to the scheme side.
                                        (random^ 0.006))))
                      (cl-rest (cl-rest note))))
          note-list))

(cl-defun mosaic-players->scheme (players &optional (file (mosaic-output-file)))
  (let ((mix-blocks (apply #'append
                           (mapcar #'reverse
                                   (mapcar #'player-mix-blocks players))))
        (render-forms ())
        (mix-forms ()))
    (dolist (b (cl-sort mix-blocks #'< :key #'cl-first))
      (cl-destructuring-bind (mix-number note-list start-time) b
        (if (mosaic-match-mix-data note-list mix-number)
            (message (format "Note list unchanged for mix block %s, skipping recomputation." mix-number))
          (progn
            (message (format "Recomputing mix block %s for start-time %f." mix-number start-time))
            ;; emit note list code if it doesn't match data file
            (push `(with-sound (:output ,(mosaic-mix-file mix-number)
                                        :channels 2
                                        :reverb nrev*
                                        :header-type mus-riff
                                        :sample-type mus-lshort)
                               ,@(mosaic-postprocess-note-list note-list))
                  render-forms)
            ;; print message and write timestamps from Scheme in between renders
            (push `(begin
                    (display ,(format "Finished rendering block %s.\n" mix-number))
                    (write-timestamp-file ,mix-number))
                  render-forms)
            ;; set up Emacs to write updated data files after completion
            (push `(with-temp-buffer
                     (fundamental-mode)
                     (insert (prin1-to-string ',note-list))
                     (mosaic-write-file (mosaic-mix-data-file ,mix-number)))
                  mosaic-after-render-forms)))
        ;; either way, mix the audio file
        ;; ---> mus-file-mix outfile infile (outloc 0) (framples) (inloc 0) mixer envs
        (push `(mus-file-mix *output* ,(mosaic-mix-file mix-number)
                             (seconds->samples ,start-time)
                             ;; trim last second to remove any clicks (this doesn't work...)
                             (- (framples ,(mosaic-mix-file mix-number))
                                ,mosaic-sample-rate)
                             )
              mix-forms)))
    `(begin
      (display ,(format "SchemeMosaic: Rendering %d sequence blocks...\n"
                        (/ (length render-forms) 2)))
      ,@(reverse render-forms)
      (display "Mixing results...\n")
      (with-sound (:output ,file
                           :channels 2
                           :header-type mus-riff
                           :sample-type mus-lshort
                           :scaled-to 0.8)
                  ,@(reverse mix-forms)))))

(defun mosaic-note-sequence-p (x)
  (and (consp x)
       (consp (cl-first x))
       (symbolp (cl-first (cl-first x)))))

(defun mosaic-note-sequences-p (x)
  (and (consp x)
       (cl-every #'mosaic-note-sequence-p x)))

(defun mosaic-normalize-sequences (x)
  (cond ((mosaic-note-sequence-p x)
         (list x))
        ((mosaic-note-sequences-p x)
         x)))

(defun mosaic-mix-sequences (a b)
  (append (mosaic-normalize-sequences a)
          (mosaic-normalize-sequences b)))

(defun mosaic-append-sequences (a b)
  (let ((c (mosaic-normalize-sequences a))
        (d (mosaic-normalize-sequences b)))
    (cl-map 'list #'append c d)))

(defun mosaic-repeat-sequence (s n)
  (let ((result s))
    (dotimes (i (1- n))
      (setf result (mosaic-append-sequences result s)))
    result))

(defun mosaic-transpose-sequence (sequence delta)
  (mapcar (lambda (event)
            (if (= 1 (length event))
                event
              (cl-destructuring-bind (note dur vol exp) event
                (list (mosaic-transpose-note note delta)
                      dur vol exp))))
          sequence))

(cl-defmethod mosaic-tracker-run ((sheet mosaic-tracker))
  "Render the current tracker."
  (mosaic-kill-thunks)
  (setf mosaic-after-render-forms nil)
  (cell-sheet-apply-settings sheet)
  (mosaic-tell-session-directory mosaic-selected-session-directory)
  (mosaic-tell-tempo)
  (with-current-buffer (cell-sheet-buffer sheet)
    (setf mosaic-mix-sequence-name (mosaic-sheet-name sheet))
    (let* ((players (mosaic-tracker->players sheet))
           (num-voices (length players))
           (file (mosaic-output-file))
           (scheme (mosaic-players->scheme players file)))
      (mosaic-clear-timestamps)
      (mosaic-tell-scheme-file `(begin ,scheme (display "Finalizing...\n") (write-timestamp-file) ,file))
      (lexical-let ((start-time (time-to-seconds (current-time))))
        (mosaic-wait-for-file (mosaic-timestamp-file)
          (let* ((end-time (time-to-seconds (current-time)))
                 (duration (- end-time start-time))
                 (text (format "Mosaic: Finished rendering sequence to %s in %s."
                               file
                               (format-seconds "%h hours, %m minutes, %s seconds" duration))))
            (save-window-excursion
              (ignore-errors (mosaic-notify-sound))
              (ignore-errors (mosaic-notify-popup text))
              (mosaic-clear-timestamps)
              (mosaic-show-scheme-dwim)
              (mosaic-set-status-icon :ok)
              (mosaic-update-header-line-in-all-sheets)
              ;; now that render has completed successfully, save any updated mix-block data
              (message "Mosaic: Saving updated note list data...")
              (mapc #'eval mosaic-after-render-forms)
              (message "Mosaic: Saving updated note list data... Done.")
              (setf mosaic-after-render-forms nil)
              (message text))))))))

(defun mosaic-render-tracker ()
  "Render the current tracker."
  (interactive)
  (save-window-excursion
    (mosaic-tracker-run cell-current-sheet)))

(provide 'mosaic-orch)
;;; mosaic-orch.el ends here
