;;; mosaic.el --- fun with sound in emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; License: GPLv3
;; Version: 1.0

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; (Version 3) as published by the Free Software Foundation, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program, in a text file called LICENSE. If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file "mosaic.el" is part of SchemeMosaic.

;; The included files "jack.el", "midi.el", "ladspa.el", "inf-snd.el",
;; and "ecasound.el" are contributed Emacs Lisp libraries,
;; redistributed here under their respective licenses as library
;; components of SchemeMosaic.  Check each file for full information.

;; Some functions and data in this file (and in the included file
;; `moorer-spectra.scm') are adapted from documentation and code in
;; the Snd editor.  These are marked as such in the comments that
;; precede them.  See the Snd website for full information.

;; On platforms where Ecasound and Snd are shipped alongside Mosaic,
;; full sources are included in the installation directory.

;; The FM violin and other string simulations are tuned using the
;; advice in Bill Schottstaedt's article "The Simulation of Natural
;; Instrument Tones Using Frequency Modulation with a Complex
;; Modulating Wave" in Computer Music Journal, vol.  1  no.  4, 1977,
;; pp.  46-50. JSTOR, www.jstor.org/stable/40731300. Special thanks to
;; Bill for further help and advice in 2019-2021 :)

;; The Emacs Lisp side of SchemeMosaic, called EmacsMosaic, is an
;; experimental object-oriented frontend to Kai Vehmanen's Ecasound
;; via ecasound.el.  It uses inf-snd.el from the Snd editor, jack.el
;; from Mario Lang and ecasound.el from Ecasound to bridge together
;; Emacs, Ecasound, Snd, and JACK for realtime exploratory
;; concatenative synthesis and improvisational looping performance.

;; In EmacsMosaic, a session consists of a set of `tracks' and some
;; audio configuration data (sampling rate, etc.)  A track consists of
;; an ecasound input specifier (or `iospec'), an ecasound output
;; iospec, a set of `operations' to be performed, and finally a
;; collection of `takes'.  A take identifies an individual audio file
;; in a series of recordings.  A `take-set' is the set of new takes
;; created when the engine is started in record-enabled mode (this
;; will be the null set if there aren't any record-enabled
;; tracks).  The audio files for the takes are stored in the session
;; directory, which serves as the name of the session.  A session may
;; be renamed by simply renaming the directory it lives in.

;; Behind the scenes, EmacsMosaic configures ecasound and jackd to match
;; the specified recording/playback/routing setup, manages the files
;; for various takes, and so on.

;; There is also a Snd-Scheme CLM music sequencer here built on
;; Cell-mode.  But it is not yet documented.

;;; Code:

(require 'cl-lib)

(defun mosaic-compile-and-load ()
  (interactive)
  (let ((syms
         '(uuidgen
           core
           plot
           chopper
           looper
           stomper
           tracker
           orchestra
           ribbon
           freesound
           extra
           data
           wizards
           beat
           hydrogen
           )))
    (cl-dolist (source syms)
      (load (format "%S.el" source)))))

(mosaic-compile-and-load)

(provide 'mosaic)
;; Local Variables:
;; indent-tabs-mode: nil
;; End:
;;; mosaic.el ends here
