;;; neural.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Loading, saving, and switching neural nets.

(define *net-variables*
  '(*nunits* *ninputs* *first-hidden* *nhidden* *first-output*
             *noutputs* *outputs* *error-sums* *errors*
             *nconnections* *connections* *weights*
             *delta-weights* *slopes* *prev-slopes*
             *weight-range* *sigmoid-prime-offset*
             *epsilon* *mu* *decay* *hyper-err*
             *split-epsilon* *symmetric*
             *input-zero-value* *input-one-value*
             *output-zero-value* *output-one-value*))

(define^ (mosaic-save-net file)
  (let ((forms ()))
    (map (lambda (var)
           (push! `(list ',var ,(eval var)) forms))
         *net-variables*)
    (sexp->file `(list ,@forms) file)))

(define^ (mosaic-read-net file)
  (format #t "Reading neural net from ~s\n" file)
  (let ((net (load file)))
    (format #t "Reading neural net from ~s... Done.\n" file)
    (eval net)))

(define^ (mosaic-select-net net)
  (format #t "Selecting new neural net...\n")
  (do ((n net (cdr n)))
      ((null? n))
    (let* ((var (car (car n)))
           (value (cadr (car n))))
      (eval `(set! ,var ',value))))
  (format #t "Selecting new neural net... Done.\n"))

(define *mosaic-blank* #f)

(define^ (blank.nn)
  (or *mosaic-blank*
      (set! *mosaic-blank*
            (mosaic-read-net (project-file "blank.nn")))))

(define *mosaic-percussion-3* #f)

(define^ (percussion-3.nn)
  (or *mosaic-percussion-3*
      (set! *mosaic-percussion-3*
            (mosaic-read-net (project-file "percussion-3.nn")))))

(define *mosaic-groove-16* #f)

(define^ (groove-16.nn)
  (or *mosaic-groove-16*
      (set! *mosaic-groove-16*
            (mosaic-read-net (project-file "groove-16.nn")))))

(define percussion-3-net percussion-3.nn)
(define groove-16-net groove-16.nn)

(define *mosaic-bb-3* #f)

(define^ (bb-3.nn)
  (or *mosaic-bb-3*
      (set! *mosaic-bb-3*
            (mosaic-read-net (project-file "bb-3.nn")))))

(define bb-3-net bb-3.nn)

(define^ (percussion-3/properties->features properties)
  (let* ((spectrum (cdr (assoc* 'spectrum properties)))
         (centroid (vector->list (cdr (assoc* 'centroid properties))))
         (freqs (vector->list (spectrum-freqs spectrum)))
         (normalized-freqs (map (lambda (f)
                                  (/ f 22050.0))
                                freqs))
         (normalized-centroid (map (lambda (f)
                                     (/ f 22050.0))
                                   centroid))
         (padded-centroid (let ((v (make-vector 256 0.0)))
                            (do ((n 0 (+ 1 n)))
                                ((or (= n (length v))
                                     (= n (length normalized-centroid))) v)
                              (vector-set! v n (normalized-centroid n)))))
         (amps (spectrum-amps spectrum))
         (features (vector-append (list->vector normalized-freqs)
                                  amps
                                  padded-centroid)))
    (assert (not (zero? (length normalized-freqs))))
    (assert (not (zero? (length normalized-centroid))))
    (assert (not (zero? (length amps))))
    features))

(define *net-inputs* ())

(define *net-outputs* ())

(define^ (learn! features outputs)
  (push! features *net-inputs*)
  (push! outputs *net-outputs*))

(define^ (mosaic-net-test-region region properties->features)
  (let ((features (properties->features (*region->properties* region))))
    (assert (vector? features))
    (assert (not (zero? (length features))))
    (assert (= *ninputs* (length features)))
    ;; (display (format #f "\n ~s Features ... ~s inputs\n"(length features) *ninputs*))
    (test-example features)))

(define^ (percussion-3-guess region)
  (let ((output (mosaic-net-test-region region percussion-3/properties->features)))
    (let* ((nums (map (lambda (n) (round (* n 10000)))
                      output))
           (largest (apply max nums)))
      (let ((sym (cond
                  ((= largest (nums 0)) 'kick)
                  ((= largest (nums 1)) 'snare)
                  ((= largest (nums 2)) 'cymbal))))
        sym))))

(define^ (percussion-3 region)
  (mosaic-net-test-region region percussion-3/properties->features))

(define^ (kick? region) (eq? 'kick (percussion-3-guess region)))
(define^ (snare? region) (eq? 'snare (percussion-3-guess region)))
(define^ (cymbal? region) (eq? 'cymbal (percussion-3-guess region)))
(define^ (confident-3? region)
  (< 0.35 (apply max (percussion-3 region))))

(define^ (percussion-3-guess-multiple region)
  (let ((output ()))
    (let* ((vec (percussion-3 region))
           (kick (vec 0))
           (snare (vec 1))
           (cymbal (vec 2)))
      (when (> kick 0.2)
        (push! 'kick output))
      (when (> snare 0.2)
        (push! 'snare output))
      (when (> cymbal 0.2)
        (push! 'cymbal output)))
    (reverse output)))

(define^ (mosaic-find-guesses file offsets)
  (let ((regions (mosaic-slice-on-offsets (open-sound file) offsets)))
    (map (lambda (region)
           (remember-region-properties '(spectrum centroid kick snare cymbal) region))
         regions)
    (map percussion-3-guess-multiple regions)))

(define^ (mosaic-tell-labels file offsets)
  (let ((beats (mosaic-find-guesses file offsets)))
    (mosaic-tell-emacs (map (lambda (beat)
                              (if (not (pair? beat))
                                  "."
                                  (apply append
                                         (map (lambda (b)
                                                (string ((format #f "~A" b) 0)))
                                              beat))))
                            beats))))

(define^ (percussion-3-from-properties properties)
  ;; (assert (assoc* 'kick properties))
  ;; (assert (assoc* 'snare properties))
  ;; (assert (assoc* 'cymbal properties))
  (if (not (assoc* 'kick properties))
      (list 0.0 0.0 0.0)
      (let ((result 
             (map (lambda (p)
                    (cdr (assoc* p properties)))
                  '(kick snare cymbal))))
        ;; (assert (= 3 (length result)))
        ;; (assert (number? (result 0)))
        ;; (assert (number? (result 1)))
        ;; (assert (number? (result 2)))
        (if (pair? (result 0))
            (map car result)
            result))))
  ;; (assert (= 3 (length result)))
  ;; (assert (number? (car result)))))

(define^ (percussion-3-guess-multiple-from-properties region (threshold 0.3))
  (show-progress "p3gmfp")
  (let ((output ()))
    (let* ((vec (list->vector (percussion-3-from-properties (*region->properties* region))))
           (kick (vec 0))
           (snare (vec 1))
           (cymbal (vec 2)))
      (when (> kick threshold)
        (push! 'kick output))
      (when (> snare threshold)
        (push! 'snare output))
      (when (> cymbal threshold)
        (push! 'cymbal output)))
    (reverse output)))

(define^ (percussion-3-guess-multiple-from-properties* region squelch)
  (percussion-3-guess-multiple-from-properties region squelch))

(define^ (percussion-3-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (assert (not (eq? #f ap)))
    (assert (not (eq? #f bp)))
    (assert (not (null? (assoc* 'kick ap))))
    (assert (not (null? (assoc* 'kick bp))))
    (euclidean-distance (percussion-3-from-properties ap)
                        (percussion-3-from-properties bp))))

(define^ (percussion-3-distance* a b)
  (+ (percussion-3-distance* a b)
     (* 0.5 (total-energy-distance a b))))

(define^ (mosaic-match-beats regions (squelch 0.9))
  (let ((guesses (map (lambda (r)
                        (percussion-3-guess-multiple-from-properties* r squelch))
                      regions))
        (uuids (map car *uuid->properties*)))
    (map (lambda (region)
           (lazy-region (try-match region spectrum-distance uuids)))
         regions)))

(define^ (mosaic-beatbox-resynth-with-samples file offsets stretch-tails net-file kick-files snare-files cymbal-files quantize)
  (let ((sound (open-sound file)))
    (if net-file
        (mosaic-select-net (mosaic-read-net net-file))
        (mosaic-select-net (percussion-3.nn)))
    (let* ((regions (mosaic-slice-on-offsets sound offsets))
           (processed-regions (map (lambda (region)
                                     (remember-region-properties '(spectrum centroid kick snare cymbal) region))
                                   regions)))
      (show-progress "resynth-with-samples")
      (jam-16/resynth-multiple-on-offsets (map *region->properties* regions)
                                          regions
                                          offsets
                                          kick-files snare-files cymbal-files
                                          stretch-tails
                                          quantize))))
      
(define^ (mosaic-beatbox-resynth file offsets database stretch-tails net-file quantize)
  (let ((sound (open-sound file)))
    ;; we can use a different net to detect the beats than we use for resynth. see below.
    (if net-file
        (mosaic-select-net (mosaic-read-net net-file))
        (mosaic-select-net (percussion-3.nn)))
    (let* ((regions (mosaic-slice-on-offsets sound offsets))
           (processed-regions (map (lambda (region)
                                     (remember-region-properties '(spectrum centroid kick snare cymbal) region))
                                   regions)))
      (show-progress "resynth-2")
      ;; no matter what net file was used for the above detection, the
      ;; properties are now stored, so we can switch to a different
      ;; network for the database search and compare apples to
      ;; oranges. Nice!
      (mosaic-select-net (percussion-3.nn))
    (when database (mosaic-load-database database))
      (let* ((beats (mosaic-match-beats regions)) ;; fix this!
             (total-length (framples sound))
             (aligned-beats (mosaic-trim-regions beats
                                                 offsets
                                                 total-length
                                                 stretch-tails
                                                 ))
             (output-sound (new-sound :channels 2 :file (session-file "mosaic-output.wav"))))
        (merge-sound-as-vectors output-sound aligned-beats)
        (save-sound-as (mosaic-output-file) output-sound :header-type mus-riff :sample-type mus-lshort)))))
        
(define^ (find-percussion-3-structure regions
                                      (show? #f)
                                      (properties-fn percussion-3-guesses-only))
  (find-structure
   (properties-fn
    (map *region->properties* regions))
   :show? show?))

(define^ (find-note-structure regions
                                      (show? #f)
                                      (properties-fn note-lists-only))
  (find-structure
   (properties-fn
    (map *region->properties* regions))
   :show? show?))

(define^ (test-structure file)
  (let ((regions (split-sound-to-regions 0.25 (open-sound file))))
    (assert (pair? regions))
    (map (lambda (r)
           (assert (pair? r))
           (assert (float-vector? (car r)))
           (assert (float-vector? (cadr r))))
         regions)
    (map (lambda (r)
           (remember-region-properties '(spectrum centroid note percussion-3-guess-multiple kick snare cymbal) r))
         regions)
    (find-percussion-3-structure regions)))

(provide 'neural.scm)
;;; neural.scm ends here
