;;; groove-16.scm                         -*- Lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define^ (groove-16/phrase->features phrase)
  (assert (pair? phrase))
  ;;(assert (= 16 (length phrase)))
  (assert (descriptor? (car phrase)))
  (apply append 
         (map (lambda (sym)
                (let ((kick (cdr (assoc* 'kick sym)))
                      (snare (cdr (assoc* 'snare sym)))
                      (cymbal (cdr (assoc* 'cymbal sym))))
                  (if (< (max kick snare cymbal) 0.9)
                      (vector 0.0 0.0 0.0)
                      (vector kick snare cymbal))))
              phrase)))

(define^ (groove-16/split-phrases sequence (len 16))
  (assert (pair? sequence))
  (assert (descriptor? (car sequence)))
  (let ((phrases ()))
    (do ((n 0 (+ 1 n)))
        ((null? sequence) (reverse! phrases))
      (if (>= (length sequence) len)
          (begin
            (push! (subsequence sequence 0 len)
                   phrases)
            (set! sequence (subsequence sequence len)))
          (set! sequence ())))))

(define^ (delete* element mylist test)
  (if (null? mylist)
      ()
      (if (test element (car mylist))
          (cdr mylist)
          (cons (car mylist)
                (delete* element (cdr mylist) test)))))

(define^ (groove-16/learn-phrase phrase)
  (push! (groove-16/phrase->features phrase)
         *net-inputs*)
  (push! (vector 1.0)
         *net-outputs*))

(define^ (groove-16/learn-noise)
  (let ((vec (make-vector (* 16 3))))
    (do ((n 0 (+ 1 n)))
        ((= n (length vec)))
      (set! (vec n) (random 1.0)))
    (push! vec *net-inputs*)
    (push! (vector 0.0) *net-outputs*)))

(define *groove-16/test-grammar* ())

(define^ (groove-16/learn-grammar grammar)
  (map groove-16/learn-phrase
       (groove-16/split-phrases
        (generate-phrase grammar (find-axiom grammar)))))

(define^ (groove-16/train!)
  (let* ((X (length (car *net-inputs*)))
         (Y 64))
    (build-classifier X Y 1)
    (set! *training-inputs* (list->vector *net-inputs*))
    (set! *training-outputs* (list->vector *net-outputs*))
    (set! *restart* #f)
    (train-test 1 100 1)))

(define *test-regions* ())

(define^ (load-test-regions)
  (let ((regions (split-sound-to-regions *mosaic-slice-size* (open-sound (session-file "theta.wav")))))
    (map (lambda (region)
           (remember-region-properties '(spectrum centroid percussion-3-guess kick snare cymbal)
                                       region))
         regions)
    (set! *test-regions* regions)))

(define^ (groove-16/build-net)
  (set! *mosaic-beats-per-minute* 140.0)
  (set! *mosaic-slice-size* (sixteenth-note))
  (mosaic-select-net (percussion-3.nn))
  (when (null? *test-regions*)
    (load-test-regions))
  (let ((regions *test-regions*))
    (let ((sequence (map *region->properties* regions)))
      (map groove-16/learn-phrase
           (groove-16/split-phrases sequence))
      (do ((n 0 (+ 1 n)))
          ((= n 300))
        (groove-16/learn-noise))
      (groove-16/train!)
      (mosaic-save-net "/home/dto/mosaic/breaks-16.nn"))))

(define^ (groove-16/test-3)
  (do ((i 0 (+ 1 i)))
      ((= i 10000))
    (let ((vec (make-vector (* 16 3))))
      (do ((n 0 (+ 1 n)))
          ((= n (length vec)))
        (set! (vec n) (random 1.0)))
      (let ((val (car (test-example vec))))
        (when (> val 0.5)
          (display "WOW"))
        (display val)
        (display "\n")))))

;;; Jam!

(define^ (jam-16/noise-1 (len 16))
  (let ((vec (make-vector (* len 3))))
    (do ((n 0 (+ 1 n)))
        ((= n (length vec)))
      (set! (vec n) (random 1.0)))
    vec))

(define^ (jam-16/noise-2 (len 16))
  (define (noise)
    (let ((vec (make-vector 3 0.0)))
      (when (zero? (random 4))
        (set! (vec (random 3))
              (+ 0.85 (random 0.15))))
      vec))
  (apply append
         (let ((result ()))
           (do ((n 0 (+ 1 n)))
               ((= n len))
             (push! (noise) result))
           result)))
         
(define^ (jam-16/beat)
  (define (kick) (vector 1.0 0.0 0.0))
  (define (snare) (vector 0.0 1.0 0.0))
  (define (kicksnare) (vector 1.0 1.0 0.0))
  (define (cymbal) (vector 0.0 0.0 1.0))
  (define (rest) (vector 0.0 0.0 0.0))
  (append (random-choose (list (kick) (kicksnare)))
          (random-choose (list (cymbal) (rest)))
          (random-choose (list (kick) (rest) (cymbal) (cymbal) (snare)))
          (random-choose (list (rest) (rest) (cymbal)))))

(define^ (jam-16/beat-2)
  (define (kick) (vector 1.0 0.0 0.0))
  (define (snare) (vector 0.0 1.0 0.0))
  (define (kicksnare) (vector 1.0 1.0 0.0))
  (define (cymbal) (vector 0.0 0.0 1.0))
  (define (rest) (vector 0.0 0.0 0.0))
  (append (random-choose (list (kicksnare)))
          (random-choose (list (cymbal) (rest)))
          (random-choose (list (cymbal) (cymbal)))
          (random-choose (list (rest) (cymbal) (snare)))))

(define^ (jam-16/measure)
  (append (jam-16/beat)
          (jam-16/beat-2)
          (jam-16/beat)
          (jam-16/beat-2)))

(define^ (jam-16/learn-phrase phrase (len 16))
  (push! (jam-16/noise-1 len)
         *net-inputs*)
  (push! (groove-16/phrase->features phrase)
         *net-outputs*)
  (push! (jam-16/noise-2 len)
         *net-inputs*)
  (push! (groove-16/phrase->features phrase)
         *net-outputs*)
  (assert (= (length (*net-inputs* 0))
             (length (*net-outputs* 0)))))

(define^ (jam-16/learn-synthetic features)
  (push! (jam-16/noise-1)
         *net-inputs*)
  (push! features
         *net-outputs*)
  (push! (jam-16/noise-1)
         *net-inputs*)
  (push! features
         *net-outputs*))

(define^ (jam-16/learn! (sequence #f) (len 16))
  (mosaic-select-net (percussion-3.nn))
  (when (null? *test-regions*)
    (load-test-regions))
  (let* ((regions *test-regions*)
         (sequence (or sequence (map *region->properties* *test-regions*)))
         (phrases (groove-16/split-phrases sequence len)))
    (do ((p phrases (cdr p)))
        ((null? p))
      (display "jam-16/learn-phrase")
      (jam-16/learn-phrase (car p) len))))

(define^ (jam-16/train! (len 16))
  (let* ((X (length (car *net-inputs*)))
         (Y (* len 2)))
    (build-encoder X Y)
    (set! *training-inputs* (list->vector *net-inputs*))
    (set! *training-outputs* (list->vector *net-outputs*))
    (set! *restart* #f)
    (train-test 1 100000 1)))

(define^ (jam-16/build-net)
  (mosaic-select-net (percussion-3.nn))
  (load-test-regions)
  (mosaic-select-net (blank.nn))
  ;; (set! *mu* 1.25)
  (jam-16/learn!)
  (jam-16/train! 16))

(define^ (jam-16/build-net-synthetic)
  (mosaic-select-net (percussion-3.nn))
  (do ((n 0 (+ n 1)))
      ((= n 64))
    (jam-16/learn-synthetic (jam-16/measure)))
  (jam-16/train! 16))

(define^ (jam-16/build-net-with-loops (len 16))
  (set! *mosaic-beats-per-minute* 140)
  (set! *mosaic-slice-size* (sixteenth-note))
  (mosaic-select-net (percussion-3.nn))
  (let ((all-files (directory->list "/home/dto/sessions/breaks.mosaic/"))
        (found-files ()))
    (do ((files all-files (cdr files)))
        ((null? files) found-files)
      (let* ((file (car files))
             (pos (or (string-position ".wav" file)
                      (string-position ".WAV" file))))
        (when (number? pos)
          (push! file found-files))))
    (display (list 'found-files (length found-files)))
    (let ((all-regions
           (map (lambda (r)
                  (normalize-region r 0.99))
                (apply append 
                       (map (lambda (f)
                              (let ((sound (open-sound (append "/home/dto/sessions/breaks.mosaic/" f))))
                                (let ((regions (split-sound-to-regions *mosaic-slice-size* sound)))
                                  (close-sound sound)
                                  regions)))
                            (subsequence found-files 0 200))))))
      (map (lambda (region)
             (remember-region-properties '(spectrum centroid kick snare cymbal) region))
           all-regions)
      (display (list 'all-regions (length all-regions)))
      (let ((sequence (map percussion-3-scalars-only (map *region->properties* all-regions))))
        (jam-16/learn! sequence len))))
  (mosaic-select-net (blank.nn))
  ;;(set! *mu* 1.75)
  (jam-16/train! len))

(define^ (jam-16/save-net)
  (mosaic-save-net "/home/dto/mosaic/breaks-16.nn"))

(define^ (jam-16/features->phrase features)
  (let ((fs features)
        (phrase ()))
    (do ((f fs))
        ((null? f) (reverse phrase))
      (let* ((kick (pop! f))
             (snare (pop! f))
             (cymbal (pop! f)))
        (push! `((kick . ,kick)
                 (snare . ,snare)
                 (cymbal . ,cymbal))
               phrase)))))

(define^ (jam-16/phrase regions)
  (map percussion-3-scalars-only-noisy (map *region->properties* regions)))

(define^ (percussion-3-scalars-from-uuid uuid)
  (let ((ps (*uuid->properties* uuid)))
    (list (cdr (assoc* 'kick ps))
          (cdr (assoc* 'snare ps))
          (cdr (assoc* 'cymbal ps)))))

(define^ (jam-16/resynth (phrase #f))
  (mosaic-load-database "/home/dto/sessions/percussion-3.mosaic")
  (let ((uuids (map car *uuid->properties*))
        (regions ()))
    (do ((p phrase (cdr p)))
        ((null? p))
      (let ((s (silent-region (list (make-float-vector 5200) (make-float-vector 5200)))))
        (push! s regions)
        (set! (*region->properties* s) (car p))))
    (let ((output (map (lambda (region)
                         (let ((beat (try-match region percussion-3-distance uuids)))
                           (if (> (apply max (percussion-3-scalars-from-uuid beat)) 0.97)
                               beat
                               (silent-region (list (make-float-vector 1000)
                                                    (make-float-vector 1000))))))
                       (reverse regions)))
          (sound (new-sound :channels 2)))
      (merge-sound-as-vectors sound (stretch-match-regions regions (map lazy-region output)))
      (save-sound-as (session-file "test.wav") sound :header-type mus-riff :sample-type mus-lshort))))

(define^ (mix-reduce regions)
  (if (or (eq? #f regions)
          (null? regions))
      (list (make-float-vector 1000 0.0)
            (make-float-vector 1000 0.0))
      (mix-regions* (lazy-region (car regions))
                    (mix-reduce (cdr regions)))))

(define^ (no-perc)
  (let ((silence (list (make-float-vector 100 0.0)
                       (make-float-vector 100 0.0))))
    (set! (*region->properties* silence)
          '((kick . 0.0) (snare . 0.0) (cymbal . 0.0)))
    silence))

(define^ (jam-16/find-drum-region region uuids kick-base snare-base cymbal-base kick-noise snare-noise cymbal-noise squelch)
  (let ((guesses (percussion-3-guess-multiple-from-properties* region squelch)))
    (let ((regions 
           (map (lambda (guess)
                  (let ((properties 
                         (cond
                          ((if (zero? (random 3))
                               (eq? 'snare guess)
                               (eq? 'cymbal guess))
                           `((cymbal . ,(+ snare-base (random cymbal-noise)))
                             (kick . ,(+ 0.0 (random 0.2)))
                             (snare . ,(+ 0.0 (random 0.2)))))
                          ((eq? 'snare guess)
                           `((snare . ,(+ cymbal-base (random snare-noise)))
                             (cymbal . ,(+ 0.2 (random 0.2)))
                             (kick . ,(+ 0.2 (random 0.2)))))
                          ((eq? 'kick guess)
                           `((kick . ,(+ kick-base (random kick-noise)))
                             (snare . ,(+ 0.1 (random 0.2)))
                             (cymbal . ,(+ 0.1 (random 0.2)))))
                          (#t 'none))))
                    (if (not (eq? 'none properties))
                        (try-match-properties properties percussion-3-distance uuids)
                        (no-perc))))
                guesses)))
      (mix-reduce regions))))

(define *mosaic-hydrogen-patterns* ())
(define *mosaic-hydrogen-notes* ())

(define (mosaic-reset-patterns)
  (set! *mosaic-hydrogen-patterns* ()))

(define (mosaic-reset-notes)
  (set! *mosaic-hydrogen-notes* ()))

(define (mosaic-push-notes)
  (push! (reverse *mosaic-hydrogen-notes*)
         *mosaic-hydrogen-patterns*))

(define^ (jam-16/find-drum-region-sampled region kicks snares cymbals squelch)
  (let ((guesses (percussion-3-guess-multiple-from-properties* region squelch))
        (samples ()))
    (if (pair? guesses)
        (let ((regions
               (map (lambda (r)
                      (scale-region r 0.8))
                    (map (lambda (guess)
                           (let ((sample
                                  (cond ((eq? 'kick guess)
                                         (random-choose kicks))
                                        ((eq? 'snare guess)
                                         (random-choose (list (random-choose snares)
                                                              (random-choose snares)
                                                              (random-choose kicks))))
                                        ((eq? 'cymbal guess)
                                         (random-choose cymbals)))))
                             (push! sample samples)
                             sample))
                         guesses))))
          ;; save notes
          (push! (map *region->filename* samples)
                 *mosaic-hydrogen-notes*)
          ;; return mixed drums
          (mix-reduce regions))
        (begin
          ;; here we use "nil" because this data is going back to Emacs
          (push! 'nil *mosaic-hydrogen-notes*)
          (list (make-float-vector 100 0.0)
                (make-float-vector 100 0.0))))))

(define^ (quantize-offsets offsets divisor total-length)
  (let ((beat-length (round (/ total-length divisor))))
    (map (lambda (offset)
           (* beat-length (round (/ offset beat-length))))
         offsets)))

(define^ (quantize-regions offsets regions)
  ;; after using quantize-offsets, some beats may have the same onset.
  ;; we need to discard any duplicates in the list of offsets, and
  ;; also discard the corresponding regions from that list.
  (let ((result-offsets ())
        (result-regions ())
        (last-offset #f))
    (do ((os offsets (cdr os)))
        ((null? os)
         (list (reverse! result-offsets)
               (reverse! result-regions)))
      (if (not (number? last-offset))
          (begin
            (push! (car os) result-offsets)
            (push! (pop! regions) result-regions)
            (set! last-offset (car os)))
          (if (= last-offset (car os))
              ;; discard region with duplicate offset
              (begin
                (pop! regions)
                (display (list "Discarded 1 region at offset" (car os) "\n")))
              (begin
                (push! (car os) result-offsets)
                (push! (pop! regions) result-regions)
                (set! last-offset (car os))))))))

(define *region->filename* #f)

(define (mosaic-reset-region->filename)
  (set! *region->filename* (make-hash-table 64 eq?)))

(define^ (jam-16/resynth-multiple-on-offsets
          (phrase #f)
          regions offsets
          kick-files snare-files cymbal-files
          stretch-tails
          quantize)
  (show-progress "resynth-multiple-on-offsets")
  (let ((kf (when (pair? kick-files) (map session-file kick-files)))
        (sf (when (pair? snare-files) (map session-file snare-files)))
        (cf (when (pair? cymbal-files) (map session-file cymbal-files))))
    (let ((kick-sounds (when (pair? kick-files) (map open-sound kf)))
          (snare-sounds (when (pair? snare-files) (map open-sound sf)))
          (cymbal-sounds (when (pair? cymbal-files) (map open-sound cf))))
      (let ((kicks (when (pair? kick-files) (map sound->region! kick-sounds)))
            (snares (when (pair? snare-files) (map sound->region! snare-sounds)))
            (cymbals (when (pair? cymbal-files) (map sound->region! cymbal-sounds))))
        (let ((output (map (lambda (r)
                             (jam-16/find-drum-region-sampled r kicks snares cymbals 0.4))
                           regions)))
          (let* ((total-length (apply + (map framples* regions)))
                 (quantized-results (if (number? quantize)
                                        (quantize-regions
                                         (quantize-offsets offsets quantize total-length)
                                         output)
                                        (list offsets output)))
                 (quantized-offsets (car quantized-results))
                 (quantized-regions (cadr quantized-results))
                 (sound (new-sound :channels 2)))
            (show-progress (list quantized-offsets 'len (length quantized-offsets) 'olen (length output)))
            (merge-sound-as-vectors sound (mosaic-trim-regions (map lazy-region quantized-regions)
                                                               quantized-offsets
                                                               total-length
                                                               stretch-tails))
            (save-sound-as (mosaic-output-file) sound :header-type mus-riff :sample-type mus-lshort)))))))

(define^ (jam-16/resynth-multiple (phrase #f) (tempo 120.0) (times 4)
                                  kick-files snare-files cymbal-files
                                  (squelch 0.91) (stretch-tails #t))
  (show-progress "resynth-multiple")
  (mosaic-reset-patterns)
  (mosaic-reset-region->filename)
  (mosaic-load-database "/home/dto/sessions/percussion-3.mosaic")
  (let ((uuids (map car *uuid->properties*))
        (regions ())
        (len (floor (/ (* 44100.0 (/ 60.0 tempo)) 4.0))))
    (let ((kf (when (pair? kick-files) (map session-file kick-files)))
          (sf (when (pair? snare-files) (map session-file snare-files)))
          (cf (when (pair? cymbal-files) (map session-file cymbal-files))))
      (let ((kick-sounds (when (pair? kick-files) (map open-sound kf)))
            (snare-sounds (when (pair? snare-files) (map open-sound sf)))
            (cymbal-sounds (when (pair? cymbal-files) (map open-sound cf))))
        (let ((kicks (when (pair? kick-files) (map sound->region! kick-sounds)))
              (snares (when (pair? snare-files) (map sound->region! snare-sounds)))
              (cymbals (when (pair? cymbal-files) (map sound->region! cymbal-sounds))))
          ;; keep track of which samples go with which files
          (do ((regions (append kicks snares cymbals) (cdr regions))
               (files (append kf sf cf) (cdr files)))
              ((null? files))
            (set! (*region->filename* (car regions))
                  (car files)))
          ;; store properties
          (do ((p phrase (cdr p)))
              ((null? p))
            (let ((s (silent-region (list (make-float-vector len) (make-float-vector len)))))
              (push! s regions)
              (set! (*region->properties* s) (car p))))
          ;; build up output
          (let ((output (let ((ms ()))
                          (do ((n 0 (+ 1 n)))
                              ((= n times))
                            (mosaic-reset-notes)
                            (let ((u (if (pair? kick-files)
                                         (map (lambda (r)
                                                (jam-16/find-drum-region-sampled r kicks snares cymbals squelch))
                                              regions)
                                         (map (lambda (r)
                                                (jam-16/find-drum-region r uuids
                                                                         (+ (random 0.4) 0.40)
                                                                         (+ (random 0.4) 0.30)
                                                                         (+ (random 0.4) 0.20)
                                                                         0.18 0.18 0.18
                                                                         squelch
                                                                         ))
                                              regions))))
                              (mosaic-push-notes)
                              (push! u ms)
                              (push! u ms)
                              (push! u ms)
                              (push! u ms)))
                          (apply append ms)))
                (sound (new-sound :channels 2)))
            (let ((offsets ()))
              (do ((n 0 (+ n 1)))
                  ((= n (length output)))
                (push! (* n len) offsets))
              (merge-sound-as-vectors sound (mosaic-trim-regions (map lazy-region output)
                                                                 (reverse offsets)
                                                                 (* len (length output))
                                                                 stretch-tails))
              (save-sound-as (mosaic-output-file) sound :header-type mus-riff :sample-type mus-lshort))))))))
  
(define^ (jam-16/test-jam-3)
  (mosaic-select-net (percussion-3.nn))
  (load-test-regions)
  (jam-16/resynth (jam-16/phrase *test-regions*)))

(define^ (jam-16/test-jam-2)
  (mosaic-select-net (mosaic-read-net (project-file "jam-16.nn")))
  (jam-16/features->phrase (test-example (jam-16/noise-1))))

(define^ (jam-16/test-jam-4)
  (jam-16/resynth (jam-16/test-jam-2)))

(define^ (jam-16/test-jam-5)
  (jam-16/resynth-multiple (jam-16/test-jam-2) 130.0))

;;; Jam 32

(define^ (jam-32/build-net-with-loops)
  (jam-16/build-net-with-loops 32))

(define^ (jam-32/save-net)
  (mosaic-save-net "/home/dto/mosaic/jam-32.nn"))

(define^ (jam-32/test-net)
  (mosaic-select-net (mosaic-read-net (project-file "jam-32.nn")))
  (test-example (jam-16/noise-1 32)))

(define^ (jam-16/test-phrase)
  (mosaic-select-net (mosaic-read-net (project-file "jam-32.nn")))
  (jam-16/features->phrase (test-example (jam-16/noise-1 32))))

(define^ (jam-16/test-resynth)
  (jam-16/resynth-multiple (jam-16/test-phrase) 130.0))

(define^ (mosaic-tell-patterns)
  (mosaic-tell-emacs (reverse *mosaic-hydrogen-patterns*)))
