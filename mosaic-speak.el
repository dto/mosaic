(require 'cl-lib)
(cl-declaim  (optimize  (safety 0) (speed 3)))
(require 'emacspeak-preamble)

(defun cell-speak-value (&optional row col message)
  (interactive)
  (cell-with-current-cell-sheet
   (let ((r (or row cursor-row))
         (c (or col cursor-column)))
     (let ((cell (cell-grid-get grid r c)))
       (if cell
           (dtk-speak (concat (or message "") " " (cell-value cell)))
         (progn (when message (dtk-speak message))
                (emacspeak-auditory-icon 'item)))))))

(global-set-key [(control c) (s)] 'cell-speak-value)

(defvar es/cell-old-row 0)
(defvar es/cell-old-column 0)

(cl-defun es/before/cell-sheet-move-cursor (direction &optional (distance 1))
  (cell-with-current-cell-sheet
   (setf es/cell-old-column cursor-column)
   (setf es/cell-old-row cursor-row)))

(cl-defun es/cell-sheet-move-cursor (direction &optional (distance 1))
  (interactive)
  (cell-with-current-cell-sheet
   (let* ((rows (cell-grid-rows grid))
          (cols (cell-grid-columns grid))
          (text 
           (cl-case direction
             (:up (when (minusp (- cursor-row distance))
                    "Top edge of buffer"))
             (:left (when (minusp (- cursor-column distance))
                      "Left edge of buffer"))
             (:down (unless (< cursor-row (- rows distance))
                      "Bottom edge of buffer"))
             (:right (unless (< cursor-column (- cols distance))
                       "Right edge of buffer")))))
     (if (null text)
         (cell-speak-value cursor-row cursor-column)
       (if (and (= es/cell-old-row cursor-row)
                (= es/cell-old-column cursor-column))
           ;; we didn't move
           (cell-speak-value es/cell-old-row es/cell-old-column text)
         ;; we moved
         (cell-speak-value cursor-row cursor-column))))))

(advice-add 'cell-sheet-move-cursor :before 'es/before/cell-sheet-move-cursor)
(advice-add 'cell-sheet-move-cursor :after 'es/cell-sheet-move-cursor)

(defun cell-speak-position ()
  (interactive)
  (cell-with-current-cell-sheet
   (dtk-speak (format "Currently at row %d column %d" cursor-row cursor-column))))

(global-set-key [(control c) (p)] 'cell-speak-position)

(defun es/cell-sheet-page-up ()
  (emacspeak-auditory-icon 'large-movement))

(defun es/cell-sheet-page-down ()
  (emacspeak-auditory-icon 'large-movement))

(advice-add 'cell-sheet-page-up :before 'es/cell-sheet-page-up)
(advice-add 'cell-sheet-page-down :before 'es/cell-sheet-page-down)

(defun es/cell-sheet-set-mark ()
  (emacspeak-auditory-icon 'mark-object))

(advice-add 'cell-sheet-set-mark :before 'es/cell-sheet-set-mark)

(defun es/cell-sheet-clear-mark ()
  (emacspeak-auditory-icon 'deselect-object))

(advice-add 'cell-sheet-clear-mark* :before 'es/cell-sheet-clear-mark)

(defun es/cell-sheet-paste ()
  (emacspeak-auditory-icon 'yank-object))

(advice-add 'cell-sheet-paste :before 'es/cell-sheet-paste)

(defun es/cell-sheet-create-cell ()
  (cell-speak-value))

(advice-add 'cell-sheet-create-cell :after 'es/cell-sheet-create-cell)
