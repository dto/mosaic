;;; mosaic-tracker.el --- sequencer                  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Mosaic Snd/CLM tracker in extensible equal temperament system

(defvar mosaic-instrument 'violin)
(make-variable-buffer-local 'mosaic-instrument)

(defclass mosaic-tracker (mosaic-chopper)
  ((voices :initform nil :initarg :voices :accessor mosaic-tracker-voices)
   (scheme :initform nil :initarg :scheme :accessor mosaic-tracker-scheme)))

(defun-memo mosaic-note-accidental (note) ()
  "Return a character giving the pitch class accidental for NOTE.
Because of the way pitch classes are handled in Mosaic, only
the standard 12-TET pitches are discriminated in this function.
To find a note's extended pitch class, use the function
`mosaic-note-extended-pitch-class'."
  (let ((s (symbol-name note)))
    (cond ((cl-position ?♭ s) ?♭)
          ((cl-position ?♮ s) ?♮)
          ((cl-position ?♯ s) ?♯)
          ;; return natural for quarter/sixth tones,
          ;; calculate actual quarter/sixth tone pitch elsewhere;
          ;; see also `mosaic-note-comma'.
          ((cl-position ?𝄫 s) ?♮)
          ((cl-position ?𝄪 s) ?♮)
          ((cl-position ?𝄳 s) ?♮)
          ((cl-position ?𝄲 s) ?♮)
          ((cl-position ?𝄰 s) ?♮)
          ((cl-position ?𝄭 s) ?♮)
          (t ?♮))))

(defun-memo mosaic-note-name (note) ()
  "Give the note name of NOTE without accidental or octave."
  (downcase (aref (symbol-name note) 0)))

(defvar mosaic-note-regexp "[abcdefg][♭𝄳𝄫𝄭♮𝄰𝄪𝄲♯]*\\([0-9]?\\)"
  "Regular expression matching note symbol names.")

(defun mosaic-note-p (note)
  "Return non-nil if NOTE is a note symbol."
  (save-match-data
    (string-match mosaic-note-regexp (symbol-name note))))

(defvar mosaic-default-octave-number 4)

(defun-memo mosaic-note-octave (note) ()
  "Return the number of NOTE's octave."
  (let ((s (symbol-name note)))
    (if (string-match mosaic-note-regexp s)
        (car (read-from-string (substring-no-properties (match-string 1 s))))
      mosaic-default-octave-number)))

(defun-memo mosaic-note-without-octave (note) ()
  "Return the NOTE and its accidental, without its octave."
  (intern (substring (symbol-name note) 0 2)))

(defun-memo mosaic-note-pitch-class (note) ()
  "Return the 12-TET pitch class of NOTE.
For extended pitch classes, use `mosaic-note-extended-pitch-class'."
  (let ((entry (assoc (concat (make-string 1 (mosaic-note-name note))
                              (make-string 1 (mosaic-note-accidental note)))
                      '(("c♮" . 0)
                        ("c♯" . 1)
                        ("d♭" . 1)
                        ("d♮" . 2)
                        ("d♯" . 3)
                        ("e♭" . 3)
                        ("e♮" . 4)
                        ("e♯" . 5)
                        ("f♭" . 4)
                        ("f♮" . 5)
                        ("f♯" . 6)
                        ("g♭" . 6)
                        ("g♮" . 7)
                        ("g♯" . 8)
                        ("a♭" . 8)
                        ("a♮" . 9)
                        ("a♯" . 10)
                        ("b♭" . 10)
                        ("b♮" . 11)
                        ("c♭" . 11)
                        ("b♯" . 12)))))
    (if entry
        (cdr entry)
      (prog1 nil
        (message (format "Mosaic warning: cannot parse pitch class for %s" note))))))

(defvar mosaic-octave-extended-pitch-classes 
  [(b♯ c♭) c𝄫 c𝄳 c𝄭 c♮ c𝄰 c𝄲 c𝄪 (c♯ d♭) d𝄫 d𝄳 d𝄭 d♮ d𝄰 d𝄲 d𝄪 (d♯ e♭) e𝄫 e𝄳
   e𝄭 e♮ e𝄰 e𝄲 e𝄪 (e♯ f♭) f𝄫 f𝄳 f𝄭 f♮ f𝄰 f𝄲 f𝄪 (f♯ g♭) g𝄫 g𝄳 g𝄭 g♮ g𝄰 g𝄲
   g𝄪 (g♯ a♭) a𝄫 a𝄳 a𝄭 a♮ a𝄰 a𝄲 a𝄪 (a♯ b♭) b𝄫 b𝄳 b𝄭 b♮ b𝄰 b𝄲 b𝄪])

(defun mosaic-note-extended-pitch-class (note)
  "Find the extended pitch class of NOTE."
  (let ((n 0))
    (cl-block searching
      (cl-loop for p across mosaic-octave-extended-pitch-classes
            do (let ((c (if (listp p) p (list p))))
                 (if (member (mosaic-note-without-octave note)
                             c)
                     (cl-return-from searching n)
                   (cl-incf n)))))))

(defun mosaic-transpose-note (note delta)
  "Transpose NOTE up or down by DELTA steps in extended pitch class."
  (let ((octave (mosaic-note-octave note))
        (pitch-class (mosaic-note-extended-pitch-class note)))
    (let ((c (+ pitch-class delta)))
      (when (> c (mosaic-note-extended-pitch-class 'b𝄪))
        (cl-incf octave))
      (when (cl-minusp c)
        (cl-decf octave))
      (let ((n (aref mosaic-octave-extended-pitch-classes
                     (mod c (length mosaic-octave-extended-pitch-classes)))))
        (intern (format "%s%d" (if (consp n) (symbol-name (cl-first n)) (symbol-name n)) octave))))))

(defun mosaic-note-number (note)
  "Find the MIDI note number of NOTE."
  (let ((octave (mosaic-note-octave note))
        (pitch-class (mosaic-note-pitch-class note)))
    (+ 12 ;; mosaic-transpose
     (* 12 octave)
     pitch-class)))

(defun mosaic-note-comma (note)
  "Find the pitch difference between NOTE and its 12-TET pitch class."
  (cond 
   ;; sixth tones 
   ((cl-position ?𝄭 (symbol-name note)) (* -2 (/ 1.0 6.0)))
   ((cl-position ?𝄰 (symbol-name note)) (* +2 (/ 1.0 6.0)))
   ;; third tones (two sixths)
   ((cl-position ?𝄫 (symbol-name note)) (* -2 (/ 1.0 3.0)))
   ((cl-position ?𝄪 (symbol-name note)) (* +2 (/ 1.0 3.0)))
   ;; quarter tones (three sixths)
   ((cl-position ?𝄳 (symbol-name note)) -0.5)
   ((cl-position ?𝄲 (symbol-name note)) +0.5)
   (t 0.0)))

(defcustom mosaic-a4-pitch 440.0 "Pitch (in Hz, cycles per second) of the A above middle C."
  :tag "A4 Pitch"
  :group 'mosaic
  :type 'number)

(defun mosaic-note->pitch (note)
  "Compute the pitch in Hz (cycles per second) from NOTE.
See also `mosaic-note-comma'."
  (let ((note-number (mosaic-note-number note))
        (comma (mosaic-note-comma note)))
    (* mosaic-a4-pitch
       (expt 2 (/ (+ comma (- note-number 69)) 12.0)))))

(defun mosaic-note-symbol->factor (symbol)
  "Compute the length of a note SYMBOL in quarter-note units."
  (* (if (= 2 (length (symbol-name symbol)))
         ;; dotted note
         1.5
       1.0)
     (or (cl-case (aref (symbol-name symbol) 0)
           (?𝅝 4)
           (?𝅗𝅥 2)
           (?𝅘𝅥 1)
           (?𝅘𝅥𝅮 0.5)
           (?𝅘𝅥𝅯 0.25)
           (?𝅘𝅥𝅰 (/ 0.25 2))
           (?𝅘𝅥𝅱 (/ 0.25 4))
           (?𝅘𝅥𝅲 (/ 0.25 8)))
         1)))

(defun mosaic-rest-symbol->factor (symbol)
  "Compute the length of a rest SYMBOL in quarter-note units."
  (* (if (= 2 (length (symbol-name symbol)))
         ;; dotted rest
         1.5
       1.0)
     (or (cl-case (aref (symbol-name symbol) 0)
           (?𝄻 4)
           (?𝄼 2)
           (?𝄽 1)
           (?𝄾 0.5)
           (?𝄿 0.25)
           (?𝅀 (/ 0.25 2))
           (?𝅁 (/ 0.25 4))
           (?𝅂 (/ 0.25 8)))
         1)))

(defun mosaic-scale-volume (vol)
  "Scale the linear VOL value to a logarithmic volume scale."
  (let ((a 1e-3)
        (b 6.908))
    ;; TODO: add a linear ramp so that f(0.0) = 0.0
    (* a (exp (* b vol)))))

(defun-memo mosaic-volume-symbol->volume (symbol) ()
  "Translate the musical volume SYMBOL into a volume."
  (when (symbolp symbol)
    (setf symbol (symbol-name symbol)))
  (mosaic-scale-volume
   (cond
    ((string= symbol "mp") 0.40) 
    ((string= symbol "p") 0.30)
    ((string= symbol "pp") 0.20)
    ((string= symbol "ppp") 0.10)
    ((string= symbol "pppp") 0.05)
    ((string= symbol "mf") 0.50)
    ((string= symbol "f") 0.55)
    ((string= symbol "ff") 0.60)
    ((string= symbol "fff") 0.65)
    ((string= symbol "ffff") 0.70)
    ((string= symbol "sfz") 0.75))))

;;; Keybindings for entering music symbols

(defun mosaic-define-music-keys ()
  "Install global keybindings for inserting musical symbols."
  (interactive)
  (global-set-key [(super f)] (lambda () (interactive) (insert "𝆃")))
  (global-set-key [(super ?\\)] (lambda () (interactive) (insert "𝆩")))
  (global-set-key [(super ?\|)] (lambda () (interactive) (insert "𝆯")))
  (global-set-key [(super a)] (lambda () (interactive) (insert "𝆭")))
  (global-set-key [(super i)] (lambda () (interactive) (insert "𝄁")))
  (global-set-key [(super c)] (lambda () (interactive) (insert "𝄫")))
  (global-set-key [(super v)] (lambda () (interactive) (insert"𝄪")))
  (global-set-key [(super b)] (lambda () (interactive) (insert "♭")))
  (global-set-key [(super n)] (lambda () (interactive) (insert "♮")))
  (global-set-key [(super m)] (lambda () (interactive) (insert "♯")))
  (global-set-key [(super ?,)] (lambda () (interactive) (insert "𝄳")))
  (global-set-key [(super ?.)] (lambda () (interactive) (insert "𝄲")))
  (global-set-key [(super ?<)] (lambda () (interactive) (insert "𝄭")))
  (global-set-key [(super ?>)] (lambda () (interactive) (insert "𝄰")))
  (global-set-key [(super ?\[)] (lambda () (interactive) (insert "𝆪")))
  (global-set-key [(super ?\])] (lambda () (interactive) (insert "𝆫")))
  (global-set-key [(super ?\{)] (lambda () (interactive) (insert "𝆲")))
  (global-set-key [(super ?\})] (lambda () (interactive) (insert "𝆱")))
  (global-set-key [(super ?\;)] (lambda () (interactive) (insert "𝆒")))
  (global-set-key [(super ?\')] (lambda () (interactive) (insert "𝆓")))
  (global-set-key (kbd "s-q 1") (lambda () (interactive) (insert "𝅝")))
  (global-set-key (kbd "s-q 2") (lambda () (interactive) (insert "𝅗𝅥")))
  (global-set-key (kbd "s-q 3") (lambda () (interactive) (insert "𝅘𝅥")))
  (global-set-key (kbd "s-q 4") (lambda () (interactive) (insert "𝅘𝅥𝅮")))
  (global-set-key (kbd "s-q 5") (lambda () (interactive) (insert "𝅘𝅥𝅯")))
  (global-set-key (kbd "s-q 6") (lambda () (interactive) (insert "𝅘𝅥𝅰")))
  (global-set-key (kbd "s-q 7") (lambda () (interactive) (insert "𝅘𝅥𝅱")))
  (global-set-key (kbd "s-q 8") (lambda () (interactive) (insert "𝅘𝅥𝅲")))
  (global-set-key (kbd "s-r 1") (lambda () (interactive) (insert "𝄻")))
  (global-set-key (kbd "s-r 2") (lambda () (interactive) (insert "𝄼")))
  (global-set-key (kbd "s-r 3") (lambda () (interactive) (insert "𝄽")))
  (global-set-key (kbd "s-r 4") (lambda () (interactive) (insert "𝄾")))
  (global-set-key (kbd "s-r 5") (lambda () (interactive) (insert "𝄿")))
  (global-set-key (kbd "s-r 6") (lambda () (interactive) (insert "𝅀")))
  (global-set-key (kbd "s-r 7") (lambda () (interactive) (insert "𝅁")))
  (global-set-key (kbd "s-r 8") (lambda () (interactive) (insert "𝅂"))))

;;; Optional MIDI note entry support

(defvar mosaic-midikbd-note-names [C_-1 Csharp_-1 D_-1 Dsharp_-1
  E_-1 F_-1 Fsharp_-1 G_-1 Gsharp_-1 A_-1 Asharp_-1 B_-1 C_0
  Csharp_0 D_0 Dsharp_0 E_0 F_0 Fsharp_0 G_0 Gsharp_0 A_0
  Asharp_0 B_0 C_1 Csharp_1 D_1 Dsharp_1 E_1 F_1 Fsharp_1 G_1
  Gsharp_1 A_1 Asharp_1 B_1 C_2 Csharp_2 D_2 Dsharp_2 E_2 F_2
  Fsharp_2 G_2 Gsharp_2 A_2 Asharp_2 B_2 C_3 Csharp_3 D_3
  Dsharp_3 E_3 F_3 Fsharp_3 G_3 Gsharp_3 A_3 Asharp_3 B_3 C_4
  Csharp_4 D_4 Dsharp_4 E_4 F_4 Fsharp_4 G_4 Gsharp_4 A_4
  Asharp_4 B_4 C_5 Csharp_5 D_5 Dsharp_5 E_5 F_5 Fsharp_5 G_5
  Gsharp_5 A_5 Asharp_5 B_5 C_6 Csharp_6 D_6 Dsharp_6 E_6 F_6
  Fsharp_6 G_6 Gsharp_6 A_6 Asharp_6 B_6 C_7 Csharp_7 D_7
  Dsharp_7 E_7 F_7 Fsharp_7 G_7 Gsharp_7 A_7 Asharp_7 B_7 C_8
  Csharp_8 D_8 Dsharp_8 E_8 F_8 Fsharp_8 G_8 Gsharp_8 A_8
  Asharp_8 B_8 C_9 Csharp_9 D_9 Dsharp_9 E_9 F_9 Fsharp_9 G_9])

(defvar mosaic-midi-note-names
  ["c♮-1" "c♯-1" "d♮-1" "d♯-1" "e♮-1" "f♮-1" "f♯-1" "g♮-1" "g♯-1"
   "a♮-1" "a♯-1" "b♮-1" "c♮0" "c♯0" "d♮0" "d♯0" "e♮0" "f♮0" "f♯0"
   "g♮0" "g♯0" "a♮0" "a♯0" "b♮0" "c♮1" "c♯1" "d♮1" "d♯1" "e♮1" "f♮1"
   "f♯1" "g♮1" "g♯1" "a♮1" "a♯1" "b♮1" "c♮2" "c♯2" "d♮2" "d♯2" "e♮2"
   "f♮2" "f♯2" "g♮2" "g♯2" "a♮2" "a♯2" "b♮2" "c♮3" "c♯3" "d♮3" "d♯3"
   "e♮3" "f♮3" "f♯3" "g♮3" "g♯3" "a♮3" "a♯3" "b♮3" "c♮4" "c♯4" "d♮4"
   "d♯4" "e♮4" "f♮4" "f♯4" "g♮4" "g♯4" "a♮4" "a♯4" "b♮4" "c♮5" "c♯5"
   "d♮5" "d♯5" "e♮5" "f♮5" "f♯5" "g♮5" "g♯5" "a♮5" "a♯5" "b♮5" "c♮6"
   "c♯6" "d♮6" "d♯6" "e♮6" "f♮6" "f♯6" "g♮6" "g♯6" "a♮6" "a♯6" "b♮6"
   "c♮7" "c♯7" "d♮7" "d♯7" "e♮7" "f♮7" "f♯7" "g♮7" "g♯7" "a♮7" "a♯7"
   "b♮7" "c♮8" "c♯8" "d♮8" "d♯8" "e♮8" "f♮8" "f♯8" "g♮8" "g♯8" "a♮8"
   "a♯8" "b♮8" "c♮9" "c♯9" "d♮9" "d♯9" "e♮9" "f♮9" "f♯9" "g♮9"])

(defun mosaic-decode-midi-note (midi-note-name)
  (cl-position midi-note-name mosaic-midikbd-note-names))

(defun mosaic-recode-midi-note (midi-note-name)
  (aref mosaic-midi-note-names (mosaic-decode-midi-note midi-note-name)))

(defun mosaic-midikbd-open ()
  (interactive)
  (midikbd-open mosaic-alsa-midi-device))

(defun mosaic-bind-midi-note (midi-note-name thunk)
  (global-set-key (vconcat (make-vector 1 'Ch1)
                           (make-vector 1 midi-note-name))
                  thunk))

(defun mosaic-bind-midi-note-to-insertion (midi-note-name)
  (mosaic-bind-midi-note midi-note-name
                         (lambda ()
                           (interactive)
                           (mosaic-handle-midi-note midi-note-name))))

(defun mosaic-handle-midi-note (midi-note-name)
  (insert (mosaic-recode-midi-note midi-note-name)))

(defun mosaic-bind-all-midi-notes ()
  (interactive)
  (cl-loop for i from 0 to 127 do
        (mosaic-bind-midi-note-to-insertion
         (aref mosaic-midikbd-note-names i))))

(defun mosaic-enable-midi ()
  "Turn on MIDI note entry support."
  (interactive)
  (mosaic-midikbd-open)
  (mosaic-bind-all-midi-notes))

;;; Extracting sequence data from a sheet

(cl-defmethod mosaic-tracker-find-voice-columns ((sheet mosaic-tracker))
  "Return a list of column numbers where voices are found in SHEET." 
  (cell-with-current-cell-sheet
   (let (columns)
     (dotimes (c (length (aref grid 0)))
       (let ((cell (cell-grid-get grid 0 c)))
         (when (and (not (null cell))
                    (cell-collect-value-p cell)
                    (let ((v (cell-find-value cell)))
                      (and (not (null v))
                           (symbolp v))))
           (push c columns))))
     (reverse columns))))

(cl-defmethod mosaic-tracker-find-voice-classes ((sheet mosaic-tracker))
  "Return the list of classes, one for each voice of the SHEET."
  (cell-with-current-cell-sheet
   (mapcar (lambda (col)
             (let ((v (cell-find-value (cell-grid-get grid 0 col))))
               (if (integerp v) 'violin v)))
           (mosaic-tracker-find-voice-columns sheet))))

(cl-defmethod mosaic-tracker-find-conductor-column ((sheet mosaic-tracker))
  "Return the column number of the conductor column in SHEET.
This is the column immediately to the left of the first voice column."
  (1- (cl-first (mosaic-tracker-find-voice-columns sheet))))

(cl-defmethod mosaic-tracker-get-conductor-column ((sheet mosaic-tracker) row)
  "Return the contents of the conductor column at the given ROW in SHEET."
  (cell-with-current-cell-sheet
   (let ((c (cell-grid-get grid row (mosaic-tracker-find-conductor-column sheet))))
     (when c
       (let ((v (cell-find-value c)))
         ;; accept either a symbol, a number, or a list of symbols and/or numbers
         (if (and (or (numberp v)
                      (symbolp v))
                  (not (consp v)))
             (list v)
           v))))))

(defun mosaic-tracker-postprocess-voice (voice)
  "Postprocess VOICE spreadsheet data to remove empty rows and so on."
  (mapcar (lambda (event)
            (cl-destructuring-bind (a b c d) event
              ;; separate rests from note events
              (if (and (null a)
                       (not (null b))
                       (null c)
                       (null d))
                  ;; rest
                  (list b)
                ;; note
                event)))
          (cl-delete-if (lambda (event)
                       (cl-every #'null event))
                     voice)))

(cl-defmethod mosaic-tracker-collect-voices ((sheet mosaic-tracker))
  "Collect all the sequenced voices in SHEET."
  (cell-with-current-cell-sheet
   (let* ((voice-columns (mosaic-tracker-find-voice-columns sheet))
          (num-voices (length voice-columns))
          (voices (make-vector num-voices nil))
          (measures* (make-vector num-voices nil))
          (conductor-column (mosaic-tracker-find-conductor-column sheet))
          (conductor-syms ())
          (last-conductor-syms ()))
     (cl-labels ((note-column (n) (nth n voice-columns))
                 (dur-column (n) (+ 1 (nth n voice-columns)))
                 (vol-column (n) (+ 2 (nth n voice-columns)))
                 (exp-column (n) (+ 3 (nth n voice-columns)))
                 (voice (n) (aref voices n))
                 (emit (n x) (push x (aref voices n)))
                 (safe-value (r c)
                             (let ((q (cell-grid-get grid r c)))
                               (when (and (not (null q))
                                          (cell-collect-value-p q))
                                 (cell-find-value q)))))
       (dotimes (i num-voices)
         ;; scan down each voice's spreadsheet column (skipping row 0)
         (dotimes (r (1- (length grid)))
           (setf conductor-syms (mosaic-tracker-get-conductor-column sheet (+ r 1)))
           ;; possibly end measure here
           (when (or conductor-syms
                     (= r (- (length grid) 2)))
             (push (list last-conductor-syms (aref voices i))
                   (aref measures* i))
             (setf (aref voices i) nil)
             (setf last-conductor-syms conductor-syms))
           ;; save this row's event to current voice
           (emit i (list (safe-value (+ r 1) (note-column i))
                         (safe-value (+ r 1) (dur-column i))
                         (safe-value (+ r 1) (vol-column i))
                         (safe-value (+ r 1) (exp-column i))))))
       (setf measures* (mapcar #'reverse measures*))
       (let ((output (mapcar (lambda (m*)
                               (mapcar (lambda (m)
                                         (vector (cl-first m) (mosaic-tracker-postprocess-voice (reverse (cl-second m)))))
                                       m*))
                             measures*)))
         (mapcar (lambda (m*)
                   ;; delete empty rows
                   (cl-delete-if (lambda (vec)
                                (cl-every #'null vec))
                              m*))
                 output))))))

;;; Rendering sequences to Scheme by interpreting cell-sheet data

(defclass player ()
  ((time :initform 0.0 :initarg :time :accessor player-time)
   (was-legato-p :initform nil :initarg :was-legato-p :accessor player-was-legato-p)
   (legato-time :initform 0.17 :initarg :legato-time :accessor player-legato-time)
   (staccato-time :initform 0.07 :initarg :staccato-time :accessor player-staccato-time)
   (expression :initform nil :initarg :expression :accessor player-expression)
   (tempo :initform 120 :initarg :tempo :accessor player-tempo)
   (volume :initform 0.45 :initarg :volume :accessor player-volume)
   (clm-instrument :initform 'fm-violin :initarg :clm-instrument :accessor player-clm-instrument)
   (note-list :initform () :initarg :note-list :accessor player-note-list)
   (mix-blocks :initform () :initarg :mix-blocks :accessor player-mix-blocks)))

(cl-defmethod player-find-expressions ((v player))
  (let ((exp (player-expression v)))
    (if (symbolp exp)
        (list exp)
      exp)))

(cl-defmethod player-legato-p ((v player))
  (member '- (player-find-expressions v)))

(cl-defmethod player-staccato-p ((v player))
  (member 'st (player-find-expressions v)))

(cl-defmethod player-pizzicato-p ((v player))
  (or (member '𝆭 (player-find-expressions v))
      (member 'pz (player-find-expressions v))))

(cl-defmethod player-accent-p ((v player))
  (member '> (player-find-expressions v)))

(defun mosaic-check-envelope (envelope)
  "Return ENVELOPE if envelope is valid, signal an error otherwise."
  (if (symbolp envelope)
      ;; can't validate null envelopes
      envelope
    (let ((x-values (let ((en envelope)
                          (xs ()))
                      (while en
                        (push (pop en) xs)
                        ;; skip y values
                        (pop en))
                      xs)))
      ;; can't validate envelopes with scheme expressions here
      (if (cl-every #'numberp x-values) 
          (if (apply #'< (reverse x-values))
              ;; x-values are strictly increasing; envelope ok
              envelope
            (error "Mosaic: Bad envelope: %S " envelope))
        envelope))))

(cl-defmethod player-parameterized-expression ((p player) sym)
  "Return the value of the parameterized expression SYM."
  (let ((exp (player-find-expressions p)))
    (let ((entry (cl-find-if (lambda (item)
                            (and (consp item)
                                 (eq sym (cl-first item))))
                          exp)))
      (when entry
        (cl-rest entry)))))

(cl-defmethod player-vibrato-env ((p player))
  "Return the vibrato envelope for this player."
  (player-parameterized-expression p 'vi))

(cl-defmethod player-vibrato-env :around ((p player))
  (mosaic-check-envelope (cl-call-next-method)))

(cl-defmethod player-gliss-env ((p player))
  (player-parameterized-expression p 'gl))
  
(cl-defmethod player-gliss-env :around ((p player))
  (mosaic-check-envelope (cl-call-next-method)))

(cl-defmethod player-note-parameters ((v player) freq volume) nil)

(cl-defmethod player-note-parameters :around ((p player) duration freq volume)
  (let ((input (cl-call-next-method))
        (output ()))
    (cl-do ()
        ((null input) (reverse output))
      (push (pop input) output)
      (push (let ((i (pop input)))
              (if (consp i)
                  ;; make sure envelopes get a LIST prefix in the output
                  (cons 'list (if (symbolp i)
                                  ;; no envelope, pass thru Scheme falsehood var
                                  'false
                                (if (not (consp (cl-first i)))
                                    ;; just an envelope with scheme subexpressions.
                                    i
                                  ;; lists of envelopes with scheme subexpressions.
                                  ;; see also `player-partial-amp-envs'
                                  (mapcar (lambda (x)
                                            (cons 'list x))
                                          i))))
                
                i))
            output))))

(cl-defmethod player-amp-env ((p player) duration)
  '(0 0  25 1  75 1  100 0))

(cl-defmethod player-vibrato-parameters ((p player) duration freq volume) nil)

(cl-defmethod player-note-number ((v player) note)
  (mosaic-note-number note))

(cl-defmethod player-quarter-note-time ((v player))
  (/ 1
     (/ (player-tempo v) 60.0))) ;; fixme 

(cl-defmethod player-note-duration ((v player) note-symbol)
  (+ (* (mosaic-note-symbol->factor note-symbol)
        (player-quarter-note-time v))
     (if (player-legato-p v)
         (player-legato-time v)
       0.0)))

(cl-defmethod player-note-volume ((v player) volume-symbol)
  (or (mosaic-volume-symbol->volume volume-symbol)
      0.45))

(cl-defmethod player-rest-duration ((v player) rest-symbol)
  (* (mosaic-rest-symbol->factor rest-symbol)
     (player-quarter-note-time v)))

(cl-defmethod player-note-pitch ((v player) note)
  (mosaic-note->pitch note))

(cl-defmethod player-set-time ((v player) time)
  (setf (player-time v) time))

(cl-defmethod player-advance-time ((v player) delta)
  (player-set-time v (+ delta (player-time v))))

(cl-defmethod player-restart-time ((v player))
  (player-set-time v 0.0))

(defun player-sync-players (players &optional time)
  (let ((time-0 (or time (apply #'max (mapcar #'player-time players)))))
    (dolist (v players)
      (player-set-time v time-0))))

(defun random^ (f)
  (/ (random (truncate (* f 10000)))
     10000.0))

;; c♮ d♮ e𝄭 f♯ g♮ a𝄭 b𝄰
(defvar mosaic-default-scale '(c c♯ d d♯ e f f♯ g g♯ a a♯ b))
(defcustom mosaic-scale mosaic-default-scale
  "The scale to use for `mosaic-define-octave-keys'.
The default value is `mosaic-default-scale'."
  :tag "Musical scale"
  :group 'mosaic)

(defvar mosaic-current-octave 4)

(defun mosaic-collect-octave-notes ()
  (cl-loop for name across "cdefgab"
        append (cl-loop for acc across "♭𝄫𝄳𝄭♮𝄰𝄲𝄪♯"
                     collect (intern (format "%s%s"
                                             (make-string 1 name)
                                             (make-string 1 acc))))))

(defun mosaic-octave-up ()
  (interactive)
  (setf mosaic-current-octave (min 8 (1+ mosaic-current-octave)))
  (message "Mosaic: Current octave set to %d" mosaic-current-octave))

(defun mosaic-octave-down ()
  (interactive)
  (setf mosaic-current-octave (max 1 (1- mosaic-current-octave)))
  (message "Mosaic: Current octave set to %d" mosaic-current-octave))

(cl-defun mosaic-insert-note (degree &optional (octave mosaic-current-octave))
  (let ((note-string (format "%s%d"
                             (nth (mod degree (length mosaic-scale))
                                  mosaic-scale)
                             octave)))
    (if (not (eq major-mode 'cell-mode))
        (insert note-string)
      (cell-with-current-cell-sheet
       (let ((c (cell-grid-get grid cursor-row cursor-column)))
         (when (null c)
           (setf c (make-instance 'cell-expression))
           (cell-grid-set grid cursor-row cursor-column c))
         (cell-set-value c note-string))
       (cell-push-undo-history)
       (cell-sheet-update)))))

(defun mosaic-define-octave-keys ()
  (interactive)
  (global-set-key [(super ?-)] 'mosaic-octave-down)
  (global-set-key [(super ?=)] 'mosaic-octave-up)
  (global-set-key [(super f1)] (lambda () (interactive) (mosaic-insert-note 0))) 
  (global-set-key [(super f2)] (lambda () (interactive) (mosaic-insert-note 1))) 
  (global-set-key [(super f3)] (lambda () (interactive) (mosaic-insert-note 2))) 
  (global-set-key [(super f4)] (lambda () (interactive) (mosaic-insert-note 3))) 
  (global-set-key [(super f5)] (lambda () (interactive) (mosaic-insert-note 4))) 
  (global-set-key [(super f6)] (lambda () (interactive) (mosaic-insert-note 5))) 
  (global-set-key [(super f7)] (lambda () (interactive) (mosaic-insert-note 6)))
  (global-set-key [(super f8)] (lambda () (interactive) (mosaic-insert-note 0 (1+ mosaic-current-octave))))
  (global-set-key [(super f9)] (lambda () (interactive) (mosaic-insert-note 1 (1+ mosaic-current-octave)))) 
  (global-set-key [(super f10)] (lambda () (interactive) (mosaic-insert-note 2 (1+ mosaic-current-octave))))
  (global-set-key [(super f11)] (lambda () (interactive) (mosaic-insert-note 3 (1+ mosaic-current-octave))))
  (global-set-key [(super f12)] (lambda () (interactive) (mosaic-insert-note 4 (1+ mosaic-current-octave))))
  (global-set-key [(super insert)] (lambda () (interactive) (mosaic-insert-note 5 (1+ mosaic-current-octave))))
  (global-set-key [(super delete)] (lambda () (interactive) (mosaic-insert-note 6 (1+ mosaic-current-octave))))
  (global-set-key [(super home)] (lambda () (interactive) (mosaic-insert-note 0 (+ 2 mosaic-current-octave))))
  (global-set-key [(super end)] (lambda () (interactive) (mosaic-insert-note 1 (+ 2 mosaic-current-octave))))
  (global-set-key [(super prior)] (lambda () (interactive) (mosaic-insert-note 2 (+ 2 mosaic-current-octave))))
  (global-set-key [(super next)] (lambda () (interactive) (mosaic-insert-note 3 (+ 2 mosaic-current-octave)))))

(defun mosaic-sequence->measure (seq)
  (vector '(𝄁) seq))

(cl-defun sonata-make-triad (note &optional (interval-1 (+ 5 (random 8))) (interval-2 (+ 6 (random 9))))
  (cl-map 'list #'append (list `(,note 𝅝 p nil)
                            `(,(mosaic-transpose-note note interval-1) 𝅝 p nil)
                            `(,(mosaic-transpose-note note (+ interval-1 interval-2)) 𝅝 p nil))))

(defun mosaic-render-sections (sections)
  (let (time players note-list)
    (setf time 0.0)
    (dolist (section sections)
      (let ((ps (mosaic-section->players section time)))
        (dolist (player ps)
          (push player players))
        (let ((new-time (apply #'max (mapcar #'player-time ps))))
          (setf time new-time))))
    (mosaic-tell-scheme-file (mosaic-players->scheme players ".all-sections.wav"))))

;;; Simple line-segment envelope generator.

;; This is only for phrase-level control, or for rendering previews of
;; vibrato/glissando envelopes.

(defun mosaic-env-pairs (env)
  (let (pairs)
    (while env
      (push (cl-subseq env 0 2) pairs)
      (setf env (cl-subseq env 2)))
    (reverse pairs)))

(defun mosaic-interp (x1 y1 desired-x x2 y2)
  (cl-assert (<= x1 desired-x x2))
  (let* ((dist-x (- x2 x1))
         (pos-x (- desired-x x1))
         (a (/ pos-x dist-x))
         (dist-y (- y2 y1))
         (pos-y (* a dist-y)))
    (+ y1 pos-y)))

(defun mosaic-env-interp (env desired-x)
  (let ((pair nil)
        (pairs (mosaic-env-pairs env)))
    (cl-block interp
      (while pairs
        (setf pair (pop pairs))
        (cl-destructuring-bind (x y) pair
          (if (and (>= desired-x x)
                   (or (null pairs)
                       ;; peek ahead at next x value
                       (< desired-x (cl-first (cl-first pairs)))))
              (if (null pairs)
                  (cl-return-from interp y)
                (cl-return-from interp
                  (mosaic-interp x y
                                 desired-x
                                 (cl-first (cl-first pairs))
                                 (cl-second (cl-first pairs)))))))))))

(provide 'mosaic-tracker)
;;; mosaic-tracker.el ends here
