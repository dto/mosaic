;;; mosaic-extra.el --- extra commands               -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Generic render command.

(cl-defmethod mosaic-render* ((s mosaic-sheet)) nil)

(cl-defmethod mosaic-render* ((s mosaic-tracker)) (mosaic-render-tracker))
(cl-defmethod mosaic-render* ((s mosaic-looper)) (mosaic-start-playing))
(cl-defmethod mosaic-render* ((s mosaic-chopper)) (mosaic-render-chopper))

(cl-defmethod mosaic-render* :before ((s mosaic-sheet))
  (mosaic-load-session-scheme)
  (when mosaic-auto-clean-up-p
    (mosaic-clean-up-temporary-files)
    (mosaic-clean-up-scheme)))

;; (cl-defmethod mosaic-render* :after ((s mosaic-sheet))

(cl-defun mosaic-render (&optional (file (buffer-file-name)))
  "Render the sheet in FILE.
By default this is the current buffer's cell sheet file."
  (interactive)
  (mosaic-set-status-icon :progress-1)
  (mosaic-update-header-line-in-all-sheets)
  (with-current-buffer (or (find-buffer-visiting file)
                           (progn
                             (find-file file)
                             (find-buffer-visiting file)))
    (when (not (eq major-mode 'cell-mode))
      (mosaic-error "Mosaic: Not a Mosaic buffer"))
    (mosaic-render* cell-current-sheet)))

;;; Extra commands.

(defun mosaic-close-all-sounds ()
  "Close all the sounds in Snd."
  (interactive)
  (mosaic-tell-scheme `(close-all-sounds)))

(defun mosaic-open-sound (file)
  "Open the sound file FILE in the Snd editor."
  (interactive "fOpen sound file in Snd editor: ")
  (mosaic-tell-scheme `(open-sound ,(expand-file-name file)))
  (mosaic-show-scheme-dwim))

(cl-defun mosaic-new-sound (&optional (channels 2))
  "Open a new empty sound with CHANNELS channels in Snd."
  (interactive)
  (mosaic-tell-scheme `(select-sound (new-sound :channels ,channels))))

(cl-defun mosaic-mix-sound (&optional (file (mosaic-output-file)))
  (interactive "fMix sound file in Snd: ")
  (mosaic-tell-scheme `(begin (mix ,file 0 t) (arrange-mixes))))

(cl-defun mosaic-save-sound-as (file)
  (interactive "FSave Snd mixdown to WAV file: ")
  (mosaic-tell-scheme `(save-sound-as ,(expand-file-name file))))

(cl-defun mosaic-enable-beat-grid ()
  "Enable the beat grid in Snd."
  (interactive)
  (mosaic-tell-scheme `(mosaic-enable-beat-grid)))

(defun mosaic-help-screen ()
  "Show the Mosaic help screen."
  (interactive)
  (find-file (mosaic-project-file "help.org")))

(defun mosaic-toggle-speedbar ()
  "Show or hide the speedbar."
  (interactive)
  (when (fboundp 'sr-speedbar-toggle)
    (sr-speedbar-toggle)))

(defvar mosaic-chopper-file "chopper.cell")
(defvar mosaic-looper-file "looper.cell")
(defvar mosaic-tracker-file "tracker.cell")
(defvar mosaic-stomper-file "stomper.cell")
(defvar mosaic-browser-file "browser.cell")

(defun mosaic-visit-chopper ()
  "Visit the current session's chopper."
  (interactive)
  (mosaic-ensure-sheet-exists mosaic-chopper-file)
  (find-file (mosaic-session-file mosaic-chopper-file)))

(defun mosaic-visit-looper ()
  "Visit the current session's looper cell sheet."
  (interactive)
  (mosaic-ensure-sheet-exists mosaic-looper-file)
  (find-file (mosaic-session-file mosaic-looper-file)))

(defun mosaic-visit-stomper ()
  "Visit the current session's stomper cell sheet."
  (interactive)
  (mosaic-ensure-sheet-exists mosaic-stomper-file)
  (find-file (mosaic-session-file mosaic-stomper-file)))

(defun mosaic-visit-tracker ()
  "Visit the current session's tracker cell sheet."
  (interactive)
  (mosaic-ensure-sheet-exists mosaic-tracker-file)
  (find-file (mosaic-session-file mosaic-tracker-file)))

(defun mosaic-visit-browser ()
  "Visit the current session's browser cell sheet."
  (interactive)
  (mosaic-ensure-sheet-exists mosaic-browser-file)
  (find-file (mosaic-session-file mosaic-browser-file)))

(defun mosaic-visit-session-directory ()
  "Visit the current session directory in Dired."
  (interactive)
  (find-file (mosaic-session-directory)))

(defun mosaic-visit-engine ()
  "Visit the current engine's customization buffer."
  (interactive)
  (eieio-customize-object mosaic-engine-object))

(defun mosaic (name &rest args )
  (apply (intern (concat "mosaic-" name)) args))

(defun mosaic-help ()
  "Display help."
  '("Try the following commands:" begin find-session help-on-command render end show-sound-cards visit-chopper visit-looper sheet-apply-settings midikbd-open close-all-sounds start-playing stop-playing find-sheet))

(provide 'mosaic-extra)
;;; mosaic-extra.el ends here

