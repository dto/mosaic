#!/bin/sh
SQUEAK=~/Squeak
TMP=~/tmp_scratch_
cd ~
mkdir $TMP
cd $TMP
wget https://files.squeak.org/6.0/Squeak6.0-22104-64bit/Squeak6.0-22104-64bit-202206021410-Linux-x64.tar.gz
tar xzf Squeak6.0-22104-64bit-202206021410-Linux-x64.tar.gz
mv Squeak6.0-22104-64bit-202206021410-Linux-x64 $SQUEAK
cd ~
rm -rf $TMP
cd ~/Squeak/shared
git clone https://gitlab.com/dto/mosaic.git mosaic
git clone https://gitlab.com/dto/smosaic.git squeakmosaic
git clone https://gitlab.com/dto/lisprunner.git lisprunner
wget https://ccrma.stanford.edu/software/snd/snd-22.8.tar.gz
tar xvzf snd-22.8.tar.gz
mv snd-22.8 snd
cd snd
./configure --without-gsl --without-audio
make
cd ~
$SQUEAK/squeak.sh 


