;;; synth.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2022 by David O'Toole
;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Maintainer: David O'Toole <deeteeoh1138@gmail.com>
;; Version: 2.0
;; Keywords: audio, sound, music, dsp

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

(define *mosaic-target-regions* #f)
(define *mosaic-source-regions* #f)

;;; Base class for concatenative synthesis objects.

(define-class^ synth ()
  '(target-regions
    source-regions
    output-regions
    output-sound
    lowpass-cutoff
    descriptors
    (params-fn region->descriptor)
    grammar
    axiom
    (play #f)
    (random-match #f)))

(document^ 'synth "SYNTH is the SchemeMosaic base class for
concatenative synthesis generators. Each synth object is both a
consumer and producer of regions. The following slots are available to
initialize with keyword arguments during MAKE-INSTANCE^:

 - TARGET-REGIONS, the sequence of regions you are trying to
   reconstruct or emulate.  
 - SOURCE-REGIONS, the sequence of regions to
   choose from during matching against the TARGET-REGIONS. The output
   will be composed of these, or some transformation thereof.
 - RANDOM-MATCH, telling the synth whether to randomly choose one match
   whenever multiple simultaneous matches occur. By default this is 
   set to #t. When this is #f instead, the first match is always chosen.
 - PARAMS-FN, a function to compute the descriptors. This is optional
   and by default is REGION->DESCRIPTOR, which caches commonly used 
   properties.

During initialization of a synth object, the following slots are
filled automatically based on the parameters given to MAKE-INSTANCE^:

 - DESCRIPTORS, the sequence of property alists corresponding to each 
   of the TARGET-REGIONS.
 - GRAMMAR, a set of production rules derived from structural analysis
   of the DESCRIPTORS.
 - AXIOM, a mixed sequence containing both variables and descriptors, 
   which expands back into DESCRIPTORS when passed to GENERATE-PHRASE.

Aside from INITIALIZE-INSTANCE, the main way of using a synth after
creation is to call its GENERATE-REGIONS (or FIND-REGIONS) method with
a suitable phrase (i.e. a suitable sequence of descriptors that the
synth will attempt to match in its output.) 
")

(document^ (find-regions (s synth) (phrase #f) (force? #f))
           "FIND-REGIONS is a caching front end to GENERATE-REGIONS. When you call
FIND-REGIONS for the first time on a synth, the OUTPUT-REGIONS slot
will be filled with the result of GENERATE-REGIONS, and the next time
FIND-REGIONS is called it will return the cached OUTPUT-REGIONS unless
the FORCE? argument is given.")

(define-method^ (find-regions (s synth) (phrase #f) (force? #f))
  (if (or force?
          (eq? #f (s 'output-regions)))
      (set! (s 'output-regions)
        (generate-regions s phrase))
      (s 'output-regions)))

(define-method^ (synth->resource (s synth))
  s)

(define^ (synth-sound s)
  "Return a new sound composed of the generated regions of the synth
S."
  (merge-sound-as-vectors (new-sound :channels 2) (find-regions s)))

(document^ '(initialize-instance (s synth))
           "This method is called after an object is created. As in
any method you define with DEFINE-METHOD^, you can use
(CALL-NEXT-METHOD) within the body to invoke the parent class
method.")

(define-method^ (initialize-instance (s synth))
  (with-let s
            (when target-regions
              (set! descriptors (map params-fn target-regions)))
            ;; properties might not be pre-cached until PARAMS-FN,
            ;; so we do that before finding the grammar.
            (when target-regions
              (set! grammar (or grammar (find-grammar target-regions)))
              (set! axiom (cdr (assoc* +axiom-symbol+ grammar)))
              (assert (not (null? grammar)))
              (assert (not (null? axiom))))))

(define *synth* #f)
(define^ (current-synth?) (not (null? *synth*)))
(define^ (current-synth) *synth*)
(set! (setter current-synth) (lambda (value)
                               (set! *synth* value)))

(define-macro (with-synth expression . body)
  `(begin (set! (current-synth) ,expression)
          ,@body))

(define-method^ (ensure-phrase (s synth) phrase)
  (let ((new-phrase (generate-phrase (s 'grammar)
                                     (if (eq? #f phrase)
                                         (s 'axiom)
                                         phrase))))
    (verify-all-terminals new-phrase)
    new-phrase))

(document^ '(ensure-phrase (s synth) phrase)
           "Return a default phrase for this synth if PHRASE is #f.")

;;; Default resynthesis method.

(document^ '(generate-regions (s synth) (phrase #f))
           "Generate a new sound (as a sequence of regions) from the
current synth. Return a sequence of regions matching the sequence of
properties that expands from PHRASE. See also GENERATE-PHRASE and the
functions RESYNTHESIS-TEST-1 and RESYNTHESIS-TEST-2 etc.  If not
supplied with a PHRASE, the synth will choose the axiom (i.e. the
entire structure.) You can use (SHOW-GRAMMAR (MY-SYNTH 'GRAMMAR)) to
get an idea what values to try giving for PHRASE.")

(define-method^ (find-candidates (s synth) properties regions)
  (match-properties properties regions))

(define-method^ (generate-regions (s synth) (phrase #f))
  (show-progress "s/g")
    ;; just pass the source-regions through
  (or (s 'source-regions) (s 'target-regions)))

  ;; (assert (not (eq? #f (s 'grammar))))
  ;; ;;(set! phrase (ensure-phrase s phrase))
  ;; (assert (pair? phrase))
  ;; ;; concatenate regions by matching properties in PHRASE
  ;; (let* ((extra-region (car (s 'target-regions)))
  ;;        (the-silent-region (silent-region extra-region)))
  ;;   (do ((regions ()))
  ;;       ((null? phrase) (reverse regions))
  ;;     (let ((candidates (find-candidates s (car phrase) (s 'target-regions))))
  ;;       (if (pair? candidates)
  ;;           (set! regions
  ;;                 (cons
  ;;                  (lazy-region
  ;;                   (if (not (s 'random-match))
  ;;                       (car candidates)
  ;;                       (random-choose candidates)))
  ;;                   regions))
  ;;           ;; fill in a silent region when possible
  ;;           (set! regions
  ;;             (cons the-silent-region
  ;;                   regions)))
  ;;       (set! phrase (cdr phrase))))))

(define-method^ (generate-sound (s synth) (phrase #f)
                                (output-file (mosaic-output-file)))
  "Call GENERATE-REGIONS and merge the results into a sound."
  (show-progress "g-s")
  (let* ((regions (find-regions s phrase))
         (sound (merge-sound-as-vectors (new-sound :channels 2)
                                        regions)))
    (set! (s 'output-sound) sound)
    (set! (s 'output-regions) regions)
    (save-sound-as output-file sound
                   :header-type mus-riff
                   :sample-type mus-lshort
                   :comment "Processed with SchemeMosaic.")
    sound))

(define-method^ (find-output-sound (s synth) (phrase #f))
  (or (s 'output-sound) (generate-sound s phrase)))

(define-method^ (find-output-sound-file (s synth) (phrase #f))
  (file-name (find-output-sound s phrase)))

;;; FM Violin "imitator" multivoice synth.

(define-class^ violin-synth (list synth)
  '((max-note-value 100)))

(define^ (violin-region (start-time 0) duration frequency amplitude amp-env)
  (sound->region! (with-sound (:channels 2)
                              (fm-violin start-time duration frequency amplitude))))

(define-method^ (generate-regions (v violin-synth) (phrase #f))
  (show-progress "v/g")
  (set! phrase (ensure-phrase v phrase))
  (let* ((duration (/ (framples* (car (v 'target-regions)))
                      (srate (car (v 'target-regions)))))
         (mnv (v 'max-note-value)))
    (map (lambda (descriptor)
           (if (or
                (not (list? descriptor))
                (not (pair? (assoc* 'notes descriptor))))
               (silent-region (car (v 'target-regions)))
               (let* ((notes (cdr (assoc* 'notes descriptor)))
                      (note-1 (car notes))
                      (note+ (if (pair? (cdr notes))
                                 (cdr notes)
                                 #f))
                      (amps (find-highest-amplitudes
                             (car (match-properties descriptor (v 'target-regions)))))
                      (output-region #f))
                 (set! output-region
                   (violin-region :duration duration
                                  :frequency (note->pitch note-1)
                                  :amplitude (* 1 0.6)))
                 (do ((ns note+ (cdr ns))
                      (j 1 (+ 1 j)))
                     ((null? ns) output-region)
                   (set! output-region
                     (mix-regions output-region
                                  (violin-region :duration duration
                                                 :frequency (note->pitch (car ns))
                                                 :amplitude (* 0.5 (/ 1 (* 1.5 j))))))))))
         phrase)))

;; Synth wrappers for MATCH-REGIONS and SHIFT-MATCH-REGIONS. Pass in
;; the source REGIONS and TARGET-REGIONS, then call:
;;     (MY-SYNTH (FIND-REGIONS ...))

;;; match-synth

(define-class^ match-synth (list synth)
  '((score-function pitch-distance)
    (target-regions ())))

(define-method^ (generate-regions (s match-synth) (phrase #f))
  (show-progress "m/g")
  (set! phrase (ensure-phrase s phrase))
  (match-regions (s 'score-function)
                 (s 'target-regions)
                 (or (s 'source-regions) (s 'target-regions))))

;;; shift-match-synth

(define-class^ shift-match-synth (list synth)
  '((target-regions ())
    (tolerance 5.0)))

(document^ 'shift-match-synth "Pitch-shift each SOURCE-REGION to match
its TARGET region. Use the TOLERANCE argument (in cycles per second)
to control sensitivity of shifting.")

(define-method^ (generate-regions (s shift-match-synth) (phrase #f))
  (show-progress "s-m/g")
  (set! phrase (ensure-phrase s phrase))
  (shift-match-regions (s 'target-regions)
                       (s 'source-regions)
                       (s 'tolerance)))

;;; unvoice-synth

(define-class^ unvoice-synth (list synth)
  '((target-regions ())))

(define-method^ (generate-regions (s unvoice-synth) (phrase #f))
  (show-progress "u-s/g")
  (set! phrase (ensure-phrase s phrase))
  (set! (s 'output-regions)
        (map unvoice-region (s 'source-regions))))

;;; cross-synth

(define-class^ cross-synth (list synth)
  '((target-regions ())))

(define-method^ (generate-regions (s cross-synth) (phrase #f))
  (show-progress "c-s/g")
  (set! phrase (ensure-phrase s phrase))
  (set! (s 'output-regions)
        (let ((out ()))
          (do ((in (s 'source-regions) (cdr in))
               (tg (s 'target-regions) (cdr tg)))
              ((or (null? in) (null? tg))
               (reverse out))
            (push! (cross-regions (car tg) (car in))
                   out)))))
        
;;; voice-synth

(define-class^ voice-synth (list synth)
  '((target-regions ())))

(define-method^ (generate-regions (s voice-synth) (phrase #f))
  (show-progress "v-s/g")
  (set! phrase (ensure-phrase s phrase))
  (set! (s 'output-regions)
        (map voice-region (s 'source-regions))))

;;; stretch-match-synth

(define-class^ stretch-match-synth (list synth)
  '((target-regions ())
    (score-function pitch-distance)))

(document^ 'stretch-match-synth "Time-stretch each SOURCE-REGION to
match the length of its corresponding TARGET-REGION.")

(define-method^ (generate-regions (s stretch-match-synth) (phrase #f))
  (show-progress "s-m/g")
  (set! phrase (ensure-phrase s phrase))
  (stretch-match-regions (s 'target-regions)
                         (s 'source-regions)))


;;; trim-match-synth

(define-class^ trim-match-synth (list synth)
  '((target-regions ())
    (stretch-tails #f)
    (score-function pitch-distance)))

(document^ 'trim-match-synth "Pad or truncate each SOURCE-REGION to
equal the length of its corresponding TARGET-REGION.")

(define-method^ (generate-regions (s trim-match-synth) (phrase #f))
  (show-progress "trim-m/g")
  (set! phrase (ensure-phrase s phrase))
  (trim-match-regions (s 'target-regions)
                      (s 'source-regions)
                      (s 'stretch-tails)))

(define^ (find-trim-offsets target-regions)
  (let ((len (framples* (car target-regions)))
        (x 0)
        (offsets ()))
    (do ((i 0 (+ 1 i)))
        ((null? (cdr target-regions)))
      (pop! target-regions)
      (push! x offsets)
      (set! x (+ x len)))
    (reverse offsets)))

(define^ (trim-match-regions target-regions source-regions stretch-tails)
  (let ((offsets (find-trim-offsets target-regions)))
    (show-progress (list "Found offsets... " offsets))
    (mosaic-trim-regions source-regions
                         offsets
                         (* (length target-regions)
                            (framples* (car target-regions)))
                         stretch-tails)))

;;; Loading a file into a synth.

(define^ (file->synth file (slice-size (mosaic-slice-size)))
  "Load FILE into a new synth at slice size SLICE-SIZE."
  (make-instance^ 'synth
                  :target-regions
                  (split-sound-to-regions slice-size
                                          (open-sound file))))

(define^ (file->synth-verbatim file (slice-size (mosaic-slice-size)))
  "Load FILE into a new PASS-THROUGH-SYNTH at slice size SLICE-SIZE."
  (make-instance^ 'pass-through-synth
                  :target-regions
                  (split-sound-to-regions slice-size
                                          (open-sound file))))

;;; Recombining and modifying synths. 

(define^ (splice-synths s1 s2)
  "Make a copy of S1, but with the TARGET-REGIONS of S2 spliced in
as the SOURCE-REGIONS of S1. The new synth keeps S1's class, Sequitur
structure, and other data. See also RESYNTHESIS-TEST-2."
  (let ((s (copy s1)))
    (set! (s 'source-regions) (s2 'target-regions))
    s))

(define-method^ (chain-synths (s1 synth) s2 (phrase #f))
  (let ((s (copy s1)))
    (set! (s 'source-regions) (find-regions s2 phrase))
    (initialize-instance s)
    s))

(document^ '(chain-synths (s1 synth) s2 (phrase #f))
           "Make a copy of S1, but with the generated regions of S2 spliced
in as the SOURCE-REGIONS (or concatenated as INPUT-FILE, in the case
of mosaic-synths) of S1. INITIALIZE-INSTANCE is called on the new
synth. PHRASE is passed through to GENERATE-REGIONS when obtaining the
output of S2.")

(define-class^ pass-through-synth (list synth))

(define-method^ (initialize-instance (s pass-through-synth))
  ;; don't do any initialization.
  (show-progress "i/p-t-s"))

(define-method^ (generate-regions (s pass-through-synth) (phrase #f))
  ;; just pass the source-regions through
  (or (s 'source-regions) (s 'target-regions)))

(define-method^ (find-regions (s pass-through-synth) (phrase #f) (force? #f))
  (generate-regions s phrase))

(define-class^ file-synth (list pass-through-synth)
  '(input-file slice-size))

(document^ 'file-synth "Load and slice a file without further processing at this stage.")

(define-method^ (initialize-instance (fs file-synth))
  (assert (string? (fs 'input-file)))
  (set! (fs 'source-regions)
    (split-sound-to-regions
     (or (fs 'slice-size) *mosaic-slice-size*)
     (open-sound (fs 'input-file)))))

(define^ (mix-synths s1 s2)
  "Mix pairwise the output regions of S2 into those of S1,
and return a new synth whose Sequitur structure is retained from
S1 but whose regions are mixed together."
  (let ((pts (make-instance^ 'pass-through-synth
                             :source-regions
                             (mix-pairwise (find-regions s1)
                                           (find-regions s2)))))
    (set! (pts 'grammar) (s1 'grammar))
    (set! (pts 'axiom) (s1 'axiom))
    (set! (pts 'descriptors) (s1 'descriptors))))

(define^ (match-synths s1 s2)
  "Make a new MATCH-SYNTH matching the output of synth S2 to
the output of synth S1."
  (make-instance^ 'match-synth
                  :target-regions (find-regions s1)
                  :source-regions (find-regions s2)))

(define^ (shift-match-synths s1 s2 (tolerance 5.0))
  "Make a new SHIFT-MATCH-SYNTH matching the output of synth S2 to the
output of synth S1."
  (make-instance^ 'shift-match-synth
                  :target-regions (find-regions s1)
                  :source-regions (find-regions s2)
                  :tolerance tolerance))

(define^ (stretch-match-synths s1 s2)
  "Make a new STRETCH-MATCH-SYNTH stretching the output of synth S2 to
the output of synth S1."
  (make-instance^ 'stretch-match-synth
                  :target-regions (find-regions s1)
                  :source-regions (find-regions s2)))

(define^ (morph-match-synths target-synth source-synth (tolerance 5.0))
  "Make a new synth morphing the output of synth SOURCE-SYNTH to the
output of synth TARGET-SYNTH. The regions of SOURCE-SYNTH are matched,
pitch-shifted, and then stretched to the length of the regions of
TARGET-SYNTH. The TOLERANCE argument only affects shift-matching."
  (let* ((shifter (shift-match-synths target-synth source-synth tolerance)))
    (stretch-match-synths target-synth shifter)))

(define-class^ morph-match-synth (list synth)
  '((target-regions ())))

(define-method^ (generate-regions (mms morph-match-synth) (phrase #f))
  (show-progress "s-m/g")
  (set! phrase (ensure-phrase mms phrase))
  (stretch-match-regions
   (mms 'target-regions)
   (shift-match-regions (mms 'target-regions)
                        (mms 'source-regions))))

;;; Mosaic synth class

(define-class^ mosaic-synth (list synth)
  '((processor (lambda (x) x))
    (slice-size #f)
    (tempo #f)
    (sound #f)
    (measure-length *mosaic-beats-per-measure*)
    input-file
    (srate 44100)
    (output-file (mosaic-temp-file))
    input-regions
    output-regions))

(document^ 'mosaic-synth
           "MOSAIC-SYNTH is a utility class for chopping, screwing,
and matching multiple elements to a backing track that may itself also
be chopped, screwed, etc. 

The following keywords are available during initialization with
MAKE-INSTANCE^:

  - INPUT-FILE, the file to read and slice.
  - OUTPUT-FILE, the file to write. By default this is (MOSAIC-TEMP-FILE).
  - TEMPO, the number of beats per minute of the INPUT-FILE.
    The default value #f means to use FIND-TEMPO.
  - SLICE-SIZE, the length in seconds of each slice. The default is
    a quarter note (relative to the TEMPO).
  - MEASURE-LENGTH, number of beats per measure. By default this is 4.
  - PROCESSOR, a function of one argument (a list of regions to
    operate on). It can rearrange and transform the regions
    (nondestructively of course) into new output regions. See SHUFFLE
    and SHUFFLE-8.
  - SRATE, the sampling rate.

After initialization, the following fields will be filled:

  - INPUT-REGIONS, the sliced input file.
  - OUTPUT-REGIONS, the resulting mosaic.
  - SOUND, the resulting output sound. It will be configured to 
    display beats/measures on the X axis, and you can use 
      (ENABLE-SNAP-MARK-TO-BEAT)
    to position marks on beats in Snd. See also:
    DISABLE-SNAP-MARK-TO-BEAT, ENABLE-SNAP-MIX-TO-BEAT.
  - SOURCE-REGIONS, the same as INPUT-REGIONS.
  - TARGET-REGIONS, the same as OUTPUT-REGIONS.
 
These last two are for chaining mosaic-synths with other synths.
See also CHAIN-SYNTHS and SPLICE-SYNTHS.

Unlike other SYNTH classes, all the processing is done during
INITIALIZE-INSTANCE. GENERATE-REGIONS just returns the mosaic-synth's
OUTPUT-REGIONS.")

(define-method^ (initialize-instance (m mosaic-synth))
  (show-progress "i/m-s")
  (with-let m
            ;; (set! *mosaic-beats-per-minute*
            ;;   (if (provided? 'squeak-mosaic)
            ;;       *mosaic-beats-per-minute*
            ;;       (find-tempo input-file)))
            (set! tempo *mosaic-beats-per-minute*)
            (assert (number? tempo))
            (set! *mosaic-beats-per-measure* measure-length)
;;            (unless (emacs-mosaic?)
              (when (eq? #f slice-size)
                (set! slice-size *mosaic-slice-size*))
              (set! *mosaic-slice-size* slice-size)
            ;;(set! *mosaic-input* input-file)
            ;; (open-sound (session-file input-file))
            (set! input-regions source-regions)
            (set! source-regions input-regions)
            (set! output-regions (processor input-regions))
            (set! target-regions output-regions)
            ;; compute new tempo based on change in slice length.
            (let ((new-tempo (* tempo (/ (framples* (car target-regions))
                                         (framples* (car source-regions))))))
              (map (lambda (region)
                     (set-region-property! 'tempo region new-tempo))
                   target-regions))
            ;; concatenate regions
            (set! sound (new-sound output-file
                                   :channels 2
                                   :srate srate
                                   :size (* (length (car (car output-regions)))
                                            (length output-regions))))
            (merge-sound-as-vectors sound output-regions)
            (save-sound sound))
  (call-next-method))

(define-method^ (generate-regions (m mosaic-synth) (phrase #f))
  (show-progress "m/g")
  (m 'output-regions))

(define-method^ (chain-synths (s1 mosaic-synth) s2 (phrase #f))
  (let ((s (copy s1)))
    (set! (s 'input-file)
      (file-name (merge-sound-as-vectors (new-sound :channels 2)
                              (find-regions s2 phrase))))
    (initialize-instance s)
    s))

(define-class^ search-match-synth (list synth)
  '(input-file target-regions score-function lowpass-cutoff properties->query database))

(document^ 'search-match-synth "Configurably match target regions to a
specified database. See the documentation
for `(INITIALIZE-INSTANCE (SMS SEARCH-MATCH-SYNTH)) for more
information on how to use this class.")

(define-method^ (initialize-instance (sms search-match-synth))
  (if (not (sms 'score-function))
      (set! (sms 'score-function) spectrum-distance)
      (set! (sms 'score-function)
            (eval (sms 'score-function))))
  (when (not (sms 'properties->query))
    (set! (sms 'properties->query) properties->query))
  (when (not (sms 'database))
    (set! (sms 'database) (mosaic-session-directory)))
  (set! *lowpass-cutoff* (sms 'lowpass-cutoff))
  (show-progress "Loading database...")
  (display (list (sms 'database)))
  (display "\n")
  (mosaic-new-database)
  (mosaic-load-database (sms 'database))
  (map (lambda (region) (remember-region-properties '(rms note notes spectrum pitch pitches) region))
       (sms 'target-regions))
  (call-next-method))

(document^ (initialize-instance (sms search-match-synth))
           "Initialize a SEARCH-MATCH-SYNTH with the following arguments:
 - :TARGET-REGIONS     The regions to be matched.
 - :DATABASE           The session directory where the database is to be loaded from. 
                       The default is the currently loaded database.
 - :SCORE-FUNCTION     This function accepts a region and a UUID and compares their metadata.
                       Choices include SPECTRUM-DISTANCE, CENTROID-DISTANCE, TEMPO-DISTANCE, and WEIGHTED-DISTANCE.
 - :PROPERTIES->QUERY  Control which UUID's get analyzed during search. Use MATCH-ALL to exhaustively match the entire database.
                       The default is a function called PROPERTIES->QUERY, which respects the value of *SEARCH-PROPERTIES*.")

(define-method^ (generate-regions (sms search-match-synth) (phrase #f))
  (show-progress "search-match-synth")
  (let* ((extra-region (car (sms 'target-regions)))
         (the-silent-region (silent-region extra-region))
         (n 0))
    (map (lambda (r)
           (remember-region-properties
            '(total-energy pitch pitches spectrum centroid note notes) r))
         (sms 'target-regions)) ;; dto
    (do ((regions ())
         (target-regions (sms 'target-regions) (cdr target-regions)))
      ((null? target-regions) (reverse regions))
      (let* ((query ((sms 'properties->query) (*region->properties* (car target-regions))))
             (candidates (mosaic-search-database^ query)))
        ;;(show-progress (list '%%%N%%% (length candidates)))
        (set! n (+ 1 n))
        (if (pair? candidates)
            (set! regions
                         (cons
                          (lazy-region
                           (if (not (sms 'random-match))
                               (try-match (car target-regions) (sms 'score-function) candidates)
                               (random-choose candidates)))
                          regions))
            ;; fill in a silent region when possible
            (set! regions
                  (cons the-silent-region
                        regions)))))))

;;; Obsolete code.

(define-class^ everything-synth (list synth)
  '(input-regions database))

(define-method^ (initialize-instance (s everything-synth))
  (call-next-method)
  (mosaic-new-database)
  (forget-all-region-properties)
  (mosaic-load-database (s 'database))
  (mosaic-snarf-database))

(define-method^ (foo-regions (s everything-synth) (phrase #f))
  (show-progress "e/g-r")
  (mosaic-all-regions))

(define-method^ (find-regions (s everything-synth) (phrase #f) (force? #f)) 
  (show-progress "e/f-r")
  (if (or force?
          (eq? #f (s 'output-regions)))
      (set! (s 'output-regions)
            (foo-regions s phrase))
      (s 'output-regions))
  (set! (s 'source-regions) (s 'output-regions)))

(define-method^ (find-output-sound (s everything-synth) (phrase #f))
  (new-sound :channels 2))

;;; Shift-search-match-synth

(define-class^ shift-search-match-synth (list search-match-synth)
  '((target-regions ())
    input-file target-regions score-function lowpass-cutoff properties->query database
    (tolerance 5.0)
    (max-shift 440.0)))

(define-method^ (generate-regions (ssms shift-search-match-synth) (phrase #f))
  (show-progress "shift-search-match-synth")
  (let* ((extra-region (car (ssms 'target-regions)))
         (the-silent-region (silent-region extra-region)))
    (map (lambda (r)
           (remember-region-properties '(note notes centroid) r))
         (ssms 'target-regions))
    (do ((regions ())
         (target-regions (ssms 'target-regions) (cdr target-regions)))
      ((null? target-regions) (reverse regions))
      (let* ((query ((ssms 'properties->query) (*region->properties* (car target-regions))))
             (candidates (mosaic-search-database^ query)))
        (if (pair? candidates)
            (set! regions
                         (cons
                          (lazy-region
                           (if (not (ssms 'random-match))
                               ;; 
                               (let* ((best-match (lazy-region (try-match (car target-regions) (ssms 'score-function) candidates)))
                                      (pitch-ratio (/ (region-pitch* (car target-regions))
                                                      (region-pitch* best-match))))
                                 (if (and (> (pitch-distance (car target-regions) best-match) (ssms 'tolerance))
                                          (< (pitch-distance (car target-regions) best-match) (ssms 'max-shift)))
                                     (shift-region* best-match pitch-ratio)
                                     best-match))
                                          
                               ;;
                               (random-choose candidates)))
                          regions))
            ;; fill in a silent region when possible
            (set! regions
                  (cons the-silent-region
                        regions)))))))

(define-class^ beat-synth (list synth)
  '(input-file (repeat 1) (threshold *mosaic-beat-energy-threshold*) 
               skip-amount scale-amount quantize (stretch-tails #t)
               offsets beats output-regions))

(define-method^ (generate-regions (bs beat-synth) (phrase #f))
  (show-progress "beat-synth")
  (let* ((scaled-beats (mosaic-scale-beats (bs 'input-file)
                                           (bs 'scale-amount)
                                           (bs 'threshold)
                                           (bs 'skip-amount)
                                           (bs 'stretch-tails)))
         (scaled-sound (merge-sound-as-vectors (new-sound :channels 2)
                                               scaled-beats))
         (resliced-regions (split-sound-to-regions (mosaic-slice-size)
                                                   scaled-sound)))
    (let ((output-regions ()))
      (do ((n 0 (+ 1 n)))
          ((= n (bs 'repeat)))
        (do ((rs resliced-regions (cdr rs)))
            ((null? rs))
          (push! (car rs) output-regions)))
      (reverse output-regions))))

(define-method^ (find-regions (bs beat-synth) (phrase #f) (force? #f)) 
  (show-progress "b/f-r")
  (if (or force?
          (eq? #f (bs 'output-regions)))
      (set! (bs 'output-regions)
            (generate-regions bs phrase))
      (bs 'output-regions))
  (set! (bs 'source-regions) (bs 'output-regions)))

(define-method^ (initialize-instance (bs beat-synth))
  #f)

