;; (set! *mosaic-session-directory* "/home/dto/Desktop/")

;; (song-test (session-file "skeleton-key-2-clip.wav")
;;          (session-file "shankar.wav")
;;          (session-file "unchained-clip.wav"))

;; (define^ (mosaic-render-waveform sound-file output-file)
;;   (set! *eps-size* 0.5)
;;   (let ((old-show (show-full-duration)))
;;     (set! (show-full-duration) #t)
;;     (let ((sound (open-sound sound-file)))
;;       (select-sound sound)
;;       (set! (channel-style sound) channels-separate)
;;       (graph->ps output-file)
;;       (set! (show-full-duration) old-show)
;;       (close-sound sound))))

;; Adapted from snd documentation

;; (definstrument (mosaic-wave-table start-time dur freq amp partials)
;;   (let ((tab (make-table-lookup :frequency freq
;;                                 :type mus-interp-bezier
;;                                 :wave (partials->wave partials)))
;;         (beg (seconds->samples start-time))
;;         (end (seconds->samples (+ start-time dur))))
;;     (run
;;      (loop for i from beg below end do
;;            (outa i (* amp (table-lookup tab)))))))

;; (define (make-rand-attack freq-rads amp)
;;   (let  

(definstrument (mosaic-violin start-time dur frequency amplitude
                              partials
                              (attack-index-env #f)
                              (attack-noise-freq #f)
                              (attack-noise-env #f)
                              (attack-noise-amp-env #f)
                              (fm-index 1.0)
                              (amp-env (float-vector 0 0  25 1  75 1  100 0))
                              (periodic-vibrato-rate 5.0) 
                              (random-vibrato-rate 16.0)
                              (periodic-vibrato-amplitude 0.0025) 
                              (random-vibrato-amplitude 0.005)
                              (noise-amount 0.0) 
                              (noise-freq 1000.0)
                              (ind-noise-freq 10.0) 
                              (ind-noise-amount 0.0)
                              (amp-noise-freq 20.0) 
                              (amp-noise-amount 0.0)
                              (gliss-env (float-vector 0 0 100 0))
                              (glissando-amount 0.0) 
                              fm1-env 
                              fm2-env
                              fm3-env
                              (fm1-rat 1.0) 
                              (fm2-rat 3.0)	 
                              (fm3-rat 4.0)                    
                              fm1-index
                              fm2-index
                              fm3-index
                              degree
                              (distance 1.0)
                              (reverb-amount 0.01)
                              (base 1.0))
  (let* ((violin
               (make-fm-violin      :frequency frequency
                                    :amplitude amplitude
                                    :fm-index fm-index
                                    :amp-env (let ((e (make-env :envelope amp-env
                                                                :scaler amplitude
                                                                :length (seconds->samples dur))))
                                               (lambda () (env e)))
                                    :periodic-vibrato-rate periodic-vibrato-rate
                                    :random-vibrato-rate random-vibrato-rate
                                    :periodic-vibrato-amplitude periodic-vibrato-amplitude
                                    :random-vibrato-amplitude random-vibrato-amplitude
                                    :noise-amount noise-amount
                                    :noise-freq noise-freq
                                    :ind-noise-freq ind-noise-freq
                                    :ind-noise-amount ind-noise-amount
                                    :amp-noise-freq amp-noise-freq
                                    :amp-noise-amount amp-noise-amount
                                    :gliss-env (let ((e (make-env :envelope gliss-env
                                                                  :scaler glissando-amount
                                                                  :length (seconds->samples dur))))
                                                 (lambda () (env e)))
                                    ;; :glissando-amount glissando-amount
                                    :fm1-env (let ((e (make-env :envelope fm1-env
                                                                :scaler fm1-index
                                                                :length (seconds->samples dur))))
                                               (lambda () (env e)))
                                    :fm2-env (let ((e (make-env :envelope fm2-env
                                                                :scaler fm2-index
                                                                :length (seconds->samples dur))))
                                               (lambda () (env e)))
                                    :fm3-env (let ((e (make-env :envelope fm3-env
                                                                :scaler fm3-index
                                                                :length (seconds->samples dur))))
                                               (lambda () (env e)))
                                    :fm1-rat fm1-rat
                                    :fm2-rat fm2-rat
                                    :fm3-rat fm3-rat
                                    :fm1-index fm1-index
                                    :fm2-index fm2-index
                                    :fm3-index fm3-index
                                    :base base))
        (freq-scale (hz->radians frequency))
        (vibrato-wave (make-triangle-wave :frequency periodic-vibrato-rate :amplitude (* periodic-vibrato-amplitude freq-scale)))
        (random-vibrato-wave (make-rand-interp random-vibrato-rate (* random-vibrato-amplitude freq-scale)))
        (attack-index-env (if (not (eq? #f attack-index-env))
                              (make-env attack-index-env 1.0 0.2)
                              #f))
        (attack-noise-env (if (not (eq? #f attack-noise-env))
                              (make-env attack-noise-env 1.0 0.2)
                              #f))
        (attack-noise-amp-env (if (not (eq? #f attack-noise-amp-env))
                                  (make-env attack-noise-amp-env 1.0 0.2)
                                  #f))
        (attack-wave (if (not (eq? #f attack-noise-freq))
                         (make-rand-interp (hz->radians attack-noise-freq) 1.0)
                         #f))
        (beg (seconds->samples start-time))
        (end (seconds->samples (+ start-time dur)))
        (osc (make-sawtooth-wave frequency))
        (ls (make-locsig :channels 2))
        ;;
        (gls-env (make-env gliss-env (* glissando-amount freq-scale) dur))
        (polyos (if (pair? partials)
                    (make-polyshape frequency
                                    :coeffs (partials->polynomial (normalize-partials partials)))
                    #f))
        (amp-env (make-env amp-env amplitude dur)))
    (setf (locsig-ref ls 0) 1.0)
    (setf (locsig-ref ls 1) 1.0)
    (do ((i beg (+ i 1))) 
        ((= i end))
      (locsig ls i (+ (* 1 (env attack-noise-amp-env)
                             (+ (rand-interp attack-wave)
                                (if (not (eq? #f partials))
                                    (polyshape polyos 1.0 (+ (triangle-wave vibrato-wave)
                                                             (rand-interp random-vibrato-wave)
                                                             (env gls-env)))
                                    0.0)))
                      (* 0.9 (violin)))))))

        ;; (editfun (lambda (g)
        ;;            (let ((grain (mus-data g))
        ;;                  (len (mus-length g)))
        ;;              (let ((sound (new-sound)))
        ;;                (insert-samples 0 len grain sound 0)
        ;;                ;;(shift-channel-pitch (+ frequency (rand-interp noise)) :snd sound :chn 0)
        ;;                (let ((left (channel->float-vector 0  len sound 0)))
        ;;                  (do ((i 0) (+ 1 i))
        ;;                      ((= i len) len)
        ;;                    (set! (grain i) (left i))))))))
(definstrument (mosaic-violin-2 start-time dur frequency amplitude
                              partials
                              (fm-index 1.0)
                              (amp-env (float-vector 0 0  25 1  75 1  100 0))
                              (color-frequency 50.0)
                              (color-env (float-vector 0 0 5 10 15 1 75 10 100 0))
                              (color-amplitude 0.2)
                              (periodic-vibrato-rate 5.0) 
                              (random-vibrato-rate 16.0)
                              (periodic-vibrato-amplitude 0.0025) 
                              (random-vibrato-amplitude 0.005)
                              (noise-amount 0.0) 
                              (noise-freq 1000.0)
                              (ind-noise-freq 10.0) 
                              (ind-noise-amount 0.0)
                              (amp-noise-freq 20.0) 
                              (amp-noise-amount 0.0)
                              (gliss-env (float-vector 0 0 100 0))
                              (glissando-amount 0.0) 
                              fm1-env 
                              fm2-env
                              fm3-env
                              (fm1-rat 1.0) 
                              (fm2-rat 3.0)	 
                              (fm3-rat 4.0)                    
                              fm1-index
                              fm2-index
                              fm3-index
                              degree
                              (distance 1.0)
                              (reverb-amount 0.01)
                              (base 1.0))
  (let* ((temp-file (with-sound (:channels 2 :output "temp.snd")
                                (fm-violin :startime start-time
                                           :dur dur
                                           :frequency frequency
                                           :amplitude amplitude
                                           :fm-index fm-index
                                           :amp-env amp-env
                                           :periodic-vibrato-rate periodic-vibrato-rate
                                           :random-vibrato-rate random-vibrato-rate
                                           :periodic-vibrato-amplitude periodic-vibrato-amplitude
                                           :random-vibrato-amplitude random-vibrato-amplitude
                                           :noise-amount noise-amount
                                           :noise-freq noise-freq
                                           :ind-noise-freq ind-noise-freq
                                           :ind-noise-amount ind-noise-amount
                                           :amp-noise-freq amp-noise-freq
                                           :amp-noise-amount amp-noise-amount
                                           :gliss-env gliss-env
                                           :glissando-amount glissando-amount
                                           :fm1-env fm1-env
                                           :fm2-env fm2-env
                                           :fm3-env fm3-env
                                           :fm1-rat fm1-rat
                                           :fm2-rat fm2-rat
                                           :fm3-rat fm3-rat
                                           :fm1-index fm1-index
                                           :fm2-index fm2-index
                                           :fm3-index fm3-index
                                           :degree degree
                                           :distance distance
                                           :reverb-amount reverb-amount
                                           :base base)))
         (frq-scl (hz->radians frequency))
         (vibrato-wave (make-triangle-wave :frequency periodic-vibrato-rate :amplitude (* periodic-vibrato-amplitude frq-scl)))
         (random-vibrato-wave (make-rand-interp random-vibrato-rate (* random-vibrato-amplitude frq-scl)))
         (random-color-wave (make-rand-interp color-frequency (* color-amplitude frq-scl)))
         (beg (seconds->samples start-time))
         (end (seconds->samples (+ start-time dur)))
	 (reader (make-readin temp-file))
         (osc (make-sawtooth-wave frequency))
         (ls (make-locsig :channels 2))
         ;;
         (gls-env (make-env gliss-env (* glissando-amount frq-scl) dur))
         (polyos (make-polyshape frequency
                                 :coeffs (partials->polynomial (normalize-partials partials))))
         (color-env (make-env color-env color-amplitude dur))
         (color-wave (make-sawtooth-wave :frequency (/ frequency 1.0)))
         (amp-env (make-env amp-env amplitude dur)))
    (setf (locsig-ref ls 0) 1.0)
    (setf (locsig-ref ls 1) 1.0)
    (with-sound (:channels 2 :output "spect.snd")
                (do ((i beg (+ i 1))) 
                    ((= i end))
                  (locsig ls i (* (env amp-env) (polyshape polyos 1.0 (+ (triangle-wave vibrato-wave)
                                                        (rand-interp random-vibrato-wave)
                                                        (env gls-env)))))))
    (select-sound (find-sound "spect.snd"))
    (map-channel (cross-synthesis (find-sound "temp.snd") 0.5 128 6.0)
                 0 (framples (find-sound "temp.snd")) (sound->integer (selected-sound)) 0)
    (map-channel (cross-synthesis (find-sound "temp.snd") 0.5 128 6.0)
                 0 (framples (find-sound "temp.snd")) (sound->integer (selected-sound)) 1)
    (let ((left (channel->float-vector 0 (framples (selected-sound)) (sound->integer (selected-sound)) 0))
          (right (channel->float-vector 0 (framples (selected-sound)) (sound->integer (selected-sound)) 1)))
      (do ((i beg (+ i 1))) 
          ((= i end))
        (outa i (vector-ref left i))
        (outb i (vector-ref right i))))))
      


;; (define *region->pitch* (make-hash-table))
;; (define (clear-cache)
;;   (setf *region->pitch* (make-hash-table)))
;; (define (region-pitch* region)
;;   (or (*region->pitch* region)
;;       (set! (*region->pitch* region)
;; 	(region-pitch region))))

;; (define (mix-mosaic m1 m2)
;;   (show-progress "mix**")
;;   (let ((sound (new-sound :file (snd-tempnam))))
;;     (save-sound m1)
;;     (save-sound m2)
;;     (select-sound sound)
;;     (insert-sound (file-name m1) 0)
;;     (mix-sound (file-name m2) 0)
;;     (save-sound sound)
;;     sound))


(define (merge-sound pos snd chn regions)
  (merge-sound^ reduce-clicks pos snd chn regions))
(define (merge-sound^ fn pos snd chn regions)
  (if (null? regions)
      snd
      (let ((len (framples (car regions))))
	(insert-samples pos 
			(framples (car regions))
			(region->float-vector (car regions))
			snd chn)
	(fn pos len snd chn)
	(merge-sound^ fn (+ pos len) snd chn (cdr regions)))))

;;; Other merge functions.

(define (merge-sound-mix snd chn regions)
  (let ((pos 0))
    (do ((r regions (cdr r)))
	((null? r))
      (mix-region (car r) pos snd chn)
      (set! pos (+ pos (framples (car r))))))
  snd)

(define (merge-sound-vector snd chn regions)
  (let ((pos 0))
    (do ((r regions (cdr r)))
	((null? r))
      (let* ((len (framples (car r)))
	     (left (region->float-vector (car r) 0 len 0))
	     (right (region->float-vector (car r) 0 len 1)))
	(insert-samples pos len left snd 0)
	(insert-samples pos len right snd 1)
	(set! pos (+ pos (framples (car r))))))
    snd))
  

(define* (xwarp region (shift 1.0) (stretch 1.0))
  (let ((file (snd-tempnam)))
    (save-region region file)
    (open-sound file)
    (sound->region!
     (with-sound (:output (snd-tempnam) :channels 2)
		 (clm-expsrc* 0
			      (samples->seconds (framples region))
			      file
			      stretch
			      shift
			      :rev #f)))))
;; alternate version; this one needs work.
(define* (stretch-region* region factor)
  (let* ((new-region '())
	 (temp-1 (snd-tempnam))
	 (temp-2 (snd-tempnam))
	 (new-sound (new-sound :channels 2 :srate 44100 :file temp-2))
	 (len (floor (* factor (framples region)))))
    (save-region region temp-1)
    (select-sound new-sound)
    (insert-sound temp-1)
    (let ((left (expsnd (list 0 factor 1 factor) new-sound 0))
	  (right (expsnd (list 0 factor 1 factor) new-sound 0)))
      (set! (framples new-sound) 0)
      (insert-samples 0 len left new-sound 0)
      (insert-samples 0 len right new-sound 1))
    (save-sound new-sound)
    (select-sound new-sound)
    (set! new-region (make-region 0 (- len 1) new-sound #t))
    (close-sound new-sound)
    new-region))
  
(define* (randomly-stretch-regions regions factor amount)
  (if (null? regions)
      '()
      (if (<= (random 1.0) amount)
	  (cons (stretch-region (car regions) factor)
		(randomly-stretch-regions (cdr regions) factor amount))
	  (cons (car regions)
		(randomly-stretch-regions (cdr regions) factor amount)))))

(define* (rhythmically-stretch-regions^ n regions factor divisor)
  (if (null? regions)
      '()
      (if (= 0 (modulo n divisor))
	  (cons (stretch-region (car regions) factor)
		(rhythmically-stretch-regions^ (+ n 1) (cdr regions) factor divisor))
	  (cons (car regions)
		(rhythmically-stretch-regions^ (+ n 1) (cdr regions) factor divisor)))))

(define (rhythmically-stretch-regions regions factor divisor)
  (rhythmically-stretch-regions^ 0 regions factor divisor))
;; <moving-pitch>
(define^ (region-moving-pitch region)
  (let ((file (snd-tempnam)))
    (save-region region file)
    (let ((rd (make-readin file))
	  (cur-srate (srate file))
	  (len (framples region))
	  (total 0.0)
	  (average 0.0))
      (let-temporarily ((*clm-srate* cur-srate))
		       (let ((scn (make-moving-pitch rd 4096 256))
			     (last-pitch 0.0)
			     (pitch 0.0))
			 (do ((i 0 (+ i 1)))
			     ((= i len))
			   (set! last-pitch pitch)
			   (set! pitch (moving-pitch scn))
			   (set! total (+ pitch total))
			   (when (not (= 0 i))
			     (set! average (/ total i))))
			 average)))))

;; Finding the loudest frequency in a region by analyzing
;; <moving-spectrum> data. The analyzed spectra can be saved in
;; *REGION->PROPERTIES*.

(define^ (region-moving-spectrum region)
  (let ((file (snd-tempnam)))
    (save-region region file)
    (let ((rd (make-readin file))
	  (cur-srate (srate file))
	  (len (framples region)))
      (let-temporarily ((*clm-srate* cur-srate))
		       (let ((scn (make-moving-spectrum rd 512 128)))
			 (do ((i 0 (+ i 1)))
			     ((= i len))
			   (moving-spectrum scn))
			 scn)))))

(define (find-max-amp amps)
  (let ((m 0.0))
    (map (lambda (a)
	   (when (> a m)
	     (set! m a)))
	 amps)
    m))

(define (find-max-amp-index amps)
  (let* ((amp0 (find-max-amp amps))
	 (len (length amps))
	 (index 0))
    (do ((i 0 (+ i 1)))
	((= i len))
      (when (>= (vector-ref amps i) amp0)
	(set! index i)))
    index))

(define (find-2nd-max-amp-index amps)
  (let* ((amp0 (find-max-amp amps))
	 (len (length amps))
	 (index2 0)
	 (index 0))
    (do ((i 0 (+ i 1)))
	((= i len))
      (when (>= (vector-ref amps i) amp0)
	(set! index2 index)
	(set! index i)))
    index2))
