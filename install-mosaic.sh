echo Installing Mosaic...
echo Downloading Mosaic distribution from http://xelf.me/mosaic-install-kit-0.5.zip...
curl http://xelf.me/mosaic-install-kit-0.5.zip --output mosaic-install-kit-0.5.zip
echo Extracting Mosaic distribution...
unzip mosaic-install-kit-0.5.zip
echo Updating Mosaic and project examples from Gitlab...
cd ~/mosaic
git pull
cd ~/cell-mode
git pull
cd ~/src
echo Installing SND...
curl http://xelf.me/snd-19.7-windows-prebuilt.tar.gz --output snd-19.7-windows-prebuilt.tar.gz
cd ~/src
tar xzf snd-19.7-windows-prebuilt.tar.gz
cd ~/src/snd-19.7
# don't build on windows, as we ship prepackaged for now.
# PATH_MPG123="/usr/bin/mpg123", PATH_FLAC="/usr/bin/flac" ./configure --without-audio --with-motif
make install && echo SND install completed!
# cd ~/src/snd-20.9
# make install
echo Installing Ecasound...
cd ~/src/ecasound-2.9.3
make install && echo Mosaic install completed!
cd ~



