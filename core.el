;;; mosaic-core.el --- core operations for mosaic    -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Required libraries

(require 'cl-lib)
(require 'rx)
(require 'cell-mode)
(require 'moorer-spectra)
(require 'eieio-base)
(require 'eieio-custom)
(require 'eieio-speedbar)
(require 'notifications)
(require 'inf-snd)
(require 'ecasound)
(require 'rot13)
(ignore-errors (require 'jack))
(ignore-errors (require 'midi-kbd))

;;; Memoization facility.

;; The `defun-memo' facility is based on code by Peter Norvig
;; for his book "Paradigms of Artificial Intelligence
;; Programming". The modified versions are redistributed here under
;; the terms of the General Public License as given above.

;; You can find more information on Norvig's book at his website:
;; http://www.norvig.com/paip.html
;; http://www.norvig.com/license.html

(cl-defmacro defun-memo (name args memo-args &body body)
  "Define a memoized function named NAME.
ARGS is the lambda list giving the memoized function's arguments.
MEMO-ARGS is a list with optional keyword arguments for the
memoization process: :KEY, :VALIDATOR, and :TEST.  The BODY
forms go inside the lambda."
  `(memoize (cl-defun ,name ,args . ,body) ,@memo-args))

(cl-defun memo (fn &key (key #'cl-first) (test #'eql) validator name)
  "Return a memo-function of the function FN.
KEY is the function used to obtain the hash key from the argument
list.  TEST is the equality predicate.  VALIDATOR if non-nil must
return non-nil for the value to be cached.  NAME is the name of
the function being memoized."
  (let ((table (make-hash-table :test test)))
    (setf (get name 'memo) table)
    (lexical-let ((key* key)
                  (table* table)
                  (validator* validator)
                  (fn* fn))
      #'(lambda (&rest args)
          (let ((k (funcall key* args)))
            (let* ((val (gethash k table* :default-|-value))
                   (found-p (not (eq val :default-|-value))))
              (if found-p
                  val
		;; only cache if value is valid
		(let ((candidate-value (apply fn* args)))
		  (prog1 candidate-value
		    (when (or (null validator*)
			      (funcall validator* candidate-value))
		      (setf (gethash k table*) candidate-value)))))))))))

(cl-defun memoize (fn-name &key (key #'cl-first) (test #'eql) validator)
  "Replace FN-NAME's global definition with a memoized version.
KEY is the function used to obtain the hash key from the argument
list.  TEST is the equality predicate.  VALIDATOR if non-nil must
return non-nil for the value to be cached."
  (clear-memoize fn-name)
  (setf (symbol-function fn-name)
        (memo (symbol-function fn-name)
              :name fn-name :key key :test test :validator validator)))

(cl-defun clear-memoize (fn-name)
  "Clear the hash table from the memo function FN-NAME."
  (let ((table (get fn-name 'memo)))
    (when table (clrhash table))))

(cl-defun get-memo-table (fn-name)
  "Get the hash table from the memo function FN-NAME."
  (get fn-name 'memo))

;;; Checking the version number

(defvar mosaic-version-string nil)
(setf mosaic-version-string "Mosaic version 0.91")

(defun mosaic-version ()
  "Display a version string for this build of Mosaic."
  (interactive)
  (message mosaic-version-string))

;;; Checking platform info

(defun mosaic-windows-p ()
  "Return non-nil if we are running on MS Windows."
  (or (eq system-type 'cygwin)
      (eq system-type 'windows-nt)))

(defun mosaic-cygwin-p ()
  "Return non-nil if we are running on MS Windows via Cygwin."
  (eq system-type 'cygwin))

(defun mosaic-linux-p ()
  "Return non-nil if we are running on GNU/Linux."
  (eq system-type 'gnu/linux))

;;; Utility function to display sound card numbers on GNU/Linux.

(defun mosaic-show-sound-cards ()
  "Show a list of ALSA sound devices.
This only works on GNU/Linux."
  (interactive)
  (if (mosaic-linux-p)
      (shell-command "cat /proc/asound/cards")
    (mosaic-error "Mosaic: M-x mosaic-show-sound-cards not implemented on MS Windows")))

;;; Configuration variables

(defgroup mosaic nil
  "EmacsMosaic is a companion to SchemeMosaic."
  :group 'applications)

(defcustom mosaic-cygwin-use-pulseaudio-p t
  "When non-nil, use Pulseaudio PADSP on MS Windows.
This setting is experimental and might not work.
This setting has no effect on GNU/Linux."
  :tag "Use PulseAudio on Windows?"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-cygwin-oss-emulator-program "padsp"
  "Name of program to use for OSS emulation on MS Windows.
Only takes effect if `mosaic-cygwin-use-pulseaudio-p' is non-nil.
This setting has no effect on GNU/Linux."
  :tag "OSS emulator program"
  :group 'mosaic
  :type 'string)

(defcustom mosaic-alsa-midi-device "/dev/midi00"
  "ALSA MIDI device name for MIDI input.
ALSA settings only take effect on GNU/Linux."
  :tag "ALSA MIDI device name"
  :group 'mosaic
  :type 'file)

(defcustom mosaic-use-jack-p nil
  "When non-nil, use JACK Audio Connection Kit."
  :tag "Use Jack"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-use-ecasound-p t
  "When non-nil, use Ecasound."
  :tag "Use Ecasound engine."
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-ecasound-program
  (if (mosaic-windows-p)
      "/usr/local/bin/ecasound"
      "/usr/bin/ecasound")
  "Program to use for Ecasound."
  :tag "Ecasound program name"
  :group 'mosaic
  :type 'file)

(defcustom mosaic-jack-program (executable-find "jackd")
  "Path to the jackd executable."
  :tag "Jack program name"
  :group 'mosaic
  :type 'directory)

(defcustom mosaic-sample-rate 44100
  "The sample rate to use for Mosaic."
  :tag "Sample rate"
  :group 'mosaic
  :type 'integer)

(defcustom mosaic-period-size 4096
  "The period size to use for Mosaic."
  :tag "Ecasound engine period size"
  :group 'mosaic
  :type 'integer)

(defcustom mosaic-alsa-device "hw:2"
  "The device name to use for Jack.
Should be a string like \"hw:2\" where the 2 is replaced by the
number of the sound device you want. See the contents of
/proc/asound/cards on your GNU/Linux system to discover the
available numbers. You can use the command
`mosaic-show-sound-cards' to see the list of device numbers.
ALSA settings only take effect on GNU/Linux."
  :tag "ALSA audio device name (Jack)"
  :group 'mosaic
  :type 'string)

(defcustom mosaic-alsa-engine-device "alsa,default"
  "The device name to use for Ecasound live output.
This only takes effect when Jack is not being used.  See the
contents of /proc/asound/cards on your GNU/Linux system to
discover the available numbers. You can use the command
`mosaic-show-sound-cards' to see the list of device numbers.
ALSA settings only take effect on GNU/Linux.

You may wish to add the following to your /etc/asound.conf to
make Ecasound output through PulseAudio:

  pcm.default pulse
  ctl.default pulse

In that case set this variable to \"alsa,default\"."
  :tag "ALSA audio device name (Ecasound)"
  :group 'mosaic
  :type 'string)

(defcustom mosaic-use-jack-rc nil
  "When non-nil, use the Jack run control file to configure Jack.
In this case, the other Jack configuration variables are ignored."
  :tag "Use JackRC"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-jack-rc "~/.jackdrc"
  "File name to try reading Jack configuration from.
This only has an effect when `MOSAIC-USE-JACK-RC' is non-nil."
  :tag "JackRC file name"
  :group 'mosaic
  :type 'file)

(defvar mosaic-set-rtlimits nil
  "When non-nil, configure Jack to use realtime process priority.
This does not seem to be necessary to getting realtime privileges
in 2021, but is supported for compatibility with jack.el.")

(defvar mosaic-set-rtlimits-program (executable-find "set_rtlimits")
  "Program to use for setting realtime process priority.")

;;; Global variables

(defvar mosaic-ecasound-buffer nil
  "Buffer for Ecasound process.")

(defvar mosaic-beats-per-minute 125
  "Tempo of current session in beats per minute.")

(defvar mosaic-beats-per-measure 4
  "Number of beats per measure in the current session.")

(defvar mosaic-slice-size 0.25
  "Default size in seconds of each slice.")

(defvar mosaic-resources (make-list 1024 nil))

(defvar mosaic-state :stopped "One of nil, :stopped, :running, :crashed.")

(defvar mosaic-record-enabled-p nil
  "When non-nil at start of playback, enable recording.
This affects all tracks for which record-p is set.")

(defvar mosaic-engine-object nil "Mosaic engine object.")

(defvar mosaic-jack-queued-connections nil
  "List of 4-tuples of strings (IN-CLIENT IN-PORT OUT-CLIENT OUT-PORT).")

(defvar mosaic-jack-port-suffix-number 1)

;;; Playing sounds in the background (simple method)

(cl-defun mosaic-play-sound (&optional (file (mosaic-output-file)))
  "Play the sound file FILE in the Snd editor."
  (interactive "fPlay sound from file: ")
  (mosaic-tell-scheme `(mosaic-play-sound-file ,file)))

(cl-defun mosaic-play-looping (&optional (file (mosaic-output-file)))
  "Repeatedly play the sound file FILE in a loop in the Snd editor."
  (interactive "fPlay looping sound from file: ")
  (mosaic-tell-scheme `(mosaic-play-looping ,file)))

(cl-defun mosaic-stop-sound ()
  "Stop playing sounds in the Snd editor."
  (interactive)
  (mosaic-tell-scheme `(stop-playing)))

;;; Playing a notification sound when a job is complete

(defcustom mosaic-use-notification-sounds-p t
  "When non-nil, use notification sounds."
  :tag "Use notification sounds?"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-notify-sound-program "play"
  "Name of the program to use for playing alert sound files."
  :tag "Notification sound playing program"
  :group 'mosaic
  :type 'file)

(defcustom mosaic-notify-happy-sound-file "happy-sound.wav"
  "Filename of a suitable happy notification alert sound.
To be used with `mosaic-notify-sound-program'."
  :tag "Happy notification sound file"
  :group 'mosaic
  :type 'file)

(defcustom mosaic-notify-sad-sound-file "sad-sound.wav"
  "Filename of a suitable sad notification alert sound.
To be used with `mosaic-notify-sound-program'."
  :tag "Sad notification sound file"
  :group 'mosaic
  :type 'file)

(defun mosaic-notify-sound (&optional error force)
  "Play an appropriate happy sound (or a sad one if ERROR is non-nil.)
If FORCE is non-nil, disregard `mosaic-use-notification-sounds-p'.
See the Mosaic customize screen to change the sound files."
  (when (or force mosaic-use-notification-sounds-p)
    (ignore-errors 
      (start-process "mosaic-play" nil mosaic-notify-sound-program
                     (mosaic-project-file (if error
                                              mosaic-notify-sad-sound-file
                                            mosaic-notify-happy-sound-file))))))

(defun mosaic-play-sound-asynchronously (file)
  (ignore-errors 
    (start-process "mosaic-play" nil mosaic-notify-sound-program file)))

;;; Displaying notifications

(defcustom mosaic-use-notification-popups-p t
  "When non-nil, use notification popups."
  :tag "Use notification popups?"
  :group 'mosaic
  :type 'boolean)

(defun mosaic-notify-on-action (id key)
  "Function to be called when desktop notification is clicked.
ID and KEY are ignored."
  (mosaic-play-output))

(defun mosaic-notify-popup (text &optional force)
  "Display a popup notification with TEXT in the body."
  (when (or force mosaic-use-notification-popups-p)
    (notifications-notify
     :title "Mosaic"
     :body text
     :actions '("Play")
     :on-action 'mosaic-notify-on-action)))

;;; Tracking the output-file

(defun mosaic-output-file ()
  "Find the current output file."
  (or mosaic-output-file-name
      (mosaic-session-file "mosaic-output.wav")))

(defun mosaic-set-output-file (file)
  "Set the output file to be FILE."
  (setf mosaic-output-file-name file))

(defsetf mosaic-output-file mosaic-set-output-file)

;;; Cleaning up temporary files

(defcustom mosaic-auto-clean-up-p t
  "When non-nil, clean up temporary files before rendering."
  :tag "Clean up temporary files?"
  :group 'mosaic
  :type 'boolean)

(defun mosaic-clean-up-temporary-files ()
  (interactive)
  (let ((files (directory-files "/tmp" t ".*.snd" :no-sort)))
    (message "Mosaic: cleaning up %s temporary files..." (length files))
    (dolist (file files)
      (delete-file file))
    (message "Mosaic: cleaned up temporary files. Done.")))

(defun mosaic-clean-up-scheme ()
  (interactive)
  (mosaic-tell-scheme
   '(begin
     (close-all-sounds)
     (mosaic-close-all-temp-files)
     (clear-resources)
     (forget-all-region-properties))))

(defun mosaic-clean-up-everything ()
  (interactive)
  (mosaic-clean-up-temporary-files)
  (mosaic-clean-up-scheme))

;;; Session-local Scheme definitions. 

(defun mosaic-session-scheme-file ()
  (mosaic-session-file "session.scm"))

(defun mosaic-ensure-session-scheme-exists ()
  (unless (file-exists-p (mosaic-session-file "session.scm"))
    (message "Mosaic: Copying session scheme from template.")
    (copy-file (mosaic-project-file "session.scm")
               (mosaic-session-file "session.scm"))))

(defun mosaic-load-session-scheme ()
  (interactive)
  (message "Mosaic: loading session Scheme...")
  (mosaic-ensure-session-scheme-exists)
  (let ((file (mosaic-session-scheme-file)))
    (if (file-exists-p file)
        (progn (mosaic-sending-scheme (mosaic-tell-scheme `(load ,file)))
               (message "Mosaic: loading session Scheme... Done."))
      ("Mosaic: loading session Scheme... None found. Ok."))))

(defun mosaic-visit-session-scheme ()
  (interactive)
  (find-file (mosaic-session-scheme-file)))

;;; Determining whether an output file is complete. This doesn't always work.

(defun mosaic-file-size (file)
  "Return the file size of FILE."
  (file-attribute-size (file-attributes file)))

(defun mosaic-file-not-empty-p (file)
  "Return non-nil if FILE exists and is not 0 bytes in size."
  (and (file-exists-p file)
       (cl-plusp (mosaic-file-size file))))

(defmacro mosaic-wait-for-file (file &rest thunk-forms)
  "When FILE becomes non-empty, evaluate THUNK-FORMS."
  (declare (indent defun))
  (let ((file* (gensym)))
    `(let ((,file* ,file))
       (mosaic-add-thunk
        (lambda ()
          (when (mosaic-file-not-empty-p ,file*)
            (prog1 t ;; returning t causes thunk to expire; see `mosaic-update'
              ,@thunk-forms)))))))

;;; Numbered sound files

(defun mosaic-numbered-sound-file (name number)
  "Return a sound file name built from NAME and NUMBER.
This is relative to `mosaic-session-directory'."
  (mosaic-session-file (concat name "-" (prin1-to-string number) ".wav")))

(defun mosaic-find-new-numbered-sound-file (name)
  "Return the unique name of a new sound file based on NAME.
This is relative to `mosaic-session-directory'."
  (cl-do
      ((n 0 (+ 1 n)))
      ((not (file-exists-p (mosaic-numbered-sound-file name n)))
       (mosaic-numbered-sound-file name n))))

(cl-defun mosaic-wait-and-import (file &optional (prefix "import"))
  "After FILE is completed, copy it to the session directory with name PREFIX."
  (mosaic-wait-for-file file
    (copy-file file (mosaic-find-new-numbered-sound-file prefix))))

(cl-defun mosaic-find-sound-files (directory &optional include-hidden)
  "Return the names of all the resource sound files in DIRECTORY.
If INCLUDE-HIDDEN is non-nil, include hidden files."
  (let ((all-files (directory-files directory :full "^resource-.*.wav$")))
    (cl-delete-if (lambda (f)
                    (or include-hidden (string-match "^\\."
                                                     (file-name-nondirectory f))))
                  all-files)))

;;; Tempo detection via Soundtouch.

;; The Scheme side also has a version of this.

(defvar mosaic-found-tempo nil)

(defcustom mosaic-soundstretch-program
  (if (mosaic-windows-p)
      "soundstretch.exe"
    "soundstretch")
  "Name of the program to use for BPM detection."
  :tag "Soundstretch BPM detection program"
  :group 'mosaic
  :type 'file)

(defvar mosaic-bpm-output-marker "Detected BPM rate "
  "String token used to parse output of Soundtouch.")

(defun mosaic-find-bpm-output-marker (str)
  "Find the Soundtouch output marker in string STR."
  (string-match mosaic-bpm-output-marker str))

(defun mosaic-bpm-command-string (file)
  "Return a shell command string to run Soundtouch on FILE."
  ;; Because the info is output to *stderr*, we need the 2>&1 at the end.
  (format "%s \"%s\" -bpm 2>&1" mosaic-soundstretch-program file))

(defun mosaic-find-tempo (file)
  "Attempt to use Soundtouch to find tempo of FILE.
The Soundtouch program must be installed.  See also the variable
`mosaic-soundstretch-program'."
  (interactive "fUse Soundtouch to find tempo of file: ")
  (message "Calling Soundtouch to find tempo of %s..." file)
  (let* ((output (mosaic-command-to-string
                  (mosaic-bpm-command-string (expand-file-name file))))
         (pos (mosaic-find-bpm-output-marker output)))
    (let ((tempo
           (if (numberp pos)
               (let* ((value+ (cl-subseq output (+ pos (length mosaic-bpm-output-marker))))
                      (end (cl-position ?\n value+)))
                 (when (integerp end)
                   (car (read-from-string (substring value+ 0 end)))))
             (prog1 nil (message "Mosaic: error parsing soundstretch output.")))))
      (setf mosaic-found-tempo
            (if (null tempo)
                (prog1 nil (message "Mosaic: Error running soundstretch."))
              (prog1 tempo
                (message "Calling Soundtouch to find tempo of %s... Done." file)
                (message "Soundtouch reported tempo %S for file %s." tempo file)
                ;; repeat message in one second, in case it disappeared.
                (run-at-time 1.0 nil (lambda ()
                                       (message "Soundtouch reported tempo %S for file %s." tempo file)))))))))

;;; Timer setup

(defcustom mosaic-timer-interval 1
  "How many seconds to wait between updates."
  :tag "User interface update interval"
  :group 'mosaic
  :type 'number)

(defvar mosaic-timer nil "Timer object for periodic updates.")

(defvar mosaic-thunks nil "List of no-argument functions to call on each update.
Functions are removed from this list if they return any non-nil value.  See also
`mosaic-update'.")

(defun mosaic-add-thunk (thunk)
  "Arrange for THUNK to be called at the next update.
See also `mosaic-update'."
  (push thunk mosaic-thunks))

(defun mosaic-update ()
  "Update everything that needs periodic updating in Mosaic.
This function calls all the thunks in `mosaic-thunks', thereafter
removing a thunk from the list if it returned a non-nil value (or
if an error occurs.)"
  (let (new-thunks)
    (dolist (thunk mosaic-thunks)
      ;; remove from thunks list if thunk returns t or if an error occurs
      (let ((value (condition-case-unless-debug nil
                       (funcall thunk)
                     (error t))))
        (unless value
          (push thunk new-thunks))))
    (setf mosaic-thunks new-thunks))
  (when (file-exists-p (mosaic-error-timestamp-file))
    (mosaic-kill-thunks)
    (run-at-time 1.0 nil (lambda () (mosaic-clear-timestamps)))
    (mosaic-error "Mosaic error: %s"
                  (with-temp-buffer (insert-file-contents-literally (mosaic-error-timestamp-file))
                                    (buffer-substring-no-properties (point-min) (point-max)))))
  (when (and (or (eq mosaic-status-icon-name :progress-1)
                 (eq mosaic-status-icon-name :progress-2))
             (zerop (mod mosaic-updates 3)))
    (mosaic-set-status-icon (if (eq mosaic-status-icon-name :progress-1)
                                :progress-2
                              :progress-1))
    (mosaic-update-header-line-in-all-sheets))
  (cl-incf mosaic-updates))

(defun mosaic-update-header-line-in-all-sheets ()
  (mosaic-update-header-line)
  (dolist (sheet (mosaic-find-sheets))
    (with-current-buffer (cell-sheet-buffer sheet)
      (force-mode-line-update t))))

(defun mosaic-kill-thunks ()
  "Delete all timer thunks."
  (interactive)
  (setf mosaic-thunks nil))

(defvar mosaic-updates 0)

(defun mosaic-start-timer ()
  "Start the update timer for Mosaic."
  (when (timerp mosaic-timer)
    (cancel-timer mosaic-timer))
  (setf mosaic-timer (run-with-timer
                      0 mosaic-timer-interval
                      'mosaic-update)))

(defun mosaic-stop-timer ()
  "Stop the update timer for Mosaic."
  (when (timerp mosaic-timer)
    (cancel-timer mosaic-timer)))

;;; Talking to the Scheme side

(defcustom mosaic-snd-default-directory nil
  "TRAMP specification directory for Snd server."
  :tag "Snd default directory (TRAMP)"
  :group 'mosaic
  :type 'string)

(defcustom mosaic-snd-enable-remote nil
  "When non-nil, allow remote Snd."
  :tag "Snd enable remote"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-snd-program "/usr/local/bin/snd"
  "Program to use for starting Snd."
  :tag "Snd program name"
  :group 'mosaic
  :type 'file)

(defcustom mosaic-use-snd-p t
  "When non-nil, use Snd."
  :tag "Use Snd-Scheme"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-suppress-snd-gui-p t
  "When non-nil, tell Snd to use batch processing and not open the GUI."
  :tag "Suppress Snd GUI"
  :group 'mosaic
  :type 'boolean)

(defun mosaic-snd-command ()
  "Return a shell command string for starting the Snd Scheme process."
  (concat mosaic-snd-program
          (if mosaic-suppress-snd-gui-p
              " -b"
            "")))

(defun mosaic-command-to-string (command)
  "Run the shell command COMMAND and return its output as a string."
  (with-temp-buffer
    (shell-command command (current-buffer))
    (buffer-substring-no-properties (point-min) (point-max))))

(defvar mosaic-spool nil)

(defvar mosaic-spooling-p nil)

(defun mosaic-spool-scheme (expression)
  (push expression mosaic-spool))

(defun mosaic-despool-scheme ()
  (let ((scheme mosaic-spool))
    (setf mosaic-spool nil)
    `(begin ,@(reverse scheme))))

(cl-defmacro mosaic-gathering-scheme (&body body)
  `(let ((mosaic-spooling-p t)) ,@body (mosaic-despool-scheme)))

(cl-defmacro mosaic-sending-scheme (&body body)
  `(let ((mosaic-spooling-p nil))
     (mosaic-tell-scheme-file (mosaic-gathering-scheme ,@body))))

(defun mosaic-tell-scheme (expression)
  "Evaluate EXPRESSION in Snd.
The EXPRESSION must conform to S7 Scheme read syntax;
otherwise, use `mosaic-tell-scheme-string'.

To get Scheme's #t and #f values, use `true' and `false'
respectively, and these will map to the proper values on the
Scheme side.
"
  (interactive "xTell Scheme to evaluate expression: ")
  (if mosaic-spooling-p
      (mosaic-spool-scheme expression)
    (inf-snd-send-string (prin1-to-string expression))))

(defcustom mosaic-catch-all-errors
  (if (mosaic-windows-p) nil t)
  "When non-nil, use CATCH-ALL* on the Scheme side."
  :type 'boolean
  :group 'mosaic)

(defun mosaic-tell-scheme-file (expression)
  "Evaluate EXPRESSION in Snd, using a temporary file."
  (if mosaic-spooling-p
      (mosaic-spool-scheme expression)
    (with-temp-buffer
      (let ((script (mosaic-session-file ".emacs-mosaic.scm"))
            (make-backup-files nil))
        (insert (prin1-to-string expression))
        (mosaic-write-file (if (or (null mosaic-snd-enable-remote)
                                   (null mosaic-snd-default-directory))
                               script
                             (concat mosaic-snd-default-directory script)))
        (let ((mosaic-spooling-p nil))
          (mosaic-tell-scheme (if mosaic-catch-all-errors
                                  `(catch-all* (lambda () (load ,script)))
                                `(load ,script))))))))

(defun mosaic-tell-scheme-string (string)
  "Evaluate STRING in Snd.
The S-expression in the STRING must conform to S7 Scheme read
syntax."
  (interactive "MTell Scheme to evaluate string: ")
  (inf-snd-send-string string))

(defun mosaic-tell-provided ()
  "Tell SchemeMosaic that EmacsMosaic is running.
On the Scheme side, use `emacs-mosaic?' to determine whether or
not this is the case."
  (interactive)
  (mosaic-tell-scheme '(provide 'emacs-mosaic)))

;;; Finding files

(defvar mosaic-selected-session-directory nil
  "Directory of currently selected session.")

(defvar mosaic-output-file-name nil
  "File where SchemeMosaic or Ecasound output will be written.")

(defvar mosaic-selected-session-name nil "String name of current session.")

(defvar mosaic-selected-session nil "Object storing current session state.")

(defcustom mosaic-project-directory (expand-file-name "~/mosaic/")
  "Directory to look for Mosaic source code and project files in."
  :tag "Mosaic project directory"
  :group 'mosaic
  :type 'directory)

(defcustom mosaic-session-parent-directory (expand-file-name "~/sessions/")
  "Default directory to store and find new session directories."
  :tag "Session parent directory"
  :group 'mosaic
  :type 'directory)

(defcustom mosaic-session-directory-extension ".mosaic"
  "Extension to use for session directory names."
  :tag "Session directory extension"
  :group 'mosaic
  :type 'string)

(cl-defun mosaic-tell-project-directory (&optional (dir mosaic-project-directory))
  "Tell SchemeMosaic to use the same project directory DIR as EmacsMosaic."
  (interactive)
  (mosaic-tell-scheme `(set! (mosaic-project-directory)
                             ,mosaic-project-directory)))

(cl-defun mosaic-tell-session-directory (&optional (dir (mosaic-session-directory)))
  "Tell SchemeMosaic to use the same session directory DIR as
EmacsMosaic."
  (interactive)
  (mosaic-tell-scheme `(set! (mosaic-session-directory)
                             ,(mosaic-session-directory))))

(cl-defun mosaic-tell-output-file (&optional (file (mosaic-output-file)))
  "Tell SchemeMosaic which FILE to write sound output in."
  (interactive)
  (mosaic-tell-scheme `(set! (mosaic-output-file)
                             ,file)))

(defun mosaic-project-file (file)
  "Return the expanded path of FILE in the current project.
This is relative to `mosaic-project-directory'."
  (expand-file-name file
                    (file-name-as-directory
                     mosaic-project-directory)))

(defun mosaic-scheme-source-file ()
  "Return the filename of the SchemeMosaic source.
This is relative to `mosaic-project-directory'."
  (mosaic-project-file "mosaic.scm"))

(cl-defun mosaic-session-directory (&optional (name mosaic-selected-session-directory) absolute-p)
  "Return the full path of the directory for session NAME.
Unless ABSOLUTE-P is non-nil, this is relative to `mosaic-session-parent-directory'."
  (when (null name)
    (mosaic-error "Mosaic: No session selected!"))
  (file-name-as-directory
   (expand-file-name name (unless absolute-p
                            (file-name-as-directory
                             mosaic-session-parent-directory)))))

(cl-defun mosaic-session-file (file)
  "Return the full path of FILE within the selected session directory."
  (expand-file-name file mosaic-selected-session-directory))

;;; Session resources

(defun mosaic-resource (index)
  "Return the resource object at INDEX."
  (nth index mosaic-resources))

(defun mosaic-set-resource (index value)
  "Store the resource object VALUE at INDEX."
  (setf (nth index mosaic-resources)
        value))

(defsetf mosaic-resource mosaic-set-resource)

(defun mosaic-tell-resource (index value)
  "Tell Scheme to store the resource VALUE at INDEX."
  (mosaic-tell-scheme
   `(set! (mosaic-resource ,index) ,value)))

(defun mosaic-resource-file-name (n)
  "Return a resource file name including N in the name."
  (format "resource-%d.wav" n))

(defvar mosaic-resource-file-regexp "\\.+[^\\.]+\\.\\(scm\\|wav\\|png\\|gpi\\)"
  "Regular expression used to match resource files.")

(cl-defun mosaic-clear-resources (&optional (session-directory (mosaic-session-directory)))
  "Delete hidden resource files in SESSION-DIRECTORY.
These are files whose names match `mosaic-resource-file-regexp'."
  (interactive)
  (setf mosaic-resources (make-list 1024 nil))
  (let ((files (directory-files session-directory :full mosaic-resource-file-regexp)))
    (dolist (file files)
      (message "Mosaic: deleting resource file %S" file)
      (ignore-errors (delete-file file)))))
  
(defun mosaic-tell-clear-resources ()
  "Tell SchemeMosaic to delete any resource files.
This function is obsolete, as resource file deletion is now handled on the Emacs
side."
  (mosaic-tell-scheme '(clear-resources)))

;;; Initializing SchemeMosaic

(cl-defun mosaic-tell-tempo ()
  "Tell SchemeMosaic to use the same tempo configuration as EmacsMosaic."
  (interactive)
  (mosaic-tell-scheme
   `(begin
     (set! *mosaic-beats-per-minute* ,mosaic-beats-per-minute)
     (set! *mosaic-beats-per-measure* ,mosaic-beats-per-measure)
     (set! *mosaic-slice-size* ,mosaic-slice-size))))

(defun mosaic-inject-scheme ()
  "Evaluate the SchemeMosaic source in Snd."
  (interactive)
  (mosaic-tell-scheme `(load ,(mosaic-scheme-source-file)))
  (mosaic-tell-provided)
  (mosaic-tell-scheme `(mosaic-load-extensions))
  (unless (null mosaic-selected-session-directory)
    (mosaic-find-session mosaic-selected-session-directory)))
   
(defun mosaic-run-workarounds ()
  "Run a brief Sequitur internal test on the Scheme side.
This works around an error message that can occur the first time
Sequitur is run."
  (interactive)
  (mosaic-tell-scheme '(sequitur-test-2)))

(defun mosaic-tell-initialize ()
  "Tell SchemeMosaic to initialize itself."
  (interactive)
  (mosaic-tell-scheme '(initialize-mosaic)))

(defun mosaic-start-scheme* ()
  "Start up the Snd editor."
  (interactive)
  (let ((default-directory (or (when mosaic-snd-enable-remote
                                 mosaic-snd-default-directory)
                               default-directory)))
    (run-snd-scheme (mosaic-snd-command))))

(defun mosaic-stop-scheme ()
  "Shut down the Snd editor."
  (interactive)
  (with-temp-buffer (snd-scheme-mode) (snd-kill))
  (kill-buffer (inf-snd-proc-buffer)))

(defun mosaic-show-scheme ()
  "Show the Snd editor's Scheme buffer."
  (interactive)
  (condition-case-unless-debug nil
      (display-buffer (inf-snd-proc-buffer))
    (error (mosaic-error "Mosaic: cannot find Scheme buffer"))))

(defun mosaic-show-scheme-gui ()
  "Show the Scheme GUI in a subwindow.
This only works in EXWM."
  (interactive)
  (if mosaic-suppress-snd-gui-p
      (message "Mosaic: Note: not showing suppressed Snd GUI")
    (let ((snd-buffer (get-buffer "Snd")))
      (if snd-buffer
          (display-buffer snd-buffer)
        (message "Mosaic: Note: cannot find Snd EXWM buffer")))))

(defun mosaic-show-scheme-dwim ()
  "Show either the Scheme buffer or, if applicable, the Scheme GUI."
  (if mosaic-suppress-snd-gui-p (mosaic-show-scheme) (mosaic-show-scheme-gui)))

(cl-defun mosaic-begin ()
  (interactive)
  (ignore-errors (split-window-below))
  (message "Starting Mosaic...")
  (mosaic-start-timer)
  (when mosaic-use-jack-p
    (message "Mosaic: Starting jack...")
    (mosaic-configure-jack)
    (save-window-excursion
      (mosaic-start-jack))
    (message "Mosaic: Starting jack... Done."))
  (when mosaic-use-ecasound-p
    (message "Mosaic: Starting ecasound...")
    (mosaic-configure-ecasound)
    (save-window-excursion
      (mosaic-start-engine)
      (message "Mosaic: Starting ecasound... Done.")))
  (when mosaic-use-snd-p
    (message "Mosaic: Starting snd...")
    (save-window-excursion
      (mosaic-start-scheme*))
    (message "Mosaic: Starting snd... Done.")
    (message "Mosaic: Injecting and configuring SchemeMosaic...")
    (mosaic-inject-scheme)
    (mosaic-sending-scheme
     (mosaic-tell-project-directory)
     (mosaic-tell-initialize)
     (mosaic-run-workarounds)
     (message "Mosaic: Injecting and configuring SchemeMosaic... Done.")
     (message "Mosaic: Showing Snd/Scheme...")
     (mosaic-show-scheme-dwim)
     (message "Mosaic: Showing Snd/Scheme... Done."))
    ;; (message "Mosaic: Showing Speedbar...")
    ;; (mosaic-turn-on-speedbar)
    ;; (message "Mosaic: Showing Speedbar... done.")
    (message "Starting Mosaic... Done."))
  (unless mosaic-inhibit-hello-wizard
    (mosaic-show-hello-wizard)
    (delete-other-windows)
    (goto-char (point-min))))

(defun mosaic-end ()
  "Quit Mosaic."
  (interactive)
  (message "Quitting Mosaic...")
  (mosaic-kill-thunks)
  (mosaic-stop-timer)
  (when mosaic-use-ecasound-p (mosaic-stop-engine))
  (when mosaic-use-jack-p (mosaic-stop-jack))
  (mosaic-stop-scheme)
  (message "Quitting Mosaic... Done."))

(cl-defun mosaic-find-timestamped-session (&optional (name "session"))
  "Open a new timestamped session whose prefix is NAME."
  (interactive)
  (let ((stamp (format-time-string "%Y-%m-%d--%H-%M-%S" (current-time))))
    (mosaic-find-session
     (concat name "-" stamp mosaic-session-directory-extension))))

(cl-defun mosaic-timestamped-sound-file-name (&optional (prefix ""))
  (mosaic-session-file (concat prefix (format-time-string "%Y-%m-%d--%H-%M-%S" (current-time))
          ".wav")))

(defun mosaic-customize-global-settings ()
  "Open the Mosaic customization group."
  (interactive)
  (customize-group "mosaic"))

(defun mosaic-start-scheme ()
  "Start Snd/Scheme interactively within Mosaic."
  (interactive)
  (save-window-excursion
    (mosaic-start-scheme*)
    (mosaic-inject-scheme)
    (mosaic-sending-scheme
     (mosaic-tell-initialize)
     (mosaic-run-workarounds)
     (mosaic-tell-project-directory)
     (mosaic-tell-tempo)
     (mosaic-find-session (mosaic-session-directory)))))

(easy-menu-define mosaic-menu global-map
  "Menu for Mosaic commands."
  '("Mosaic" 
    ["Show help" mosaic-help-screen]
    ["Restore previous window configuration" winner-undo]
    ["Toggle speedbar" mosaic-toggle-speedbar]
    ["Apply current sheet settings" mosaic-sheet-apply-settings]
    ["Show available sound devices" mosaic-show-sound-cards]
    ["Customize global settings" mosaic-customize-global-settings]
    ["Start SchemeMosaic" mosaic-begin]
    ("Wizards"
     ["Create new Session" mosaic-show-new-session-wizard]
     ["Import/slice sounds" mosaic-show-import-wizard]
     ["Chop and screw" mosaic-show-chop-wizard]
     ["Score matches" mosaic-show-score-wizard]
     ["Beat Slicer" mosaic-show-beat-slicer-wizard]
     ["Neural Network Jammer" mosaic-show-nn-wizard]
     ["Device setup" mosaic-show-device-wizard]
     ["Looper wizard" mosaic-show-loop-wizard]
     ["Hello wizard" mosaic-show-hello-wizard]
     )
    ("Session"
     ["Load (or create) a session" mosaic-find-session]
     ["Create a timestamped session" mosaic-find-timestamped-session]
     ["Save session" mosaic-save-session]
     ["Visit session directory (Dired)" mosaic-visit-session-directory]
     ["Visit session Scheme definitions" mosaic-visit-session-scheme]
     ["Import marked sounds (Dired only)" mosaic-import-marked-files]
     ["Import session sound into database" mosaic-import-sound]
     ["Import session mp3's into database" mosaic-import-session-mp3s]
     ["Load data file" mosaic-load-data-file]
     ["Reload session database" mosaic-load-data-folder])
    ("Chop/Loop/Sequence"
     ["Render currently selected sheet" mosaic-render]
     ["Edit chopper" mosaic-visit-chopper]
     ["Edit looper" mosaic-visit-looper]
     ["Edit stomper" mosaic-visit-stomper]
     ["Edit tracker" mosaic-visit-tracker]
     ["Play tracker output" mosaic-play-output]
     ["Start playing looper" mosaic-start-playing]
     ["Stop playing looper" mosaic-stop-playing]
     ["Start Snd" mosaic-start-scheme]
     ["Stop Snd" mosaic-stop-scheme])
    ("Ecasound Engine"
     ["Show Mosaic engine/tracks in speedbar" mosaic-turn-on-speedbar]
     ["Customize engine settings" mosaic-visit-engine]
     ["Add track" mosaic-engine-add-track]
     ["Remove track" mosaic-engine-remove-track]
     ["Select track" mosaic-select-track]
     ["Start Ecasound" mosaic-start-engine]
     ["Stop Ecasound" mosaic-stop-engine])
    ("Utilities"
     ["Reload SchemeMosaic source" mosaic-inject-scheme]
     ["Clear cached resources in current session" mosaic-clear-resources]
     ["Cache waveforms for all sounds" mosaic-find-all-waveforms]
     ["Find tempo of song using Soundtouch" mosaic-find-tempo]
     ["Save found tempo in current sheet" mosaic-save-tempo-settings]
     ["Enable MIDI note entry" mosaic-enable-midi]
     ["Enable PC keyboard note entry" mosaic-define-octave-keys]
     ["Update Mosaic/SchemeMosaic" mosaic-update-project]
     ["Reinitialize Jack settings" mosaic-configure-jack]
     ["Start Jack" mosaic-start-jack]
     ["Stop Jack" mosaic-stop-jack]
     ["Show Jack output" mosaic-show-jack-output]
     )
  ["Stop Mosaic/SchemeMosaic" mosaic-end]))

(defun mosaic-document-package ()
  (interactive)
  (with-temp-buffer
    (let (docstrings)
      (mapatoms (lambda (sym)
                  (when (and (or (string-match "^mosaic-" (symbol-name sym))
                                 (string-match "^cell-" (symbol-name sym))
                                 (string-match "^player-" (symbol-name sym))
                                 (string-match "^orch-" (symbol-name sym)))
                             (not (string-match "--" (symbol-name sym))))
                    (let ((doc (cond ((fboundp sym)
                                      (documentation sym))
                                     (t (documentation-property sym 'variable-documentation)))))
                      (push (list sym doc)
                            docstrings)))))
      (setf docstrings (sort docstrings (lambda (a b)
                                          (string< (symbol-name (car a))
                                                   (symbol-name (car b))))))
      (dolist (entry docstrings)
        (insert (format "** %s \n %s \n\n"
                        (upcase (concat (symbol-name (cl-first entry) ) " "
                                        (if (fboundp (cl-first entry))
                                            nil ;;(delete ?\n (fourth (symbol-function (first entry))))
                                          "")))
                        (or (cl-second entry) "")))))
    (mosaic-write-file (mosaic-project-file "elisp-doc-entries.org"))))

;;; Self-updating the installation from Gitlab. Currently disabled on
;;; GNU/Linux systems.

(defun mosaic-update-project ()
  "Update the Mosaic installation from Gitlab.
This is only enabled on MS Windows; GNU/Linux users should use
`git pull' from the shell in order to update."
  (interactive)
  (if (not (mosaic-windows-p))
    (mosaic-error "Mosaic: Self-update functionality not currently enabled on GNU/Linux")
    (progn (message "Updating Mosaic/SchemeMosaic...")
           (shell-command "source ~/mosaic/update-mosaic.sh")
           (byte-compile-file (mosaic-project-file "cell.el") :load)
           (load (mosaic-project-file "~/mosaic/mosaic.el"))
           (message "Updating Mosaic/SchemeMosaic... Done."))))

(defun mosaic-enable-emacspeak ()
  (interactive)
  (load-library "mosaic-speak.el"))

(defun mosaic-insinuate-tool-bar ()
  (interactive)
  (tool-bar-add-item
   "left-arrow"
   'winner-undo 'winner-undo 
   :label "Back"
   :help "Back to previous window configuration."
   :enable t)
  (tool-bar-add-item
   "right-arrow"
   'winner-redo 'winner-redo 
   :label "Forward"
   :help "Forward to next window configuration."
   :enable t)
  (define-key-after tool-bar-map [mosaic-separator-x]
    (list 'menu-item "--") 'winner-redo))

(advice-add 'tool-bar-setup :before 'mosaic-insinuate-tool-bar)
(tool-bar-setup)

(defun mosaic-find-all-commands ()
  "Return a list of all Mosaic command symbols."
  (save-match-data 
    (let (commands)
      (mapatoms (lambda (sym)
                  (when (and (string-match "mosaic-" (symbol-name sym))
                             (commandp (symbol-function sym)))
                    (push sym commands))))
      (cl-sort commands #'string< :key #'symbol-name))))

(defun mosaic-read-command ()
  "Read a Mosaic command name from the user."
  (interactive)
  (completing-read "Mosaic command (press TAB to show completions): "
                   (mosaic-find-all-commands)
                   nil
                   t
                   "mosaic-"))

(defun mosaic-help-on-command ()
  "Show the documentation for a command the user chooses." 
  (interactive)
  (describe-function (intern (mosaic-read-command))))

(defclass mosaic-sheet (cell-sheet)
  ((buffer) (rendering)))

(cl-defmethod initialize-instance :after ((sheet mosaic-sheet) &rest slots)
  (with-current-buffer (cell-sheet-buffer sheet)
    (mosaic-insinuate-header-line)))

(defun mosaic-write-file (filename)
  "Write the current buffer to the file FILENAME."
  (write-region (point-min) (point-max) filename nil nil))

(cl-defun mosaic-error (format-string &rest args)
  "Notify the user of an error with FORMAT-STRING and ARGS passed to `error'."
  (mosaic-notify-sound :error)
  (mosaic-set-status-icon :error)
  (mosaic-update-header-line-in-all-sheets)
  (apply #'error format-string args))

(defun mosaic-error-timestamp-file ()
  "Return the name of the timestamp error file."
  (mosaic-session-file ".timestamp-error.nil"))

(cl-defun mosaic-timestamp-file (&optional n)
  "Return the filename of the main (or Nth, if N is supplied) timestamp."
  (mosaic-session-file (if (null n)
                           ".timestamp.nil"
                         (format ".timestamp-%d.nil" n))))

(defun mosaic-clear-timestamps ()
  "Delete all timestamp files."
  (message "Mosaic: Clearing timestamp files...")
  (ignore-errors (delete-file (mosaic-timestamp-file)))
  (ignore-errors (delete-file (mosaic-error-timestamp-file)))
  (cl-do ((n 0 (+ n 1)))
      ((not (file-exists-p (mosaic-timestamp-file n))))
    (ignore-errors (delete-file (mosaic-timestamp-file n))))
  (message "Mosaic: Clearing timestamp files... Done."))

;;; Input and output specifications.

;; An `iospec' is an expression designating a source or sink of audio
;; data. Examples:
;; 
;; (:jack "alsa_pcm" "capture_3")
;; ((:jack "alsa_pcm" "playback_1") (:jack "alsa_pcm" "playback_1"))
;; (:file "~/wildlife/nightbird.wav")
;; (:loopback 1)
;; (:loop "~/drumloops/120bpm-break-21.wav")
;; (:master)

;; When an iospec is a list with multiple inputs or outputs (as in the
;; second example above) then that track will have the corresponding
;; number of channels.

(cl-defun mosaic-iospec-to-string (iospec &optional (prefix ""))
  "Transform IOSPEC into a list of ecasound-formatted specifier strings."
  (cond
   ;; it's a plain ecasound string. pass it through
   ((stringp iospec) (list iospec))
   ;; it's a list of iospecs
   ((and (listp iospec) (listp (car-safe iospec)))
    (mapcar (lambda (io)
              (mosaic-iospec-to-string io prefix))
            iospec))
   ;; it's a single iospec
   ((listp iospec)
    (cl-case (car iospec)
      (:jack (format "jack_generic,%s" prefix))
      (:file (expand-file-name (cl-second iospec)))
      (:loopback (format "loop,%d" (cl-second iospec)))
      (:loop (format "audioloop,\"%s\"" (mosaic-session-file (cl-second iospec))))
      (:master
       (if (mosaic-windows-p) "/dev/dsp"
         (if mosaic-use-jack-p
             "jack_alsa"
           mosaic-alsa-engine-device)))))))

;;; Runtime engine

(defclass mosaic-engine (eieio-persistent eieio-speedbar-directory-button)
  ((started-p :initform nil :initarg :started-p)
   (record-enabled-p :initform nil :initarg :record-enabled-p
                     :custom boolean :documentation "When non-nil, recording is enabled.")
   (ecasound-buffer :initform nil :initarg :ecasound-buffer)
   (session-directory :initform (expand-file-name "~") :initarg :session-directory)
   (tracks :initform nil :initarg :tracks :type (list-of mosaic-track))
   (jack-queued-connections :initform nil :initarg :jack-queued-connections)
   (jack-port-suffix-number :initform nil :initarg :jack-port-suffix-number)
   (beats-per-minute :initform 125 :initarg :beats-per-minute :documentation "Tempo in beats per minute."
                     :custom number)
   (beats-per-measure :initform 4 :initarg :beats-per-measure :documentation "Number of beats per measure."
                      :custom integer)
   (slice-size :initform nil :initarg :slice-size
               :documentation "Size of each audio slice, in seconds."
               :custom sexp)
   (marks :initform nil :initarg :marks)
   (selected-track :initform nil :initarg :selected-track)
   (sample-rate :initform 44100 :initarg :sample-rate :custom integer :documentation "Sample rate to use for this session.")
   (period-size :initform 128 :initarg :period-size :custom integer :documentation "Period size to use for this session.")
   (jack-output-device :initform (eval 'mosaic-alsa-device) :initarg :jack-output-device
                       :custom string :documentation "ALSA name of audio output device.")
   (engine-output-device :initform (eval 'mosaic-alsa-engine-device) :initarg :engine-output-device
                         :custom string :documentation "Ecasound name of audio output device.")
   ;; eieio fields
   (file :initform "engine.eieio")
   (file-header-line :initform ";; Mosaic EIEIO persistent engine object")))

(cl-defmethod eieio-speedbar-object-buttonname ((self mosaic-engine))
  (format "Ecasound Engine <%s>" mosaic-selected-session-name))

(cl-defmethod eieio-speedbar-object-children ((engine mosaic-engine))
  (slot-value engine 'tracks))

(cl-defmethod eieio-speedbar-derive-line-path ((engine mosaic-engine))
  (mosaic-session-directory))

(cl-defmethod eieio-persistent-path-relative ((engine mosaic-engine) file)
  (mosaic-session-file file))

(cl-defmethod eieio-done-customizing ((engine mosaic-engine))
  (cl-call-next-method)
  (mosaic-engine-apply-settings engine))

(cl-defmethod mosaic-engine-apply-settings ((engine mosaic-engine))
  "Set global configuration variables to match the ENGINE object."
  (with-mosaic-engine engine
    (when (null slice-size)
      (setf slice-size (/ 1 (/ beats-per-minute 60) beats-per-measure)))
    (setf mosaic-sample-rate sample-rate
          mosaic-period-size period-size
          mosaic-beats-per-minute beats-per-minute
          mosaic-beats-per-measure beats-per-measure
          mosaic-slice-size slice-size
          mosaic-alsa-device jack-output-device
          mosaic-alsa-engine-device engine-output-device)))

(cl-defmethod eieio-speedbar-handle-click ((engine mosaic-engine))
  (eieio-customize-object engine))

(defvar mosaic-speedbar-keymap nil)

(defun mosaic-find-sheets ()
  "Find all the current `cell-mode' buffers."
  (interactive)
  (let (sheets)
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (when (eq major-mode 'cell-mode)
          (push cell-current-sheet sheets))))
    sheets))

(defun mosaic-find-session-sheets ()
  "Find all the cell sheets in the current session.
Don't use this."
  (let ((files
         (cl-remove-if (lambda (f)
                      (string-match "#" f))
                    (directory-files (mosaic-session-directory) :full ".cell$")))
        sheets)
    (dolist (file files)
      (let ((buffer (find-buffer-visiting file)))
        (when (null buffer)
          (find-file file)
          (setf buffer (find-buffer-visiting file)))
        (with-current-buffer buffer
          (push cell-current-sheet sheets))))
    sheets))

(defun mosaic-find-speedbar-items (i)
  "Return a list of speedbar-compatible items for the speedbar.
The argument I is ignored."
  (append (list mosaic-engine-object)
          (mosaic-find-sheets)
          (mosaic-find-session-sounds)))

(defun mosaic-insinuate-speedbar ()
  "Create the Mosaic speedbar display."
  (interactive)
  (eieio-speedbar-create 'eieio-speedbar-make-map
                         'eieio-speedbar-key-map
                         'eieio-speedbar-menu
                         "mosaic"
                         'mosaic-find-speedbar-items))

(defun mosaic-speedbar-mode-initialize ()
  "Show the Mosaic speedbar."
  (interactive)
  ;; (speedbar-frame-mode 1)
  (speedbar-change-initial-expansion-list "mosaic")
  (speedbar-get-focus))

(defun mosaic-turn-on-speedbar ()
  "Create and show the Mosaic speedbar."
  (interactive)
  (mosaic-insinuate-speedbar)
  (mosaic-speedbar-mode-initialize))

(defmacro with-mosaic-engine (engine &rest body)
  "Bind the slots of ENGINE and evaluate the BODY forms."
  (declare (indent defun))
  (let ((eng (gensym)))
    `(let ((,eng ,engine))
       (with-slots (started-p record-enabled-p ecasound-buffer
                              session-directory tracks
                              jack-queued-connections
                              beats-per-minute beats-per-measure
                              slice-size jack-port-suffix-number
                              jack-output-device engine-output-device
                              marks selected-track sample-rate
                              period-size)
           ,eng
         ,@body))))

(defmacro with-ecasound (self &rest body)
  "In the ecasound buffer of SELF, evaluate the BODY forms."
  (declare (indent defun))
  `(with-current-buffer mosaic-ecasound-buffer
     ,@body))

(cl-defmethod initialize-instance :after ((self mosaic-engine) &rest slots)
  "Initialize a new MOSAIC-ENGINE."
  (with-mosaic-engine self
    (setf beats-per-minute mosaic-beats-per-minute
          beats-per-measure mosaic-beats-per-measure
          slice-size mosaic-slice-size)))

(cl-defmethod mosaic-start-ecasound ((self mosaic-engine))
  (with-mosaic-engine self
    (setf mosaic-ecasound-buffer (eci-init))))

(cl-defmethod mosaic-stop-ecasound ((self mosaic-engine))
  (with-mosaic-engine self
    (ecasound-kill-daemon)
    (kill-buffer mosaic-ecasound-buffer)))

(cl-defmethod mosaic-state ((self mosaic-engine))
  "Get the state of the ecasound engine."
  (with-ecasound (eci-engine-status)))

(cl-defmethod mosaic-position ((self mosaic-engine))
  "Get the play position of the engine."
  (with-ecasound (eci-cs-get-length-samples)))

(cl-defmethod mosaic-seek ((self mosaic-engine) seconds)
  "Set the play position of the engine, in seconds."
  (with-ecasound (eci-cs-set-position seconds)))

(cl-defmethod mosaic-seek-sample ((self mosaic-engine) samples)
  "Set the play position of the engine, in samples."
  (with-ecasound (eci-cs-set-position-samples samples)))

(cl-defmethod mosaic-add-track ((self mosaic-engine) &optional track)
  "Add TRACK to the track list, or a new track if TRACK is nil."
  (with-slots (tracks) self
    (let ((new-track (or track (make-instance 'mosaic-track))))
      (setf tracks (append tracks (list new-track)))
      new-track)))

(cl-defmethod mosaic-remove-track ((self mosaic-engine) track-name)
  "Remove the track named TRACK-NAME from the current mosaic engine."
  (setf (slot-value self 'tracks)
        (cl-remove-if (lambda (track)
                     (string= track-name (slot-value track 'name)))
                   (slot-value self 'tracks))))

(cl-defmethod mosaic-get-track ((self mosaic-engine) track-name)
  "Return the track named TRACK-NAME from the current mosaic engine."
  (cl-find-if (lambda (track)
             (string= track-name (slot-value track 'name)))
           (slot-value self 'tracks)))

(cl-defmethod mosaic-select-track ((self mosaic-engine) track-name)
  "Select the track named TRACK-NAME from the current mosaic engine."
  (setf (slot-value mosaic-engine 'selected-track)
        (mosaic-get-track self track-name)))

(defvar mosaic-engine-output-file ".engine.wav")

(cl-defmethod mosaic-configure-tracks ((self mosaic-engine))
  "Build current configuration of the engine SELF in Ecasound."
  (with-mosaic-engine self
    (setf jack-queued-connections nil)
    (setf jack-port-suffix-number 1)
    (with-ecasound
      (eci-cs-select "untitled-chainsetup")
     (eci-cs-remove)
      (eci-cs-add "mosaic")
      (eci-cs-select "mosaic")
      ;; live output to loopback 1
      (let ((n 0))
        (dolist (track tracks)
          (when (slot-value track 'play-p)
            (mosaic-track-to-looper track n n))
          (cl-incf n)))
      (eci-c-select-all)
      (eci-ao-add "loop,1")
      (eci-c-clear)
      ;; select loopback 1 as input on two new chains
      (eci-c-add "a")
      (eci-c-add "b")
      (eci-c-select "a,b")
      (eci-ai-add (mosaic-iospec-to-string '(:loopback 1)))
      (eci-c-clear)
      ;; route loopback 1 to master on chain "a"
      (eci-c-select "a")
      (eci-ao-add (mosaic-iospec-to-string '(:master)))
      (eci-c-clear)
      ;; route loopback 1 to WAV file on chain "b"
      (eci-c-select "b")
      (eci-ao-add (mosaic-iospec-to-string `(:file ,(mosaic-timestamped-sound-file-name "looper-"))))
      (eci-c-clear)
      ;; connect chains
      (eci-cs-connect))))

(cl-defmethod mosaic-queue-jack-connection ((self mosaic-engine) a b c d)
  (with-mosaic-engine self
    (push (list a b c d) jack-queued-connections)
    (cl-incf jack-port-suffix-number)))

(cl-defmethod mosaic-configure-jack-connections ((self mosaic-engine))
  (with-mosaic-engine self
    (mapc #'jack-connect jack-queued-connections)))

;;; Tracks for the audio engine to play and record

(defclass mosaic-track (eieio-persistent)
  ((name :initform "{unnamed track}" :initarg :name :custom string :documentation "Name of this track. This is used in filenames, so don't use a slash.")
   (num-channels :initform 2 :initarg :num-channels :custom integer :documentation "Number of channels. Usually 1 or 2.")
   (input :initform nil :initarg :input :custom sexp :documentation "IOSPEC input expression.")
   (output :initform nil :initarg :output :custom sexp :documentation "IOSPEC output expression.")
   (operators :initform nil :initarg :operators :custom (repeat string) :documentation "List of strings, in Ecasound chainop syntax")
   (record-p :initform nil :initarg :record-p :custom boolean :documentation "When non-nil, create a new take when recording.")
   (play-p :initform t :initarg :play-p :custom boolean :documentation "When non-nil, hear the selected take.")
   (selected-take :initform 0 :initarg :selected-take :documentation "Which take plays when you listen back.")
   (next-take :initform 0 :initarg :next-take :documentation "Number of next take to be recorded.")
   (update-p :initform nil :initarg :update-p :documentation "Whether any caches/widgets should be re-initialized from track data.")
   ))

(cl-defmethod eieio-speedbar-object-buttonname ((self mosaic-track))
  (slot-value self 'name))

(cl-defmethod eieio-done-customizing ((self mosaic-track))
  (cl-call-next-method))

(defun mosaic-track-file-name (track-number)
  "Return a file name for TRACK-NUMBER's track object serialization."
  (mosaic-session-file (format "track-%d.eieio" track-number)))

(defmacro with-mosaic-track (track &rest body)
  "Bind the slots of TRACK and evaluate the BODY forms."
  (let ((trk (gensym)))
    `(let ((,trk ,track))
       (with-slots (name num-channels input output
                         operators record-p play-p selected-take next-take
                         update-p)
           ,trk ,@body))))

(cl-defmethod mosaic-track-take-file ((self mosaic-track) &optional take-number)
  "Given a TRACK and an optional TAKE-NUMBER, return the name of
the file where audio will be recorded for this take. If TAKE is
not given, assume the next take number."
  (with-mosaic-track self
                     (mosaic-session-file (format "%s.take-%d.wav"
                                                  (slot-value track 'name)
                                                  (or take-number next-take)))))

(cl-defmethod initialize-instance :after ((self mosaic-track) &rest slots)
  "Create an empty track." nil)
;; (with-mosaic-track self
;;   (setf name (or name "{unnamed track}")
;;        play-p t
;;        next-take 1
;;        num-channels 2
;;        selected-take 0)))

(cl-defmethod mosaic-track-connect-input ((self mosaic-track) prefix iospec &optional mono-p)
  "Add an iospec input."
  (with-mosaic-track self
                     (setf input iospec)
                     ;; handle any special connections
                     (cl-case (car iospec)
                       (:file (when (not (file-exists-p (cl-second iospec)))
                                (message "Mosaic: track input file %s does not exist." (cl-second iospec))))
                       (:loop (when (not (file-exists-p (cl-second iospec)))
                                (message "Mosaic: loop input file %s does not exist." (cl-second iospec))))
                       (:jack
                        (let* ((in-client (cl-second iospec))
                               (in-port (cl-third iospec))
                               (out-client "ecasound")
                               (out-port (concat prefix (format "_%d" jack-port-suffix-number))))
                          (queue-jack-connection mosaic-engine-object in-client in-port out-client out-port))))))

(cl-defmethod mosaic-track-connect-output ((self mosaic-track) prefix iospec)
  "Add an iospec output."
  (with-mosaic-track self
                     (setf output iospec)
                     ;; handle jack connections
                     (when (eq :jack (car iospec))
                       (let ((in-client "ecasound")
                             (in-port (concat prefix (format "_%d" jack-port-suffix-number)))
                             (out-port (cl-third iospec))
                             (out-client (cl-second iospec)))
                         (push (list in-client in-port out-client out-port)
                               (slot-value mosaic-engine-object 'jack-queued-connections))
                         (cl-incf (slot-value mosaic-engine-object 'jack-port-suffix-number))))))

(cl-defun mosaic-audio-format-string (num-channels &optional
                                                 (sample-rate jack-sample-rate))
  "Format a string suitable for telling ecasound what audio format to use."
  (format "f32_le,%d,%d,i" num-channels sample-rate))

;;; User commands

(defun mosaic-configure-jack ()
  "Configure jack.el variables."
  (setf jack-program mosaic-jack-program
        jack-sample-rate mosaic-sample-rate
        jack-period-size mosaic-period-size
        jack-alsa-device mosaic-alsa-device
        jack-rc (list mosaic-jack-rc)
        jack-use-jack-rc mosaic-use-jack-rc
        jack-set-rtlimits mosaic-set-rtlimits
        jack-set-rtlimits-program mosaic-set-rtlimits-program))

(defun mosaic-configure-ecasound ()
  "Configure ecasound.el variables."
  (setf eci-program mosaic-ecasound-program
        ecasound-timer-interval mosaic-timer-interval))

(defun mosaic-ecasound-daemon-p ()
  "Return non-nil if Ecasound has a connection."
  (ecasound-daemon-p))

(defun mosaic-start-jack ()
  "Start Jack."
  (interactive)
  (mosaic-configure-jack)
  ;; Why is this here twice? Because it always fails the first time.
  (ignore-errors (jack-start))
  (ignore-errors (jack-start)))

(defun mosaic-stop-jack ()
  "Stop Jack."
  (interactive)
  (ignore-errors (jack-kill)))

(defun mosaic-wait-for-ecasound ()
  "Wait for ecasound to start running, then return."
  (while (not (string= "running" (eci-engine-status mosaic-ecasound-buffer)))
    (sit-for 0.1)))

(defun mosaic-wait-for-ecasound-to-finish ()
  "Wait for ecasound to finish running, then return."
  (while (string= "running" (eci-engine-status mosaic-ecasound-buffer))
    (sit-for 0.1)))

(cl-defmethod mosaic-start-take ((self mosaic-engine))
  "Begin a new take on the engine object SELF.
If the engine's `record-enabled-p' is non-nil, then record all
tracks that are also record enabled."
  (with-ecasound
    (eci-start)
    (mosaic-wait-for-ecasound)
    (mosaic-session-to-jack)
    (setf mosaic-state :running)))

(cl-defmethod mosaic-stop-take ((self mosaic-engine))
  "Complete the current take for the engine object SELF."
  (with-ecasound (eci-cs-disconnect))
  (setf mosaic-state :stopped)
  ;; turn off global record enable. this means you must explicitly
  ;; enable recording for each take.
  (setf (slot-value self 'record-enabled-p) nil)
  (mosaic-session-finish-take))

;; Rendering track setups to ecasound and jack.

(defun mosaic-queue-jack-input (prefix iospec &optional mono-p)
  "Prepare with PREFIX the jack output spec IOSPEC.
Use MONO-P if you want mono sound instead of stereo."
  (let* ((in-client (cl-second iospec))
         (in-port (cl-third iospec))
         (out-client "ecasound")
         (out-port (concat prefix (format "_%d" mosaic-jack-port-suffix-number))))
    (push (list in-client in-port out-client out-port)
          mosaic-jack-queued-connections)
    (cl-incf mosaic-jack-port-suffix-number (if mono-p 1 2))))

(defun mosaic-track-to-looper (track n &optional id)
  "Add a loop output spec to Ecasound for the TRACK object and track N."
  (let ((inputs (slot-value track 'input))
        (outputs (slot-value track 'output)))
    (eci-c-add (format "%d" (or id n)))
    (eci-ai-add (mosaic-iospec-to-string inputs))))

(defun mosaic-track-to-ecasound (track)
  "Configure ecasound with the parameters of this TRACK."
  (with-ecasound
    (let ((inputs (slot-value track 'inputs))
          (outputs (slot-value track 'outputs))
          (channels (slot-value track 'num-channels)))
      ;; set track format
      (eci-cs-set-audio-format (format "s16_le,%d,%d,i" channels mosaic-sample-rate))
      ;; are we monitoring?
      (when (slot-value track 'monitor-p)
        (eci-c-add (concat (slot-value track 'name) "-monitor"))
        ;; decide whether to monitor input or selected take
        (if (and mosaic-record-enabled-p (mosaic-track-record-p track))
            ;; monitoring the recorded input. this will need jack connections.
            (progn
              (eci-ai-add (mosaic-iospec-to-string (car inputs) track))
              (dotimes (channel channels)
                (mosaic-queue-jack-input (mosaic-track-name track)
                                         (nth channel inputs)
                                         (= 1 channels))))
          ;; monitoring the selected take.
          (eci-ai-add (mosaic-track-take-file track (mosaic-track-selected-take track)))))
      ;; send to output
      (eci-ao-add (mosaic-iospec-to-string outputs track))
      ;; check whether we are recording
      (when (and mosaic-record-enabled-p (mosaic-track-record-p track))
        (eci-c-add (concat (mosaic-track-name track) "-record"))
        (eci-ai-add (mosaic-iospec-to-string (car inputs) track))
        (dotimes (channel channels)
          (mosaic-queue-jack-input (mosaic-track-name track)
                                   (nth channel inputs)
                                   (= 1 channels)))
        (eci-ao-add (mosaic-track-take-file track))))))

(defun mosaic-update-mode-line (buffer)
  "Update the mode line in BUFFER with Mosaic status text."
  (ecasound-update-mode-line buffer))

(defun mosaic-find-session (session-directory)
  "Load session from the directory SESSION-DIRECTORY.
If no such session exists, create it.  Return the engine
initialized with the session data.  If the SESSION-DIRECTORY is a
relative filename, it will be looked for in
`mosaic-session-parent-directory'."
  (interactive "DSession directory: ")
  (when (null (get-buffer-process (inf-snd-proc-buffer)))
    (let ((mosaic-inhibit-hello-wizard t))
      (mosaic-begin)))
  (setf mosaic-selected-session-name (file-name-nondirectory session-directory))
  (setf session-directory (mosaic-session-directory
                           session-directory
                           (cl-find ?/ session-directory)))
  (setf mosaic-selected-session-directory session-directory)
  (if (file-exists-p session-directory)
      (if (file-directory-p session-directory)
          ;; open existing session
          nil
        (error "Mosaic: File exists, but is not a directory"))
    ;; create new session
    (make-directory session-directory)
    (message "Created session in %s" session-directory))
  ;; (save-window-excursion
  ;;   (mosaic-visit-chopper)
  ;;   (cell-sheet-apply-settings cell-current-sheet))
  ;; (save-window-excursion
  ;;   (mosaic-visit-looper)
  ;;   (cell-sheet-apply-settings cell-current-sheet))
  (let ((engine-file (mosaic-engine-object-file)))
    (if (file-exists-p engine-file)
        (setf mosaic-engine-object (eieio-persistent-read engine-file mosaic-engine))
      (setf mosaic-engine-object (make-instance mosaic-engine))))
  (when mosaic-engine-object
    (setf (slot-value mosaic-engine-object 'session-directory) session-directory))
  (mosaic-engine-apply-settings mosaic-engine-object)
  (mosaic-tell-session-directory session-directory)
  (setf (mosaic-output-file) (mosaic-session-file "mosaic-output.wav"))
  (mosaic-tell-output-file)
  (mosaic-tell-tempo)
  (when (mosaic-cygwin-p)
    (message "Mosaic: Real session directory is %s"
             (cygwin-convert-file-name-to-windows session-directory)))
  (mosaic-insinuate-speedbar)
  (ignore-errors (mosaic-load-database session-directory))
  (mosaic-load-session-scheme)
  session-directory)

(defun mosaic-save-tempo-settings ()
  "Save the result of `mosaic-find-tempo' to the current cell sheet's settings cells."
  (interactive)
  (if (null mosaic-found-tempo)
      (mosaic-error "Mosaic: No tempo found.  Try using M-x `mosaic-find-tempo' first")
    (if (not (eq major-mode 'cell-mode))
        (mosaic-error "Mosaic: Not in a spreadsheet buffer!")
      (cell-with-current-cell-sheet
       (let ((beats-per-measure (float (or (cell-sheet-setting-value cell-current-sheet 'mosaic-beats-per-measure)
                                           4))))
         (cell-sheet-change-setting cell-current-sheet 'mosaic-beats-per-minute (float mosaic-found-tempo))
         (cell-sheet-change-setting cell-current-sheet 'mosaic-beats-per-measure (truncate beats-per-measure))
         (cell-sheet-change-setting cell-current-sheet 'mosaic-slice-size 'quarter-note)
         (cell-sheet-apply-settings cell-current-sheet)
         (cell-sheet-update)
         (setf (buffer-modified-p (cell-sheet-buffer cell-current-sheet)) t))))))
       
(cl-defmethod mosaic-finish-take ((self mosaic-engine))
  "Complete a recorded take."
  (with-mosaic-engine self
    (dolist (track tracks)
      ;; check whether we recently recorded anything
      (when (and (slot-value self 'record-enabled-p)
                 (slot-value track 'record-p))
        ;; set update flag so that the cell sheet updates
        (setf (slot-value track 'update-p) t)
        ;; select the take we just recorded for playback
        (setf (slot-value track 'selected-take)
              (slot-value track 'next-take))
        ;; prepare for next take
        (cl-incf (slot-value track 'next-take))))))

(defun mosaic-engine-object-file ()
  "Return the file name of the current engine object's serialization."
  (mosaic-session-file "engine.eieio"))

(defun mosaic-save-session ()
  "Save any unsaved buffers and any session configuration data."
  (interactive)
  (save-some-buffers)
  (eieio-persistent-save mosaic-engine-object
                         (mosaic-engine-object-file)))

(defun mosaic-restart-engine ()
  (interactive)
  (save-window-excursion
    (mosaic-start-playing* mosaic-engine-object)))
    ;; (eci-command "start" mosaic-ecasound-buffer)
    ;; (mosaic-wait-for-ecasound-to-finish)
    ;; (eci-command "cs-disconnect" mosaic-ecasound-buffer)))

(defun mosaic-play-engine-output ()
  "Play the engine output."
  (interactive)
  (save-window-excursion (shell-command (format "play %s &" (mosaic-session-file mosaic-engine-output-file)))))

(defun mosaic-play-output ()
  "Play the output."
  (interactive)
  (save-window-excursion (shell-command (format "play %s &" (mosaic-output-file)))))

(defun mosaic-configure-ecasound-log ()
  "Set up environment variables so that Ecasound logs into the session directory."
  (setenv "ECASOUND_LOGFILE" (mosaic-session-file "ecasound.log"))
  (setenv "ECASOUND_LOGLEVEL" ""))

(cl-defun mosaic-start-engine ()
  "Start up Mosaic's Ecasound engine."
  (interactive)
  (mosaic-configure-ecasound)
  ;; (mosaic-configure-ecasound-log)
  (setf mosaic-engine-object (make-instance 'mosaic-engine))
  (mosaic-start-ecasound mosaic-engine-object))

(defun mosaic-stop-engine ()
  "Stop Mosaic's Ecasound engine."
  (interactive)
  (mosaic-stop-ecasound mosaic-engine-object))

;;; Status predicates

(defun mosaic-ready-p ()
  "Return non-nil if all Mosaic components are ready.
This includes Snd Scheme, Ecasound, and JACK.  Each component is
tested only if it was scheduled to be run."
  (and (if mosaic-use-jack-p (jack-running-p) t)
       (if mosaic-use-ecasound-p (mosaic-ecasound-daemon-p) t)
       (if mosaic-use-snd-p (buffer-live-p (get-buffer (inf-snd-proc-buffer))) t)
       (or (eq mosaic-state :running)
           (eq mosaic-state :stopped))
       ))

(defun mosaic-engine-running-p ()
  "Return non-nil if Mosaic is running."
  (and (if mosaic-use-jack-p (jack-running-p) t)
       (if mosaic-use-ecasound-p (mosaic-ecasound-daemon-p) t)
       (if mosaic-use-snd-p (buffer-live-p (get-buffer (inf-snd-proc-buffer))) t)
       (eq mosaic-state :running)))

(defun mosaic-engine-stopped-p ()
  "Return non-nil if Mosaic is stopped and ready."
  (and (if mosaic-use-jack-p (jack-running-p) t)
       (if mosaic-use-ecasound-p (mosaic-ecasound-daemon-p) t)
       (if mosaic-use-snd-p (buffer-live-p (get-buffer (inf-snd-proc-buffer))) t)
       (eq mosaic-state :stopped)))

(cl-defmethod mosaic-start-playing* ((self mosaic-engine))
  "Start the engine playing and/or recording."
  (mosaic-configure-tracks self)
  (eci-command "start" mosaic-ecasound-buffer))
  ;;(mosaic-wait-for-ecasound-to-finish)
  ;;(eci-command "cs-disconnect" mosaic-ecasound-buffer))

(cl-defmethod mosaic-stop-playing* ((self mosaic-engine))
  "Stop the engine at the current play position."
  (eci-command "stop" mosaic-ecasound-buffer)
  (eci-command "cs-disconnect" mosaic-ecasound-buffer))

(defun mosaic-start-playing ()
  "Start the Mosaic Ecasound engine playing."
  (interactive)
  (mosaic-start-playing* mosaic-engine-object)) 

(defun mosaic-stop-playing ()
  "Stop the Mosaic Ecasound engine playing."
  (interactive)
  (mosaic-stop-playing* mosaic-engine-object)) 

;;; XM splitter/slicer

(defvar mosaic-xm-bpm-regexp "BPM\\[\\([0-9]*\\)\\]")
(defvar mosaic-xm-channels-regexp "^Channels.*: \\([0-9]*\\)")
(defvar mosaic-xm-program "xmp")

(defun mosaic-xm-find-bpm (file)
  (with-temp-buffer
    (call-process-shell-command
     (format "script -qefc '%s --time 1 --driver=null \"%s\"' /dev/null | cat"
             mosaic-xm-program
             (expand-file-name file))
     nil
     (current-buffer))
    (goto-char (point-min))
    (if (re-search-forward mosaic-xm-bpm-regexp nil t)
        (car (read-from-string (match-string 1)))
      (mosaic-error "Mosaic: Could not find BPM for file %s" file))))

(defun mosaic-xm-find-channels (file)
  (with-temp-buffer
    (call-process-shell-command
     (format "script -qefc '%s --time 1 --driver=null \"%s\"' /dev/null | cat"
             mosaic-xm-program
             (expand-file-name file))
     nil
     (current-buffer))
    (goto-char (point-min))
    (if (re-search-forward mosaic-xm-channels-regexp nil t)
        (car (read-from-string (match-string 1)))
      (mosaic-error "Mosaic: Could not find CHANNELS for file %s" file))))

(defun mosaic-xm-export-channel (file channel)
  (let ((output-file (format "%s-%s.wav" (expand-file-name file) channel)))
    (call-process-shell-command
     (format "%s --driver=wav --solo=%d --output-file=\"%s\" \"%s\""
             mosaic-xm-program channel output-file (expand-file-name file)))))

(defun mosaic-xm-split (file)
  (let ((num-channels (mosaic-xm-find-channels file)))
    (cl-dotimes (channel num-channels)
      (mosaic-xm-export-channel file channel))))

;;; Soundfonts

(defun mosaic-explode-soundfont (file)
  (interactive "fExtract AIF files with loop points from Soundfont: ")
  (mosaic-tell-scheme `(mosaic-explode-soundfont ,file)))

;;; Obtaining data values from Scheme

(defun mosaic-return-file ()
  (mosaic-session-file ".return.scm"))

(defun mosaic-delete-return-file ()
  (ignore-errors (delete-file (mosaic-return-file))))

(defun mosaic-hear-scheme ()
  (let ((file (mosaic-return-file)))
    (if (file-exists-p file)
        (prog1 (eval (mosaic-sexp-from-file file))
          (delete-file file))
      (error "Cannot find Scheme return file %s" file))))

(cl-defmacro mosaic-wait-for-scheme (var &body body)
  `(mosaic-wait-for-file (mosaic-return-file)
     (let ((,var (mosaic-hear-scheme)))
       ,@body)))

(cl-defmacro mosaic-sit-for-scheme (var &body body)
  `(progn
     (while (not (file-exists-p (mosaic-return-file)))
       (sit-for 0.1))
     (let ((,var (mosaic-hear-scheme)))
       ,@body)))

(defun mosaic-select-percussion-3 ()
  (interactive)
  (mosaic-tell-scheme `(mosaic-select-net (percussion-3-net))))

(provide 'mosaic-core)
;;; mosaic-core.el ends here
