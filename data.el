;;; data.el --- building and searching a database for concatsynth  -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defvar mosaic-data-file-extension ".dat")

(defun mosaic-data-file (file)
  (concat (expand-file-name file)
          mosaic-data-file-extension))

(defun mosaic-sexp-from-file (file)
  (with-temp-buffer
    (insert-file-contents-literally (expand-file-name file))
    (car (read-from-string (buffer-substring-no-properties (point-min) (point-max))))))

(defun mosaic-data (file)
  (mosaic-sexp-from-file (mosaic-data-file file)))

(defmacro mosaic-passthrough (name &rest args)
  `(defun ,(intern (concat "mosaic-" (symbol-name name)))
     (interactive)
     (mosaic-tell-scheme-file ',(cons name args))))

(defcustom mosaic-import-find-offset nil
  "When non-nil, attempt to find the offset of the first beat when importing.
If this fails, try setting it to nil."
  :tag "Find offset when importing"
  :group 'mosaic
  :type 'boolean)

(defcustom mosaic-import-find-tempo t
  "When non-nil, attempt to find the tempo of imported files.
If this fails, try setting it to nil."
  :tag "Find tempo when importing"
  :group 'mosaic
  :type 'boolean)

(cl-defun mosaic-import-sound (file &optional tempo (slice-size 'quarter-note) read-whole)
  "Import and slice FILE into the current session's database."
  (interactive "fMosaic import sound: ")
  (let* ((tempo (or tempo (mosaic-find-tempo (mosaic-session-file file)))))
    (if (null tempo)
      (message "Ignoring %s, tempo detection failed." file)
      (progn
	(setf mosaic-slice-size slice-size)
	(setf mosaic-beats-per-minute tempo)
	(mosaic-configure-slices*)
        (message "Mosaic: BPM %s  Slice size %s" mosaic-beats-per-minute mosaic-slice-size)
	(mosaic-tell-tempo)
	(let ((scheme `(mosaic-import-sound ,file
                                            ,(if mosaic-import-find-tempo tempo 'false)
                                            ;;,(or tempo 'false)
                                            ,mosaic-slice-size
                                            ,(if mosaic-import-find-offset 'true 'false)
                                            ,read-whole)))
	  (mosaic-tell-scheme-file scheme))))))

(cl-defun mosaic-import-sounds (files &optional tempo (slice-size 'quarter-note) read-whole)
  (mosaic-clear-timestamps)
  (lexical-let ((start-time (time-to-seconds (current-time))))
    (cl-dolist (file files)
      (when mosaic-import-find-tempo
        (setf tempo (or (mosaic-find-tempo file) tempo)))
      (mosaic-import-sound file tempo slice-size read-whole))
    (mosaic-wait-for-file (mosaic-timestamp-file)
      (let* ((end-time (time-to-seconds (current-time)))
             (duration (- end-time start-time))
             (text (format "Mosaic: Finished importing sounds in %s."
                           (format-seconds "%h hours, %m minutes, %s seconds" duration))))
        (save-window-excursion
          (ignore-errors (mosaic-notify-sound))
          (ignore-errors (mosaic-notify-popup text))
          (message text)
          (mosaic-clear-timestamps))))))

(cl-defun mosaic-session-mp3s (&optional (dir mosaic-selected-session-directory))
  "Return a list of all MP3 files in a session."
  (let ((d (file-name-as-directory dir)))
    (let ((files (directory-files d :full "\\.mp3$")))
      (prog1 files
	(if files
	    (message "Searched %s, found %d sources in MP3 format." d (length files))
	  (message "Searched %s, found no sources!" d))))))

(cl-defun mosaic-import-session-mp3s ()
  "Import all the mp3's in the current session."
  (interactive)
  (mosaic-sending-scheme
    (mosaic-tell-session-directory)
    (cl-dolist (mp3 (mosaic-session-mp3s))
      (let ((wav (concat mp3 ".wav")))
        (shell-command (format "mpg123 -w \"%s\" \"%s\"" wav mp3))
        (mosaic-import-sound (file-name-nondirectory wav))))))

(cl-defun mosaic-new-database ()
  "Clear the current database. This does not alter files."
  (interactive)
  (mosaic-tell-scheme `(mosaic-new-database)))

(cl-defun mosaic-load-data-file (file)
  "Load the slice metadata file FILE into the database."
  (interactive "fLoad Mosaic data file:")
  (mosaic-tell-scheme `(mosaic-load-data-file ,file)))

(cl-defun mosaic-load-database (&optional (folder mosaic-selected-session-directory))
  "Load the session database from FOLDER."
  (interactive)
  (let* ((dir (file-name-as-directory folder)))
    (mosaic-tell-scheme-file `(mosaic-load-database ,dir))))

(defun mosaic-import-marked-files ()
  "Import the Dired-marked files into the current session."
  (interactive)
  (mosaic-sending-scheme
   (mosaic-tell-session-directory)
   (let ((files (dired-get-marked-files)))
     (cl-dolist (file files)
       (message "Importing %s ..." file)
       (mosaic-import-sound (file-name-nondirectory file))))))

(provide 'mosaic-data)
;;; data.el ends here
